import { configure, addDecorator } from '@storybook/react';
import { withKnobs } from '@storybook/addon-knobs'
import { withThemesProvider } from 'storybook-addon-styled-component-theme';
// @ts-ignore
import { theme } from '@src/utils/styles';
// @ts-ignore
import { config } from '@src/config';

// @ts-ignore
window.config = config;

addDecorator(withKnobs);

const uiKit = require.context('../src/uikit', true, /\.stories\.tsx?$/);
configure(uiKit, module);

addDecorator(withThemesProvider([theme]));
