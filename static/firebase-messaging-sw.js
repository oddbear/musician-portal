// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here. Other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/8.0.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.0.1/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing in
// your app's Firebase config object.
// https://firebase.google.com/docs/web/setup#config-object
firebase.initializeApp({
	apiKey: getQueryVariable('apiKey'),
	projectId: getQueryVariable('projectId'),
	messagingSenderId: getQueryVariable('messagingSenderId'),
	appId: getQueryVariable('appId'),
});

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler((payload) => {
	console.log('[firebase-messaging-sw.js] Received background message ', payload);

	void self.registration.showNotification(payload.notification.title, {
		body: payload.notification.body,
		icon: payload.notification.image,
		vibrate: [500, 100, 100, 100, 100],
	});
});

function getQueryVariable(name) {
	const query = location.search.substring(1);
	const vars = query.split('&');
	for (let i = 0; i < vars.length; i++) {
		const pair = vars[i].split('=');
		if (decodeURIComponent(pair[0]) === name) {
			return decodeURIComponent(pair[1]);
		}
	}
	throw new Error(`Query variable '${name}' not found`);
}
