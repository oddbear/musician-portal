const fetch = require('node-fetch');
const fs = require('fs');
const dotenv = require('dotenv');
const path = require('path');

dotenv.config();

;(function request() {
	fetch(process.env.GRAPHQL_URL, {
		method: 'POST',
		headers: { 'Content-Type': 'application/json' },
		body: JSON.stringify({
			variables: {},
			query: `
      {
        __schema {
          types {
            kind
            name
            possibleTypes {
              name
            }
          }
        }
      }
    `,
		}),
	})
		.then(result => result.json())
		.then(result => {
			const filteredData = result.data.__schema.types.filter(
				type => type.possibleTypes !== null,
			);
			result.data.__schema.types = filteredData;
			fs.writeFile(path.resolve(__dirname, '../src/fragmentTypes.json'), JSON.stringify(result.data), err => {
				if (err) {
					console.error('Error writing fragmentTypes file', err);
				} else {
					console.log('Fragment types successfully extracted!');
				}
			});
		});
})();
