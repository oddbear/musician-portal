import * as enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { config } from '../src/config';

enzyme.configure({ adapter: new Adapter() });

jest.mock('enzyme', () => {
	const original = jest.requireActual('enzyme');

	// eslint-disable-next-line @typescript-eslint/no-unsafe-return
	return {
		...original,
		...jest.requireActual('../src/__mocks__/enzyme.mock'),
	};
});

// mock all cloudinary setups cause it calls to external sources
jest.mock('cloudinary-core', () => {
	// eslint-disable-next-line @typescript-eslint/no-unsafe-return
	return jest.requireActual('../src/__mocks__/cloudinary.mock');
});

// set all envs
// eslint-disable-next-line @typescript-eslint/no-explicit-any,@typescript-eslint/no-unsafe-member-access
(window as any).config = config;
