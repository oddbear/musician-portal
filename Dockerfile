FROM node:11

ENV NODE_ENV production

WORKDIR /app

EXPOSE 80

CMD bash -c "npm i && npm run prod"
