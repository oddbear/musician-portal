FROM node:11

ENV NODE_ENV development

WORKDIR /app

EXPOSE $NODE_PORT
EXPOSE 9229

ENV NODE_PORT ${NODE_PORT}

CMD bash -c "npm i && npm run storybook:watch -- -p ${NODE_PORT}"
