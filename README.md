### About project
This is musician portal that lets people find each other by any filters they want. Bands search for musician or producer or... any they want, okay? :)

`Profile` contains any needed info for finder like: name, top records, area, genres and etc.

`Selection` page makes you possible to find any profession or band you want by filters. Here you can send invites or accept them.

`Authorization` lets to all users have a key-pass protected profiles as always.

### How to start project
First of all copy `.env.example` as `.env`.

For development version use `docker-compose up -d` to use containers or execute npm commands `npm i && npm run dev`.

For production version use `docker build . -t musician-portal` and command to run image on local port `docker run -p 4210:80 musician-portal`.
You can use docker-compose the same way as development version, but specify docker-compose file `docker-compose -f docker-compose.prod.yml up -d`.

### Testing
To start tests use `npm run test:start`.

### Storybook
To start storybook in watch mode use `npm run storybook:watch`, just for build use `npm run storybook:build`.

### Commit
Pre-commit validation service implemented in `package.json` with `husky`. Right now all code are checked with eslint, prettier and unit-tests (jest) before it commited, so if any error was found then commit interrupts and you need to fix logged errors.

### Stack
- react
- typescript
- apollo
- next.js
- styled-components
- eslint + prettier
- jest
- storybook
