const storage: { [key: string]: string } = {};

export interface Context {
	get(name: string): string;
	set(name: string, value: string): void;
}

const Cookies: Context = {
	get(name: string): string {
		return storage[name];
	},
	set(name: string, value: string): void {
		storage[name] = value;
	},
};
export default Cookies;
