import React from 'react';
import { ThemeProvider } from 'styled-components';
import { theme } from 'Utils/styles';
import Enzyme from 'enzyme';

const original = jest.requireActual('enzyme') as typeof Enzyme;

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const mount = (element: React.ReactElement, ...args: any[]): React.ReactElement => {
	// @ts-ignore
	return original.mount.apply(original, [
		<ThemeProvider key="any" theme={theme}>
			{element}
		</ThemeProvider>,
		// @ts-ignore
		args,
	]);
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const render = (element: React.ReactElement, ...args: any[]): React.ReactNode => {
	return original.render.apply(original, [
		<ThemeProvider key="any" theme={theme}>
			{element}
		</ThemeProvider>,
		args,
	]);
};
