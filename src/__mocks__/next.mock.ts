type Callback = (location?: { url: string }) => boolean;

const routers: Callback[] = [];
export const beforePopState = (callback: Callback): void => {
	routers.push(callback);
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const fireBeforePopState = (...args: any[]): void => {
	// eslint-disable-next-line @typescript-eslint/no-unsafe-call
	routers.forEach((callback) => callback(...args));
};

export const push = (): void => {
	return;
};

export const replace = (): void => {
	return;
};

export interface Context {
	beforePopState(): void;
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	setPush(value: any): void;
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	setReplace(value: any): void;
}
