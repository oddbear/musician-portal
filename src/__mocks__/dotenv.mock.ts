let envs: { [key: string]: string | number } = {};

export const set = (key: string, value: string | number): void => {
	envs[key] = value;
};
export const clear = (): void => {
	envs = {};
};
export const config = (): { parsed: { [key: string]: string | number } } => {
	return {
		parsed: envs,
	};
};

export interface Context {
	config(): { parsed: { [key: string]: string | number } };
	set(key: string, value: string | number): void;
	clear(): void;
}
