// eslint-disable-next-line @typescript-eslint/no-explicit-any
type Callback = () => any;

const effects: Callback[] = [];

export function useEffect(callback: Callback): void {
	effects.push(callback);
}

export function triggerEffects(): void {
	// eslint-disable-next-line @typescript-eslint/no-unsafe-return
	effects.forEach((callback) => callback());
}

const layoutEffects: Callback[] = [];

export function useLayoutEffect(callback: Callback): void {
	layoutEffects.push(callback);
}

export function triggerLayoutEffects(): void {
	// eslint-disable-next-line @typescript-eslint/no-unsafe-return
	layoutEffects.forEach((callback) => callback());
}

let ref: object | null = null;

export function useRef(): object | null {
	return ref;
}

export function setRef(newRef: object | null): void {
	ref = newRef;
}

export interface Context {
	triggerEffects(): void;
	triggerLayoutEffects(): void;
	setRef(ref: object | null): void;
}
