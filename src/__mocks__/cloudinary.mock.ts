interface Obj {
	loaded: boolean;
	error: boolean;
	src: string;
	plannedWidth: number;
	width?: number;
	height?: number;
	naturalWidth?: number;
	naturalHeight?: number;
	listeners: {
		[key: string]: Array<() => void>;
	};
	addEventListener(name: string, callback: () => void): void;
	removeEventListener(name: string, callback: () => void): void;
}

export class Cloudinary {
	public static instances: Cloudinary[] = [];

	public objects: Array<{
		loaded: boolean;
		error: boolean;
		src: string;
		plannedWidth: number;
		width?: number;
		height?: number;
		naturalWidth?: number;
		naturalHeight?: number;
		listeners: {
			[key: string]: Array<() => void>;
		};
		addEventListener(name: string, callback: () => void): void;
		removeEventListener(name: string, callback: () => void): void;
	}>;

	constructor() {
		this.objects = [];

		Cloudinary.instances.push(this);
	}

	public static load(): void {
		Cloudinary.instances.forEach((cl) => {
			cl.objects.forEach((object) => {
				if (object.loaded) {
					return;
				}
				object.loaded = true;

				if (Object.prototype.hasOwnProperty.call(object, 'naturalWidth')) {
					object.naturalWidth = object.plannedWidth;
					object.naturalHeight = object.naturalWidth * 0.75;
				} else if (Object.prototype.hasOwnProperty.call(object, 'width')) {
					object.width = object.plannedWidth;
					object.width = object.width * 0.75;
				}

				object.listeners.load.forEach((callback) => callback());
				object.listeners.loadeddata.forEach((callback) => callback());
			});
		});
	}

	public static error(): void {
		Cloudinary.instances.forEach((cl) => {
			cl.objects.forEach((object) => {
				if (object.error) {
					return;
				}
				object.error = true;

				object.listeners.error.forEach((callback) => callback());
			});
		});
	}

	public image(src: string, { width }: { width: number }): Obj {
		const object: Obj = {
			loaded: false,
			error: false,
			src,
			plannedWidth: width,
			naturalWidth: undefined,
			naturalHeight: undefined,
			listeners: {
				load: [],
				loadeddata: [],
			},
			addEventListener(name: string, callback: () => void) {
				if (this.listeners[name] === undefined) {
					this.listeners[name] = [];
				}

				this.listeners[name].push(callback);
			},
			removeEventListener(name: string, callback: () => void) {
				if (this.listeners[name] === undefined) {
					this.listeners[name] = [];
				}

				if (this.listeners[name].includes(callback)) {
					this.listeners[name].splice(this.listeners[name].indexOf(callback));
				}
			},
		};

		object.addEventListener = object.addEventListener.bind(object);
		object.removeEventListener = object.removeEventListener.bind(object);

		this.objects.push(object);

		return object;
	}
}

export interface Context {
	load(): void;
	error(): void;
}
