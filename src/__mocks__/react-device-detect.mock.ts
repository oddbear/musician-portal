export let isTablet = false;
export let isMobileOnly = false;

export function setIsTablet(value: boolean): void {
	isTablet = value;
}
export function setIsMobileOnly(value: boolean): void {
	isMobileOnly = value;
}

export interface Context {
	setIsTablet(value: boolean): void;
	setIsMobileOnly(value: boolean): void;
}
