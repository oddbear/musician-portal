import { config } from './config';
import webpack from 'webpack';
import { WebpackOptions } from 'webpack/declarations/WebpackOptions';

export default {
	webpack(webpackOptions: WebpackOptions) {
		const definePlugin = new webpack.DefinePlugin({
			config: JSON.stringify(config),
		});

		if (webpackOptions.plugins?.push(definePlugin) === undefined) {
			webpackOptions.plugins = [definePlugin];
		}

		const graphQLRule = {
			test: /\.graphql$/,
			exclude: /node_modules/,
			loader: 'graphql-tag/loader',
		};

		if (webpackOptions.module === undefined) {
			webpackOptions.module = {
				rules: [graphQLRule],
			};
		} else {
			if (webpackOptions.module.rules === undefined) {
				webpackOptions.module.rules = [graphQLRule];
			} else {
				webpackOptions.module.rules.push(graphQLRule);
			}
		}

		return webpackOptions;
	},
};
