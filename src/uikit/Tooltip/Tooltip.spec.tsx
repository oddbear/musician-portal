import * as React from 'react';
import { tooltipContext } from 'Contexts/tooltip.context';
import { useState } from 'react';
import { mount } from 'enzyme';
import Tooltip, { FloatingWrap } from './Tooltip';

interface Props {
	visible?: boolean;
}

describe('UI Tooltip tests', () => {
	// eslint-disable-next-line react/prop-types
	const Container: React.FC<Props> = ({ visible }) => {
		const [element] = useState<HTMLElement | null>(null);
		const [isVisible] = useState<boolean>(visible === true);

		return (
			<tooltipContext.Provider
				value={{
					attachedElement: element,
					visible: isVisible,
				}}
			>
				<Tooltip>Foo Bar</Tooltip>
			</tooltipContext.Provider>
		);
	};

	it('become visible if property "visible" set as true in context and invisible else', () => {
		let containerWrapper = mount(<Container visible={true} />);
		let containerProps = containerWrapper.find(FloatingWrap).props() as {
			active: boolean;
			visible: boolean;
		};

		expect(containerProps.active).toEqual(true);
		expect(containerProps.visible).toEqual(true);

		containerWrapper = mount(<Container visible={false} />);
		containerProps = containerWrapper.find(FloatingWrap).props() as {
			active: boolean;
			visible: boolean;
		};

		expect(containerProps.active).toEqual(false);
		expect(containerProps.visible).toEqual(false);
	});
});
