import { tooltipContext } from 'Contexts/tooltip.context';
import { Position } from 'Types/position.type';
import { useDelay } from 'Hooks/delay.hook';
import { usePrevious } from 'Hooks/previous.hook';
import {
	AvailableSides,
	getAvailableSides,
	getRelativePosition,
	RelativeData,
	RelativePositionResponse,
} from 'Utils/position';
import React, { MouseEvent, useContext, useEffect, useRef, useState } from 'react';
import styled, { css } from 'styled-components';

const POINTER_SIZE = 10;
const SIDE_PRIORITY = ['top', 'bottom', 'right', 'left'];
// needs to find diagonal of square 'cause our square rotated by 45 degrees that makes the rhombus
const SqFormulaComponent = Math.sqrt(2);

interface Props {
	position?: AvailableSides;
	transitionDuration?: number; // ms
	className?: string;
	pointer?: boolean;
	children: React.ReactNode;
	onMouseEnter?(): void;
	onMouseLeave?(): void;
	onMouseDown?(event: MouseEvent<HTMLDivElement>): void;
	onClick?(event: MouseEvent<HTMLDivElement>): void;
}

interface FloatingWrapProps {
	active: boolean;
	transitionDuration: number;
	visible: boolean;
	position: RelativeData;
}

export const FloatingWrap = styled.div.attrs<FloatingWrapProps>(({ position }) => ({
	style: Object.entries(position).reduce<{ [key: string]: string }>((style, [side, value]: [string, number]) => {
		style[side] = value.toString() + 'px';
		return style;
	}, {}),
}))<FloatingWrapProps>`
	position: fixed;
	z-index: 21;
	${({ visible, transitionDuration }) =>
		visible
			? css({
					opacity: 1,
					transition: 'opacity 0.2s',
			  })
			: css({
					opacity: 0,
					transition: 'opacity ' + transitionDuration.toString() + 'ms cubic-bezier(0.95, 0.05, 0.795, 0.035)',
			  })};
	pointer-events: ${({ active }) => (active ? 'initial' : 'none')};
`;

interface ContainerProps {
	side: AvailableSides;
}

const Container = styled.div.attrs<ContainerProps>(({ side }) => {
	let style = {};

	switch (side) {
		case 'left':
		case 'right':
			style = {
				margin: '0 ' + (POINTER_SIZE / 2).toString() + 'px',
			};
			break;

		case 'top':
		case 'bottom':
			style = {
				margin: (POINTER_SIZE / 2).toString() + 'px 0',
			};
			break;
	}

	return {
		style,
	};
})<ContainerProps>`
	position: relative;
	z-index: 3;
	background-color: white;
	color: black;
	padding: 15px 20px;
	border-radius: 4px;
	box-shadow: 0 0 5px -2px black;
`;

interface PointerWrapProps {
	height: number;
	width: number;
	side: AvailableSides;
}

const PointerWrap = styled.div.attrs<PointerWrapProps>(({ side, height, width }) => {
	let style = {};

	switch (side) {
		case 'left':
		case 'right':
			style = {
				width: '100%',
				height: (height - SqFormulaComponent * POINTER_SIZE).toString() + 'px',
				margin: ((SqFormulaComponent * POINTER_SIZE) / 2).toString() + 'px 0',
			};
			break;
		case 'top':
		case 'bottom':
			style = {
				width: (width - SqFormulaComponent * POINTER_SIZE).toString() + 'px',
				height: '100%',
				margin: '0 ' + ((SqFormulaComponent * POINTER_SIZE) / 2).toString() + 'px',
			};
			break;
	}

	return {
		style,
	};
})<PointerWrapProps>`
	position: absolute;
	box-sizing: border-box;
	z-index: 2;
`;

interface PointerProps {
	side: AvailableSides;
	offset: RelativeData;
}

export const Pointer = styled.div.attrs<PointerProps>(({ side, offset }) => {
	let style;

	switch (side) {
		case 'left':
			style = {
				marginTop: (-POINTER_SIZE / 2).toString() + 'px',
				right: 0,
				top: ((0.5 + offset.top!) * 100).toString() + '%',
			};
			break;
		case 'right':
			style = {
				marginTop: (-POINTER_SIZE / 2).toString() + 'px',
				left: 0,
				top: ((0.5 + offset.top!) * 100).toString() + '%',
			};
			break;
		case 'top':
			style = {
				marginLeft: (-POINTER_SIZE / 2).toString() + 'px',
				left: ((0.5 + offset.left!) * 100).toString() + '%',
				bottom: 0,
			};
			break;
		case 'bottom':
			style = {
				marginLeft: (-POINTER_SIZE / 2).toString() + 'px',
				left: ((0.5 + offset.left!) * 100).toString() + '%',
				top: 0,
			};
			break;
	}

	return {
		style,
	};
})<PointerProps>`
	width: ${POINTER_SIZE}px;
	height: ${POINTER_SIZE}px;
	position: absolute;
	background-color: white;
	transform: rotate(45deg);
`;

const Tooltip: React.FC<Props> = (props) => {
	const { attachedElement, visible } = useContext(tooltipContext);
	const containerRef = useRef<HTMLDivElement>(null);
	const [sidePriority, setSidePriority] = useState<string[]>(SIDE_PRIORITY);
	const [position, setPosition] = useState<Position>({
		left: 0,
		top: 0,
		right: 0,
		bottom: 0,
	});
	const [isActive, setActiveWithDelay, resetActive] = useDelay<boolean>(false, props.transitionDuration!);
	const [currentSide, setCurrentSide] = useState<AvailableSides>('left');
	const [preparedPosition, setPreparedPosition] = useState<RelativePositionResponse>({
		position: {
			left: 0,
		},
		offset: {
			left: 0,
		},
	});

	useEffect(() => {
		const callback = (): void => {
			if (attachedElement !== null && visible) {
				setPosition(attachedElement.getBoundingClientRect());
			}
		};
		const $scrollContainer = document.getElementById('container');
		if ($scrollContainer !== null) {
			$scrollContainer.addEventListener('scroll', callback);
			$scrollContainer.addEventListener('resize', callback);
		}

		if (attachedElement !== null && visible) {
			setPosition(attachedElement.getBoundingClientRect());
		}

		return () => {
			if ($scrollContainer !== null) {
				$scrollContainer.removeEventListener('scroll', callback);
				$scrollContainer.removeEventListener('resize', callback);
			}
		};
	}, [attachedElement, visible]);

	const prevVisible = usePrevious(visible);
	useEffect(() => {
		if (prevVisible !== visible) {
			if (visible) {
				resetActive(true);
			} else {
				setActiveWithDelay(false);
			}
		}
	}, [prevVisible, setActiveWithDelay, visible, resetActive]);

	const containerOffset = {
		width: containerRef.current?.offsetWidth ?? 0,
		height: containerRef.current?.offsetHeight ?? 0,
	};

	useEffect(() => {
		const sideIndex = props.position === undefined ? -1 : SIDE_PRIORITY.indexOf(props.position);
		if (sideIndex === -1) {
			setSidePriority(SIDE_PRIORITY);
		} else {
			setSidePriority(SIDE_PRIORITY.slice(sideIndex).concat(SIDE_PRIORITY.slice(0, sideIndex)));
		}
	}, [props.position]);

	const prevPosition = usePrevious(position);
	useEffect(() => {
		if (visible && prevPosition !== position) {
			const availableSides = getAvailableSides(position, containerOffset);
			const side = (sidePriority.find((sideName) => sideName in availableSides) ?? sidePriority[0]) as AvailableSides;

			setCurrentSide(side);
			setPreparedPosition(getRelativePosition(position, containerOffset, side));
		}
	}, [containerOffset, prevPosition, position, sidePriority, visible]);

	return (
		<FloatingWrap
			active={isActive}
			visible={visible}
			transitionDuration={props.transitionDuration!}
			position={preparedPosition.position}
			onMouseEnter={props.onMouseEnter}
			onMouseLeave={props.onMouseLeave}
			onMouseDown={props.onMouseDown}
			onClick={props.onClick}
		>
			{props.pointer! && (
				<PointerWrap
					height={containerRef.current?.offsetHeight ?? 0}
					width={containerRef.current?.offsetWidth ?? 0}
					side={currentSide}
				>
					<Pointer offset={preparedPosition.offset} side={currentSide} />
				</PointerWrap>
			)}
			<Container ref={containerRef} className={props.className} side={currentSide}>
				{props.children}
			</Container>
		</FloatingWrap>
	);
};

Tooltip.defaultProps = {
	transitionDuration: 0.5,
	pointer: true,
};

export default Tooltip;
