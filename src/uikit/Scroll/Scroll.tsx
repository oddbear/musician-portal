import React from 'react';
import styled from 'styled-components';

interface Props {
	className?: string;
	getRef?: React.MutableRefObject<HTMLDivElement | null>;
	children: React.ReactNode;
}

const Container = styled.div`
	overflow: auto;

	::-webkit-scrollbar {
		width: 4px;
	}

	::-webkit-scrollbar-track {
		border-width: 1px 1px 1px 2px;
		border-color: #777;
		background-color: rgb(220, 219, 219);
		border-radius: 5px;
	}

	::-webkit-scrollbar-track:hover {
		border-left: solid 1px #aaa;
		background-color: #eee;
	}

	::-webkit-scrollbar-thumb {
		background: rgb(175, 175, 175);
		border-radius: 5px;
	}
`;

const Scroll: React.FC<Props> = ({ className, getRef, children }) => {
	return (
		<Container className={className} ref={getRef}>
			{children}
		</Container>
	);
};

export default Scroll;
