import React from 'react';
import styled from 'styled-components';
import { useLazyLoad } from 'Hooks/lazyload.hook';

const SEAL_SRC = 'musician-portal/Прямоугольник_6';

interface Props {
	title: string;
	className?: string;
}

interface ContainerProps {
	src: string;
}

const Container = styled.div<ContainerProps>`
	background: url(${({ src }) => src}) no-repeat center;
	background-size: 100% 100%;
`;

const Title = styled.div`
	text-align: center;
	padding: 5px 15px 0 15px;
	color: ${({ theme }) => theme.color.third};
`;

const Seal: React.FC<Props> = (props) => {
	const response = useLazyLoad({
		src: SEAL_SRC,
	});

	return (
		<Container className={props.className} src={response.src}>
			<Title>{props.title.toUpperCase()}</Title>
		</Container>
	);
};

export default Seal;
