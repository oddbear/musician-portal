import * as React from 'react';
import Seal from './Seal';
import { storiesOf } from '@storybook/react';
import styled from 'styled-components';

const Container = styled.div`
	margin: 50px;
	width: 300px;
`;

storiesOf('PageTitle', module).add('default', () => (
	<Container>
		<Seal title="Lorem Ipsum" />
	</Container>
));
