export interface ValidateOptions {
	required?: {
		errorMessage: string;
		value: boolean;
	};
	match?: {
		errorMessage: string;
		value: RegExp;
	};
	minLength?: {
		errorMessage: string;
		value: number;
	};
	maxLength?: {
		errorMessage: string;
		value: number;
	};
	callback?: {
		errorMessage: string;
		value(fieldValue: string): boolean;
	};
}

export const validate = (options: ValidateOptions, fieldValue: string): { errorMessage: string | null } => {
	if (options.required?.value === true && fieldValue === '') {
		return {
			errorMessage: options.required.errorMessage,
		};
	} else if (options.match !== undefined && options.match.value.exec(fieldValue) === null) {
		return {
			errorMessage: options.match.errorMessage,
		};
	} else if (options.minLength !== undefined && fieldValue.length < options.minLength.value) {
		return {
			errorMessage: options.minLength.errorMessage,
		};
	} else if (options.maxLength !== undefined && fieldValue.length > options.maxLength.value) {
		return {
			errorMessage: options.maxLength.errorMessage,
		};
	} else if (options.callback !== undefined && !options.callback.value(fieldValue)) {
		return {
			errorMessage: options.callback.errorMessage,
		};
	}

	return {
		errorMessage: null,
	};
};
