import { validate } from '../validation';

describe('UI Input validation tests', () => {
	const errorMessage = 'validation failed';

	it('passes required validation', () => {
		expect(
			validate(
				{
					required: {
						errorMessage,
						value: true,
					},
				},
				'foobar',
			).errorMessage,
		).toEqual(null);

		expect(
			validate(
				{
					required: {
						errorMessage,
						value: false,
					},
				},
				'',
			).errorMessage,
		).toEqual(null);
	});

	it('passes match validation with correct value', () => {
		expect(
			validate(
				{
					match: {
						errorMessage,
						value: /^foobar\d{2}$/,
					},
				},
				'foobar31',
			).errorMessage,
		).toEqual(null);
	});

	it('passes minLength validation with correct value', () => {
		expect(
			validate(
				{
					minLength: {
						errorMessage,
						value: 6,
					},
				},
				'foobar',
			).errorMessage,
		).toEqual(null);

		expect(
			validate(
				{
					minLength: {
						errorMessage,
						value: 3,
					},
				},
				'foobar',
			).errorMessage,
		).toEqual(null);
	});

	it('passes maxLength validation with correct value', () => {
		expect(
			validate(
				{
					maxLength: {
						errorMessage,
						value: 6,
					},
				},
				'foobar',
			).errorMessage,
		).toEqual(null);

		expect(
			validate(
				{
					maxLength: {
						errorMessage,
						value: 10,
					},
				},
				'foobar',
			).errorMessage,
		).toEqual(null);
	});

	it('passes callback validation with correct value', () => {
		expect(
			validate(
				{
					callback: {
						errorMessage,
						value: (value) => value === 'foobar',
					},
				},
				'foobar',
			).errorMessage,
		).toEqual(null);
	});

	it('fails required validation', () => {
		expect(
			validate(
				{
					required: {
						errorMessage,
						value: true,
					},
				},
				'',
			).errorMessage,
		).toEqual(errorMessage);
	});

	it('fails match validation with incorrect value', () => {
		expect(
			validate(
				{
					match: {
						errorMessage,
						value: /^foobar\d{2}$/,
					},
				},
				'foobar',
			).errorMessage,
		).toEqual(errorMessage);
	});

	it('fails minLength validation with incorrect value', () => {
		expect(
			validate(
				{
					minLength: {
						errorMessage,
						value: 7,
					},
				},
				'foobar',
			).errorMessage,
		).toEqual(errorMessage);
	});

	it('fails maxLength validation with incorrect value', () => {
		expect(
			validate(
				{
					maxLength: {
						errorMessage,
						value: 5,
					},
				},
				'foobar',
			).errorMessage,
		).toEqual(errorMessage);
	});

	it('fails callback validation with incorrect value', () => {
		expect(
			validate(
				{
					callback: {
						errorMessage,
						value: () => false,
					},
				},
				'foobar',
			).errorMessage,
		).toEqual(errorMessage);
	});
});
