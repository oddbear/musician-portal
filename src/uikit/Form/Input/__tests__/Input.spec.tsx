import * as React from 'react';
import { fieldValidatorContext } from 'Contexts/form.context';
import Input, { ErrorMessage, EyeIcon, StyledInput } from '../Input';
import { mount, render } from 'enzyme';
import { useEffect, useReducer, useState } from 'react';

jest.mock('Hooks/lazyload.hook', () => ({
	useLazyLoad: () => 'foo/bar/image',
}));

jest.mock('js-cookie', () => {
	// eslint-disable-next-line @typescript-eslint/no-unsafe-return
	return jest.requireActual('../../../../__mocks__/js-cookie.mock');
});

interface Props {
	value?: string;
	type?: 'text' | 'password';
	placeholder?: string;
	iconSrc?: string;
	backgroundType?: number;
	validation?: object;
	textMemory?: boolean;
}

const Container: React.FC<Props> = (props) => {
	// eslint-disable-next-line react/prop-types
	const [value, setValue] = useState<string>(props.value ?? '');
	return <Input onChange={setValue} value={value} name="login" {...props} />;
};

describe('UI Form Input tests', () => {
	it('renders correctly', () => {
		expect(render(<Container />)).toEqual(render(<Container type="text" />));
	});

	it('password shows and hides successfully', () => {
		const inputWrapper = mount(<Container type="password" />);
		expect(inputWrapper.find(StyledInput).render()[0].attribs.type).toEqual('password');

		const eyeIconWrapper = inputWrapper.find(EyeIcon);
		eyeIconWrapper.simulate('click');
		expect(inputWrapper.find(StyledInput).render()[0].attribs.type).toEqual('text');

		eyeIconWrapper.simulate('click');
		expect(inputWrapper.find(StyledInput).render()[0].attribs.type).toEqual('password');
	});

	it('error message appears after blur and disappears on resolve successfully', () => {
		const errorMessage = 'validation failed!';
		const inputWrapper = mount(
			<Container
				validation={{
					required: {
						value: true,
						errorMessage,
					},
				}}
			/>,
		);
		expect(inputWrapper.find(ErrorMessage).length).toEqual(0);

		inputWrapper.find(StyledInput).simulate('focus');
		inputWrapper.find(StyledInput).simulate('blur');
		expect(inputWrapper.find(ErrorMessage).text()).toEqual(errorMessage);

		inputWrapper.find(StyledInput).simulate('change', { target: { value: 'it should be okay!' } });
		expect(inputWrapper.find(ErrorMessage).length).toEqual(0);
	});

	it("memorizes input's text with flag textMemory", () => {
		let value = '';
		let onChange: (v: string) => void = () => {
			return;
		};

		const inputValue = 'foo bar';
		const name = 'memorize_test';
		mount(<Input textMemory={true} name={name} value={inputValue} onChange={onChange} />)
			.find('input')
			.simulate('change');

		onChange = (newValue: string): void => {
			value = newValue;
		};

		mount(<Input textMemory={true} name={name} value="" onChange={onChange} />);

		expect(value).toEqual(inputValue);
	});

	it('adds fieldValidator then value changed and removes old one', () => {
		type Callback = () => boolean;
		type Methods = 'add' | 'remove';
		type Reducer = (state: Callback[], action: { type: Methods; payload: Callback }) => Callback[];

		const reducer: Reducer = (state, action) => {
			switch (action.type) {
				case 'add':
					return state.concat(action.payload);

				case 'remove':
					if (state.includes(action.payload)) {
						state.splice(state.indexOf(action.payload), 1);
						return state;
					} else {
						return state;
					}
			}
		};

		let validatorsLength = 0;

		const Component = (): React.ReactElement => {
			const [value, setValue] = useState('');
			const [validators, setValidator] = useReducer<Reducer>(reducer, []);

			useEffect(() => {
				validatorsLength = validators.length;
			}, [validators]);

			return (
				<fieldValidatorContext.Provider
					value={{
						addCallback: (callback: Callback) =>
							setValidator({
								type: 'add',
								payload: callback,
							}),
						removeCallback: (callback: Callback) =>
							setValidator({
								type: 'remove',
								payload: callback,
							}),
					}}
				>
					<Input onChange={setValue} value={value} name="login" />
				</fieldValidatorContext.Provider>
			);
		};

		const componentWrapper = mount(<Component />);
		componentWrapper.find('input').simulate('change', { target: { value: 'foo bar' } });

		expect(validatorsLength).toEqual(1);
	});
});
