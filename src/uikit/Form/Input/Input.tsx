import LazyImage from 'Components/LazyImage';
import { Context as FieldValidatorContext, fieldValidatorContext } from 'Contexts/form.context';
import { useLazyLoad } from 'Hooks/lazyload.hook';
import { usePrevious } from 'Hooks/previous.hook';
import Cookies from 'js-cookie';
import React, {
	ChangeEvent,
	ClipboardEvent,
	KeyboardEvent,
	MouseEvent,
	useCallback,
	useContext,
	useEffect,
	useState,
} from 'react';
import styled from 'styled-components';
import { ValidateOptions, validate } from './validation';
import classNames from 'classnames';

export enum InputStatus {
	loading,
	idle,
	error,
}

const EYE_ICON = 'General%20elements/show_iz7l0a';
const CLOSED_EYE_ICON = 'General%20elements/not_show_iilpxr';
export const BACKGROUNDS_SRC = ['Sign_up__Sign_in/Login_input_digxbw', 'Sign_up__Sign_in/Login_input_digxbw'];

export interface Props {
	value: string;
	name: string;
	type?: 'text' | 'password' | 'textarea';
	canSeePassword?: boolean;
	disabled?: boolean;
	placeholder?: string;
	iconSrc?: string;
	validation?: ValidateOptions;
	backgroundType?: number;
	textMemory?: boolean;
	className?: string;
	status?: InputStatus;
	error?: string;
	instantValidation?: boolean;
	getRef?: React.MutableRefObject<HTMLInputElement | HTMLTextAreaElement | null>;
	onChange(value: string): void;
	onBlur?(): void;
	onFocus?(): void;
	onKeyDown?(e: KeyboardEvent<HTMLInputElement | HTMLTextAreaElement>): void;
	onMouseDown?(e: MouseEvent<HTMLInputElement | HTMLTextAreaElement>): void;
	onClick?(e: MouseEvent<HTMLInputElement | HTMLTextAreaElement>): void;
	onPaste?(e: ClipboardEvent<HTMLTextAreaElement | HTMLInputElement>): void;
}

const Container = styled.div`
	display: flex;
	width: 100%;
	flex-direction: column;

	.password input {
		text-transform: initial !important;
	}
`;

interface WrapProps {
	status?: InputStatus;
}

const Wrap = styled.div<WrapProps>`
	position: relative;
	border-radius: 4px;
	width: 100%;
	box-shadow: 0 0 4px -2px black;
	overflow: hidden;

	&:after {
		content: '';
		position: absolute;
		left: 0;
		top: 0;
		width: 100%;
		height: 100%;
		pointer-events: none;
		opacity: 0.1;
		transition: background-color 0.2s;
		background-color: ${({ status }) =>
			status === InputStatus.error
				? 'red'
				: status === InputStatus.idle
				? 'transparent'
				: status === InputStatus.loading
				? 'yellow'
				: 'transparent'};
		z-index: 1;
	}
`;

interface StyledProps {
	src: string;
}

export const StyledInput = styled.input<StyledProps>`
	position: relative;
	width: 100%;
	padding: 16px 92px;
	color: black;
	border: none;
	outline: none;
	box-sizing: border-box;
	background: url(${({ src }) => src}) no-repeat center;
	background-size: 100% 100%;
	margin: 0;
	font-family: ${({ theme }) => theme.fontFamily.quiet};
	font-size: 1.04rem;
	font-weight: 400;
	text-transform: none;
`;

export const StyledTextarea = styled.textarea<StyledProps>`
	width: 100%;
	height: 100%;
	padding: 15px 60px;
	color: black;
	border: none;
	outline: none;
	box-sizing: border-box;
	background: url(${({ src }) => src}) #cacbcc no-repeat center;
	background-size: 100% 100%;
	margin: 0;
	font-family: inherit;
	font-size: 1.04rem;
	font-weight: inherit;
	text-transform: none;
	resize: none;
`;

export const Icon = styled(LazyImage)`
	position: absolute;
	margin: 12px 0 0 17px;
	z-index: 2;
`;

export const EyeIcon = styled(LazyImage)`
	cursor: pointer;
	height: 30%;
	position: absolute;
	right: 20px;
	top: 35%;
	z-index: 2;
`;

export const ErrorMessage = styled.span`
	color: ${({ theme }) => theme.color.error};
	font-family: ${({ theme }) => theme.fontFamily.quiet};
	font-weight: 300;
	font-size: 0.7rem;
`;

const buildMemoryName = (name: string): string => {
	if (typeof window === 'undefined') {
		return '';
	}

	return `${location.pathname}/${name}`;
};

export const setCookiesMemory = (inputName: string, inputValue: string): void => {
	Cookies.set(buildMemoryName(inputName), encodeURIComponent(inputValue));
};

const Input: React.FC<Props> = (props) => {
	const [type, setType] = useState<string>(props.type!);
	const [error, setError] = useState<string | null>(null);
	const [changed, setChanged] = useState<boolean>(props.instantValidation === true);
	const { src } = useLazyLoad({
		src: BACKGROUNDS_SRC[props.backgroundType ?? 0],
	});
	const { validation, value } = props;
	const fieldValidator = useContext<FieldValidatorContext>(fieldValidatorContext);

	useEffect(() => {
		setError(props.error ?? null);
		if (props.error !== undefined) {
			setChanged(true);
		}
	}, [props.error]);

	const fieldValidatorCallback = useCallback(() => {
		if (validation === undefined) {
			return true;
		}

		const { errorMessage } = validate(validation, value);

		setChanged(true);
		setError(errorMessage);

		return errorMessage === null;
	}, [value, validation]);

	useEffect(() => {
		fieldValidator.addCallback(fieldValidatorCallback);
		return () => fieldValidator.removeCallback(fieldValidatorCallback);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [fieldValidatorCallback]);

	const cookieValue = buildMemoryName(props.name);
	const prevCookieValue = usePrevious(cookieValue);
	useEffect(() => {
		if (props.textMemory === true && cookieValue !== prevCookieValue) {
			if (props.type === 'password') {
				console.warn(`It is unsafe to memorize input '${props.name}' with type password`);
			}

			const memorizedValue = Cookies.get(buildMemoryName(props.name));
			if (memorizedValue !== undefined) {
				props.onChange(decodeURIComponent(memorizedValue));
			}
		}
	}, [cookieValue, prevCookieValue, props]);

	const changeType = useCallback(() => {
		setType(type === 'text' ? 'password' : 'text');
	}, [type]);

	const onChange = useCallback(
		(e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
			const inputValue = e.target.value;

			setError(validate(validation!, inputValue).errorMessage);

			if (props.textMemory === true) {
				setCookiesMemory(props.name, inputValue);
			}

			props.onChange(inputValue);
		},
		[props, validation],
	);

	const onFocus = useCallback((): void => {
		setError(validate(validation!, value).errorMessage);
		props.onFocus?.();
	}, [props.onFocus, validation, value]);

	const onBlur = useCallback((): void => {
		setChanged(true);
		props.onBlur?.();
	}, [props.onBlur]);

	const onKeyDown = useCallback(
		(e: KeyboardEvent<HTMLInputElement | HTMLTextAreaElement>): void => {
			if (e.key === 'Enter') {
				setChanged(true);
			}

			props.onKeyDown?.(e);
		},
		[props.onKeyDown],
	);

	return (
		<Container>
			<Wrap
				className={classNames(props.className, {
					password: props.type === 'password',
				})}
				status={props.status}
			>
				{props.iconSrc !== undefined && <Icon src={props.iconSrc} alt="icon" fitHeight={true} />}
				{props.type === 'textarea' ? (
					<StyledTextarea
						disabled={props.disabled}
						value={value}
						name={props.name}
						placeholder={props.placeholder}
						src={src}
						onChange={onChange}
						onBlur={onBlur}
						onFocus={onFocus}
						onKeyDown={onKeyDown}
						onMouseDown={props.onMouseDown}
						onClick={props.onClick}
						onPaste={props.onPaste}
						// @ts-ignore
						ref={props.getRef}
					/>
				) : (
					<StyledInput
						disabled={props.disabled}
						type={type}
						value={value}
						name={props.name}
						placeholder={props.placeholder}
						src={src}
						onChange={onChange}
						onBlur={onBlur}
						onFocus={onFocus}
						onKeyDown={onKeyDown}
						onMouseDown={props.onMouseDown}
						onClick={props.onClick}
						onPaste={props.onPaste}
						// @ts-ignore
						ref={props.getRef}
					/>
				)}
				{props.type === 'password' && props.canSeePassword !== false && (
					<EyeIcon src={type === 'password' ? CLOSED_EYE_ICON : EYE_ICON} alt="show" onClick={changeType} />
				)}
			</Wrap>
			{changed && error !== null && <ErrorMessage>{error}</ErrorMessage>}
		</Container>
	);
};

Input.defaultProps = {
	type: 'text',
	validation: {},
};

export default styled(Input)``;
