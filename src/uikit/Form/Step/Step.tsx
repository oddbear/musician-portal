import React from 'react';

interface Props {
	children?: React.ReactNode;
}

const Step: React.FC<Props> = (props) => <>{props.children}</>;

export default Step;
