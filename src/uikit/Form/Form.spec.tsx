import * as React from 'react';
import Button from 'UI/Button';
import { ErrorMessage } from './Input/Input';
import Form, { CurChildren, Input, PrevChildren, Step } from './index';
import { mount, ReactWrapper } from 'enzyme';

const onChange = (): void => {
	return;
};

describe('UI Form tests', () => {
	let formWrapper: ReactWrapper;
	let validFormWrapper: ReactWrapper;
	let onError: () => void;
	let onSubmit: (event: React.FormEvent) => void;

	const error = (): void => onError();
	const submit = (event: React.FormEvent): void => onSubmit(event);

	beforeEach(() => {
		formWrapper = mount(
			<Form onSubmit={submit} onError={error}>
				<Input
					onChange={onChange}
					value=""
					name="login"
					validation={{ required: { value: true, errorMessage: 'validation failed!' } }}
				/>
				<Input
					onChange={onChange}
					type="password"
					value="foobar"
					name="password"
					validation={{ minLength: { value: 50, errorMessage: 'validation failed!' } }}
				/>
				<Button />
			</Form>,
		);

		validFormWrapper = mount(
			<Form onSubmit={submit} onError={error}>
				<Input
					onChange={onChange}
					value="foobar"
					name="login"
					validation={{ required: { value: true, errorMessage: 'validation failed!' } }}
				/>
				<Button />
			</Form>,
		);
	});

	it('shows error of each field on submit and calls onError', () => {
		onError = jest.fn();

		expect(formWrapper.find(ErrorMessage)).toHaveLength(0);

		formWrapper.simulate('submit');
		expect(formWrapper.find(ErrorMessage)).toHaveLength(2);
		expect(onError).toBeCalledTimes(1);
	});

	it('calls onSubmit and prevents default then all fields are valid', () => {
		const preventDefault = jest.fn();

		onSubmit = jest.fn();
		validFormWrapper.simulate('submit', { preventDefault });

		expect(onSubmit).toBeCalledTimes(1);
		expect(preventDefault).toBeCalledTimes(1);
	});

	// it('triggers submit on enter press', () => {
	// 	onSubmit = jest.fn();
	// 	onError = jest.fn();
	//
	// 	validFormWrapper.simulate('keydown', { key: 'Enter' });
	// 	expect(onSubmit).toBeCalledTimes(1);
	//
	// 	formWrapper.simulate('keydown', { key: 'Enter' });
	// 	expect(onError).toBeCalledTimes(1);
	// });

	describe('switches steps (creates previous steps in <PrevChildren />) and submits on last step', () => {
		it('for only step', () => {
			onSubmit = jest.fn();

			const wrapper = mount(
				<Form onSubmit={onSubmit}>
					<Step>
						<Input
							onChange={onChange}
							value="foobar"
							name="login"
							validation={{ required: { value: true, errorMessage: 'validation failed!' } }}
						/>
					</Step>
					<Button />
				</Form>,
			);

			expect(wrapper.find(CurChildren).find(Step)).toHaveLength(1);
			expect(wrapper.find(PrevChildren).find(Step)).toHaveLength(0);

			wrapper.simulate('submit');
			expect(onSubmit).toBeCalledTimes(1);
		});

		it('for many steps', () => {
			onSubmit = jest.fn();

			const wrapper = mount(
				<Form onSubmit={onSubmit}>
					<Step>
						<Input
							onChange={onChange}
							value="foobar"
							name="login"
							validation={{ required: { value: true, errorMessage: 'validation failed!' } }}
						/>
					</Step>
					<Step>
						<Input
							onChange={onChange}
							value="foobar"
							name="name"
							validation={{ required: { value: true, errorMessage: 'validation failed!' } }}
						/>
					</Step>
					<Button />
				</Form>,
			);

			expect(wrapper.find(CurChildren).find(Step)).toHaveLength(1);
			expect(wrapper.find(PrevChildren).find(Step)).toHaveLength(0);

			wrapper.simulate('submit');

			expect(wrapper.find(CurChildren).find(Step)).toHaveLength(1);
			expect(wrapper.find(PrevChildren).find(Step)).toHaveLength(1);

			wrapper.simulate('submit');
			expect(onSubmit).toBeCalledTimes(1);
		});
	});
});
