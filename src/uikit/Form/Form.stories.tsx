import * as React from 'react';
import { BACKGROUNDS_SRC } from './Input/Input';
import { storiesOf } from '@storybook/react';
import { Input, Title } from 'UI/Form';

const ICON =
	'musician-portal/%D0%92%D0%B5%D0%BA%D1%82%D0%BE%D1%80%D0%BD%D1%8B%D0%B9_%D1%81%D0%BC%D0%B0%D1%80%D1%82-%D0%BE%D0%B1%D1%8A%D0%B5%D0%BA%D1%82_%D0%BA%D0%BE%D0%BF%D0%B8%D1%8F_2';

interface ContainerProps {
	iconSrc?: string;
	type?: 'text' | 'password';
	backgroundType?: number;
}

// eslint-disable-next-line react/prop-types
const Container: React.FC<ContainerProps> = ({ iconSrc, type, backgroundType }) => {
	const [value, setValue] = React.useState('');

	return (
		<Input
			backgroundType={backgroundType ?? 0}
			name="login"
			onChange={setValue}
			value={value}
			iconSrc={iconSrc}
			type={type}
		/>
	);
};

storiesOf('Input', module)
	.add('text', () => <Container iconSrc={ICON} />)
	.add('all backgrounds', () => (
		<div>
			{BACKGROUNDS_SRC.map((src: string, index: number) => (
				<Container key={src} backgroundType={index} />
			))}
		</div>
	))
	.add('password', () => <Container type="password" />)
	.add('with label', () => (
		<label>
			<Title>FooTitle</Title>
			<Container />
		</label>
	));
