import React from 'react';
import styled from 'styled-components';

const BORDER_COLOR = '#fff';
const BORDER_SIZE = '2px';

interface Props {
	color?: string;
	className?: string;
	children: React.ReactNode;
}

interface ContainerProps {
	color: string;
}

const Container = styled.span<ContainerProps>`
	position: relative;
	font-size: 1.45rem;
	font-weight: 500;
	text-transform: uppercase;

	border-bottom: ${BORDER_SIZE} solid ${({ color }) => color};

	&:before,
	&:after {
		content: '';
		position: absolute;
		bottom: 0;
		height: 8px;
		width: ${BORDER_SIZE};
		background-color: ${({ color }) => color};
	}

	&:before {
		left: 0;
	}

	&:after {
		right: 0;
	}
`;

const Title: React.FC<Props> = (props) => {
	return (
		<Container className={props.className} color={props.color!}>
			{props.children}
		</Container>
	);
};

Title.defaultProps = {
	color: BORDER_COLOR,
};

export default Title;
