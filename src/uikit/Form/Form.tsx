import { fieldValidatorContext } from 'Contexts/form.context';
import Step from './Step';
import styled from 'styled-components';
import { useLazyLoad } from 'Hooks/lazyload.hook';
import Input, { EyeIcon, Icon } from './Input';
import React, { FormEvent, KeyboardEvent, useCallback, useReducer, useState } from 'react';

const INPUT_UNDERLINE_SRC = 'Sign_up__Sign_in/%D0%A1%D0%BB%D0%BE%D0%B9_43_%D0%BA%D0%BE%D0%BF%D0%B8%D1%8F_u2ku7f';

const stepType = (<Step />).type;

interface Props {
	className?: string;
	suppressOnEnter?: boolean;
	children: React.ReactNode;
	onChangeStep?: (step: number) => void;
	onSubmit?: (event: FormEvent) => void;
	onError?: () => void;
}

interface PrevChildrenProps {
	underlineSrc: string;
}

export const PrevChildren = styled.div<PrevChildrenProps>`
	${Input} {
		box-shadow: none;
		background: url(${({ underlineSrc }) => underlineSrc}) no-repeat bottom;
		background-size: 100% 2px;

		input {
			background: none;
			color: ${({ theme }) => theme.color.secondary};
			padding-right: 5px;
			text-align: right;
		}

		${Icon} {
			left: 5px;
		}

		${EyeIcon} {
			display: none;
		}
	}
`;

export const CurChildren = styled.div`
	width: 100%;
`;

type Callback = () => boolean;
type Methods = 'add' | 'remove';
type Reducer = (state: Callback[], action: { type: Methods; payload: Callback }) => Callback[];

const reducer: Reducer = (state, action) => {
	switch (action.type) {
		case 'add':
			return state.concat(action.payload);

		case 'remove':
			if (state.includes(action.payload)) {
				state.splice(state.indexOf(action.payload), 1);
				return state;
			} else {
				return state;
			}
	}
};

const Form: React.FC<Props> = (props) => {
	const [validators, setValidator] = useReducer<Reducer>(reducer, []);
	const [step, setStep] = useState<number>(0);
	const { src: underlineSrc } = useLazyLoad({
		src: INPUT_UNDERLINE_SRC,
	});

	let lastStepIndex = -1;
	const children = React.Children.toArray(props.children);
	const { prevChildren, curChildren } = children.reduce<Record<'prevChildren' | 'curChildren', React.ReactNode[]>>(
		(childCollection, child) => {
			const isStep = typeof child === 'object' && 'type' in child && child.type === stepType;
			if (isStep) {
				lastStepIndex++;
			}

			return {
				prevChildren:
					isStep && lastStepIndex < step ? [...childCollection.prevChildren, child] : childCollection.prevChildren,
				curChildren:
					!isStep || lastStepIndex === step ? [...childCollection.curChildren, child] : childCollection.curChildren,
			};
		},
		{ prevChildren: [], curChildren: [] },
	);

	const submit = useCallback(
		(event: FormEvent): void => {
			// goes through all validators and activates them
			// then compares passed validators length with initial length
			// ".every" and ".some" can't be used because stops on first mistake and activates no every validator
			if (validators.filter((validator) => validator()).length < validators.length) {
				props.onError?.();
			} else {
				if (step >= lastStepIndex) {
					props.onSubmit?.(event);
				} else {
					props.onChangeStep?.(step + 1);
					setStep(step + 1);
				}
			}
		},
		[lastStepIndex, props.onError, props.onSubmit, props.onChangeStep, step, validators],
	);

	const onSubmit = useCallback(
		(e: FormEvent): void => {
			e.preventDefault();
			submit(e);
		},
		[submit],
	);

	const onKeyDown = useCallback(
		(e: KeyboardEvent<HTMLFormElement>): void => {
			if (props.suppressOnEnter === true) {
				if (e.key === 'Enter') {
					e.preventDefault();
				}
			}
		},
		[props.suppressOnEnter],
	);

	const addCallback = useCallback(
		(callback: Callback) =>
			setValidator({
				type: 'add',
				payload: callback,
			}),
		[],
	);

	const removeCallback = useCallback(
		(callback: Callback) =>
			setValidator({
				type: 'remove',
				payload: callback,
			}),
		[],
	);

	return (
		<fieldValidatorContext.Provider
			value={{
				addCallback,
				removeCallback,
			}}
		>
			<form onSubmit={onSubmit} onKeyDown={onKeyDown} className={props.className}>
				<PrevChildren underlineSrc={underlineSrc}>{prevChildren}</PrevChildren>
				<CurChildren>{curChildren}</CurChildren>
			</form>
		</fieldValidatorContext.Provider>
	);
};

export default styled(Form)``;
