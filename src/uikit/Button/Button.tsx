import styled from 'styled-components';
import React, { MouseEvent } from 'react';
import classNames from 'classnames';

type Themes = 'primary' | 'secondary' | 'simple';

interface Props {
	color?: Themes;
	className?: string;
	children?: React.ReactNode;
	disabled?: boolean;
	active?: boolean;
	getRef?: React.MutableRefObject<HTMLButtonElement | null>;
	onClick?(event?: MouseEvent<HTMLButtonElement>): void;
	onMouseEnter?(event?: MouseEvent<HTMLButtonElement>): void;
	onMouseLeave?(event?: MouseEvent<HTMLButtonElement>): void;
}

const StyledButton = styled.button`
	cursor: pointer;
	padding: 5px 40px;
	background-color: ${({ theme }) => theme.button.background.primary};
	border: 1.5px solid transparent;
	border-radius: 3px;
	font-size: 1.2rem;
	font-family: inherit;
	text-align: center;
	outline: none;
	transition: border ${({ theme }) => theme.other.defaultTransitionDuration},
		background-color ${({ theme }) => theme.other.defaultTransitionDuration};

	&:hover {
		background-color: ${({ theme }) => theme.button.background.hoverfocus};
	}

	&:active {
		background-color: ${({ theme }) => theme.button.background.hoverfocus};
		border: 1.5px solid rgba(113, 121, 122, 0.48);
	}
`;

const PrimaryButton = styled(StyledButton)`
	color: #192022;
`;

const SecondaryButton = styled(StyledButton)`
	color: rgb(153, 3, 15);
`;

const SimpleButton = styled.button`
	cursor: pointer;
	background-color: #fff;
	color: #888;
	border: 1px solid #888;
	border-radius: 20px;
	font-size: 0.6rem;
	padding: 2px 10px;
	box-shadow: 0 0 2px 0 transparent;
	height: 100%;
	outline: none;
	transition: background-color ${({ theme }) => theme.other.defaultTransitionDuration};

	&:not(:disabled) {
		&:hover {
			box-shadow: 0 0 2px 0 #888;
			color: #2c2c2c;
		}

		&:active {
			background-color: #eee;
			color: rgba(44, 44, 44, 0.7);
			box-shadow: 0 0 2px 0 #888;
		}

		&.active {
			background-color: #eee;
			color: #2c2c2c;
		}
	}

	&:disabled {
		cursor: default;
		background-color: #eee;
		color: #2c2c2c;
		border: 1px solid #eee;
	}
`;

const Button: React.FC<Props> = (props) => {
	switch (props.color) {
		case 'simple':
			return (
				<SimpleButton
					className={classNames(props.className, {
						active: props.active === true,
					})}
					disabled={props.disabled}
					onClick={props.onClick}
					onMouseEnter={props.onMouseEnter}
					onMouseLeave={props.onMouseLeave}
					ref={props.getRef}
				>
					{props.children}
				</SimpleButton>
			);

		case 'secondary':
			return (
				<SecondaryButton
					className={props.className}
					disabled={props.disabled}
					onMouseEnter={props.onMouseEnter}
					onMouseLeave={props.onMouseLeave}
					onClick={props.onClick}
					ref={props.getRef}
				>
					{props.children}
				</SecondaryButton>
			);

		case 'primary':
		default:
			return (
				<PrimaryButton
					className={props.className}
					disabled={props.disabled}
					ref={props.getRef}
					onMouseEnter={props.onMouseEnter}
					onMouseLeave={props.onMouseLeave}
					onClick={props.onClick}
				>
					{props.children}
				</PrimaryButton>
			);
	}
};

export default styled(Button)``;
