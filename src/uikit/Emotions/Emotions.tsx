import React, { useCallback, useContext, useState } from 'react';
import { ResponseEmotion } from 'Types/response.type';
import styled from 'styled-components';
import FullsizeBackground from 'Components/FullsizeBackground';
import Scroll from 'UI/Scroll';
import { tooltipContext } from 'Contexts/tooltip.context';
import { appContext } from 'Contexts/app.context';
import { d } from 'Utils/dictionary';
import { Rarity } from 'Utils/enum';
import { routePush } from 'Components/Link';

const enum TabsColor {
	premium = 'premium',
	legendary = '#ff8710',
	mythical = '#9200ee',
	epic = '#50fd00',
	rare = '#f9ff00',
	uncommon = '#639cff',
	common = '#3b3b3b',
	basic = '#5d5d5d',
}

interface Props {
	emotions: ResponseEmotion[];
	addEmotion: (alias: string) => void;
}

const Container = styled.div`
	max-width: 216px;
	display: flex;
	flex-direction: column;
	box-shadow: 0 0 5px -2px black;
`;

const Wrap = styled(Scroll)`
	display: flex;
	align-items: center;
	flex-direction: row;
	flex-flow: row wrap;
	height: 200px;
`;

export const EmotionImage = styled(FullsizeBackground)`
	cursor: pointer;
	width: 24px;
`;

const StyleEmotionImage = styled(EmotionImage)`
	margin: 6px;
`;

const Head = styled.div`
	margin-bottom: 10px;
	overflow-y: auto;
`;

interface TabProps {
	active: boolean;
	color: string;
}

const Tab = styled.div<TabProps>`
	cursor: pointer;
	position: relative;
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
	padding: 3px 15px 3px 5px;
	color: ${({ active }) => (active ? '#333' : '#424242')};
	font-family: ${({ theme }) => theme.fontFamily.quiet};
	font-size: 0.7em;
	box-shadow: ${({ active }) => active && 'inset 0 0 4px -2px black'};
	border-bottom: 1px solid #bbb;
	box-sizing: border-box;
	background-color: ${({ color }) => (color === 'premium' ? '#ffcf14' : '#fff')};

	&:after {
		content: '';
		position: absolute;
		right: 5px;
		background-color: ${({ color }) => (color === 'premium' ? '#fff' : color)};
		width: 7px;
		height: 7px;
		box-shadow: 0 0 5px -2px black;
	}
`;

const Emotions: React.FC<Props> = ({ emotions, addEmotion }) => {
	const { user } = useContext(appContext);
	const { setVisible } = useContext(tooltipContext);

	const [tab, setTab] = useState(user?.dignities[0].name ?? 'newbie');

	const changeTab = useCallback(
		(tabName: string) => () => {
			setTab(tabName);
		},
		[],
	);

	const setEmotion = useCallback(
		(alias: string) => () => {
			addEmotion(alias);
			setVisible?.(false);
		},
		[addEmotion, setVisible],
	);

	const redirectToShop = useCallback(() => {
		void routePush({
			pathname: '/shop',
			query: {},
		});
	}, []);

	return (
		<Container>
			<Head>
				{user?.dignities.every(({ rarity }) => rarity !== Rarity.premium) === true && (
					<Tab
						key="premium"
						// @ts-ignore
						color={TabsColor.premium}
						active={false}
						onClick={redirectToShop}
					>
						{d(`dignities.profile.champion`)}
					</Tab>
				)}
				{user?.dignities.map((dignity) => (
					<Tab
						key={dignity.name}
						onClick={changeTab(dignity.name)}
						// @ts-ignore
						color={TabsColor[Rarity[dignity.rarity]]}
						active={tab === dignity.name}
					>
						{d(`dignities.profile.${dignity.name}`)}
					</Tab>
				))}
			</Head>
			<Wrap>
				{emotions
					.filter(({ dignity }) => dignity === tab)
					.map(({ _id, title, alias, src }) => (
						<StyleEmotionImage key={_id} src={src} ratio={1} title={title} onClick={setEmotion(alias)} />
					))}
			</Wrap>
		</Container>
	);
};

export default Emotions;
