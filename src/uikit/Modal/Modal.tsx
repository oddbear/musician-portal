import React, { useEffect } from 'react';
import styled from 'styled-components';

interface Props {
	isOpen: boolean;
	children: React.ReactNode;
	onClose?(): void;
}

interface ContainerProps {
	isOpen: boolean;
}

const Container = styled.div<ContainerProps>`
	position: fixed;
	left: 0;
	top: 0;
	width: 100%;
	height: 100%;
	opacity: ${({ isOpen }) => (isOpen ? 1 : 0)};
	pointer-events: ${({ isOpen }) => (isOpen ? 'initial' : 'none')};
	transition: opacity 0.15s;
	text-align: center;
	z-index: 10;
`;

interface ShadowProps {
	clickable: boolean;
}

const Shadow = styled.div<ShadowProps>`
	cursor: ${({ clickable }) => (clickable ? 'pointer' : 'default')};
	position: absolute;
	width: 100%;
	height: 100%;
	background-color: rgba(0, 0, 0, 0.3);
	z-index: 1;
`;

const Wrap = styled.div`
	position: absolute;
	left: 50%;
	top: 50%;
	transform: translate(-50%, -50%);
	z-index: 2;
`;

const Content = styled.div`
	padding: 10px 20px;
	background-color: white;
	color: #000;
`;

const Modal: React.FC<Props> = ({ isOpen, onClose, children }) => {
	useEffect(() => {
		const keyDown = (event: KeyboardEvent): void => {
			if (event.key !== undefined && event.key.toLowerCase() === 'escape') {
				onClose?.();
			}
		};

		window.addEventListener('keydown', keyDown);
		return () => window.removeEventListener('keydown', keyDown);
	}, [onClose]);

	return (
		<Container isOpen={isOpen}>
			<Shadow clickable={onClose !== undefined} onClick={onClose} />
			<Wrap>
				<Content>{children}</Content>
			</Wrap>
		</Container>
	);
};

export default Modal;
