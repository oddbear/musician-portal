import React, { MouseEvent, useCallback, useContext } from 'react';
import styled from 'styled-components';
import Button from 'UI/Button/Button';
import { d } from 'Utils/dictionary';
import { Response, ResponseReaction, ResponseProfile } from 'Types/response.type';
import { tooltipContext } from 'Contexts/tooltip.context';
import { EmotionImage } from 'UI/Emotions/Emotions';
import { appContext } from 'Contexts/app.context';

interface Props {
	_id: string;
	isPost?: boolean;
	reactions: ResponseReaction[];
	className?: string;
	addEmotion: (alias: string) => void;
}

const Container = styled.div`
	display: flex;
	flex-direction: row;
	align-items: center;
	width: 100%;
`;

const EmotionButton = styled(Button)`
	display: flex;
	flex-direction: row;
	align-items: center;
	margin-left: 4px;
`;

const EmotionCounter = styled.div`
	font-size: 1rem;
	margin-right: 6px;
`;

const EmotionsBar: React.FC<Props> = ({ isPost, _id, reactions, className, addEmotion }) => {
	const { user } = useContext(appContext);
	const { setContext, setAttachedElement, setVisible } = useContext(tooltipContext);

	const showEmotionTooltip = useCallback(
		(event: MouseEvent<HTMLButtonElement>) => {
			setAttachedElement?.(event.currentTarget);
			setContext?.({
				emotions: {
					isPost,
					_id,
					reactions,
				},
			});
		},
		[isPost, reactions, setAttachedElement, setContext, _id],
	);

	const setEmotion = useCallback(
		(alias: string) => () => {
			if (user !== null) {
				addEmotion(alias);
			}
		},
		[user, addEmotion],
	);

	const mouseEnter = useCallback(
		(authors: Response<ResponseProfile>, alias: string, title: string) => (event: MouseEvent<HTMLButtonElement>) => {
			setAttachedElement?.(event.currentTarget);
			setContext?.({
				emotionsInfo: {
					authors,
					alias,
					title,
				},
			});
		},
		[setContext, setAttachedElement],
	);

	const mouseLeave = useCallback(() => {
		setVisible?.(false);
	}, [setVisible]);

	return (
		<Container className={className}>
			<Button color="simple" onClick={showEmotionTooltip}>
				{d('emotions.add')}
			</Button>
			{reactions.map(({ authors, emotion }) => (
				<EmotionButton
					key={emotion.alias}
					color="simple"
					onClick={setEmotion(emotion.alias)}
					onMouseEnter={mouseEnter(authors, emotion.alias, emotion.title)}
					onMouseLeave={mouseLeave}
				>
					{authors.totalLength > 1 && <EmotionCounter>{authors.totalLength}</EmotionCounter>}
					<EmotionImage src={emotion.src} ratio={1} />
				</EmotionButton>
			))}
		</Container>
	);
};

export default EmotionsBar;
