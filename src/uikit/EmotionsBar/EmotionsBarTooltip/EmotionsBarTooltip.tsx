import React from 'react';
import Link from 'Components/Link/Link';
import { d } from 'Utils/dictionary';
import styled from 'styled-components';
import { Response, ResponseProfile } from 'Types/response.type';

interface Props {
	emotionsInfo: {
		authors: Response<ResponseProfile>;
		alias: string;
		title: string;
	};
}

const Author = styled.span`
	font-size: 0.8em;
	text-decoration: underline;

	&:not(:first-child) {
		margin-left: 4px;
	}
`;

const EmotionTooltipContainer = styled.div`
	max-width: 200px;
	display: flex;
	flex-direction: row;
	flex-flow: row wrap;
	place-content: center;
`;

const EmotionName = styled.div`
	width: 100%;
	text-align: center;
	font-size: 0.8em;
	color: #999;
	margin-bottom: 10px;
`;

const EmotionsBarTooltip: React.FC<Props> = ({ emotionsInfo }) => {
	return (
		<EmotionTooltipContainer>
			<EmotionName>{emotionsInfo.title}</EmotionName>
			{emotionsInfo.authors.data.map((profile, index) => (
				<Author key={profile._id}>
					<Link
						location={{
							pathname: '/profile',
							query: {
								login: profile.login,
							},
						}}
						asLocation={{
							pathname: `/profile/${profile.login}`,
						}}
					>
						{profile.name}
						{index !== emotionsInfo.authors.data.length - 1 && ','}
					</Link>
				</Author>
			))}
			{emotionsInfo.authors.totalLength - emotionsInfo.authors.data.length > 0 && (
				<Author>
					{d('emotions.andMoreAuthors', emotionsInfo.authors.totalLength - emotionsInfo.authors.data.length)}
				</Author>
			)}
		</EmotionTooltipContainer>
	);
};

export default EmotionsBarTooltip;
