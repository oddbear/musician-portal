import React from 'react';
import styled from 'styled-components';

interface Props {
	on: boolean;
	title?: string;
	onClick: () => void;
}

interface StyleProps {
	enabled: boolean;
}

const Container = styled.div<StyleProps>`
	position: relative;
	cursor: pointer;
	width: 46px;
	height: 20px;
	border: 1px solid black;
	background-color: ${({ enabled }) => (enabled ? '#333' : '#111')};
	padding: 3px 0;
	transition: background-color ${({ theme }) => theme.other.defaultTransitionDuration};
`;

const Switcher = styled.div<StyleProps>`
	position: absolute;
	width: 20px;
	height: 20px;
	box-shadow: 0 0 5px -2px black;
	background-color: ${({ enabled }) => (enabled ? 'white' : '#aaa')};
	left: ${({ enabled }) => (enabled ? '23px' : '3px')};
	transition: background-color ${({ theme }) => theme.other.defaultTransitionDuration},
		left ${({ theme }) => theme.other.defaultTransitionDuration};
`;

const Tumbler: React.FC<Props> = ({ on, title, onClick }) => {
	return (
		<Container enabled={on} title={title} onClick={onClick}>
			<Switcher enabled={on} />
		</Container>
	);
};

export default Tumbler;
