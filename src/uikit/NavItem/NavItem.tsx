import Link from 'Components/Link';
import React from 'react';
import styled from 'styled-components';
import { useLazyLoad } from 'Hooks/lazyload.hook';

const PRIMARY_SRC = 'BG/Jacks/Red_yywp2m';
const SECONDARY_SRC = 'BG/Jacks/White_rwpilw';

interface Props {
	href: string;
	title?: string;
	secondary?: boolean;
	className?: string;
}

const Container = styled.div`
	display: inline-flex;
	flex-direction: column;
	align-content: center;
	align-items: center;
	transition: color ${({ theme }) => theme.other.defaultTransitionDuration};

	&:hover {
		color: ${({ theme }) => theme.color.hovernav};
	}
`;

interface ItemProps {
	src: string;
}

const SIZE = 50;

const Item = styled.div<ItemProps>`
	cursor: pointer;
	background: url(${({ src }) => src}) no-repeat center;
	background-size: cover;
	width: ${SIZE}px;
	height: ${SIZE}px;
	margin: 15px 0;
`;

const Head = styled.div`
	padding: 0;
`;

const NavItem: React.FC<Props> = (props) => {
	const { src } = useLazyLoad({
		src: props.secondary === true ? SECONDARY_SRC : PRIMARY_SRC,
	});

	return (
		<Link
			location={{
				query: {},
				pathname: props.href,
			}}
			ariaLabel="nav-item"
		>
			<Container className={props.className}>
				{props.title !== undefined && (
					<Head>
						<span>{props.title.toUpperCase()}</span>
					</Head>
				)}
				<Item src={src} />
			</Container>
		</Link>
	);
};

export default NavItem;

const GroupBGColor = '#fff';

interface GroupProps {
	title: string | React.ReactNode;
	className?: string;
	children: React.ReactNode;
}

const GroupContainer = styled(Container)`
	border: 2px solid ${GroupBGColor};
	border-radius: 8px;
	overflow: hidden;
`;

const GroupHead = styled(Head)`
	color: ${({ theme }) => theme.color.fourth};
	background-color: ${GroupBGColor};
	word-spacing: -14px;
	padding: 0 10px;

	&:hover {
		color: #672020;
	}
`;

const GroupContent = styled.div`
	place-content: flex-end;
	display: flex;
	width: 100%;
	padding: 0 10px;
	box-sizing: border-box;
`;

export const Group: React.FC<GroupProps> = (props) => {
	return (
		<GroupContainer className={props.className}>
			<GroupHead>{props.title}</GroupHead>
			<GroupContent>{props.children}</GroupContent>
		</GroupContainer>
	);
};
