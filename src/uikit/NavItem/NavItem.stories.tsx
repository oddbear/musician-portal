import * as React from 'react';
import { storiesOf } from '@storybook/react';
import styled from 'styled-components';
import NavItem, { Group } from './NavItem';

const GroupContainer = styled.div`
	padding: 20px;
	background-color: #333;
`;

storiesOf('NavItem', module)
	.add('primary', () => <NavItem href="#" />)
	.add('secondary', () => <NavItem href="#" secondary={true} />)
	.add('with title', () => <NavItem href="#" title="Foo Link" />)
	.add('grouped', () => (
		<GroupContainer>
			<Group title="Foo Bar">
				<NavItem href="#" />
				<NavItem href="#" />
				<NavItem href="#" secondary={true} />
			</Group>
		</GroupContainer>
	));
