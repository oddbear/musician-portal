import React, {
	ClipboardEvent,
	FormEvent,
	KeyboardEvent,
	MouseEvent,
	RefObject,
	useCallback,
	useContext,
	useEffect,
	useReducer,
	useRef,
	useState,
} from 'react';
import styled from 'styled-components';
import { ResponseEmotion } from 'Types/response.type';
import { escapeRegExp } from 'Utils/regexp';
import Emotions from 'UI/Emotions';
import Tooltip from 'UI/Tooltip';
import { tooltipContext } from 'Contexts/tooltip.context';
import Button from 'UI/Button/Button';
import { useGetRef } from 'Hooks/getRef.hook';
import LazyImage from 'Components/LazyImage';
import { appContext } from 'Contexts/app.context';
import { buildCloudinaryUrl } from 'Utils/cloudinary';

const SMILE_ICON_SRC = 'emotions/r5ltjgjgl567lyctk0g9';

interface Props {
	emotions: ResponseEmotion[];
	value: string;
	placeholder: string;
	getRef?: RefObject<HTMLTextAreaElement>;
	onChange?: (value: string) => void;
	onKeyDown?: (event: KeyboardEvent<HTMLTextAreaElement>) => void;
	onMouseDown?: (event: MouseEvent<HTMLTextAreaElement>) => void;
	onPaste?: (event: ClipboardEvent<HTMLTextAreaElement>) => void;
}

const Container = styled.div`
	position: relative;
	display: block;
	width: 100%;
	height: 100%;
	font-size: 1rem;
`;

interface InputEditorProps {
	height: number;
}

const InputEditor = styled.textarea<InputEditorProps>`
	width: 100%;
	min-height: 90px;
	height: ${({ height }) => height.toString() + 'px'};
	border: 1px solid black;
	border-radius: 4px;
	padding: 10px;
	box-sizing: border-box;
	outline: none;
	resize: none;
	font-size: 1em;
	font-family: inherit;
	line-height: 30px;
	overflow: visible;
`;

const OutputEditor = styled.div`
	position: absolute;
	left: 0;
	top: 0;
	width: 100%;
	min-height: 90px;
	border: 1px solid black;
	border-radius: 4px;
	padding: 10px;
	box-sizing: border-box;
	outline: none;
	word-break: break-word;
	font-size: 1em;
	font-family: inherit;
	z-index: 1;
	pointer-events: none;
	line-height: 30px;
	vertical-align: bottom;

	.emotion {
		height: 1.1em;
		width: 1.1em;
		vertical-align: top;
		margin-right: 1.1px;
	}
`;

const StyledTooltip = styled(Tooltip)`
	padding: 0;
	margin: 0;
`;

const SmilesImage = styled(LazyImage)`
	width: 20px;
`;

const EmotionsPanelContainer = styled.div`
	display: inline-block;
`;

type Reducer = (times: number) => number;
const reducer: Reducer = (state) => state + 1;

const MessageArea: React.FC<Props> = ({
	emotions,
	value,
	placeholder,
	getRef,
	onChange,
	onKeyDown,
	onMouseDown,
	onPaste,
}) => {
	const [isEmotionsVisible, setIsEmotionsVisible] = useState<boolean>(false);
	const [selectionStart, setSelectionStart] = useState<number>(0);

	const [, updateComponent] = useReducer<Reducer>(reducer, 0);
	const outputEditorRef = useRef<HTMLDivElement>(null);
	const buttonRef = useGetRef<HTMLButtonElement>();

	const { user } = useContext(appContext);

	const keyDown = useCallback(
		(event: KeyboardEvent<HTMLTextAreaElement>) => {
			setSelectionStart(window.getSelection()?.focusOffset ?? 0);
			onKeyDown?.(event);
		},
		[onKeyDown],
	);

	const mouseDown = useCallback(
		(event: MouseEvent<HTMLTextAreaElement>) => {
			setSelectionStart(window.getSelection()?.focusOffset ?? 0);
			onMouseDown?.(event);
		},
		[onMouseDown],
	);

	const change = useCallback(
		(event: FormEvent<HTMLTextAreaElement>) => {
			const target = event.currentTarget;

			onChange?.(target.value);
			setSelectionStart(window.getSelection()?.focusOffset ?? 0);
		},
		[onChange],
	);

	const paste = useCallback(
		(event: ClipboardEvent<HTMLTextAreaElement>) => {
			onPaste?.(event);
			setSelectionStart(window.getSelection()?.focusOffset ?? 0);
		},
		[onPaste],
	);

	let preparedValue = value
		.replace(/(?:^\s|\s+)/g, '&nbsp;')
		.replace(/</g, '&lt;')
		.replace(/>/g, '&gt;')
		.replace(/\n/g, '<br/>');
	if (preparedValue !== '') {
		for (const emotion of emotions.filter(
			({ dignity }) => user?.dignities.map(({ name }) => name).includes(dignity) === true,
		)) {
			const regex = new RegExp(escapeRegExp(emotion.alias), 'g');
			preparedValue = preparedValue.replace(
				regex,
				`<img alt="${emotion.alias}" class="emotion" src="${buildCloudinaryUrl(emotion.src)}">`,
			);
		}
	}

	useEffect(() => {
		updateComponent();
	}, [preparedValue]);

	const addEmotion = useCallback(
		(alias: string) => {
			onChange?.(`${value.substr(0, selectionStart)}${alias}${value.substring(selectionStart)}`);
		},
		[selectionStart, onChange, value],
	);

	const openEmotionMenu = useCallback(() => {
		setIsEmotionsVisible(true);
	}, []);

	const closeEmotionMenu = useCallback(() => {
		setIsEmotionsVisible(false);
	}, []);

	useEffect(() => {
		const mouseDownHandler = (): void => {
			setIsEmotionsVisible(false);
		};

		window.addEventListener('mousedown', mouseDownHandler);
		return () => window.removeEventListener('mousedown', mouseDownHandler);
	}, []);

	const prevent = useCallback((event: MouseEvent<HTMLDivElement>) => {
		event.stopPropagation();
	}, []);

	return (
		<tooltipContext.Provider
			value={{
				visible: isEmotionsVisible,
				attachedElement: buttonRef.current,
			}}
		>
			<Container>
				<InputEditor
					onKeyDown={keyDown}
					onMouseDown={mouseDown}
					onFocus={focus}
					onBlur={blur}
					ref={getRef}
					onPaste={paste}
					onChange={change}
					value={value}
					placeholder={placeholder}
					height={outputEditorRef.current?.offsetHeight ?? 0}
				/>
				<OutputEditor ref={outputEditorRef} dangerouslySetInnerHTML={{ __html: preparedValue }} />
				<EmotionsPanelContainer onMouseDown={prevent} onMouseLeave={closeEmotionMenu}>
					<Button color="simple" getRef={buttonRef} onClick={openEmotionMenu} onMouseEnter={openEmotionMenu}>
						<SmilesImage src={SMILE_ICON_SRC} alt="more smiles" fitHeight={true} />
					</Button>
					<StyledTooltip>
						<Emotions emotions={emotions} addEmotion={addEmotion} />
					</StyledTooltip>
				</EmotionsPanelContainer>
			</Container>
		</tooltipContext.Provider>
	);
};

export default MessageArea;
