import React, { useCallback, useState } from 'react';
import styled from 'styled-components';

interface Props {
	title: React.ReactNode;
	children: React.ReactNode;
	className?: string;
}

const Title = styled.h2`
	cursor: pointer;
	margin: 0;
`;

interface AreaProps {
	open: boolean;
}

export const Area = styled.div<AreaProps>`
	display: ${({ open }) => (open ? 'block' : 'none')};
	width: 50%;
	padding: 20px 40px;
	font-family: ${({ theme }) => theme.fontFamily.quiet};
	font-size: 0.8em;
`;

const Spoiler: React.FC<Props> = ({ title, children, className }) => {
	const [opened, setOpened] = useState<boolean>(false);

	const changeOpened = useCallback(() => {
		setOpened(!opened);
	}, [opened]);

	return (
		<div className={className}>
			<Title onClick={changeOpened}>{title}</Title>
			<Area open={opened}>{children}</Area>
		</div>
	);
};

export default styled(Spoiler)``;
