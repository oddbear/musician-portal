import React, { useCallback } from 'react';
import styled from 'styled-components';

interface Props<T> {
	titles: { title: string; value: T; disabled?: boolean }[];
	tab: T;
	setTab: (tab: T) => void;
	className?: string;
}

const Row = styled.div`
	width: 100%;
	display: flex;
	place-content: space-between;
	flex-direction: row;
	align-items: center;
	margin-bottom: 6px;
`;

interface TitleProps {
	disabled: boolean;
}

const Title = styled.h3<TitleProps>`
	cursor: ${({ disabled }) => (disabled ? 'default' : 'pointer')};
	font-size: 1.4rem;
	margin: 0 20px 10px 0;
	color: ${({ disabled }) => (disabled ? '#888 !important' : '#fff')};

	&.unactive {
		color: gray;
	}
`;

function Tabs<T>({ titles, tab, setTab, className }: Props<T>): React.ReactElement {
	const changeTab = useCallback(
		(value: T) => () => {
			setTab(value);
		},
		[setTab],
	);

	return (
		<Row className={className}>
			{titles.map(({ title, value, disabled }, index) => (
				<Title
					key={typeof value === 'string' || typeof value === 'number' ? value : index}
					disabled={disabled === true}
					onClick={disabled !== true ? changeTab(value) : undefined}
					className={value === tab ? 'active' : 'unactive'}
				>
					{title}
				</Title>
			))}
		</Row>
	);
}

export default Tabs;
