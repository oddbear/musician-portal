import React from 'react';
import styled from 'styled-components';

interface Props {
	isVisible: boolean;
	children: React.ReactNode;
}

interface ContainerProps {
	visible: boolean;
}

const Container = styled.div<ContainerProps>`
	display: ${({ visible }) => (visible ? 'block' : 'none')};
	position: fixed;
	bottom: 0;
	width: 100%;
	padding: 20px 40px;
	background-color: ${({ theme }) => theme.color.fourth};
	color: white;
	z-index: 12;
	font-family: ${({ theme }) => theme.fontFamily.quiet};
	font-weight: 300;
	font-size: 0.7rem;
`;

const Notification: React.FC<Props> = ({ isVisible, children }) => {
	return <Container visible={isVisible}>{children}</Container>;
};

export default Notification;
