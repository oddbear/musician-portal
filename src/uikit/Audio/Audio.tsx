import React, { MouseEvent, useCallback, useContext } from 'react';
import styled from 'styled-components';
import { playerContext } from 'Contexts/player.context';
import LazyImage from 'Components/LazyImage';
import { d } from 'Utils/dictionary';
import { appContext } from 'Contexts/app.context';
import { tooltipContext } from 'Contexts/tooltip.context';

const PLAY_SRC = 'Band_Profile/ecdgvjbjfjlfkohzcusx';
const PAUSE_SRC = 'Band_Profile/j0wwgrnj1bge8s1k6gbf';

interface Props {
	_id: string;
	name: string;
	src: string;
	onRemove?: (_id: string) => void;
}

const Row = styled.div`
	display: flex;
	flex-direction: row;
	align-items: center;
	font-family: ${({ theme }) => theme.fontFamily.quiet};
`;

const Text = styled.span`
	font-size: 0.8em;
	color: #333;
`;

const Container = styled(Row)`
	font-size: 1rem;
	width: 100%;
	place-content: space-between;
`;

const Button = styled(LazyImage)`
	margin-right: 20px;
	width: 10px;
	height: 12px;
`;

const Action = styled.span`
	opacity: 0.7;
	margin-left: 6px;
	font-size: 0.8em;
	cursor: pointer;

	&:hover {
		opacity: 1;
	}
`;

const AudioComponent: React.FC<Props> = ({ _id, name, src, onRemove }) => {
	const { state, current, play, pause } = useContext(playerContext);
	const { user } = useContext(appContext);
	const { setAttachedElement, setContext, setVisible } = useContext(tooltipContext);

	const playAudio = useCallback(() => {
		play(src, _id);
	}, [play, src, _id]);

	const pauseAudio = useCallback(() => {
		pause();
	}, [pause]);

	const remove = useCallback(() => {
		onRemove?.(_id);
	}, [_id, onRemove]);

	const attachElement = useCallback(
		(event: MouseEvent<HTMLElement>) => {
			setAttachedElement?.(event.currentTarget);
			setVisible?.(true);
			setContext?.({
				audioId: _id,
			});
		},
		[_id, setVisible, setContext, setAttachedElement],
	);

	const detachElement = useCallback(() => {
		setVisible?.(false);
	}, [setVisible]);

	const isPlaying = state === 'play' && src === current.src && _id === current._id;

	return (
		<Container>
			<Row onClick={isPlaying ? pauseAudio : playAudio}>
				<Button src={isPlaying ? PAUSE_SRC : PLAY_SRC} alt={isPlaying ? 'pause button' : 'play button'} />
				<Text>{name}</Text>
			</Row>
			<Row>
				{user !== null && (
					<Action onMouseEnter={attachElement} onMouseLeave={detachElement}>
						+
					</Action>
				)}
				{onRemove !== undefined && <Action onClick={remove}>{d('audios.remove')}</Action>}
			</Row>
		</Container>
	);
};

export default AudioComponent;
