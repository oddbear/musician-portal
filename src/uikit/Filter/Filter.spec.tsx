import * as React from 'react';
import Checkbox from './Checkbox/Checkbox';
import { FilterVariant } from 'Types/filters.type';
import Group from 'UI/Filter/Group';
import Radio from './Radio/Radio';
import { useState } from 'react';
import Filter, { Option, SelectAll } from './index';
import { mount } from 'enzyme';

describe('UI Filter tests', () => {
	describe('radio-filter sets only value', () => {
		const options: string[] = ['first', 'second', 'third'];

		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		let spy: (value: any) => void = () => {
			return;
		};

		const Container: React.FC = () => {
			const [value, setValue] = useState(options[0]);

			// eslint-disable-next-line @typescript-eslint/no-explicit-any
			const changeValue = (newValue: any): void => {
				// eslint-disable-next-line @typescript-eslint/no-unsafe-call
				setValue(newValue);
				// eslint-disable-next-line @typescript-eslint/no-unsafe-call
				spy(newValue);
			};

			return (
				<Filter title="filter" type="radio" value={value} onChange={changeValue}>
					{options.map((optionValue) => (
						<Option key={optionValue} value={optionValue} title="foo bar" />
					))}
				</Filter>
			);
		};

		it('first radio checked on init', (done) => {
			// eslint-disable-next-line @typescript-eslint/no-unsafe-return,@typescript-eslint/no-unsafe-call
			spy = (value) => expect(value).toEqual(options[0]);

			done();
		});

		const filterWrapper = mount(<Container />);

		it('changes options', (done) => {
			// eslint-disable-next-line @typescript-eslint/no-unsafe-return,@typescript-eslint/no-unsafe-call
			spy = (value) => expect(value).toEqual(options[1]);
			filterWrapper.find(Option).at(1).find(Radio).simulate('click');

			// eslint-disable-next-line @typescript-eslint/no-unsafe-return,@typescript-eslint/no-unsafe-call
			spy = (value) => expect(value).toEqual(options[2]);
			filterWrapper.find(Option).at(2).find(Radio).simulate('click');

			done();
		});
	});

	it('sets default values for all checkboxes if not set', (done) => {
		interface Values {
			[key: string]: boolean;
		}

		const options: string[] = ['first', 'second', 'third'];

		const Container0: React.FC = () => {
			const [value, setValue] = useState<Values>({});

			const changeValue = (newValue: Values): void => {
				expect(options.every((optionValue) => newValue[optionValue] !== undefined)).toEqual(true);
				setValue(newValue);
			};

			return (
				<Filter title="filter" type="checkbox" value={value} onChange={changeValue}>
					{options.map((optionName) => (
						<Option key={optionName} value={optionName} title="foo bar" />
					))}
				</Filter>
			);
		};
		mount(<Container0 />);

		const variants: FilterVariant[] = [
			{
				value: 'foo0',
				title: 'any0',
			},
			{
				value: 'foo1',
				title: 'any1',
			},
			{
				value: 'foo2',
				title: 'any2',
			},
		];

		const Container1: React.FC = () => {
			const changeValue = (value: Values): void => {
				expect(options.every((optionValue) => value[optionValue]) !== undefined).toEqual(true);
			};

			return (
				<Filter title="filter" type="checkbox" value={{}} variants={variants} onChange={changeValue}>
					{options.map((value) => (
						<Option key={value} value={value} title="foo bar" />
					))}
				</Filter>
			);
		};
		mount(<Container1 />);

		done();
	});

	describe('checkbox-filter sets multiple values', () => {
		interface Values {
			[key: string]: boolean;
		}

		const options = ['first', 'second', 'third'];

		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		let spy: (value: any) => void = () => {
			return;
		};

		const Container: React.FC = () => {
			const [value, setValue] = useState<Values>({});

			const changeValue = (newValue: Values): void => {
				setValue(newValue);
				spy(newValue);
			};

			return (
				<Filter title="filter" type="checkbox" value={value} onChange={changeValue}>
					{options.map((optionValue) => (
						<Option key={optionValue} value={optionValue} title="foo bar" />
					))}
				</Filter>
			);
		};

		it('all checkboxes unchecked on init', (done) => {
			spy = (value) =>
				// eslint-disable-next-line @typescript-eslint/no-unsafe-return,@typescript-eslint/no-unsafe-call
				expect(value).toMatchObject({
					[options[0]]: false,
					[options[1]]: false,
					[options[2]]: false,
				});

			done();
		});

		const filterWrapper = mount(<Container />);

		it('checks successfully on first check', (done) => {
			spy = (value) =>
				// eslint-disable-next-line @typescript-eslint/no-unsafe-return,@typescript-eslint/no-unsafe-call
				expect(value).toMatchObject({
					[options[0]]: false,
					[options[1]]: true,
					[options[2]]: false,
				});
			filterWrapper.find(Option).at(1).find(Checkbox).simulate('click');

			done();
		});

		it('checks successfully on another box check', (done) => {
			spy = (value) =>
				// eslint-disable-next-line @typescript-eslint/no-unsafe-return,@typescript-eslint/no-unsafe-call
				expect(value).toMatchObject({
					[options[0]]: false,
					[options[1]]: true,
					[options[2]]: true,
				});
			filterWrapper.find(Option).at(2).find(Checkbox).simulate('click');

			done();
		});

		it('uncheck successfully', (done) => {
			spy = (value) =>
				// eslint-disable-next-line @typescript-eslint/no-unsafe-return,@typescript-eslint/no-unsafe-call
				expect(value).toMatchObject({
					[options[0]]: false,
					[options[1]]: true,
					[options[2]]: false,
				});
			filterWrapper.find(Option).at(2).find(Checkbox).simulate('click');

			done();
		});
	});

	describe('selectAll-button', () => {
		it('checks all boolean-checkboxes and disappears', () => {
			const optionName = 'second';

			const options: string[] = ['first', optionName, 'third'];
			let spy: (value: { [key: string]: boolean }) => void = () => {
				return;
			};

			const Container: React.FC = () => {
				const [value, setValue] = useState<{ [key: string]: boolean }>({
					[optionName]: true,
				});

				const changeValue = (newValue: { [key: string]: boolean }): void => {
					setValue(newValue);
					spy(newValue);
				};

				return (
					<Filter title="filter" type="checkbox" value={value} onChange={changeValue}>
						{options.map((optionValue) => (
							<Option key={optionValue} value={optionValue} title="foo bar" />
						))}
					</Filter>
				);
			};

			const filterWrapper = mount(<Container />);

			spy = (value) => {
				expect(options.every((optionValue) => value[optionValue])).toEqual(true);
			};

			filterWrapper.find(SelectAll).simulate('click');

			expect(filterWrapper.find(SelectAll)).toHaveLength(0);
		});

		it('checks all variable-checkboxes and disappears', () => {
			const variantName = 'foo';
			const optionName = 'second';

			const options: string[] = ['first', optionName, 'third'];
			const variants: FilterVariant[] = [
				{
					value: variantName,
					title: 'any',
				},
				{
					value: 'foo1',
					title: 'any1',
				},
				{
					value: 'foo2',
					title: 'any2',
				},
			];
			let spy: (value: { [key: string]: string | boolean }) => void = () => {
				return;
			};

			const Container: React.FC = () => {
				const [value, setValue] = useState<{ [key: string]: string | boolean }>({
					[optionName]: variantName,
				});

				const changeValue = (newValue: { [key: string]: string | boolean }): void => {
					setValue(newValue);
					spy(newValue);
				};

				return (
					<Filter variants={variants} title="filter" type="checkbox" value={value} onChange={changeValue}>
						{options.map((optionValue) => (
							<Option key={optionValue} value={optionValue} title="foo bar" />
						))}
					</Filter>
				);
			};

			const filterWrapper = mount(<Container />);

			spy = (value) => {
				expect(options.every((optionValue) => value[optionValue] === variants[0].value)).toEqual(true);
			};

			filterWrapper.find(SelectAll).simulate('click');

			expect(filterWrapper.find(SelectAll)).toHaveLength(0);
		});
	});

	describe('splits options on groups', () => {
		const groups = ['business', 'art', 'live'];
		const options = [
			{
				value: 'foo0',
				group: groups[0],
			},
			{
				value: 'foo1',
				group: groups[1],
			},
			{
				value: 'foo2',
				group: groups[1],
			},
			{
				value: 'foos',
				group: groups[1],
			},
			{
				value: 'koos',
				group: groups[2],
			},
			{
				value: 'loose',
				group: groups[2],
			},
			{
				value: 'fooz',
				group: null,
			},
		];

		const Container: React.FC = () => {
			const [value, setValue] = useState<{ [key: string]: boolean }>({});

			return (
				<Filter title="filter" type="checkbox" value={value} onChange={setValue}>
					{options.map(({ value: optionValue, group }) => (
						<Option key={optionValue} value={optionValue} title="foo bar" group={group} />
					))}
				</Filter>
			);
		};

		const filterWrapper = mount(<Container />);

		it('have groups count depended on options', () => {
			const renderedGroups = filterWrapper.find(Group).map((group) => group.props().value);
			expect(groups.sort((a, b) => (a > b ? 1 : -1))).toMatchObject(renderedGroups.sort((a, b) => (a > b ? 1 : -1)));
		});

		it('have unassigned group which contains all options without group', () => {
			expect(
				// eslint-disable-next-line @typescript-eslint/no-unsafe-call,@typescript-eslint/no-unsafe-member-access
				filterWrapper
					.find(Group)
					.parent()
					.at(0)
					.props()
					.children.find((child: React.ReactElement | React.ReactNode) => child instanceof Array)
					.find(
						(child: React.ReactElement) =>
							// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
							child.props.value === options.find(({ group }) => group === null)?.value,
					),
			).toBeDefined();
		});

		it('sorted by mention', () => {
			expect(
				filterWrapper
					.find(Group)
					.getElements()
					.filter((group: React.ReactElement | React.ReactNode) => !(group instanceof Array))
					// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access,@typescript-eslint/no-unsafe-return
					.map((group) => group.props.value),
			).toMatchObject(
				groups.sort((groupName0, groupName1) =>
					options.reduce<number>((acc, { group }) => (group === groupName0 ? acc + 1 : acc), 0) >
					options.reduce<number>((acc, { group }) => (group === groupName1 ? acc + 1 : acc), 0)
						? -1
						: 1,
				),
			);
		});
	});
});
