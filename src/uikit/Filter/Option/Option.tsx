import { filterContext } from 'Contexts/filter.context';
import React, { MouseEvent, ReactElement, useContext } from 'react';
import Radio from '../Radio';
import Checkbox from '../Checkbox';
import Multibox from '../Multibox';

export interface Props {
	value: string;
	title: string;
	group?: string | null;
}

const Component: React.FC<Props> = (props) => {
	const { type, variants } = useContext(filterContext);

	if (type === 'radio') {
		return <Radio value={props.value} title={props.title} />;
	} else {
		if (variants !== undefined) {
			return <Multibox value={props.value} title={props.title} />;
		} else {
			return <Checkbox value={props.value} title={props.title} />;
		}
	}
};

export type OptionElement = ReactElement<Props>;

const Option: React.FC<Props> = (props) => {
	return (
		<div onClick={onClick}>
			<Component {...props} />
		</div>
	);
};

Option.defaultProps = {
	group: null,
};

export default Option;

const onClick = (e: MouseEvent): void => e.stopPropagation();
