import { filterContext } from 'Contexts/filter.context';
import { useLazyLoad } from 'Hooks/lazyload.hook';
import styled from 'styled-components';
import React, { useCallback, useContext } from 'react';

const MARK_SRC = 'Search/%D0%93%D0%B0%D0%BB%D0%BA%D0%B0_tvr2uf';

interface Props {
	value: string;
	title: string;
}

const Container = styled.div`
	cursor: pointer;
	display: flex;
	align-items: center;
	line-height: 30px;
`;

interface CircleProps {
	checked: boolean;
	src: string;
}

export const Circle = styled.div<CircleProps>`
	display: flex;
	align-items: center;
	place-content: center;
	position: relative;
	min-width: 12px;
	height: 12px;
	margin-right: 16px;
	border: 1px solid ${({ theme }) => theme.button.background.primary};

	&:after {
		content: '';
		display: ${({ checked }) => (checked ? 'block' : 'none')};
		background: url(${({ src }) => src});
		position: absolute;
		background-size: cover;
		margin: 0 0 1px 10px;
		width: 100%;
		height: 100%;
		z-index: 1;
	}
`;

const Title = styled.span`
	font-size: 0.8rem;
	font-family: ${({ theme }) => theme.fontFamily.quiet};
	font-weight: 300;
`;

const Radio: React.FC<Props> = (props) => {
	const { value, onChange } = useContext(filterContext);

	const onClick = useCallback(() => {
		onChange(props.value);
	}, [props.value, onChange]);

	const { src } = useLazyLoad({
		src: MARK_SRC,
	});

	return (
		<Container onClick={onClick}>
			<Circle src={src} checked={value === props.value} />
			<Title>{props.title}</Title>
		</Container>
	);
};

export default Radio;
