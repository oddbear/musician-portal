import { filterContext } from 'Contexts/filter.context';
import { useLazyLoad } from 'Hooks/lazyload.hook';
import styled from 'styled-components';
import React, { useCallback, useContext } from 'react';

const MARK_SRC = 'Search/%D0%93%D0%B0%D0%BB%D0%BA%D0%B0_tvr2uf';

interface Props {
	value: string;
	title?: string;
}

const Container = styled.div`
	cursor: pointer;
	display: flex;
	align-items: center;
	line-height: 25px;
	user-select: none;
`;

interface BoxProps {
	checked: boolean;
	src: string;
}

export const Box = styled.div<BoxProps>`
	display: flex;
	align-items: center;
	place-content: center;
	position: relative;
	border: 1px solid ${({ theme }) => theme.button.background.primary};
	min-width: 12px;
	height: 12px;
	margin-right: 16px;

	&:after {
		content: '';
		display: ${({ checked }) => (checked ? 'block' : 'none')};
		background: url(${({ src }) => src});
		position: absolute;
		background-size: cover;
		margin: 0 0 1px 10px;
		width: 100%;
		height: 100%;
		z-index: 1;
	}
`;

const Title = styled.span`
	font-size: 0.8rem;
	font-family: ${({ theme }) => theme.fontFamily.quiet};
	font-weight: 300;
	color: rgba(241, 241, 241, 0.5);
`;

const Checkbox: React.FC<Props> = (props) => {
	const filtersContext = useContext(filterContext);
	const { onChange } = filtersContext;
	const filterValue = filtersContext.value as { [key: string]: string | boolean } | { [key: string]: boolean };

	const onClick = useCallback(() => {
		const oldValue = filterValue[props.value];
		onChange(props.value, typeof oldValue === 'boolean' ? !oldValue : false);
	}, [onChange, filterValue, props.value]);

	const { src } = useLazyLoad({
		src: MARK_SRC,
	});

	const value = filterValue[props.value];

	if (typeof value === 'string') {
		throw new Error('checkbox should have only boolean value');
	}

	return (
		<Container onClick={onClick}>
			<Box src={src} checked={value} />
			{props.title !== undefined && <Title>{props.title}</Title>}
		</Container>
	);
};

export default Checkbox;
