import * as React from 'react';
import Group from 'UI/Filter/Group/index';
import { useState } from 'react';
import Filter, { Option, SelectAll } from 'UI/Filter';
import { mount, ReactWrapper } from 'enzyme';

describe('UI Filters group tests', () => {
	const options = ['first', 'second', 'third'];

	const Container: React.FC = () => {
		const [value, setValue] = useState<{ [key: string]: boolean }>({});

		return (
			<Filter title="filter" type="checkbox" value={value} onChange={setValue}>
				{options.map((optionName) => (
					<Option key={optionName} value={optionName} title="foo bar" group="foo bar group" />
				))}
			</Filter>
		);
	};

	let filterWrapper: ReactWrapper;

	beforeEach(() => {
		filterWrapper = mount(<Container />);
	});

	it('hides and shows options on close and open group', () => {
		expect(filterWrapper.find(Group).find(Option)).toHaveLength(0);

		filterWrapper.find(Group).simulate('click');
		expect(filterWrapper.find(Group).find(Option)).toHaveLength(options.length);

		filterWrapper.find(Group).simulate('click');
		expect(filterWrapper.find(Group).find(Option)).toHaveLength(0);
	});

	it('auto-open group if option was checked', () => {
		expect(filterWrapper.find(Group).find(Option)).toHaveLength(0);

		filterWrapper.find(SelectAll).simulate('click');
		expect(filterWrapper.find(Group).find(Option)).toHaveLength(options.length);
	});
});
