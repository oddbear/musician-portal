import { filterContext } from 'Contexts/filter.context';
import isEqual from 'lodash/isEqual';
import styled from 'styled-components';
import { usePrevious } from 'Hooks/previous.hook';
import { OptionElement, Props as OptionProps } from 'UI/Filter/Option';
import React, { useCallback, useContext, useEffect, useState } from 'react';
import { d } from 'Utils/dictionary';

interface Props {
	value: string;
	children: OptionElement | OptionElement[];
}

const Container = styled.div`
	color: rgba(255, 255, 255, 0.65);
`;

const Header = styled.div`
	cursor: pointer;
	display: flex;
	align-items: center;
`;

const Title = styled.span`
	font-family: ${({ theme }) => theme.fontFamily.quiet};
	font-weight: 300;
	color: ${({ theme }) => theme.color.secondary};
	font-size: 0.8rem;
`;

interface ArrowProps {
	direction: 'top' | 'bottom';
}

const ARROW_SIZE = '0.2rem';

const Arrow = styled.div<ArrowProps>`
	margin: 0 20px 0 4px;
	width: 0;
	height: 0;
	border-left: ${ARROW_SIZE} solid transparent;
	border-right: ${ARROW_SIZE} solid transparent;
	border-${({ direction }) => direction}: 0.3rem solid ${({ theme }) => theme.color.secondary};
`;

const Group: React.FC<Props> = ({ value, children }) => {
	const [hidden, setHidden] = useState<boolean>(true);
	const { value: filterValue } = useContext(filterContext);

	const onClick = useCallback(() => {
		setHidden(!hidden);
	}, [hidden]);

	const prevFilterValue = usePrevious(filterValue);
	useEffect(() => {
		if (!isEqual(prevFilterValue, filterValue)) {
			Object.entries(filterValue).forEach(([fieldKey, fieldValue]) => {
				const oldValue = prevFilterValue as { [key: string]: string | boolean } | { [key: string]: boolean };
				if (
					oldValue[fieldKey] !== fieldValue &&
					(oldValue[fieldKey] === false || (fieldValue !== false && oldValue[fieldKey] === undefined))
				) {
					if (children instanceof Array) {
						if (
							(children as OptionElement[]).some(
								(child) => child.props.value === fieldKey && child.props.group === value,
							)
						) {
							setHidden(false);
						}
					} else if (
						(children.props as OptionProps).value === fieldKey &&
						(children.props as OptionProps).group === value
					) {
						setHidden(false);
					}
				}
			});
		}
	}, [prevFilterValue, children, filterValue, value]);

	return (
		<Container onClick={onClick}>
			<Header>
				<Arrow direction={hidden ? 'top' : 'bottom'} />
				<Title>{d(`groups.${value}`)}</Title>
			</Header>
			<div>{!hidden && children}</div>
		</Container>
	);
};

export default Group;
