import { filterContext } from 'Contexts/filter.context';
import { FilterTypes, FilterValue, FilterVariant } from 'Types/filters.type';
import React, { useCallback, useEffect, useState } from 'react';
import styled from 'styled-components';
import { OptionElement, Props as OptionProps } from 'UI/Filter/Option';
import LED from 'UI/LED';
import { d } from 'Utils/dictionary';
import Group from './Group';

interface Props<T> {
	value: T;
	type: FilterTypes;
	title?: string;
	children: OptionElement | OptionElement[];
	variants?: FilterVariant[];
	onChange?(filterValue: T): void;
}

type OnChangeRadio = (fv: string) => void;
type OnChangeCheckbox = (fv: { [key: string]: string | boolean }) => void;

const Container = styled.div`
	display: flex;
	flex-direction: column;
`;

export const Header = styled.div`
	margin: 20px 0;
	display: flex;
	place-content: space-between;
	font-size: 1.35rem;
`;

export const SelectAll = styled.div`
	cursor: pointer;
	display: flex;
	align-items: center;
	margin-right: 22px;

	span {
		margin-left: 2px;
	}
`;

function Filter({
	value,
	title,
	type,
	onChange,
	children,
	variants,
}: Props<string> | Props<{ [key: string]: string | boolean }> | Props<{ [key: string]: boolean }>): React.ReactElement {
	const [led, setLed] = useState(false);

	const ledOn = useCallback(() => setLed(true), []);
	const ledOff = useCallback(() => setLed(false), []);

	const selectAll = useCallback(() => {
		if (typeof value !== 'string' && onChange !== undefined) {
			(onChange as OnChangeCheckbox)(
				Object.entries(value).reduce<{ [key: string]: string | boolean }>((fields, [fieldKey, fieldValue]) => {
					if (fieldValue === false) {
						fields[fieldKey] = variants !== undefined ? variants[0].value : true;
					} else {
						fields[fieldKey] = fieldValue;
					}

					return fields;
				}, {}),
			);
		}

		ledOff();
	}, [ledOff, onChange, variants, value]);

	const change = useCallback(
		(key: string, filterValue?: FilterValue, switchback = false) => {
			if (onChange === undefined) {
				return;
			}

			if (typeof value === 'string') {
				(onChange as OnChangeRadio)(key);
			} else if (typeof value === 'object') {
				if (variants !== undefined) {
					const variantsKeys = variants.map((variant) => variant.value);

					let newVariant: string | false;
					if (switchback as boolean) {
						const keyIndex = variantsKeys.indexOf(filterValue!.toString()) - 1;
						if (keyIndex === -2) {
							newVariant = variantsKeys[variants.length - 1];
						} else {
							newVariant = keyIndex < 0 ? false : variantsKeys[keyIndex];
						}
					} else {
						const keyIndex = variantsKeys.indexOf(filterValue!.toString()) + 1;
						newVariant = variantsKeys.length - 1 < keyIndex ? false : variantsKeys[keyIndex];
					}

					(onChange as OnChangeCheckbox)({
						...value,
						[key]: newVariant,
					});
				} else {
					(onChange as OnChangeCheckbox)({
						...value,
						[key]: filterValue!,
					});
				}
			}
		},
		[onChange, variants, value],
	);

	const reactChildren = React.Children.toArray(children);
	const setInitialValues = useCallback(() => {
		if (typeof value === 'object') {
			const result = reactChildren.reduce<{ [key: string]: string | boolean }>((newValue, child) => {
				if (typeof child !== 'object' || !('props' in child)) {
					return newValue;
				}

				const fieldValue = (child.props as OptionProps).value;
				newValue[fieldValue] = typeof value[fieldValue] === 'undefined' ? false : value[fieldValue];

				return newValue;
			}, {});

			if (onChange !== undefined) {
				(onChange as OnChangeCheckbox)(result);
			}
		}
	}, [onChange, reactChildren, value]);

	useEffect(() => {
		if (Object.keys(value).length !== reactChildren.length) {
			setInitialValues();
		}
	});

	/*
	reduce all children by going through groups and separate them, so it will looks like:
	{
		popular: [ReactChild, ReactChild],
		national: [ReactChild],
		unassigned: [ReactChild],
	}
	
	and then sort them by length except "unassigned" - they always go in the end
	 */
	const groupedChildren = Object.entries(
		reactChildren.reduce<{ [key: string]: OptionElement[] }>(
			(groups, child) => {
				if (typeof child !== 'object' || !('props' in child)) {
					return groups;
				}

				const group = (child.props as OptionProps).group;
				if (group !== null) {
					const reducerGroup = groups[group!];
					if (reducerGroup !== undefined) {
						reducerGroup.push(child);
					} else {
						groups[group!] = [child];
					}
				} else {
					groups.unassigned.push(child);
				}

				return groups;
			},
			{
				unassigned: [],
			},
		),
	).sort(([key1, options1], [key2, options2]) => {
		if (key1 === 'unassigned' || options1.length < options2.length) {
			return 1;
		} else if (key2 === 'unassigned' || options1.length > options2.length) {
			return -1;
		} else {
			return 0;
		}
	});

	return (
		<filterContext.Provider
			value={{
				type,
				value,
				variants,
				onChange: change,
			}}
		>
			<Container>
				{title !== undefined && (
					<Header>
						<span>{title}</span>
						{type === 'checkbox' && Object.values(value).some((filterValue) => filterValue === false) && (
							<SelectAll onMouseOver={ledOn} onMouseOut={ledOff} onClick={selectAll}>
								<LED on={led} />
								<span>{d('search.filters.selectAll')}</span>
							</SelectAll>
						)}
					</Header>
				)}
				<div>
					{groupedChildren.map(([groupName, options]: [string, OptionElement[]]) => {
						if (groupName === 'unassigned') {
							return options;
						} else {
							return (
								<Group key={groupName} value={groupName}>
									{options}
								</Group>
							);
						}
					})}
				</div>
			</Container>
		</filterContext.Provider>
	);
}

Filter.defaultProps = {
	onChange: () => {
		return;
	},
};

export default Filter;
