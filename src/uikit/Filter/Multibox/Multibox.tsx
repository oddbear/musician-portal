import { filterContext } from 'Contexts/filter.context';
import styled from 'styled-components';
import React, { useCallback, useContext } from 'react';
import { d } from 'Utils/dictionary';

interface Props {
	value: string;
	title: string;
}

const Container = styled.div`
	display: flex;
	align-items: center;
	user-select: none;
`;

export const Wrap = styled.div`
	display: flex;
	align-items: center;
	margin-right: 10px;
`;

export const Text = styled.span`
	width: 60px;
	padding: 0 5px;
	font-family: ${({ theme }) => theme.fontFamily.quiet};
	font-size: 0.6rem;
	text-align: center;
	text-overflow: ellipsis;
	overflow: hidden;

	&.default {
		color: gray;
	}

	&.selected {
		color: #eee;
	}
`;

const Arrow = styled.div`
	cursor: pointer;
	width: 0;
	height: 0;
	border-style: solid;
	border-width: 0.4rem 0.4rem 0.4rem;
	border-color: transparent transparent transparent ${({ theme }) => theme.color.primary};

	&:first-child {
		transform: rotate(180deg);
	}
`;

const Title = styled.span`
	font-size: 0.8rem;
	font-family: ${({ theme }) => theme.fontFamily.quiet};
	font-weight: 300;
	color: rgba(241, 241, 241, 0.5);
`;

const Multibox: React.FC<Props> = (props) => {
	const filtersContext = useContext(filterContext);
	const { variants, onChange } = filtersContext;
	const filterValue = filtersContext.value as { [key: string]: string | boolean } | { [key: string]: boolean };

	if (variants === undefined) {
		throw new Error("variants can't be empty for multibox");
	}

	const onClick = useCallback(
		(switchback: boolean) => () => {
			const oldValue = filterValue[props.value];

			onChange(props.value, oldValue, switchback);
		},
		[onChange, filterValue, props.value],
	);

	const value = filterValue[props.value];
	const variant = variants.find((matchingVariant) => matchingVariant.value === value)?.title;

	return (
		<Container>
			<Wrap>
				<Arrow onClick={onClick(true)} />
				<Text className={variant === undefined ? 'default' : 'selected'}>{variant ?? d('multibox.default')}</Text>
				<Arrow onClick={onClick(false)} />
			</Wrap>
			<Title>{props.title}</Title>
		</Container>
	);
};

export default Multibox;
