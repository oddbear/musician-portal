import React, { MouseEvent, useCallback, useContext, useEffect, useRef, useState } from 'react';
import styled from 'styled-components';
import { useLazyLoad } from 'Hooks/lazyload.hook';
import { usePrevious } from 'Hooks/previous.hook';
import { playerContext } from 'Contexts/player.context';
import Button from 'UI/Button/Button';

interface Props {
	state: string;
	src: string;
}

const Container = styled.div`
	display: flex;
	position: fixed;
	bottom: 0;
	left: 0;
	width: 100%;
	padding: 20px 0;
	background-color: white;
	z-index: 100;
	flex-direction: row;
`;

const LineContainer = styled.div`
	margin: 0 20px;
	width: 100%;
`;

const LineWrap = styled.div`
	cursor: pointer;
	display: flex;
	width: 100%;
	height: 30px;
	align-items: center;
	position: relative;
`;

const Line = styled.div`
	width: 100%;
	height: 1px;
	background-color: black;
`;

interface PointerProps {
	left: number | string;
}

const Pointer = styled.div.attrs<PointerProps>(({ left }) => ({
	style: {
		left,
	},
}))<PointerProps>`
	position: absolute;
	height: 90%;
	width: 4px;
	margin-left: -2px;
	background-color: black;
`;

const Close = styled.div`
	position: absolute;
	right: 10px;
	top: 5px;
	cursor: pointer;
	font-size: 14px;
	font-family: monospace;
	color: gray;
`;

const Player: React.FC<Props> = ({ state, src }) => {
	const [currentTime, setCurrentTime] = useState<number>(0);
	const wrapRef = useRef<HTMLDivElement>(null);
	const pointerRef = useRef<HTMLDivElement>(null);
	const { element } = useLazyLoad({ src, tag: 'audio' });

	const { setState, play } = useContext(playerContext);

	const stopPlayer = useCallback(() => {
		if (element instanceof HTMLVideoElement) {
			setState('pause');
			void element.pause();
			element.currentTime = 0;
		}
	}, [setState, element]);

	const pausePlayer = useCallback(() => {
		if (element instanceof HTMLVideoElement) {
			setState('pause');
			void element.pause();
		}
	}, [setState, element]);

	const playPlayer = useCallback(() => {
		if (element instanceof HTMLVideoElement) {
			setState('play');
			void element.play();
		}
	}, [setState, element]);

	const changeTime = useCallback(
		(event: MouseEvent<HTMLDivElement>) => {
			if (element instanceof HTMLVideoElement && wrapRef.current !== null && pointerRef.current !== null) {
				setState('play');

				const newTime = Math.trunc(
					((event.clientX - wrapRef.current.getBoundingClientRect().left + pointerRef.current.offsetWidth * 0.5) /
						wrapRef.current.offsetWidth) *
						element.duration,
				);
				if (typeof newTime !== 'number') {
					console.error(`incorrect currentTime format: ${(newTime as string).toString()}`);
				} else {
					element.currentTime = newTime;
				}

				void element.play();
			}
		},
		[element, setState],
	);

	useEffect(() => {
		if (element instanceof HTMLVideoElement) {
			element.style.display = 'none';
			document.body.appendChild(element);

			const ended = (): void => {
				stopPlayer();
			};
			element.addEventListener('ended', ended);

			const timeupdated = (): void => {
				setCurrentTime(element.currentTime);
			};
			element.addEventListener('timeupdate', timeupdated);

			return () => {
				document.body.removeChild(element);
				element.removeEventListener('ended', ended);
				element.removeEventListener('timeupdate', timeupdated);
			};
		}
	}, [setState, stopPlayer, element]);

	const prevSrc = usePrevious(src);
	useEffect(() => {
		if (element instanceof HTMLVideoElement && prevSrc === src) {
			if (state === 'play') {
				void element.play();
			} else if (state === 'pause') {
				void element.pause();
			}
		}
	}, [prevSrc, src, state, element]);

	const close = useCallback(() => {
		play(null);
	}, [play]);

	return (
		<Container>
			<Button color="simple" onClick={state === 'play' ? pausePlayer : playPlayer}>
				{state === 'play' ? 'pause' : 'play'}
			</Button>
			<LineContainer>
				<LineWrap ref={wrapRef} onClick={changeTime}>
					<Line />
					<Pointer
						ref={pointerRef}
						left={element instanceof HTMLVideoElement ? ((currentTime / element.duration) * 100).toString() + '%' : 0}
					/>
				</LineWrap>
			</LineContainer>
			<Close onClick={close}>x</Close>
		</Container>
	);
};

export default Player;
