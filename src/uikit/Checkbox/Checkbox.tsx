import { useLazyLoad } from 'Hooks/lazyload.hook';
import styled from 'styled-components';
import React from 'react';

const MARK_SRC = 'Search/%D0%93%D0%B0%D0%BB%D0%BA%D0%B0_tvr2uf';

interface Props {
	value: boolean;
	onClick?: () => void;
	className?: string;
}

const Container = styled.div`
	cursor: pointer;
	display: flex;
	align-items: center;
	line-height: 25px;
	user-select: none;
`;

interface BoxProps {
	checked: boolean;
	src: string;
}

export const Box = styled.div<BoxProps>`
	display: flex;
	align-items: center;
	place-content: center;
	position: relative;
	border: 1px solid ${({ theme }) => theme.button.background.primary};
	min-width: 12px;
	height: 12px;

	&:after {
		content: '';
		display: ${({ checked }) => (checked ? 'block' : 'none')};
		background: url(${({ src }) => src});
		position: absolute;
		background-size: cover;
		margin: 0 0 1px 10px;
		width: 100%;
		height: 100%;
		z-index: 1;
	}
`;

const Checkbox: React.FC<Props> = ({ value, className, onClick }) => {
	const { src } = useLazyLoad({
		src: MARK_SRC,
	});

	return (
		<Container className={className} onClick={onClick}>
			<Box src={src} checked={value} />
		</Container>
	);
};

export default styled(Checkbox)``;
