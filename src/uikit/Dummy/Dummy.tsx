import React, { useContext } from 'react';
import styled, { keyframes } from 'styled-components';
import { loaderContext } from 'Contexts/loader.context';

interface Props {
	loading: string | boolean;
	transparent?: boolean;
	className?: string;
	children?: React.ReactNode;
	getRef?: React.MutableRefObject<HTMLDivElement | null>;
}

const Container = styled.div`
	position: relative;
`;

const LeftToRight = keyframes`
	from {
		left: -200%;
	}
	
	to {
		left: 300%;
	}
`;

const Plate = styled.div`
	position: absolute;
	overflow: hidden;
	left: 0;
	top: 0;
	width: 100%;
	height: 100%;
	z-index: 22;

	&.regular {
		background-color: gray;

		&:after {
			content: '';
			position: absolute;
			left: -100%;
			top: -50%;
			width: 20px;
			height: 200%;
			transform: rotate(15deg);
			background-color: white;
			animation: ${LeftToRight} 2s ease-in-out infinite;
		}
	}

	&.transparent {
		display: flex;
		place-content: center;
		align-items: center;
		background-color: rgba(0, 0, 0, 0.6);
	}
`;

const Dummy: React.FC<Props> = ({ transparent, loading, className, children, getRef }) => {
	const { isLoading } = useContext(loaderContext);

	return (
		<Container className={className} ref={getRef}>
			{(typeof loading === 'string' ? isLoading(loading) !== false : loading) && (
				<Plate className={transparent === true ? 'transparent' : 'regular'} />
			)}
			{children}
		</Container>
	);
};

export default styled(Dummy)``;
