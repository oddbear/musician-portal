import styled from 'styled-components';
import React, { ChangeEvent, MouseEvent, useCallback, useEffect, useState } from 'react';
import { useLazyLoad } from 'Hooks/lazyload.hook';
import { d } from 'Utils/dictionary';
import Scroll from 'UI/Scroll';

const MARK_SRC = 'Search/%D0%93%D0%B0%D0%BB%D0%BA%D0%B0_tvr2uf';
const BACKGROUND_SRC = 'BG/rityqxu3z10uzj8qdgwt';
const SELECT_SRC = 'BG/plda0cjmfev1mcceh8dc';

interface Option {
	value: string | null;
	title: string;
}

interface Props<T> {
	options: Option[];
	value: T | null;
	placeholder?: string;
	multi?: boolean;
	disabled?: boolean;
	overlay?: boolean;
	opened?: boolean;
	className?: string;
	onChange?: (value: T) => void;
	onClose?: () => void;
}

export const Container = styled.div`
	position: relative;
	display: flex;
	flex-direction: column;
	color: black;
	width: 100%;
	font-family: ${({ theme }) => theme.fontFamily.quiet};
	font-size: 0.8rem;
	font-weight: 400;
`;

interface MenuProps {
	isOpen: boolean;
	backgroundSrc: string;
	src?: string;
	overlay: boolean;
}

const Option = styled.div`
	cursor: pointer;
	display: flex;
	background-color: transparent;
	padding: 4px 25px;

	&:hover {
		background-color: #ddd;
	}
`;

const ScrollMenu = styled(Scroll)`
	max-height: 300px;
`;

export const Menu = styled.div<MenuProps>`
	display: ${({ isOpen }) => (isOpen ? 'flex' : 'none')};
	flex-direction: column;
	width: 100%;
	margin-top: 4px;
	background: url(${({ backgroundSrc }) => backgroundSrc}) #eee no-repeat center;
	background-size: 100% 100%;
	border-radius: 4px;
	position: ${({ overlay }) => (overlay ? 'absolute' : 'initial')};
	top: 34px;
	box-shadow: 0 0 5px -3px black;
	z-index: 3;

	&.multi {
		.current {
			&:after {
				content: '';
				display: flex;
				background: url(${({ src }) => src});
				position: absolute;
				margin: 4px 0 0 8px;
				background-size: cover;
				width: 13px;
				height: 14px;
			}
		}
	}

	&.single {
		.current {
			background-color: #aaa;
		}
	}
`;

const CheckboxMark = styled.div`
	position: relative;
	bottom: 3px;
	display: flex;
	align-items: center;
	place-content: center;
	padding: 14px 14px;
	margin-right: 10px;
	cursor: pointer;
	background-color: #bfbfbf;
	border: 1px solid transparent;
	border-radius: 3px;
	outline: none;
`;

const Input = styled.input`
	cursor: inherit;
	border: none;
	padding: 2px 6px;
	flex: 1;
	outline: none;
	margin: 0 15px;
	background: transparent;
`;

interface ContainerProps {
	menuOpened: boolean;
	selectSrc?: string;
}

const CurrentOptionsContainer = styled.label<ContainerProps>`
	cursor: ${({ menuOpened }) => (menuOpened ? 'text' : 'pointer')};
	display: flex;
	flex-flow: row wrap;
	width: 100%;
	padding: 8px 8px;
	box-sizing: border-box;
	background: url(${({ selectSrc }) => selectSrc}) white no-repeat center;
	background-size: 100% 100%;
	border-radius: 4px;
`;

const CurrentOption = styled.div<ContainerProps>`
	display: inline-block;
	margin: 2px -9px 2px 15px;
	padding: 12px 4px 12px 25px;
	border: 1.5px solid transparent;
	border-radius: 3px;
	background-color: ${({ menuOpened }) => (menuOpened ? 'none' : 'rgba(153, 153, 153, 0.25)')};
	text-align: center;
	outline: none;
`;

const Remove = styled.span<ContainerProps>`
	position: relative;
	bottom: 2px;
	padding: ${({ menuOpened }) => (menuOpened ? '2px 2px' : '14px 14px')};
	margin-left: ${({ menuOpened }) => (menuOpened ? '5px' : '10px')};
	cursor: pointer;
	color: rgba(44, 44, 44, 1);
	font-size: 8px;
	font-weight: 700;
	border: 1px solid transparent;

	&:hover {
		padding: ${({ menuOpened }) => (menuOpened ? '2px 2px' : '14px 14px')};
		background-color: #bfbfbf;
		border: 1px solid transparent;
		border-radius: 3px;
		outline: none;
	}
`;

const CurrentOptionTitle = styled.span`
	cursor: default;
	color: rgba(20, 20, 20, 1);
`;

const Buttons = styled.div`
	cursor: pointer;
	display: flex;
	align-items: center;
`;

const ARROW_SIZE = '4px';

const Arrow = styled.div`
	margin-left: 8px;
	border-left: ${ARROW_SIZE} solid transparent;
	border-right: ${ARROW_SIZE} solid transparent;
	border-top: 8px solid #aeaeae;

	&:hover {
		border-top: 8px solid #7b7b7b;
	}
`;

const Cross = styled.div`
	font-size: 11px;
	font-weight: 700;
	color: #aeaeae;

	&:hover {
		color: #7b7b7b;
	}
`;

function Select(props: Props<string> | Props<string[]>): React.ReactElement;
function Select<T extends string | string[]>(props: Props<T>): React.ReactElement {
	const [immediateClick, setImmediateClick] = useState<boolean>(false);
	const [isMenuOpen, setIsMenuOpen] = useState<boolean>(props.opened ?? false);
	const [inputValue, setInputValue] = useState<string>('');
	const [isFiltering, setIsFiltering] = useState<boolean>(false);

	const openMenu = useCallback(() => {
		setIsMenuOpen(true);
	}, []);

	let currentOption: null | Option | Option[];
	if (props.multi === true) {
		currentOption =
			props.value === null
				? []
				: (props.value as string[])
						.filter((value) => value !== null && props.options.some((option) => option.value === value))
						.map((value) => props.options.find((option) => option.value === value)!);
	} else {
		currentOption = props.options.find(({ value }) => props.value === value) ?? null;
	}

	const onChange = useCallback(
		(response: Option) => () => {
			if (props.multi === true) {
				props.onChange?.(
					([...(currentOption as Option[]), response] as Option[])
						.filter(({ value }) => value !== null)
						.map(({ value }) => value) as T,
				);
				setInputValue('');
			} else {
				props.onChange?.(response.value as T);
				setInputValue(response.value === null ? '' : response.title);
				setTimeout(() => {
					setIsMenuOpen(false);
				}, 0);
			}
			setIsFiltering(false);
		},
		[props.multi, props.onChange, currentOption],
	);

	const stopPropagation = useCallback(() => {
		setImmediateClick(true);
	}, []);

	const changeInputValue = useCallback((event: ChangeEvent<HTMLInputElement>) => {
		const value = event.target.value;
		setIsFiltering(true);
		setInputValue(value);
	}, []);

	const removeOption = useCallback(
		(value: string) => () => {
			props.onChange?.((props.value as string[]).filter((optionValue) => value !== optionValue) as T);
		},
		[props.value, props.onChange],
	);

	const setDefaultInputValue = useCallback(() => {
		if (props.multi !== true) {
			if (props.value === null) {
				setInputValue('');
			} else {
				setInputValue(
					props.options.find((option) => option.value !== null && option.value === (props.value as string))?.title ??
						'',
				);
			}
		}
	}, [props.multi, props.options, props.value]);

	const removeAll = useCallback(
		(event: MouseEvent<HTMLDivElement>) => {
			event.stopPropagation();

			if (props.multi === true) {
				props.onChange?.(([] as unknown) as T);
			}
		},
		[props.onChange, props.multi],
	);

	useEffect(() => {
		const onMouseUp = (): void => {
			if (!immediateClick) {
				setIsMenuOpen(false);
				setDefaultInputValue();
				props.onClose?.();
			}
			setImmediateClick(false);
		};

		window.addEventListener('mouseup', onMouseUp);
		return () => window.removeEventListener('mouseup', onMouseUp);
	}, [props.onClose, immediateClick, setDefaultInputValue]);

	useEffect(() => {
		setDefaultInputValue();
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const options: Option[] = isFiltering
		? props.options.filter(({ title }) => title.toLowerCase().includes(inputValue.toLowerCase()))
		: [...props.options];

	if (props.placeholder !== undefined && props.multi !== true) {
		options.unshift({
			value: null,
			title: props.placeholder,
		});
	}

	const { src } = useLazyLoad({
		src: MARK_SRC,
	});

	const { src: backgroundSrc } = useLazyLoad({
		src: BACKGROUND_SRC,
	});

	const { src: selectSrc } = useLazyLoad({
		src: SELECT_SRC,
	});

	return (
		<Container onMouseDown={stopPropagation} className={props.className}>
			<CurrentOptionsContainer menuOpened={isMenuOpen} selectSrc={selectSrc}>
				{props.multi === true &&
					(currentOption as Option[]).map(({ value, title }) => (
						<CurrentOption menuOpened={isMenuOpen} key={value!}>
							<CurrentOptionTitle>{title}</CurrentOptionTitle>
							<Remove menuOpened={isMenuOpen} onClick={removeOption(value!)}>
								<span>&#10006;</span>
							</Remove>
						</CurrentOption>
					))}
				<Input
					name="select-input"
					placeholder={props.placeholder}
					value={inputValue}
					onChange={changeInputValue}
					onClick={openMenu}
				/>
				<Buttons>
					{props.multi === true && (
						<Cross onClick={removeAll}>
							<span>&#10006;</span>
						</Cross>
					)}
					<Arrow />
				</Buttons>
			</CurrentOptionsContainer>
			<Menu
				overlay={props.overlay ?? false}
				isOpen={isMenuOpen}
				className={props.multi === true ? 'multi' : 'single'}
				backgroundSrc={backgroundSrc}
				src={src}
			>
				<ScrollMenu>
					{options.length > 0 ? (
						options.map((option) => {
							const isCurrent =
								(!(currentOption instanceof Array) && currentOption !== null && currentOption.value === option.value) ||
								(currentOption instanceof Array &&
									option.value !== null &&
									(props.value as string[]).includes(option.value));
							return (
								<Option
									key={option.value ?? 'default'}
									className={isCurrent ? 'current' : undefined}
									onClick={isCurrent ? undefined : onChange(option)}
								>
									{props.multi === true && <CheckboxMark />}
									{option.title}
								</Option>
							);
						})
					) : (
						<Option>{d('select.noOptions')}</Option>
					)}
				</ScrollMenu>
			</Menu>
		</Container>
	);
}

Select.defaultProps = {
	onChange: () => {
		return;
	},
};

export default Select;
