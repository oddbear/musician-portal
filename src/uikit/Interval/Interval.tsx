import FullsizeBackground from 'Components/FullsizeBackground';
import styled from 'styled-components';
import { useLazyLoad } from 'Hooks/lazyload.hook';
import React, { MouseEvent as MouseEventReact, RefObject, useCallback, useEffect, useRef, useState } from 'react';

export const TESTS_WRAP_CLASSNAME = 'tests-wrap';
const BACKGROUND_SRC = 'Search/experience_bg_h1ahiw';
const SLIDER_SRC = 'Search/experience_deanfy';
const LINE_SRC = 'musician-portal/whiteline';

interface Props {
	title: string;
	part: number;
	partsCount: number;
	onChange(percentage: number): void;
}

interface ContainerProps {
	src: string;
}

const Container = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
`;

const SliderContainer = styled.div<ContainerProps>`
	position: relative;
	width: 182px;
	height: 48px;
	margin: 17px 81px;
	box-sizing: border-box;
	background: url(${({ src }) => src}) no-repeat;
	background-size: 100% 100%;
`;

export const Wrap = styled.div`
	cursor: pointer;
	position: relative;
	height: 100%;
	margin-left: -35px;
`;

interface SliderProps {
	left: string;
}

export const Slider = styled(FullsizeBackground).attrs<SliderProps>(({ left }) => ({
	style: {
		left,
	},
}))<SliderProps>`
	position: absolute;
	height: 100%;
	top: 0;
	user-select: none;
`;

const Title = styled.div`
	font-size: 0.8rem;
	font-weight: 300;
	text-align: center;
	font-family: ${({ theme }) => theme.fontFamily.quiet};
`;

const Lines = styled.div`
	display: flex;
	place-content: space-between;
	align-items: center;
	height: 30px;
	margin: 10px 100px 0 100px;
`;

interface LineProps {
	isCenter: boolean;
}

export const Line = styled(FullsizeBackground)<LineProps>`
	height: ${({ isCenter }) => (isCenter ? 100 : 70)}%;
`;

const Interval: React.FC<Props> = (props) => {
	const { src } = useLazyLoad({
		src: BACKGROUND_SRC,
	});

	const containerRef = useRef<HTMLDivElement>(null);
	const [sliderRef, setSliderRef] = useState<RefObject<HTMLDivElement>>({ current: null });
	const [isPressed, setPressed] = useState<boolean>(false);
	const [startMousePosition, setStartMousePosition] = useState<number>(0);

	const pressedOff = useCallback(() => {
		setPressed(false);
	}, [setPressed]);

	const pressedOn = useCallback(
		(e: MouseEventReact<HTMLDivElement>) => {
			e.stopPropagation();

			setStartMousePosition(
				(sliderRef.current !== null && e.pageX - sliderRef.current.getBoundingClientRect().left) || 0,
			);
			setPressed(true);

			let mouseup: () => void;
			window.addEventListener(
				'mouseup',
				(mouseup = () => {
					window.removeEventListener('mouseup', mouseup);
					pressedOff();
				}),
			);
		},
		[pressedOff, sliderRef],
	);

	useEffect(() => {
		props.onChange(0);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	useEffect(() => {
		const onMouseMove = (event: MouseEvent): void => {
			if (isPressed) {
				const ratio =
					(event.pageX -
						startMousePosition -
						(containerRef.current !== null ? containerRef.current.getBoundingClientRect().left : 0)) /
					((containerRef.current !== null ? containerRef.current.offsetWidth : 0) -
						(sliderRef.current !== null ? sliderRef.current.offsetWidth : 0));

				props.onChange(Math.max(0, Math.min(1, ratio)));
			}
		};

		window.addEventListener('mousemove', onMouseMove);
		return () => window.removeEventListener('mousemove', onMouseMove);
	}, [props, startMousePosition, isPressed, sliderRef]);

	const changePosition = useCallback(
		(event: MouseEventReact<HTMLDivElement>) => {
			props.onChange(
				(event.pageX - (containerRef.current !== null ? containerRef.current.getBoundingClientRect().left : 0)) /
					((containerRef.current !== null ? containerRef.current.offsetWidth : 0) -
						(sliderRef.current !== null ? sliderRef.current.offsetWidth : 0)),
			);
		},
		[props, sliderRef],
	);

	return (
		<Container>
			<Title>{props.title}</Title>
			<SliderContainer src={src}>
				<Wrap className={TESTS_WRAP_CLASSNAME} ref={containerRef} onMouseDown={changePosition}>
					<Slider
						getRef={setSliderRef}
						left={
							(
								(Math.min(props.part, props.partsCount) / props.partsCount) *
								((containerRef.current !== null ? containerRef.current.offsetWidth : 0) -
									(sliderRef.current !== null ? sliderRef.current.offsetWidth : 0))
							).toString() + 'px'
						}
						src={SLIDER_SRC}
						ratio={0.4057971014492754}
						fitHeight={true}
						onMouseDown={pressedOn}
					/>
				</Wrap>
			</SliderContainer>
			<Lines>
				{new Array(props.partsCount - (1 - (props.partsCount % 2))).fill(0).map((_, index) => (
					<Line
						key={index}
						src={LINE_SRC}
						ratio={6.25}
						fitHeight={true}
						isCenter={index === Math.floor(props.partsCount / 2) - 1}
					/>
				))}
			</Lines>
		</Container>
	);
};

export default Interval;
