import { act } from 'react-dom/test-utils';
import { mount } from 'enzyme';
import React, { useState } from 'react';
import Interval, { Line, Slider, TESTS_WRAP_CLASSNAME, Wrap } from './Interval';

describe('UI Interval tests', () => {
	const spy: (value: number) => void = jest.fn();
	const wrapOffsetWidth = 100;
	const partsCount = 12;

	Object.defineProperties(window.HTMLElement.prototype, {
		offsetWidth: {
			get() {
				// eslint-disable-next-line @typescript-eslint/no-unsafe-call,@typescript-eslint/no-unsafe-member-access,@typescript-eslint/strict-boolean-expressions
				return this.className.includes(TESTS_WRAP_CLASSNAME) ? wrapOffsetWidth : 0;
			},
		},
	});

	const Wrapper: React.FC = () => {
		const [part, setPart] = useState<number>(0);

		const onChange = (percentage: number): void => {
			setPart(Math.trunc(percentage * partsCount));
			spy(percentage);
		};

		return <Interval title="foo bar" part={part} partsCount={partsCount} onChange={onChange} />;
	};

	it('calls onChange on component mount', () => {
		mount(<Wrapper />);
		expect(spy).toBeCalledWith(0);
	});

	it('changes values on line-wrapper (not a slider) mousedown successfully', () => {
		const pageX = 10;

		const intervalWrapper = mount(<Wrapper />);
		intervalWrapper.find(Wrap).simulate('mousedown', {
			pageX,
		});

		expect(spy).toBeCalledWith(pageX / wrapOffsetWidth);
	});

	it('changes values when slider have been dragged', () => {
		const pageX = 10;

		const intervalWrapper = mount(<Wrapper />);

		intervalWrapper.find(Slider).simulate('mousedown', {
			pageX: 0,
		});

		const event = new MouseEvent('mousemove', {
			view: window,
		});
		Object.defineProperty(event, 'pageX', {
			writable: false,
			value: pageX,
		});
		act(() => {
			window.dispatchEvent(event);
		});

		expect(spy).toBeCalledWith(pageX / wrapOffsetWidth);
	});

	it('creates odd count of Lines', () => {
		const intervalWrapper = mount(<Wrapper />);
		expect(intervalWrapper.find(Line)).toHaveLength(partsCount - (1 - (partsCount % 2)));
	});
});
