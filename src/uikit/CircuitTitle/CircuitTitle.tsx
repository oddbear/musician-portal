import FullsizeBackground from 'Components/FullsizeBackground';
import React from 'react';
import styled from 'styled-components';

const CIRCUIT_LIGHT_SRC = 'BG/BG_footer_line_huwsvn';
const CIRCUIT_DARK_SRC = 'Band_Profile/line_name_5528_svdybq';

interface Props {
	color?: string;
	title: string;
	className?: string;
}

const Container = styled(FullsizeBackground)`
	display: flex;
	place-content: flex-end;
`;

interface TitleProps {
	color?: string;
}

export const Title = styled.span<TitleProps>`
	cursor: pointer;
	font-size: 26.5px;
	font-weight: 400;
	align-items: center;
	color: ${({ theme, color }) => (color === 'dark' ? theme.color.primary : theme.color.fourth)};
	font-family: ${({ theme }) => theme.fontFamily.special};
	transition: color ${({ theme }) => theme.other.defaultTransitionDuration};

	&:hover {
		color: ${({ color }) => (color === 'dark' ? 'gray' : '#912121')};
	}
`;

const CircuitTitle: React.FC<Props> = ({ color, title, className }) => {
	return (
		<Container
			className={className}
			src={color === 'dark' ? CIRCUIT_DARK_SRC : CIRCUIT_LIGHT_SRC}
			ratio={color === 'dark' ? 0.1724137931034483 : 0.27074235807860264}
			fitHeight={true}
		>
			<Title color={color}>{title.toUpperCase()}</Title>
		</Container>
	);
};

export default CircuitTitle;
