import * as React from 'react';
import CircuitTitle from './CircuitTitle';
import { storiesOf } from '@storybook/react';
import styled from 'styled-components';

const Container = styled.div`
	width: 200px;
	font-weight: bolder;
	font-size: 2rem;
`;

storiesOf('CircuitTitle', module).add('default', () => (
	<Container>
		<CircuitTitle title="F.A.Q." />
	</Container>
));
