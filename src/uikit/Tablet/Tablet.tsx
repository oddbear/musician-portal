import React from 'react';
import styled from 'styled-components';

interface Props {
	className?: string;
	children: React.ReactNode;
}

const Container = styled.div`
	display: flex;
	position: relative;
	width: 100%;
	padding: 37px 11px;
	background-color: white;
	border-radius: 4px;
	color: #333;
	box-sizing: border-box;
`;

const Tablet: React.FC<Props> = ({ className, children }) => {
	return <Container className={className}>{children}</Container>;
};

export default styled(Tablet)``;
