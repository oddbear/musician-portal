import { storiesOf } from '@storybook/react';
import * as React from 'react';
import LED from './LED';

storiesOf('LED', module)
	.add('default', () => <LED />)
	.add('light', () => <LED on={true} />);
