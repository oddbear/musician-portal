import FullsizeBackground from 'Components/FullsizeBackground';
import React from 'react';
import styled from 'styled-components';
import { useLazyLoad } from 'Hooks/lazyload.hook';

const LED_SRC = 'BG/BS__ligts_nt27ui';
const LIGHT_SRC = 'BG/Led_Genres_hb2u2f';

interface Props {
	on?: boolean;
	className?: string;
}

interface ContainerProps {
	layersrc: string;
	on: boolean;
	className?: string;
}

const Container = styled(FullsizeBackground)<ContainerProps>`
	position: relative;
	width: 32px;
	height: 31px;

	&:after {
		content: '';
		background-image: url(${({ layersrc }) => layersrc});
		background-size: 100% 100%;
		width: 42px;
		height: 44px;
		position: absolute;
		left: -5px;
		top: -12px;
		opacity: ${({ on = false }) => (on ? 1 : 0)};
		transition: opacity 0.15s;
	}
`;

const LED: React.FC<Props> = (props) => {
	const { src } = useLazyLoad({
		src: LIGHT_SRC,
	});

	return <Container className={props.className} src={LED_SRC} ratio={0.96875} layersrc={src} on={props.on!} />;
};

export default styled(LED)``;
