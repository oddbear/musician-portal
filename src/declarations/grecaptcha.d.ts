declare const grecaptcha = {
	ready: (callback: () => void) => {
		return;
	},
	execute: (siteKey: string, options: object) => Promise.resolve('string'),
};
