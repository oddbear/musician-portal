type rewrite<T, TKey extends keyof T, TKeyType> = Omit<T, TKey> & Record<TKey, TKeyType>;
