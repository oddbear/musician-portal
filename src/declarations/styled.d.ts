import 'styled-components';
import { DefaultTheme as theme } from 'Utils/styles';

declare module 'styled-components' {
	// eslint-disable-next-line @typescript-eslint/no-empty-interface
	export interface DefaultTheme extends theme {}
}
