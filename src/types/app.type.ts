import { ResponseUser } from 'Types/response.type';

export interface Location {
	pathname: string;
	query: {
		[key: string]: string | null;
	};
}

export interface Context {
	lang: string;
	page: string;
	location: Location;
	user: ResponseUser | null;
	loading: boolean;
}
