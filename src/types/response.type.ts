import { InviteRelations, InviteType, NotificationType, PlaylistPublicity, PositionVariant, Rarity } from 'Utils/enum';

export type GraphQLWrap<TName extends string, TData> = Record<TName, TData>;

export interface AvatarInput {
	file?: File | Blob;
	defaultAvatarId?: string;
}

export interface PostCreateInput {
	alias: string;
	text?: string;
	images: Array<File | Blob>;
	video?: string;
}

export interface CommentCreateInput {
	postId: string;
	text?: string;
	images: Array<File | Blob>;
	video?: string;
	visibleData: string[];
}

// back + front

export interface ResponseProfileStatistics {
	bandsHired: number;
	bandsMember: number;
}

export interface ResponseProfile {
	_id: string;
	login: string;
	avatar: ResponseLink;
	email: string | null;
	name: string;
	description: string | null;
	experience: number;
	country: ResponseCountry | null;
	city: ResponseCity | null;
	positions: ProfilePosition[];
	genres: ResponseGenre[];
	audios: ResponseLink[];
	registeredWithSocial: boolean;
	settings: {
		privacy: UserPrivacySettings;
	};
	memberInBands: ResponseBand[];
	relations: ProfileRelations;
	statistics: ResponseProfileStatistics;
	dignities: ResponseDignity[];
	blacklist?: string[];
}

export interface ResponsePosition {
	_id: string;
	value: string;
	group: string | null;
}

export interface ResponseGenre {
	_id: string;
	value: string;
	group: string | null;
}

export interface ResponseCountry {
	_id: string;
	value: string;
}

export interface ResponseCity {
	_id: string;
	value: string;
}

export interface ProfilePosition {
	_id?: string;
	position: ResponsePosition;
}

export interface ProfileRelations {
	[key: string]: Relations[];
}

export interface BandPosition {
	_id?: string;
	position: ResponsePosition;
	status: PositionVariant;
	profile: ResponseProfile | null;
}

export interface Relations {
	position: ResponsePosition;
	status: InviteRelations;
}

export interface ResponseBandStatistics {
	membersJoined: number;
	membersInBand: number;
}

export interface ResponseReaction {
	emotion: ResponseEmotion;
	authors: Response<ResponseProfile>;
}

export interface ResponseMention {
	_id: string;
	post: ResponsePost;
	comment: ResponseComment | null;
	profile: ResponseProfile;
	checkTime: number | null;
	isHidden: boolean;
	createdAt: number;
}

export interface ResponsePost {
	_id: string;
	author: ResponseBand;
	text: string | null;
	images: ResponseLink[];
	video: string | null;
	comments: Response<ResponseComment>;
	reactions: ResponseReaction[];
	mentions: ResponseMention[];
	createdAt: number;
}

export interface ResponseComment {
	_id: string;
	author: ResponseProfile;
	text: string | null;
	images: ResponseLink[];
	video: string | null;
	reactions: ResponseReaction[];
	mentions: ResponseMention[];
	createdAt: number;
}

export interface BandPrivacySettings {
	showStatistics: boolean;
}

export interface BandSettings {
	privacy: BandPrivacySettings;
}

export interface ResponseBand {
	_id: string;
	alias: string;
	avatar: ResponseLink;
	name: string;
	description: string | null;
	experience: number;
	country: ResponseCountry | null;
	city: ResponseCity | null;
	positions: BandPosition[];
	genres: ResponseGenre[];
	audios: ResponseLink[];
	members: ResponseProfile[];
	relations: Relations[];
	statistics: ResponseBandStatistics;
	settings: BandSettings;
	dignities: ResponseDignity[];
	subscribersCount: number;
	isSubscribed: boolean;
	repetitionBase: boolean;
}

export interface ResponseMessage {
	_id: string;
	from: ResponseProfile;
	text: string;
	isChecked: boolean;
	createdAt: number;
}

export interface ResponseConversation {
	_id: string;
	profileParticipants: ResponseProfile[];
	bandParticipants: ResponseBand[];
	messages: Response<ResponseMessage>;
	createdAt: number;
}

export interface ResponseNotification {
	_id: string;
	type: NotificationType;
	castOn: ResponseProfile | null;
	castBy: ResponseProfile | null;
	castIn: ResponseBand | null;
	isChecked: boolean;
	createdAt: number;
	json: {
		fromProfile?: boolean;
		position?: ResponsePosition | null;
		mention?: {
			comment: ResponseComment | null;
			post: ResponsePost;
		};
	};
}

export interface Response<T> {
	totalLength: number;
	data: T[];
}

export interface ProfileVerification {
	verified: boolean;
	identifier: string;
	lastSendTime: Date;
}

export interface UserNotificationsSettings {
	soundOnNewMessage: boolean;
	soundOnNewNotification: boolean;
	notifyOnInvite: boolean;
	notifyOnLeave: boolean;
	notifyOnDismiss: boolean;
	notifyOnDecline: boolean;
	notifyOnHire: boolean;
	notifyOnJoin: boolean;
	notifyOnMention: boolean;
}

export interface UserPrivacySettings {
	conversationsEnabled: boolean;
	showStatistics: boolean;
}

export interface UserConversationSettings {
	_id: string;
	muted: boolean;
}

export interface UserConversationSettingsInput {
	_id: string;
	muted?: boolean;
}

export interface UserSettings {
	notifications: UserNotificationsSettings;
	conversations: UserConversationSettings[];
	privacy: UserPrivacySettings;
}

export interface ResponseUser {
	_id: string;
	login: string;
	avatar: ResponseLink;
	name: string;
	email: string | null;
	verification: ProfileVerification;
	deactivated: boolean;
	conversations: Response<ResponseConversation>;
	administeredBands: ResponseBand[];
	memberInBands: ResponseBand[];
	blacklist: string[];
	extendPack: number;
	rankPack: number;
	settings: UserSettings;
	dignities: ResponseDignity[];
	playlists: ResponsePlaylist[];
	firebaseToken: {
		[key: string]: string;
	};
}

export interface ResponseLink {
	_id: string;
	src: string;
	description: string;
	json: {
		type: 'audio' | 'image';
	};
}

export interface UserCreateInput {
	login: string;
	name: string;
	email: string | null;
	password: string;
	avatar?: ResponseLink;
}

export interface ProfileUpdateInput {
	name?: string;
	password?: string;
	email?: string;
	avatar?: AvatarInput;
	description?: string | null;
	experience?: number;
	country?: string | null;
	city?: string | null;
	positions?: string[];
	genres?: string[];
}

export interface PositionInput {
	value: string;
	status: PositionVariant;
	profileLogin?: string | null;
}

export interface BandUpdateInput {
	alias?: string;
	name?: string;
	avatar?: AvatarInput;
	description?: string | null;
	experience?: number;
	country?: string | null;
	city?: string | null;
	positions?: PositionInput[];
	genres?: string[];
	members?: string[];
}

export interface InviteInput {
	position: string;
	to: string;
	from?: string;
	type: InviteType;
}

export interface PlaylistUpdateInput {
	name?: string;
	avatar?: AvatarInput;
	publicity?: PlaylistPublicity;
}

export interface ResponseEmotion {
	_id: string;
	src: string;
	alias: string;
	title: string;
	dignity: string;
	ratingValue: number;
}

export interface ResponsePlaylist {
	_id: string;
	name: string;
	avatar: ResponseLink;
	author: ResponseProfile;
	tracks: ResponseLink[];
	reactions: ResponseReaction[];
	positions: ResponsePosition[];
	genres: ResponseGenre[];
	publicity: number;
	isFollowed: boolean;
	createdAt: number;
}

export interface ResponseDignity {
	name: string;
	rarity: Rarity;
}
