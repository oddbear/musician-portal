import { ReactElement } from 'react';

export interface CloudinaryResponseElement {
	src: string;
	width: number;
	height: number;
	element: null | ((props: object) => ReactElement) | HTMLVideoElement | HTMLAudioElement;
}

export type TagTypes = 'video' | 'img' | 'audio';

export interface CloudinaryOptions {
	src: string;
	tag?: TagTypes;
	compressedSize?: number;
	responsive?: {
		mobile?: number;
		desktop?: number;
	};
}
