export type TypeFilter = 'band' | 'repetitionBase' | 'profile';

export interface Positions {
	[key: string]: string | boolean;
}

export interface Genres {
	[key: string]: boolean;
}

export interface Filters {
	type: TypeFilter;
	positions: Positions;
	genres: Genres;
	country: string | null;
	city: string | null;
	experience: number;
}

export interface PlaylistFilters {
	positions: Positions;
	genres: Genres;
	country: string | null;
	city: string | null;
}

export interface PositionFilter {
	value: string;
	group?: string;
}

export interface GenreFilter {
	value: string;
	group?: string;
}

export interface CountryFilter {
	value: string;
}

export interface CityFilter {
	value: string;
	country: CountryFilter;
}

export interface FilterVariant {
	value: string;
	title: string;
}
export type FilterTypes = 'radio' | 'checkbox';
export type FilterValue = string | boolean;
export type FilterValues = string | { [key: string]: boolean | string } | { [key: string]: boolean };

export interface OptionsFilters {
	search?: string;
	positions?: object;
	genres?: object;
	country?: string;
	city?: string;
	experience?: number;
}
