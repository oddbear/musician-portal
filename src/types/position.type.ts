export interface Position {
	left: number;
	top: number;
	right: number;
	bottom: number;
}

export interface Offset {
	width: number;
	height: number;
}
