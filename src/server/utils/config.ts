import dotenv from 'dotenv';
import convict from 'convict';

dotenv.config();

export const config = convict({
	env: {
		env: 'NODE_ENV',
		format: ['production', 'development', 'test'],
		default: 'development',
	},
	nodePort: {
		env: 'NODE_PORT',
		format: Number,
		default: 4210,
	},
}).getProperties();
