import { Request } from 'express';
import fs from 'fs';
import path from 'path';

const LOCALES_PATH: string = path.resolve(__dirname, '../../locales');
const DEFAULT_LANGUAGE = 'ru';
const ACCEPTS_LANGUAGES_MAXIMUM_ITERATIONS = 10;

const getLocalePath = (lang: string): string => `${LOCALES_PATH}/${lang.toLowerCase()}.json`;

interface Dictionary {
	lang?: string;
	locale?: string;
}

const getDictionary = (
	req: Request,
	query: {
		lang?: string;
	},
): Dictionary => {
	let { lang } = query;
	const acceptedLanguages = req.acceptsLanguages();

	let locale: string | undefined;
	let iterations = 0;

	do {
		if (++iterations > ACCEPTS_LANGUAGES_MAXIMUM_ITERATIONS) {
			locale = '{}';
			lang = undefined;
			break;
		}

		lang = lang ?? (acceptedLanguages.splice(0, 1)[0] as string | undefined) ?? DEFAULT_LANGUAGE;

		try {
			locale = fs.readFileSync(getLocalePath(lang)).toString();
		} catch (err) {
			lang = undefined;
		}
	} while (locale === undefined);

	if (typeof lang === 'string') {
		lang = lang.toLowerCase();
	}

	return {
		lang,
		locale,
	};
};

export { DEFAULT_LANGUAGE, ACCEPTS_LANGUAGES_MAXIMUM_ITERATIONS, getLocalePath, getDictionary };
