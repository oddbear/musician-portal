import * as fs from 'fs';
import { ACCEPTS_LANGUAGES_MAXIMUM_ITERATIONS, DEFAULT_LANGUAGE, getDictionary, getLocalePath } from '../dictionary';

const testLang = 'ru';
const req: {
	languages: string[];
	acceptsLanguages(): string[];
} = {
	languages: [],
	acceptsLanguages() {
		return req.languages;
	},
};

describe('Server index tests', () => {
	describe('getDictionary function', () => {
		describe(`returns locale for "${testLang}" lang`, () => {
			it('with no acceptsLanguages in request', () => {
				const locale = fs.readFileSync(getLocalePath(testLang)).toString();

				expect(
					// @ts-ignore
					getDictionary(req, {
						lang: testLang,
					}),
				).toMatchObject({
					lang: testLang,
					locale,
				});
			});

			it('with acceptsLanguages in request', () => {
				const locale = fs.readFileSync(getLocalePath(testLang)).toString();

				expect(
					// @ts-ignore
					getDictionary(req, {
						lang: testLang,
					}),
				).toMatchObject({
					lang: testLang,
					locale,
				});
			});
		});

		describe('returns locale for no specified lang', () => {
			it('returns default locale with no acceptsLanguages in request', () => {
				const locale = fs.readFileSync(getLocalePath(DEFAULT_LANGUAGE)).toString();

				// @ts-ignore
				expect(getDictionary(req, {})).toMatchObject({
					lang: DEFAULT_LANGUAGE,
					locale,
				});
			});

			it(`returns locale for "${testLang}" with acceptsLanguages in request`, () => {
				const locale = fs.readFileSync(getLocalePath(testLang)).toString();

				req.languages = ['ru'];

				// @ts-ignore
				expect(getDictionary(req, {})).toMatchObject({
					lang: testLang,
					locale,
				});

				req.languages = [];
			});
		});

		describe('returns default value for undefined locales', () => {
			it('with specified language in query', () => {
				const locale = fs.readFileSync(getLocalePath(DEFAULT_LANGUAGE)).toString();

				expect(
					// @ts-ignore
					getDictionary(req, {
						lang: 'definitely undefined language',
					}),
				).toMatchObject({
					lang: DEFAULT_LANGUAGE,
					locale,
				});
			});

			it('with acceptsLanguages in request', () => {
				const locale = fs.readFileSync(getLocalePath(DEFAULT_LANGUAGE)).toString();

				req.languages = ['definitely undefined language'];

				expect(
					// @ts-ignore
					getDictionary(req, {
						lang: 'definitely undefined language',
					}),
				).toMatchObject({
					lang: DEFAULT_LANGUAGE,
					locale,
				});

				req.languages = [];
			});
		});

		it(`stops to iterate acceptsLanguages after ${ACCEPTS_LANGUAGES_MAXIMUM_ITERATIONS.toString()} tries and return default value`, () => {
			req.languages = new Array(ACCEPTS_LANGUAGES_MAXIMUM_ITERATIONS)
				.fill(0)
				.map(() => 'with acceptsLanguages in request')
				.concat([DEFAULT_LANGUAGE]);

			const spy = jest.fn();
			const splice = Array.prototype.splice;
			// eslint-disable-next-line @typescript-eslint/no-explicit-any
			Array.prototype.splice = (ind: number, count: number): any[] => {
				spy();
				// eslint-disable-next-line @typescript-eslint/no-unsafe-return
				return splice.call(req.languages, ind, count);
			};

			// @ts-ignore
			expect(getDictionary(req, {})).toMatchObject({
				lang: undefined,
				locale: '{}',
			});
			expect(spy).toBeCalledTimes(ACCEPTS_LANGUAGES_MAXIMUM_ITERATIONS);

			req.languages = [];
			Array.prototype.splice = splice;
		});
	});
});
