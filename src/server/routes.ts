import Routes from 'next-routes';
const route = new Routes();

interface Route {
	pattern: string;
	page: string;
}

const addRoutes = (routes: Route[]): void => {
	routes.forEach(({ pattern, page }) => {
		route.add(pattern, page);
	});
};

addRoutes([
	{ pattern: '/', page: 'index' },
	{ pattern: '/search', page: 'search' },
	{ pattern: '/signin', page: 'signin' },
	{ pattern: '/signup', page: 'signup' },
	{ pattern: '/profile', page: 'profile' },
	{ pattern: '/profile/:login', page: 'profile' },
	{ pattern: '/band/:alias', page: 'band' },
	{ pattern: '/conversations', page: 'conversations' },
	{ pattern: '/conversations/:id', page: 'conversations' },
	{ pattern: '/verification/:id', page: 'verification' },
	{ pattern: '/shop', page: 'shop' },
	{ pattern: '/restore', page: 'restore' },
	{ pattern: '/settings', page: 'settings' },
	{ pattern: '/news', page: 'news' },
	{ pattern: '/news/:id', page: 'news' },
	{ pattern: '/playlists', page: 'playlists' },
	{ pattern: '/playlist/:id', page: 'playlist' },
	{ pattern: '/faq', page: 'faq' },
	{ pattern: '/contacts', page: 'contacts' },
	{ pattern: '/404', page: '_error' },
]);

export default route;
