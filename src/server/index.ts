import express, { Request, Response } from 'express';
import LRUCache from 'lru-cache';
import nextServer from 'next';
import path from 'path';
import { ParsedUrlQuery } from 'querystring';
import routes from './routes';
import { getDictionary } from './utils/dictionary';
import { config } from './utils/config';
import url from 'url';
import fs from 'fs';
import { getType } from 'mime';

const isProduction: boolean = config.env === 'production';
const PORT: number = config.nodePort;
const URLS_REQUIRING_AUTH = [/^\/profile$/, /^\/conversations/, /^\/settings/];
const URLS_REQUIRING_UNAUTH = [/^\/signin/, /^\/signup/, /^\/restore/];

const ssrCache = new LRUCache({
	max: 100 * 1024 * 1024,
	length(n: string) {
		return n.length;
	},
	maxAge: 1000 * 60 * 60 * 24 * 30,
});

const app = nextServer({
	dev: !isProduction,
	dir: './build',
});

interface RequestHandler {
	req: Request;
	res: Response;
	route: {
		page: string;
	};
	query: {
		lang?: string;
	};
}

function getCacheKey(req: Request): string {
	const ua = req.header('user-agent');
	const isMobile = typeof ua === 'string' && /mobile/i.test(ua);

	return `${req.path}_${isMobile ? 'mobile' : 'desktop'}`;
}

const nextHandler = app.getRequestHandler();
// @ts-ignore
const renderAndCache = routes.getRequestHandler(app, async ({ req, res, route, query }: RequestHandler) => {
	const { locale, lang } = getDictionary(req, query);

	const key = getCacheKey(req);
	if (ssrCache.has(key) && isProduction) {
		res.setHeader('x-cache', 'HIT');
		res.send(ssrCache.get(key));
		return;
	}

	try {
		const responseQuery: { [key: string]: string } = {};
		if (locale !== undefined) {
			responseQuery.locale = locale;
		}
		if (lang !== undefined) {
			responseQuery.lang = lang;
		}
		const html = await app.renderToHTML(req, res, route.page, { ...query, ...responseQuery });

		if (res.statusCode !== 200) {
			res.send(html);
			return;
		}

		ssrCache.set(key, html!);

		res.setHeader('x-cache', 'MISS');
		res.send(html);
	} catch (err) {
		await app.renderError(err as Error, req, res, req.path, req.query as ParsedUrlQuery);
	}
});

void app.prepare().then(() => {
	const server = express();

	server.use((req, res, next) => {
		const parsedUrl = url.parse(req.url);
		if (
			(req.headers.cookie?.includes('logged') !== true &&
				parsedUrl.pathname !== null &&
				URLS_REQUIRING_AUTH.some((pathname) => pathname.exec(parsedUrl.pathname!))) ||
			(req.headers.cookie?.includes('logged') === true &&
				parsedUrl.pathname !== null &&
				URLS_REQUIRING_UNAUTH.some((pathname) => pathname.exec(parsedUrl.pathname!)))
		) {
			res.redirect('/404');
		} else {
			next();
		}
	});

	server.get('*', (req, res, next) => {
		const rootPath = path.resolve(__dirname, '../../static');
		const pathname = url.parse(req.originalUrl).pathname;
		if (pathname === null) {
			next();
		} else {
			fs.lstat(`${rootPath}${pathname}`, (error, stats) => {
				if (error === null && stats.isFile()) {
					try {
						res.setHeader('Cache-Control', 'public, max-age=31536000, immutable');
						res.setHeader('Content-Type', getType(path.extname(pathname)) ?? 'text/plain');
						res.status(200).sendFile(pathname, { root: rootPath });
					} catch (err) {
						console.log(`path resolve failed for static file '${pathname}'`);
						res.status(404).send();
					}
				} else {
					next();
				}
			});
		}
	});

	// @ts-ignore
	server.get('/_next/*', nextHandler);
	server.get('*', renderAndCache);

	server.listen(PORT, (err: Error) => {
		if (err !== undefined) {
			throw err;
		}
		console.log(`> Ready on http://localhost:${PORT.toString()}`);
	});
});
