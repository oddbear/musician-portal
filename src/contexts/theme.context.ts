import { createContext } from 'react';
import { DefaultTheme } from 'Utils/styles';

interface Context {
	styles?: DefaultTheme;
	setStyle(styleName: string, value: string): void;
}

export const themeContext = createContext<Context>({
	setStyle() {
		return;
	},
});
