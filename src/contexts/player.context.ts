import { createContext } from 'react';

export interface Context {
	current: {
		_id: string | null;
		src: string | null;
	};
	state: string;
	setState: (state: string) => void;
	play: (src: string | null, id?: string | null) => void;
	pause: () => void;
}

export const playerContext = createContext<Context>({
	current: {
		_id: null,
		src: null,
	},
	state: 'stop',
	setState: () => {
		return;
	},
	play: () => {
		return;
	},
	pause: () => {
		return;
	},
});
