import { createContext } from 'react';

export interface Context {
	incrementProgress: (taskName: string, progress?: number) => number;
	decrementProgress: (taskName: string, progress?: number) => number;
	clearProgress: (taskName?: string) => void;
	isLoading: (taskName: string) => number | boolean;
}

export const loaderContext = createContext<Context>({
	incrementProgress: () => {
		return 0;
	},
	decrementProgress: () => {
		return 0;
	},
	clearProgress: () => {
		return;
	},
	isLoading: () => {
		return 0;
	},
});
