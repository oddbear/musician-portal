import { createContext } from 'react';

export interface Context {
	visible: boolean;
	attachedElement: HTMLElement | null;
	setVisible?: (state: boolean) => void;
	setAttachedElement?: (element: HTMLElement | null) => void;
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	setContext?: (context: { [key: string]: any }) => void;
}

export const tooltipContext = createContext<Context>({
	visible: false,
	attachedElement: null,
});
