import { createContext } from 'react';

interface Context {
	setError(
		message: string,
		button?: {
			title: string;
			action?: () => void;
		},
	): void;
}

export const errorContext = createContext<Context>({
	setError() {
		return;
	},
});
