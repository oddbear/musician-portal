import { createContext } from 'react';
import { FilterTypes, FilterValue, FilterValues, FilterVariant } from 'Types/filters.type';

interface Context {
	type: FilterTypes;
	value: FilterValues;
	variants?: FilterVariant[];
	onChange(key: string, filterValue?: FilterValue, switchback?: boolean): void;
}

export const filterContext = createContext<Context>({
	type: 'radio',
	value: '',
	onChange(): void {
		return;
	},
});
