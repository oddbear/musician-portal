import { createContext } from 'react';

export type Callback = (callback: () => boolean) => void;
export interface Context {
	addCallback: Callback;
	removeCallback: Callback;
}

// allows to check inputs on errors by form
// addCallback needs to be used in each input
// removeCallback needs to be used on component update if it uses hooks like useEffect
export const fieldValidatorContext = createContext<Context>({
	addCallback: () => {
		return;
	},
	removeCallback: () => {
		return;
	},
});
