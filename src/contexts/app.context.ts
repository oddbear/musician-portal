import { Context } from 'Types/app.type';
import { createContext } from 'react';

export const appContext = createContext<Context & { setPage?: (name: string) => void }>({
	lang: 'en-us',
	page: 'index',
	location: {
		pathname: '/',
		query: {},
	},
	user: null,
	loading: false,
});
