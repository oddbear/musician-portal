import { MonthName } from 'Utils/enum';
import { d } from 'Utils/dictionary';

const cachedDate = new Map<string, Date>();

export const getCachedDate = (date?: string): Date => {
	const result = cachedDate.get(date ?? '');

	if (result !== undefined) {
		return result;
	}

	let newDate: Date;
	if (date === undefined) {
		newDate = new Date();
	} else {
		newDate = new Date(date);
	}
	cachedDate.set(date ?? '', newDate);

	return newDate;
};

export const isExpired = (dateOrTime: Date | number): boolean => {
	const currentTime = Date.now();

	if (typeof dateOrTime === 'number') {
		return dateOrTime < currentTime;
	} else {
		return dateOrTime.getTime() < currentTime;
	}
};

interface FormattedCache {
	date: Date;
	fullYear?: string;
	monthName?: string;
	month?: string;
	day?: string;
	hours?: string;
	minutes?: string;
	seconds?: string;
}

const formattedCache = new Map<number | Date, FormattedCache>();

export const getFormattedDate = (
	dateArg: number | Date,
): {
	getFullYear: () => string;
	getMonthName: () => string;
	getMonth: () => string;
	getDay: () => string;
	getHours: () => string;
	getMinutes: () => string;
	getSeconds: () => string;
} => {
	let date: Date;

	const tmp = formattedCache.get(dateArg);
	let cache: FormattedCache;
	if (tmp !== undefined) {
		cache = tmp;
		date = cache.date;
	} else {
		if (typeof dateArg === 'number') {
			date = new Date(dateArg);
		} else {
			date = dateArg;
		}

		cache = {
			date,
		};
		formattedCache.set(dateArg, cache);
	}

	return {
		getFullYear: () => {
			if (cache.fullYear === undefined) {
				cache.fullYear = date.getFullYear().toString();
			}
			return cache.fullYear;
		},
		getMonthName: () => {
			if (cache.monthName === undefined) {
				cache.monthName = d(`months.${MonthName[date.getMonth() + 1]}`);
			}
			return cache.monthName;
		},
		getMonth: () => {
			if (cache.month === undefined) {
				let month = date.getMonth().toString();
				if (month.length === 1) {
					month = '0' + month;
				}
				cache.month = month;
			}
			return cache.month;
		},
		getDay: () => {
			if (cache.day === undefined) {
				let day = date.getDate().toString();
				if (day.length === 1) {
					day = '0' + day;
				}
				cache.day = day;
			}
			return cache.day;
		},
		getHours: () => {
			if (cache.hours === undefined) {
				let hours = date.getHours().toString();
				if (hours.length === 1) {
					hours = '0' + hours;
				}
				cache.hours = hours;
			}
			return cache.hours;
		},
		getMinutes: () => {
			if (cache.minutes === undefined) {
				let minutes = date.getMinutes().toString();
				if (minutes.length === 1) {
					minutes = '0' + minutes;
				}
				cache.minutes = minutes;
			}
			return cache.minutes;
		},
		getSeconds: () => {
			if (cache.seconds === undefined) {
				let seconds = date.getSeconds().toString();
				if (seconds.length === 1) {
					seconds = '0' + seconds;
				}
				cache.seconds = seconds;
			}
			return cache.seconds;
		},
	};
};
