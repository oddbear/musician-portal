export const EXPERIENCE_MAXIMUM_OFFSET = 100; // in years
export const POSTS_LIMIT = 10;
export const COMMENTS_LIMIT = 10;
export const POSTS_REFRESH_DELAY = 1000 * 10; // 10 seconds
export const PROFILES_LIMIT = 5;
export const NOTIFICATIONS_LIMIT = 10;
export const PLAYLISTS_LIMIT = 10;
export const CARDS_LIMIT = 5;
export const POST_VISIBLE_ROWS = 12;

export const GA_LOADING_DATA = 'loading data';
export const GA_SUBMIT = 'submit';
export const GA_REPEAT_PASSWORD_FEATURE = 'repeat password feature set';
export const GA_CHANGE_STEP = 'change step';
export const GA_SET_FILTERS = 'set filters';
export const GA_SEARCH = 'search';
export const GA_LOAD_DATA = 'load data';
export const GA_LINK = 'link';
export const GA_CREATE = 'create';
export const GA_PUBLISHED = 'published';
export const GA_ADD_TO_PLAYLIST = 'add track to playlist';
export const GA_DISMISS = 'dismiss';
export const GA_LEAVE = 'leave';

export const GA_CATEGORY_FOOTER = 'footer';
export const GA_CATEGORY_HEADER = 'header';
export const GA_CATEGORY_SEARCH = 'search';
export const GA_CATEGORY_SIGNIN = 'signin';
export const GA_CATEGORY_SIGNUP = 'signup';
export const GA_CATEGORY_PLAYLISTS = 'playlists';
export const GA_CATEGORY_PLAYLIST = 'playlist';
export const GA_CATEGORY_PROFILE = 'profile';
export const GA_CATEGORY_BAND = 'band';

// back + front

export const TIME_TO_DELETE_UNVERIFIED_ACCOUNT = 1000 * 60 * 60 * 24 * 5; // 5 days
export const RESTORE_SEND_DELAY = 1000 * 60;

export const LOGIN_MIN_LENGTH = 3;
export const LOGIN_MAX_LENGTH = 50;
export const PASSWORD_MIN_LENGTH = 8;
export const PASSWORD_MAX_LENGTH = 200;
export const EMAIL_MAX_LENGTH = 200;
export const PROFILE_AUDIO_LIMIT = 2;
export const PROFILE_AUDIO_LIMIT_PREMIUM = 5;
export const PROFILE_GENRE_LIMIT = 3;
export const PROFILE_GENRE_LIMIT_PREMIUM = 6;
export const PROFILE_POSITION_LIMIT = 3;
export const PROFILE_POSITION_LIMIT_PREMIUM = 8;
export const MAX_PLAYLIST_TRACKS_LIMIT = 50;
export const POST_MAX_IMAGES_LENGTH = 8;
export const POST_MAX_MESSAGE_LENGTH = 600;

export const ALIAS_MIN_LENGTH = 1;
export const ALIAS_MAX_LENGTH = 50;
export const BAND_AUDIO_LIMIT = 2;
export const BAND_AUDIO_LIMIT_PREMIUM = 12;
export const BAND_GENRE_LIMIT = 3;
export const BAND_GENRE_LIMIT_PREMIUM = 5;
export const BAND_POSITION_LIMIT = 20;
export const BAND_POSITION_LIMIT_PREMIUM = 50;
export const BAND_MAX_COUNT = 1;
export const BAND_MAX_COUNT_PREMIUM = 5;

export const NAME_MIN_LENGTH = 1;
export const NAME_MAX_LENGTH = 50;
export const DESCRIPTION_MAX_LENGTH = 150;
export const RESTORE_CODE_LENGTH = 8;
export const PLAYLIST_NAME_MIN_LENGTH = 1;
export const PLAYLIST_NAME_MAX_LENGTH = 100;
