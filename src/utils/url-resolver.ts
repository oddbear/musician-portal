import { Location } from 'Types/app.type';
import Url from 'url-parse';

export interface CTX {
	page: string;
	location: Location;
}

interface Page {
	name: string;
	regexp: RegExp;
}

const pages: Page[] = [
	{
		name: 'signin',
		regexp: /\/signin\/?$/,
	},
	{
		name: 'signup',
		regexp: /\/signup\/?$/,
	},
	{
		name: 'search',
		regexp: /\/search\/?$/,
	},
	{
		name: 'create',
		regexp: /\/band\/new\/?$/,
	},
	{
		name: 'profile',
		regexp: /\/profile(?:\/[^/]+)?\/?$/,
	},
	{
		name: 'band',
		regexp: /\/band\/[^/]+\/?$/,
	},
	{
		name: 'conversations',
		regexp: /\/conversations(?:\/[^/]+)?\/?$/,
	},
	{
		name: 'verification',
		regexp: /\/verification\/[^/]+\/?$/,
	},
	{
		name: 'shop',
		regexp: /\/shop\/?$/,
	},
	{
		name: 'settings',
		regexp: /\/settings\/?$/,
	},
	{
		name: 'restore',
		regexp: /\/restore\/?$/,
	},
	{
		name: 'news',
		regexp: /\/news\/?$/,
	},
	{
		name: 'playlists',
		regexp: /\/playlists\/?$/,
	},
	{
		name: 'playlist',
		regexp: /\/playlist\/[^/]+\/?$/,
	},
	{
		name: 'faq',
		regexp: /\/faq\/?$/,
	},
	{
		name: 'contacts',
		regexp: /\/contacts\/?$/,
	},
	{
		name: 'index',
		regexp: /\/?$/,
	},
	{
		name: 'error',
		regexp: /.*/,
	},
];

export const parseLocation = (url: string): CTX => {
	const { path, query } = parseUrl(url);
	const page = parsePage(path);

	return {
		page: page ?? 'index',
		location: {
			pathname: path,
			query,
		},
	};
};

interface Query {
	[key: string]: string;
}

export const parseUrl = (url: string): { path: string; query: { [key: string]: string } } => {
	const contextUrl = Url(url, true);

	return {
		path: contextUrl.pathname,
		query: Object.entries(contextUrl.query).reduce<Query>((acc, [queryKey, queryValue]) => {
			if (queryValue !== undefined) {
				acc[queryKey] = queryValue;
			}
			return acc;
		}, {}),
	};
};

export const parsePage = (pathname: string): string | null =>
	pages.find(({ regexp }) => regexp.exec(pathname) !== null)?.name ?? null;
