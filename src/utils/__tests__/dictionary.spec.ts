import { Arg, clearDictionary, d, getDictionary, setDictionary } from '../dictionary';

const getTemplateAndResult = (
	...args: Arg[]
): ((literals: TemplateStringsArray, ...matches: string[]) => { template: string; result: string }) => {
	return (literals: TemplateStringsArray, ...matches: string[]) => {
		return {
			template: matches.map((match: string, index: number) => literals[index] + match).join(''),
			result: matches.map((match: string, index) => literals[index] + args[index].toString()).join(''),
		};
	};
};

describe('Util dictionary test', () => {
	beforeEach(() => {
		clearDictionary();
	});

	describe('satDictionary function', () => {
		it('valid json is set successfully', () => {
			console.error = jest.fn();

			const locale = {
				index: {
					popup: {
						foo: 'bar',
						one: 'two three',
					},
					jest: 'Is tests',
				},
				undefined: "it's dirty trick",
				null: "still try to confuse '\\\\'\\\\''",
			};

			setDictionary(JSON.stringify(locale));

			expect(console.error).not.toBeCalled();

			expect(getDictionary('index')).toMatchObject(locale.index);
			expect(getDictionary('index.popup')).toMatchObject(locale.index.popup);
			expect(getDictionary('index.popup.foo')).toEqual(locale.index.popup.foo);
			expect(getDictionary('index.popup.one')).toEqual(locale.index.popup.one);
			expect(getDictionary('index.jest')).toEqual(locale.index.jest);
			expect(getDictionary('undefined')).toEqual(locale.undefined);
			expect(getDictionary('null')).toEqual(locale.null);
		});

		describe('invalid json returns an exception', () => {
			beforeEach(() => {
				console.error = jest.fn();
			});

			it('json is unrecognized symbols', () => {
				setDictionary('import');
				expect(console.error).toBeCalled();
			});

			it('json is number', () => {
				setDictionary('2');
				expect(console.error).toBeCalled();
			});

			it('json is "<"', () => {
				setDictionary('<');
				expect(console.error).toBeCalled();
			});

			describe('json contains non-string-object values', () => {
				beforeEach(() => {
					console.error = jest.fn();
				});

				it('json contains array', () => {
					setDictionary(
						JSON.stringify({
							foo: {
								bar: ['1', '2', '3'],
							},
						}),
					);

					expect(console.error).toBeCalled();
				});

				it('json contains number', () => {
					setDictionary(
						JSON.stringify({
							one: 2,
						}),
					);

					expect(console.error).toBeCalled();
				});
			});
		});

		it("returns undefined if value doesn't exist, no matter how long is path", () => {
			setDictionary(
				JSON.stringify({
					foo: 'bar',
				}),
			);

			expect(getDictionary('foo.bar.one.two.three')).toBeUndefined();
		});
	});

	describe('d function', () => {
		it('returns only string', () => {
			const locale = {
				index: {
					popup: {
						foo: 'bar',
					},
				},
				undefined: "it's dirty trick",
			};

			setDictionary(JSON.stringify(locale));

			expect(d('index')).toEqual('');
			expect(d('index.popup')).toEqual('');
			expect(d('index.popup.one')).toEqual('');
			expect(d('index.popup.foo')).toEqual(locale.index.popup.foo);
			expect(d('undefined')).toEqual(locale.undefined);
		});

		describe('call with arguments', () => {
			it('returns correct string with valid values', () => {
				const digital = 5;
				const f = 3.52;
				const s = 'anything';
				const { result, template } = getTemplateAndResult(digital, f, s)`foo ${'%d%'} bar ${'%f2%'}${'%s%'}`;

				const locale = {
					index: template,
				};

				setDictionary(JSON.stringify(locale));

				expect(d('index', digital, f + 0.00345511, s)).toEqual(result);
			});

			it("shows error then argument's types doesnt match", () => {
				console.error = jest.fn();

				const { template } = getTemplateAndResult(2, 'bar')`foo ${'%d%'} ${'%s%'}`;

				const locale = {
					index: template,
				};

				setDictionary(JSON.stringify(locale));
				d('index', 'bar', 2);

				expect(console.error).toBeCalledTimes(2);
			});
		});
	});
});
