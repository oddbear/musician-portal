import { fireBeforePopState } from 'Mocks/next.mock';
import { mount } from 'enzyme';
import React from 'react';
import { beforePopState, removeEvent, RouterListener } from 'Utils/router';

jest.mock('next/router', () => {
	// eslint-disable-next-line @typescript-eslint/no-unsafe-return
	return {
		...jest.requireActual('../../__mocks__/next.mock'),
	};
});

describe('Utils router tests', () => {
	const Container = (): React.ReactElement => {
		return <RouterListener />;
	};

	it('beforePopState adds callbacks and fires each on Router.beforePopState', () => {
		mount(<Container />);

		const spy = jest.fn();
		beforePopState(spy);

		fireBeforePopState();

		expect(spy).toBeCalledTimes(1);
	});

	it('removeEvent removes callback from handlers', () => {
		mount(<Container />);

		const spy = jest.fn();
		beforePopState(spy);
		removeEvent('beforePopState', spy);

		fireBeforePopState();

		expect(spy).not.toBeCalled();
	});
});
