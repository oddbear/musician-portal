import * as deviceDetect from 'react-device-detect';
import * as MockCloudinary from 'Mocks/cloudinary.mock';
import * as MockDeviceDetect from 'Mocks/react-device-detect.mock';
import * as React from 'react';
import { Cloudinary } from 'cloudinary-core';
import { shallow } from 'enzyme';
import { cloudinaryLazyLoad, COMPRESSED_WIDTH } from '../cloudinary';
import { CloudinaryOptions, CloudinaryResponseElement } from 'Types/cloudinary.type';

jest.mock('react-device-detect', () => {
	// eslint-disable-next-line @typescript-eslint/no-unsafe-return
	return jest.requireActual('../../__mocks__/react-device-detect.mock');
});

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const mockDeviceDetect: MockDeviceDetect.Context = deviceDetect as any;
// eslint-disable-next-line @typescript-eslint/no-explicit-any
const mockCloudinary: MockCloudinary.Context = Cloudinary as any;

const load = (
	options: CloudinaryOptions,
	callback: (responseElement: CloudinaryResponseElement) => void,
	firstCallback?: (responseElement: CloudinaryResponseElement) => void,
): void => {
	let timesLoaded = 0;

	cloudinaryLazyLoad(
		{
			element: options,
		},
		({ element }) => {
			if (++timesLoaded > 1) {
				callback(element);
			} else if (firstCallback !== undefined) {
				firstCallback(element);
			}
		},
	);

	mockCloudinary.load();
};

describe('Util cloudinary tests', () => {
	let testOptions: CloudinaryOptions;

	beforeEach(() => {
		testOptions = {
			src: '/image/src',
			tag: 'img',
		};
	});

	it('returns correct object', (done) => {
		load(
			testOptions,
			() => {
				return;
			},
			(element) => {
				expect(element.src).toMatch(testOptions.src);

				done();
			},
		);
	});

	describe('returns correct elements', () => {
		it('image-element for tag "img"', (done) => {
			testOptions.tag = 'img';

			let timesLoaded = 0;

			load(testOptions, (element) => {
				if (++timesLoaded > 1) {
					return;
				}

				const Element = element.element!;
				// @ts-ignore
				expect(shallow(<Element />).find('img')).toHaveLength(1);

				done();
			});
		});
	});

	it('returns correct object with default compressed size', (done) => {
		let timesLoaded = 0;

		load(testOptions, (element) => {
			if (++timesLoaded > 1) {
				return;
			}

			expect(element.width).toEqual(COMPRESSED_WIDTH);
			done();
		});
	});

	it('returns correct object with custom compressed size', (done) => {
		testOptions.compressedSize = 60;

		let timesLoaded = 0;

		load(testOptions, (element) => {
			if (++timesLoaded > 1) {
				return;
			}

			expect(element.width).toEqual(testOptions.compressedSize);
			done();
		});
	});

	it('returns correct object with custom responsive', (done) => {
		testOptions.responsive = {
			mobile: 100,
			desktop: 500,
		};

		let timesLoaded = 0;

		mockDeviceDetect.setIsTablet(true);
		mockDeviceDetect.setIsMobileOnly(false);

		load(testOptions, (element) => {
			if (++timesLoaded > 1) {
				expect(element.width).toEqual(testOptions.responsive?.desktop);
				done();
			}
		});

		load(testOptions, (element) => {
			if (++timesLoaded > 1) {
				expect(element.width).not.toEqual(testOptions.responsive?.mobile);
				done();
			}
		});

		mockCloudinary.load();

		timesLoaded = 0;

		mockDeviceDetect.setIsTablet(false);
		mockDeviceDetect.setIsMobileOnly(true);

		load(testOptions, (element) => {
			if (++timesLoaded > 1) {
				expect(element.width).toEqual(testOptions.responsive?.mobile);
				done();
			}
		});

		load(testOptions, (element) => {
			if (++timesLoaded > 1) {
				expect(element.width).not.toEqual(testOptions.responsive?.desktop);
				done();
			}
		});

		mockCloudinary.load();
	});
});
