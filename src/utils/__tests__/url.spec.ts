import { parsePreparedQuery, prepareQuery } from '../url';

describe('Util url tests', () => {
	describe('prepareQuery', () => {
		it("parses multi-object and make query looks like {'position[singer[chorus]]': 'available'}", () => {
			const queryObject = {
				position: {
					singer: {
						chorus: 'available',
						church: 'free',
					},
					grim: 2,
				},
			};

			expect(prepareQuery(queryObject)).toMatchObject({
				'position[singer[chorus]]': queryObject.position.singer.chorus,
				'position[singer[church]]': queryObject.position.singer.church,
				'position[grim]': queryObject.position.grim.toString(),
			});
		});

		it("parses multi-array and make query looks like {'position[singer[0]]': 'Andrew','position[singer[1]]': 'Michael'}", () => {
			const queryObject = {
				position: {
					singer: ['Andrew', 'Michael', false],
				},
			};

			expect(prepareQuery(queryObject)).toMatchObject({
				'position[singer[0]]': queryObject.position.singer[0],
				'position[singer[1]]': queryObject.position.singer[1],
				'position[singer[2]]': queryObject.position.singer[2].toString(),
			});
		});

		it('returns object without unexpected types in properties', () => {
			const queryObject = {
				position: {
					singer: {
						chorus: null,
						church: 'free',
					},
					grim: null,
					jerry: null,
				},
			};

			expect(prepareQuery(queryObject)).toMatchObject({
				'position[singer[church]]': queryObject.position.singer.church,
			});
		});
	});

	describe('parsePreparedQuery', () => {
		it("parses objects're looking like prepareQuery result in regular objects", () => {
			const queryObject = {
				position: {
					singer: {
						chorus: 'Andrew',
						church: ['Alex', 'Jimmy'],
					},
				},
				genres: {
					rock: {
						alternative: true,
						metal: false,
					},
					pop: [false, false, true],
				},
				foo: 'bar',
			};

			expect(parsePreparedQuery(prepareQuery(queryObject))).toMatchObject(queryObject);
		});

		it("parses object like it's array if first property is integer and ignores object properties", () => {
			const query = {
				'position[singer[0]]': 'Andrew',
				'position[singer[chorus]]': 'anyone',
			};

			expect(parsePreparedQuery(query)).toMatchObject({
				position: {
					singer: ['Andrew'],
				},
			});
		});

		it("parses object like it's object if first property is not integer and accepts integer-keys like object-keys", () => {
			const query = {
				'position[singer[chorus]]': 'Andrew',
				'position[singer[0]]': 'anyone',
			};

			expect(parsePreparedQuery(query)).toMatchObject({
				position: {
					singer: {
						chorus: 'Andrew',
						0: 'anyone',
					},
				},
			});
		});
	});
});
