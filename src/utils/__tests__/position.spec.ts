import { getAvailableSides, getRelativePosition } from '../position';

let innerWidth = 0;
let innerHeight = 0;
Object.defineProperties(window, {
	innerWidth: {
		get() {
			return innerWidth;
		},
	},
	innerHeight: {
		get() {
			return innerHeight;
		},
	},
});

describe('Util position tests', () => {
	describe('getAvailableSize', () => {
		it('correctly detect availableness of top side', () => {
			innerWidth = 1;
			innerHeight = 1;

			expect(
				getAvailableSides(
					{
						left: 0,
						right: 1,
						top: 0,
						bottom: 1,
					},
					{
						width: 1,
						height: 1,
					},
				),
			).toMatchObject({ top: false });

			expect(
				getAvailableSides(
					{
						left: 0,
						right: 1,
						top: 1,
						bottom: 2,
					},
					{
						width: 1,
						height: 1,
					},
				),
			).toMatchObject({ top: true });
		});

		it('correctly detect availableness of bottom side', () => {
			innerWidth = 1;
			innerHeight = 2;

			expect(
				getAvailableSides(
					{
						left: 0,
						right: 1,
						top: 0,
						bottom: 1,
					},
					{
						width: 1,
						height: 1,
					},
				),
			).toMatchObject({ bottom: true });

			expect(
				getAvailableSides(
					{
						left: 0,
						right: 1,
						top: 1,
						bottom: 2,
					},
					{
						width: 1,
						height: 1,
					},
				),
			).toMatchObject({ bottom: false });
		});

		it('correctly detect availableness of left side', () => {
			innerWidth = 1;
			innerHeight = 1;

			expect(
				getAvailableSides(
					{
						left: 0,
						right: 1,
						top: 0,
						bottom: 1,
					},
					{
						width: 1,
						height: 1,
					},
				),
			).toMatchObject({ left: false });

			expect(
				getAvailableSides(
					{
						left: 1,
						right: 2,
						top: 0,
						bottom: 1,
					},
					{
						width: 1,
						height: 1,
					},
				),
			).toMatchObject({ left: true });
		});

		it('correctly detect availableness of right side', () => {
			innerWidth = 2;
			innerHeight = 1;

			expect(
				getAvailableSides(
					{
						left: 0,
						right: 1,
						top: 0,
						bottom: 1,
					},
					{
						width: 1,
						height: 1,
					},
				),
			).toMatchObject({ right: true });

			expect(
				getAvailableSides(
					{
						left: 1,
						right: 2,
						top: 1,
						bottom: 1,
					},
					{
						width: 1,
						height: 1,
					},
				),
			).toMatchObject({ right: false });
		});
	});

	describe('getRelativePosition', () => {
		describe('correctly defines positions for top side if available', () => {
			it('response without offset then children feats in screen', () => {
				innerWidth = 3;
				innerHeight = 3;

				const parentPosition = {
					left: 0,
					right: 1,
					top: 2,
					bottom: 3,
				};
				const childOffset = {
					width: 1,
					height: 1,
				};

				expect(getAvailableSides(parentPosition, childOffset)).toMatchObject({
					top: true,
				});

				expect(getRelativePosition(parentPosition, childOffset, 'top')).toMatchObject({
					position: {
						top: parentPosition.top - childOffset.height,
						left: parentPosition.right - parentPosition.left - childOffset.width,
					},
					offset: {
						left: 0,
					},
				});
			});

			it("response with offset then children doesn't feat in screen", () => {
				innerWidth = 3;
				innerHeight = 3;

				const parentPosition = {
					left: 0,
					right: 1,
					top: 2,
					bottom: 3,
				};
				const childOffset = {
					width: 2,
					height: 1,
				};

				expect(getAvailableSides(parentPosition, childOffset)).toMatchObject({
					top: true,
				});

				expect(getRelativePosition(parentPosition, childOffset, 'top')).toMatchObject({
					position: {
						top: parentPosition.top - childOffset.height,
						left: 0,
					},
					offset: {
						left: Math.max(
							-0.5,
							(parentPosition.right - parentPosition.left - childOffset.width) / 2 / childOffset.width,
						),
					},
				});
			});
		});

		describe('correctly defines positions for bottom side if available', () => {
			it('response without offset then children feats in screen', () => {
				innerWidth = 3;
				innerHeight = 3;

				const parentPosition = {
					left: 0,
					right: 1,
					top: 0,
					bottom: 1,
				};
				const childOffset = {
					width: 1,
					height: 1,
				};

				expect(getAvailableSides(parentPosition, childOffset)).toMatchObject({
					bottom: true,
				});

				expect(getRelativePosition(parentPosition, childOffset, 'bottom')).toMatchObject({
					position: {
						top: parentPosition.bottom,
						left: parentPosition.right - parentPosition.left - childOffset.width,
					},
					offset: {
						left: 0,
					},
				});
			});

			it("response with offset then children doesn't feat in screen", () => {
				innerWidth = 3;
				innerHeight = 3;

				const parentPosition = {
					left: 0,
					right: 1,
					top: 0,
					bottom: 1,
				};
				const childOffset = {
					width: 2,
					height: 1,
				};

				expect(getAvailableSides(parentPosition, childOffset)).toMatchObject({
					bottom: true,
				});

				expect(getRelativePosition(parentPosition, childOffset, 'bottom')).toMatchObject({
					position: {
						top: parentPosition.bottom,
						left: 0,
					},
					offset: {
						left: Math.max(
							-0.5,
							(parentPosition.right - parentPosition.left - childOffset.width) / 2 / childOffset.width,
						),
					},
				});
			});
		});

		describe('correctly defines positions for left side if available', () => {
			it('response without offset then children feats in screen', () => {
				innerWidth = 3;
				innerHeight = 3;

				const parentPosition = {
					left: 2,
					right: 3,
					top: 0,
					bottom: 1,
				};
				const childOffset = {
					width: 1,
					height: 1,
				};

				expect(getAvailableSides(parentPosition, childOffset)).toMatchObject({
					left: true,
				});

				expect(getRelativePosition(parentPosition, childOffset, 'left')).toMatchObject({
					position: {
						top: parentPosition.bottom - parentPosition.top - childOffset.height,
						left: parentPosition.left - childOffset.width,
					},
					offset: {
						top: 0,
					},
				});
			});

			it("response with offset then children doesn't feat in screen", () => {
				innerWidth = 3;
				innerHeight = 3;

				const parentPosition = {
					left: 2,
					right: 3,
					top: 0,
					bottom: 1,
				};
				const childOffset = {
					width: 1,
					height: 2,
				};

				expect(getAvailableSides(parentPosition, childOffset)).toMatchObject({
					left: true,
				});

				expect(getRelativePosition(parentPosition, childOffset, 'left')).toMatchObject({
					position: {
						top: 0,
						left: parentPosition.left - childOffset.width,
					},
					offset: {
						top: Math.max(
							-0.5,
							(parentPosition.bottom - parentPosition.top - childOffset.height) / 2 / childOffset.height,
						),
					},
				});
			});
		});

		describe('correctly defines positions for right side if available', () => {
			it('response without offset then children feats in screen', () => {
				innerWidth = 3;
				innerHeight = 3;

				const parentPosition = {
					left: 0,
					right: 1,
					top: 0,
					bottom: 1,
				};
				const childOffset = {
					width: 1,
					height: 1,
				};

				expect(getAvailableSides(parentPosition, childOffset)).toMatchObject({
					right: true,
				});

				expect(getRelativePosition(parentPosition, childOffset, 'right')).toMatchObject({
					position: {
						top: parentPosition.bottom - parentPosition.top - childOffset.height,
						left: parentPosition.right,
					},
					offset: {
						top: 0,
					},
				});
			});

			it("response with offset then children doesn't feat in screen", () => {
				innerWidth = 3;
				innerHeight = 3;

				const parentPosition = {
					left: 0,
					right: 1,
					top: 0,
					bottom: 1,
				};
				const childOffset = {
					width: 1,
					height: 2,
				};

				expect(getAvailableSides(parentPosition, childOffset)).toMatchObject({
					right: true,
				});

				expect(getRelativePosition(parentPosition, childOffset, 'right')).toMatchObject({
					position: {
						top: 0,
						left: parentPosition.right,
					},
					offset: {
						top: Math.max(
							-0.5,
							(parentPosition.bottom - parentPosition.top - childOffset.height) / 2 / childOffset.height,
						),
					},
				});
			});
		});
	});
});
