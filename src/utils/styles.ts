export interface DefaultTheme {
	fontFamily: {
		[key: string]: string;
	};
	color: {
		[key: string]: string;
	};
	button: {
		[key: string]: {
			[key: string]: string;
		};
	};
	other: {
		[key: string]: string;
	};
}

export const theme: DefaultTheme = {
	fontFamily: {
		quiet: "'Myriad Pro', monospace",
		aggressive: "'TrixieCyr-Plain', serif",
		special: "'Aardvark', serif",
	},
	color: {
		primary: '#000',
		secondary: '#fff',
		hovernav: 'rgb(179, 179, 179)',
		disabled: 'rgba(241, 241, 241, 0.5)',
		third: 'rgb(234, 154, 60)',
		fourth: 'rgb(128, 42, 42)',
		error: 'rgb(217,62,62)',
		gray50: 'rgba(44, 44, 44, 0.5)',
	},
	button: {
		background: {
			primary: 'rgba(255, 255, 255)',
			hoverfocus: 'rgba(255, 255, 255, 0.8)',
		},
	},
	other: {
		defaultTransitionDuration: '0.15s',
		containerMaxWidth: '1100px',
		signPagesPadding: '50px 0 20px 0',
	},
};

export const getFontSize = (windowWidth: number): string => {
	if (windowWidth > 1200) {
		return '19px';
	} else if (windowWidth > 992) {
		return '18px';
	} else if (windowWidth > 768) {
		return '17px';
	} else {
		return '15px';
	}
};
