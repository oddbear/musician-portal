import React from 'react';

const validateObject = (object: object): boolean =>
	Object.entries(object).every(
		([, value]) =>
			typeof value === 'string' ||
			(typeof value === 'object' && !(value instanceof Array) && validateObject(value as object)),
	);

interface Dictionary {
	[key: string]: Dictionary | string;
}

let dictionary: Dictionary = {};

export const setDictionary = (locale: string): void => {
	try {
		const json: Dictionary = JSON.parse(locale);

		if (typeof json !== 'object' || json instanceof Array || !validateObject(json)) {
			throw new Error('locale validation failed, some fields have no string format');
		}

		dictionary = Object.assign(dictionary, json);
	} catch (err) {
		console.error(err as Error);
	}
};

export const getDictionary = (name: string): string | Dictionary =>
	name.split('.').reduce((last: string | Dictionary, path: string) => {
		if (typeof last !== 'object') {
			last = {};
		}

		return last[path];
	}, dictionary);

const argumentError = (path: string, index: number, type: string): void => {
	console.error(`Dictionary's string '${path}' argument with index ${index.toString()} must be type of ${type}`);
};

export type Arg = string | number;
export const d = (path: string, ...args: Arg[]): string => {
	let result = getDictionary(path);
	result = typeof result !== 'string' ? '' : result;

	let matchesLength = 0;
	result = result.replace(/%([^%]+)%/g, (a, type: string): string => {
		let arg = args[matchesLength++];

		if (type === 'd') {
			if (typeof arg === 'number') {
				arg = arg.toString();
			} else {
				argumentError(path, matchesLength - 1, 'number');
			}
		} else if (/^f\d+/.exec(type) !== null) {
			const decimalLength = parseInt(/\d+/.exec(type)![0], 10);

			if (typeof arg === 'number') {
				arg = arg.toFixed(decimalLength);
			} else {
				argumentError(path, matchesLength - 1, 'number');
			}
		} else if (type === 's') {
			if (typeof arg !== 'string') {
				arg = '';
				argumentError(path, matchesLength - 1, 'string');
			}
		} else {
			arg = '';
		}

		return arg;
	});

	return result;
};

type ElementContext =
	| {
			type: 'newLine';
	  }
	| {
			type: 'text';
	  }
	| {
			type: 'anchor';
			href: string;
	  };

const prepare = (
	chunkHtml: string,
	chunkIndex: number,
	wrapperCallback: (innerText: string, elementContext: ElementContext, elementIndex: number) => React.ReactElement,
): React.ReactElement => {
	if (/\$/.exec(chunkHtml) !== null) {
		const operationsHtml = chunkHtml.split('$');
		return wrapperCallback(
			operationsHtml[1],
			{
				type: 'anchor',
				href: operationsHtml[0],
			},
			chunkIndex,
		);
	} else if (chunkHtml === '') {
		return wrapperCallback(
			'',
			{
				type: 'newLine',
			},
			chunkIndex,
		);
	} else {
		return wrapperCallback(
			chunkHtml,
			{
				type: 'text',
			},
			chunkIndex,
		);
	}
};

export const wrap = (
	nodesHtml: string,
	wrapperCallback: (innerText: string, elementContext: ElementContext, elementIndex: number) => React.ReactElement,
): React.ReactElement[] =>
	(nodesHtml === '' ? [] : nodesHtml.split('|')).map((chunkHtml, index) => prepare(chunkHtml, index, wrapperCallback));

export const clearDictionary = (): void => {
	dictionary = {};
};
