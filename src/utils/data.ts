import { ResponseBand, ResponsePlaylist, ResponseProfile } from 'Types/response.type';
import { getDigitEnumKeys, PlaylistPublicity } from 'Utils/enum';

export const dummyProfileData: ResponseProfile = {
	_id: '',
	login: '',
	name: 'loading',
	avatar: {
		_id: '',
		src: '',
		description: '',
		json: {
			type: 'image',
		},
	},
	email: '',
	description: null,
	experience: 0,
	city: null,
	country: null,
	genres: [],
	positions: [],
	audios: [],
	registeredWithSocial: true,
	settings: {
		privacy: {
			conversationsEnabled: true,
			showStatistics: false,
		},
	},
	memberInBands: [],
	statistics: {
		bandsMember: 0,
		bandsHired: 0,
	},
	dignities: [],
	relations: {},
};

export const dummyBandData: ResponseBand = {
	_id: '',
	alias: '',
	name: 'loading',
	avatar: {
		_id: '',
		src: '',
		description: '',
		json: {
			type: 'image',
		},
	},
	description: null,
	experience: 0,
	country: null,
	city: null,
	genres: [],
	positions: [],
	audios: [],
	members: [],
	statistics: {
		membersJoined: 0,
		membersInBand: 0,
	},
	settings: {
		privacy: {
			showStatistics: false,
		},
	},
	dignities: [],
	relations: [],
	subscribersCount: 0,
	isSubscribed: false,
	repetitionBase: false,
};

export const dummyPlaylistData: ResponsePlaylist = {
	_id: '',
	name: 'loading',
	avatar: {
		_id: '',
		src: '',
		description: '',
		json: {
			type: 'image',
		},
	},
	author: dummyProfileData,
	tracks: [],
	reactions: [],
	publicity: PlaylistPublicity.private,
	isFollowed: false,
	createdAt: 0,
	positions: [],
	genres: [],
};

export const getBandOwner = (band: ResponseBand): ResponseProfile => {
	const ownerPosition = band.positions.find(({ position }) => position.value === 'owner');
	if (ownerPosition === undefined || ownerPosition.profile === null || ownerPosition.profile === undefined) {
		throw new Error(`owner not found for band ${band.name}`);
	}
	return ownerPosition.profile;
};

// if band sent to arguments then extract owner from band else return profile
export const extractProfile = (profileOrBand: ResponseBand | ResponseProfile): ResponseProfile => {
	if ('alias' in profileOrBand) {
		return getBandOwner(profileOrBand);
	} else {
		return profileOrBand;
	}
};

export const getRatingName = (Enum: { [key: string]: string | number }, rating: number): string => {
	const keys = getDigitEnumKeys(Enum).sort((a, b) => (a > b ? 1 : -1));
	let currentKey = keys[keys.length - 1];
	while (rating < currentKey) {
		const nextIndex = keys.indexOf(currentKey) - 1;
		if (nextIndex < 0) {
			break;
		}

		currentKey = keys[nextIndex];
	}

	return Enum[currentKey].toString();
};

export const convertImageUrlToBlob = (image: HTMLImageElement): Promise<Blob> =>
	new Promise((res, rej) => {
		URL.revokeObjectURL(image.src);
		const canvas = document.createElement('canvas');
		const ctx = canvas.getContext('2d');

		if (ctx === null) {
			rej('no canvas 2d context support for current browser');
			return;
		}

		canvas.width = image.width;
		canvas.height = image.height;
		ctx.drawImage(image, 0, 0, image.width, image.height);

		canvas.toBlob(
			(blob) => {
				if (blob === null) {
					rej('blob is null');
				} else {
					res(blob);
				}
			},
			'image/jpeg',
			1,
		);
	});
