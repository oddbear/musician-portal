import { Cloudinary } from 'cloudinary-core';
import { CloudinaryOptions, CloudinaryResponseElement } from 'Types/cloudinary.type';
import { isMobileOnly, isTablet } from 'react-device-detect';
import React, { ReactElement } from 'react';

const isServer = typeof window === 'undefined';
// const isWebpSupported = canUseWebP();

type deviceTypes = 'desktop' | 'mobile';

export const getDevice = (): deviceTypes => {
	if (isMobileOnly) {
		return 'mobile';
	} else if (isTablet) {
		return 'desktop';
	} else {
		return 'desktop';
	}
};

// function canUseWebP(): boolean {
// 	if (isServer) {
// 		return false;
// 	}
//
// 	const elem = document.createElement('canvas');
// 	if (config.env !== 'test' && elem.getContext('2d') !== null) {
// 		return elem.toDataURL('image/webp').startsWith('data:image/webp');
// 	}
//
// 	return false;
// }

export const COMPRESSED_WIDTH = 50;

interface Response {
	[key: string]: CloudinaryResponseElement;
}

let cl: Cloudinary;
if (!isServer) {
	cl = new Cloudinary({ ['cloud_name']: config.cloudinary.cloudName, secure: true });
}

export const cloudinaryLazyLoad = (
	options: { [key: string]: CloudinaryOptions },
	callback: (response: Response) => void,
): void => {
	const result: Response = {};

	if (isServer) {
		return;
	}

	Object.entries(options).forEach(
		([key, { src, tag = 'img', compressedSize = COMPRESSED_WIDTH, responsive }]: [string, CloudinaryOptions]) => {
			let loadOptions = {};

			const device: deviceTypes = getDevice();
			if (responsive?.[device] !== undefined) {
				loadOptions = { crop: 'scale', width: responsive[device] };
			}

			if (tag === 'img') {
				// @TODO: cloudinary now can't transform image to .webp
				// if (isWebpSupported) {
				// 	src = `${src}.webp`;
				// }

				const setReturnValue = (image: HTMLImageElement): void => {
					result[key] = {
						src: image.src,
						width: image.naturalWidth,
						height: image.naturalHeight,
						element(props: object): ReactElement {
							return <img src={image.src} alt="blank" {...props} />;
						},
					};
				};

				let img: HTMLImageElement = cl.image(src, { effect: 'blur:80', crop: 'scale', width: compressedSize });

				const onHighLoad = (): void => {
					img.removeEventListener('load', onHighLoad);

					setReturnValue(img);
					callback(result);
				};

				const onLowLoad = (): void => {
					img.removeEventListener('load', onLowLoad);

					setReturnValue(img);
					callback(result);

					img = cl.image(src, loadOptions);
					img.addEventListener('load', onHighLoad);
				};

				img.addEventListener('load', onLowLoad);

				setReturnValue(img);
			} else {
				const videoHtml: string = cl.videoTag(src).toHtml();

				const videoWrap = document.createElement('div');
				videoWrap.innerHTML = videoHtml;
				const video = videoWrap.querySelector('video')!;
				let element: HTMLAudioElement | HTMLVideoElement = video;

				const source = video.querySelector('source');
				if (source === null) {
					throw new Error(`source not found then loading ${tag} with src "${src}"`);
				}

				if (tag === 'audio') {
					element = new Audio();
					element.src = source.src;
				}

				result[key] = {
					src: source.src,
					width: video.width,
					height: video.height,
					element,
				};
				callback(result);
			}
		},
	);

	callback(result);
};

export const buildCloudinaryUrl = (path: string): string =>
	`https://res.cloudinary.com/${config.cloudinary.cloudName}/image/upload/${path}`;
