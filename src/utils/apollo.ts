import ApolloClient from 'apollo-client';
import { createUploadLink } from 'apollo-upload-client';
import { InMemoryCache, IntrospectionFragmentMatcher, NormalizedCacheObject } from 'apollo-cache-inmemory';
import introspectionQueryResultData from '../fragmentTypes.json';
import { onError } from 'apollo-link-error';
import { ApolloLink, DocumentNode, split } from 'apollo-link';
import fetch from 'isomorphic-unfetch';
import { WebSocketLink } from 'apollo-link-ws';
import { getMainDefinition } from 'apollo-utilities';
import { setContext } from 'apollo-link-context';
import * as apolloHooks from '@apollo/react-hooks';
import { ExecutionResult, MutationFunctionOptions } from '@apollo/react-common';

const GRAPHQL_URL = config.graphQL.url;
const GRAPHQL_SUBSCRIPTION_URL = config.graphQL.subscriptionUrl;
const API_URL = config.api.url;
const API_NODE_PORT = config.api.port;

let token: string | null = null;

const create = (initialState?: object): ApolloClient<NormalizedCacheObject> => {
	const fragmentMatcher = new IntrospectionFragmentMatcher({
		introspectionQueryResultData,
	});

	const cache = new InMemoryCache(
		initialState !== undefined
			? {
					...initialState,
					fragmentMatcher,
			  }
			: { fragmentMatcher },
	);

	const apolloLink = ApolloLink.from([
		onError(({ graphQLErrors, networkError }): void => {
			if (graphQLErrors !== undefined)
				graphQLErrors.forEach(({ message, locations, path }) =>
					console.error(
						`[GraphQL error]: Message: ${message}, Location: ${
							locations !== undefined ? JSON.stringify(locations) : '<null>'
						}, Path: ${path !== undefined ? JSON.stringify(path) : '<null>'}`,
					),
				);
			if (networkError !== undefined) console.error(`[Network error]: ${JSON.stringify(networkError)}`);
		}),
		// eslint-disable-next-line @typescript-eslint/no-unsafe-call
		setContext((operation, previousContext: { headers?: { token?: string } }) => {
			const headers = previousContext.headers ?? {};
			if (token !== null) {
				headers.token = token;
				token = null;
			}

			return {
				...previousContext,
				headers,
			};
		}),
		createUploadLink({
			uri:
				typeof window === 'undefined'
					? API_URL.includes(API_NODE_PORT.toString())
						? `http://api:${API_NODE_PORT.toString()}/graphql`
						: GRAPHQL_URL
					: GRAPHQL_URL,
			credentials: 'include',
			fetch: fetch,
		}),
	]);

	if (typeof window !== 'undefined') {
		const wsLink = new WebSocketLink({
			uri: GRAPHQL_SUBSCRIPTION_URL,
			options: {
				reconnect: true,
			},
		});

		return new ApolloClient({
			link: split(
				({ query }) => {
					const definition = getMainDefinition(query);
					return definition.kind === 'OperationDefinition' && definition.operation === 'subscription';
				},
				wsLink,
				apolloLink,
			),
			cache,
			ssrMode: false,
			connectToDevTools: true,
		});
	} else {
		return new ApolloClient({
			link: apolloLink,
			cache,
			ssrMode: true,
			connectToDevTools: false,
		});
	}
};

let apolloClient: ApolloClient<NormalizedCacheObject>;

export const initApolloClient = (initialState?: object): ApolloClient<NormalizedCacheObject> => {
	if (typeof window === 'undefined') {
		return create(initialState);
	}

	if (apolloClient === undefined) {
		apolloClient = create(initialState);
	}

	return apolloClient;
};

const mutationHook = apolloHooks.useMutation;
export const useSecureMutation = <TData, TVariables>(
	nodeDocument: DocumentNode,
	options?: apolloHooks.MutationHookOptions<TData, TVariables>,
): apolloHooks.MutationTuple<TData, TVariables> => {
	const mutation = mutationHook(nodeDocument, options);

	const definition = nodeDocument.definitions[0];
	const docName =
		definition !== undefined && 'name' in definition && 'value' in definition.name!
			? definition.name!.value
			: undefined;

	if (docName === undefined) {
		return mutation;
	}

	const fn = (fnOptions?: MutationFunctionOptions<TData, TVariables>): Promise<ExecutionResult<TData>> =>
		new Promise((res) => {
			grecaptcha.ready(() => {
				void grecaptcha.execute(config.google.recaptcha.siteKey, { action: docName }).then(async (captchaToken) => {
					token = captchaToken;
					res(mutation[0](fnOptions));
				});
			});
		});

	return [fn, mutation[1]];
};
