import firebase from 'firebase/app';

let cachedMessaging: firebase.messaging.Messaging;

const getOrCreateMessaging = (): firebase.messaging.Messaging =>
	cachedMessaging === undefined ? (cachedMessaging = firebase.messaging()) : cachedMessaging;

export const notify = (): Promise<'default' | 'denied' | 'granted'> => {
	if (!('Notification' in window)) {
		console.error('This browser does not support desktop notification');
		return Promise.resolve('denied');
	} else if (Notification.permission === 'granted') {
		return Promise.resolve('granted');
	} else if (Notification.permission === 'denied') {
		return Promise.resolve('denied');
	} else {
		return Notification.requestPermission();
	}
};

export const getToken = (workerUrl: string): Promise<string> =>
	new Promise((res, rej) => {
		if ('serviceWorker' in navigator) {
			navigator.serviceWorker
				.register(workerUrl)
				.then((registration) => {
					const messaging = getOrCreateMessaging();
					res(
						messaging.getToken({
							vapidKey: config.google.firebase.vapidKey,
							serviceWorkerRegistration: registration,
						}),
					);
				})
				.catch((error: Error) => {
					rej(`Service worker registration failed: ${error.message}`);
				});
		} else {
			rej('Service workers are not supported');
		}
	});

export const subscribeToNotifications = (): void => {
	getOrCreateMessaging().onMessage((payload) => {
		console.log('Message received. ', payload);
	});
};
