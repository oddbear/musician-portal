interface Rule<KeyType> {
	key: KeyType;
	regexp: RegExp | ((chunkText: string) => RegExpMatchArray[]);
	splittable?: boolean;
	parse?: (match: RegExpMatchArray) => string;
}

interface Chunk<KeyType> {
	key?: KeyType;
	chunkText: string;
}

export class Replacer<ChunkKeyType> {
	constructor(public rules: Rule<ChunkKeyType>[]) {}

	public parse<O>(
		text: string,
		callback: (element: Chunk<ChunkKeyType>, index: number, arr: Chunk<ChunkKeyType>[]) => O,
	): O[] {
		let chunks: Chunk<ChunkKeyType>[] = [{ chunkText: text }];

		for (const rule of this.rules) {
			chunks = this.split(chunks, rule);
		}

		return chunks.map(callback);
	}

	private splitChunk(chunk: Chunk<ChunkKeyType>, rule: Rule<ChunkKeyType>): Chunk<ChunkKeyType>[] {
		const chunkRule = this.rules.find(({ key }) => chunk.key === key);
		if (chunkRule === undefined || chunkRule.splittable === true) {
			const chunks: Chunk<ChunkKeyType>[] = [];

			const matches =
				typeof rule.regexp === 'function' ? rule.regexp(chunk.chunkText) : [...chunk.chunkText.matchAll(rule.regexp)];
			if ((matches[0]?.index ?? 0) > 0) {
				chunks.push({
					chunkText: chunk.chunkText.substring(0, matches[0]?.index ?? 0),
				});
			} else if (matches.length === 0) {
				return [chunk];
			}

			for (const match of matches) {
				const chunkText = rule.parse?.(match) ?? (match[1] as string | undefined) ?? match[0];
				chunks.push({
					key: rule.key,
					chunkText,
				});

				const index = matches.indexOf(match);
				const m1End = (match.index ?? 0) + match[0].length;
				const m2Start = matches[index + 1]?.index ?? 0;
				if (index < matches.length - 1 && m1End < m2Start) {
					chunks.push({
						chunkText: chunk.chunkText.substring(m1End, m2Start),
					});
				} else if (index === matches.length - 1 && m1End < chunk.chunkText.length) {
					chunks.push({
						chunkText: chunk.chunkText.substr(m1End),
					});
				}
			}

			return chunks;
		} else {
			return [chunk];
		}
	}

	private split(chunks: Chunk<ChunkKeyType>[], rule: Rule<ChunkKeyType>): Chunk<ChunkKeyType>[] {
		return chunks.reduce<Chunk<ChunkKeyType>[]>((acc, chunk) => [...acc, ...this.splitChunk(chunk, rule)], []);
	}
}
