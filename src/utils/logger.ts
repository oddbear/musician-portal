export const logProfiler = (
	id: string,
	phase: string,
	actualTime: number,
	baseTime: number,
	startTime: number,
	commitTime: number,
): void => {
	console.log(
		`${id}'s ${phase} phase:
		Actual time: ${actualTime.toString()}
		Base time: ${baseTime.toString()}
		Start time: ${startTime.toString()}
		Commit time: ${commitTime.toString()}`,
	);
};
