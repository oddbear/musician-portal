import Router from 'next/router';
import React, { useEffect } from 'react';

interface BeforePopStateProps {
	url: string;
}

type EventHandler = (props: BeforePopStateProps) => boolean;

const eventHandlers: { [key: string]: EventHandler[] } = {
	beforePopState: [],
};

export const beforePopState = (callback: EventHandler): void => {
	eventHandlers.beforePopState.push(callback);
};

export const removeEvent = (eventName: string, callback: EventHandler): void => {
	const handlers = eventHandlers[eventName];

	if (handlers === undefined) {
		console.warn(`Utils router -> removeEvent, event not found '${eventName}'`);
	}

	const handlerIndex = handlers.indexOf(callback);
	if (handlerIndex !== -1) {
		handlers.splice(handlerIndex, 1);
	}
};

export const RouterListener: React.FC = () => {
	useEffect(() => {
		Router.beforePopState((props: BeforePopStateProps) => {
			eventHandlers.beforePopState.forEach((callback) => callback(props));
			return true;
		});

		return () =>
			Router.beforePopState(() => {
				return false;
			});
	}, []);

	return null;
};
