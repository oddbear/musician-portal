import { Offset, Position } from 'Types/position.type';

interface AvailableSidesResponse {
	left: boolean;
	top: boolean;
	right: boolean;
	bottom: boolean;
}

export type AvailableSides = 'left' | 'right' | 'top' | 'bottom';

const getOffsetProperties = (position: Position): Offset => ({
	width: position.right - position.left,
	height: position.bottom - position.top,
});

export const getAvailableSides = (parentPosition: Position, childOffset: Offset): AvailableSidesResponse => {
	if (typeof window === 'undefined') {
		return {
			left: false,
			top: false,
			right: false,
			bottom: false,
		};
	}

	const ww = window.innerWidth;
	const wh = window.innerHeight;

	return {
		left: parentPosition.left - childOffset.width >= 0,
		top: parentPosition.top - childOffset.height >= 0,
		right: parentPosition.right + childOffset.width <= ww,
		bottom: parentPosition.bottom + childOffset.height <= wh,
	};
};

export interface RelativeData {
	left?: number;
	top?: number;
}

export interface RelativePositionResponse {
	position: RelativeData;
	offset: RelativeData;
}

export const getRelativePosition = (
	parentPosition: Position,
	childOffset: Offset,
	side: AvailableSides,
): RelativePositionResponse => {
	let position: RelativeData;
	let offset: RelativeData = {};

	if (typeof window === 'undefined') {
		return {
			position: {
				top: 0,
				left: 0,
			},
			offset: {
				top: 0,
				left: 0,
			},
		};
	}

	let top = 0;
	let left = 0;

	const ww = window.innerWidth;
	const wh = window.innerHeight;
	const containerOffset = getOffsetProperties(parentPosition);

	switch (side) {
		case 'left':
		case 'right':
			top = parentPosition.top + (containerOffset.height - childOffset.height) / 2;
			if (top < 0) {
				offset = {
					top: Math.max(-0.5, top / childOffset.height),
				};
				top = 0;
			} else if (top > wh - childOffset.height) {
				offset = {
					top: Math.min(0.5, (top - (wh - childOffset.height)) / childOffset.height),
				};
				top = wh - childOffset.height;
			} else {
				offset = {
					top: 0,
				};
			}
			break;

		case 'top':
		case 'bottom':
			left = parentPosition.left + (containerOffset.width - childOffset.width) / 2;
			if (left < 0) {
				offset = {
					left: Math.max(-0.5, left / childOffset.width),
				};
				left = 0;
			} else if (left > ww - childOffset.width) {
				offset = {
					left: Math.min(0.5, (left - (ww - childOffset.width)) / childOffset.width),
				};
				left = ww - childOffset.width;
			} else {
				offset = {
					left: 0,
				};
			}
			break;
	}

	switch (side) {
		case 'left':
			position = {
				left: parentPosition.left - childOffset.width,
				top,
			};
			break;

		case 'right':
			position = {
				left: parentPosition.right,
				top,
			};
			break;

		case 'top':
			position = {
				top: parentPosition.top - childOffset.height,
				left,
			};
			break;

		case 'bottom':
			position = {
				top: parentPosition.bottom,
				left,
			};
			break;

		default:
			position = {
				top: 0,
				left: 0,
			};
			break;
	}

	return {
		position,
		offset,
	};
};
