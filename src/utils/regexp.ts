export const validateEmail = (email: string): boolean =>
	// eslint-disable-next-line no-control-regex
	/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)])/.exec(
		email,
	) !== null;

export const urlRegexp = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_+.~#?&//=]*)/;
export const extractUrl = (text: string): RegExpExecArray | null => urlRegexp.exec(text);

export const validateUrl = (url: string): boolean => extractUrl(url) !== null;

export const getMentionPositions = (
	text: string,
): {
	start: number;
	end: number;
	mention: string;
}[] => {
	const positions = [];

	if (text.includes('@')) {
		for (let i = 0; i < text.length; i++) {
			if (text[i] === '@' && [undefined, ' ', '\n', '\t'].includes(text[i - 1])) {
				const result = /^@([\wа-яА-Я]*)/.exec(text.substr(i));

				if (result !== null) {
					const end = i + 1 + result[1].length;
					positions.push({
						start: i,
						end,
						mention: result[1],
					});

					i = end - 1;
				}
			}
		}
	}

	return positions;
};

export const mentionRegexp = /(?:^|\s|\n|\t)@(\w+)/;

export const preventTags = (html: string): string => html.replace(/</g, '&lt;').replace(/>/g, '&gt;');

export const removeTags = (html: string): string => html.replace(/(<\/?[^>]+\/?>)/g, '');

export const escapeRegExp = (string: string): string => string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
