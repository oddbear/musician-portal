export const getEnumKeys = (enumerate: object): string[] =>
	Object.keys(enumerate).filter((key) => key !== parseInt(key, 10).toString());

export const getDigitEnumKeys = (enumerate: object): number[] =>
	Object.keys(enumerate)
		.filter((key) => key === parseInt(key, 10).toString())
		.map((key) => parseInt(key, 10));

export enum MonthName {
	jan,
	feb,
	mar,
	apr,
	may,
	jun,
	jul,
	aug,
	sep,
	nov,
	oct,
	dec,
}

// back + front

export enum InviteRelations {
	invites,
	invited,
	unavailable,
}

export enum PositionVariant {
	available,
	occupied,
}

export enum InviteType {
	invite,
	cancel,
	decline,
	accept,
}

export enum MemberHistoryActionType {
	leaveBand,
	dismissFromBand,
	hireToBand,
	decline,
	joinBand,
	createBand,
	removeBand,
}

export enum NotificationType {
	invite,
	cancel,
	decline,
	leaveBand,
	dismissFromBand,
	hireToBand,
	joinBand,
	createBand,
	removeBand,
	mention,
}

export enum VerificationResponse {
	already,
	incorrect,
	correct,
}

export enum PlaylistPublicity {
	private,
	public,
}

export enum Rarity {
	basic,
	common,
	uncommon,
	rare,
	epic,
	mythical,
	legendary,
	premium,
}
