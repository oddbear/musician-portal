interface QueryObject {
	[key: string]: string;
}

export interface QueryReceivedObject {
	[key: string]: AvailableTypes;
}
export type AvailableTypes = QueryReceivedObject | AvailableTypes[] | string | number | boolean | null;

function mergeObjectInQuery(query: QueryObject, key: string, mergeQuery: QueryObject): QueryObject {
	return Object.entries(mergeQuery).reduce<QueryObject>((preparedQuery, [queryKey, queryValue]) => {
		preparedQuery[`${key}[${queryKey}]`] = queryValue;
		return preparedQuery;
	}, query);
}

const arrayToObject = (arr: AvailableTypes[]): QueryReceivedObject =>
	Object.entries(arr).reduce<QueryReceivedObject>((acc, [queryKey, queryValue]) => {
		acc[queryKey] = queryValue;
		return acc;
	}, {});

export function prepareQuery(query: QueryReceivedObject): QueryObject {
	return Object.entries(query).reduce<QueryObject>((preparedQuery, [queryKey, queryValue]) => {
		let result: QueryObject | string | number | boolean | null;

		if (typeof queryValue === 'object' && queryValue !== null) {
			if (queryValue instanceof Array) {
				result = prepareQuery(arrayToObject(queryValue));
			} else {
				result = prepareQuery(queryValue);
			}
		} else if (queryValue === null) {
			return preparedQuery;
		} else {
			result = queryValue.toString();
		}

		if (typeof result === 'object') {
			return mergeObjectInQuery(preparedQuery, queryKey, result);
		} else {
			if (preparedQuery instanceof Array) {
				preparedQuery[getIntFromString(queryKey)!] = result;
			} else if (result !== null) {
				preparedQuery[queryKey] = result;
			}
		}

		return preparedQuery;
	}, {});
}

export const parsePreparedQuery = (query: QueryObject): QueryReceivedObject =>
	Object.entries(query).reduce<QueryReceivedObject>((parsedQuery, [queryKey, queryValue]) => {
		let keyString = queryKey;
		const regExp = /\[([^[\]]+)]/;
		const keys: string[] = [];
		let matchedKey: null | RegExpMatchArray;
		while ((matchedKey = regExp.exec(keyString)) !== null) {
			keyString =
				keyString.substr(0, keyString.indexOf(matchedKey[0])) +
				keyString.substr(keyString.indexOf(matchedKey[0]) + matchedKey[0].length);
			keys.push(matchedKey[1]);
		}
		keys.reverse().unshift(keyString);

		let lastQueryPath: AvailableTypes = parsedQuery;
		keys.forEach((key, index) => {
			if (typeof lastQueryPath === 'object' && lastQueryPath !== null) {
				const intKey = getIntFromString(key);

				if (
					lastQueryPath instanceof Array
						? intKey !== null && typeof lastQueryPath[intKey] !== 'object'
						: typeof lastQueryPath[key] !== 'object'
				) {
					const value =
						index === keys.length - 1
							? parseTypeFromString(queryValue)
							: getIntFromString(keys[index + 1]) === null
							? {}
							: [];

					if (lastQueryPath instanceof Array) {
						lastQueryPath[intKey!] = value;
					} else {
						lastQueryPath[key] = value;
					}
				}

				if (lastQueryPath instanceof Array) {
					lastQueryPath = lastQueryPath[intKey!];
				} else {
					lastQueryPath = lastQueryPath[key];
				}
			}
		});

		return parsedQuery;
	}, {});

const getIntFromString = (key: string): number | null => {
	if (key === parseInt(key, 10).toString()) {
		return parseInt(key, 10);
	}

	return null;
};

// parses any value from string, ex: 'false' => false, '2.514' => 2.514, 'foo' => 'foo'
const parseTypeFromString = (variable: string): string | number | boolean => {
	const intResult = getIntFromString(variable);

	if (intResult !== null) {
		return intResult;
	} else if ('false' === variable || 'true' === variable) {
		return variable === 'true';
	}

	return variable;
};

export const verifyImageUrl = (url: string, timeout = 10000): Promise<HTMLImageElement> => {
	return new Promise((res, rej) => {
		const img = new Image();
		img.crossOrigin = 'Anonymous';

		const timer = setTimeout(function () {
			img.src = '//!!!!/test.jpg';
			rej('timeout');
		}, timeout);

		img.onerror = img.onabort = () => {
			clearTimeout(timer);
			rej('error');
		};
		img.onload = () => {
			clearTimeout(timer);
			res(img);
		};
		img.src = url;
	});
};

export const verifyYoutubeUrl = (url: string): boolean =>
	/(?:youtube\.com\/(?:[^/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?/\s]{11})/gi.exec(url) !== null;

export const extractIdFromYoutubeUrl = (url: string): string | null => {
	const match = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#&?]*).*/.exec(url);
	return match !== null && match[7].length == 11 ? match[7] : null;
};
