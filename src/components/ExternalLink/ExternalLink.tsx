import React, { MouseEvent, useCallback } from 'react';
import styled from 'styled-components';

interface Props {
	href?: string;
	rel?: string;
	children?: React.ReactNode;
	className?: string;
}

const Container = styled.a`
	cursor: pointer;
`;

const ExternalLink: React.FC<Props> = ({ href, rel, children, className }) => {
	const click = useCallback(
		(event: MouseEvent<HTMLAnchorElement>) => {
			if (href === undefined) {
				return;
			}

			event.preventDefault();

			const otherWindow = window.open();
			if (otherWindow !== null) {
				otherWindow.opener = null;
				otherWindow.location.href = href;
			}
		},
		[href],
	);

	return (
		<Container className={className} href={href} rel={rel} onClick={click}>
			{children}
		</Container>
	);
};

export default ExternalLink;
