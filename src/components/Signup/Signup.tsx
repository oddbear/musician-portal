import Button from 'UI/Button';
import { d } from 'Utils/dictionary';
import styled from 'styled-components';
import Form, { Input, PrevChildren, Step, Title } from 'UI/Form';
import React, { FormEvent, useCallback, useContext, useEffect, useReducer, useState } from 'react';
import { signup as signupMutation } from 'Schemas/User';
import { checkLogin as checkLoginMutation, checkEmail as checkEmailMutation } from 'Schemas/Profile';
import { GraphQLWrap, ResponseUser, UserCreateInput } from 'Types/response.type';
import { ErrorMessage, InputStatus } from 'UI/Form/Input';
import { routePush } from 'Components/Link';
import debounce from 'lodash/debounce';
import {
	GA_CATEGORY_SIGNUP,
	GA_CHANGE_STEP,
	GA_REPEAT_PASSWORD_FEATURE,
	GA_SUBMIT,
	LOGIN_MAX_LENGTH,
	LOGIN_MIN_LENGTH,
	NAME_MAX_LENGTH,
	PASSWORD_MAX_LENGTH,
	PASSWORD_MIN_LENGTH,
} from 'Utils/constants';
import LED from 'UI/LED/LED';
import { errorContext } from 'Contexts/error.context';
import { appContext } from 'Contexts/app.context';
import { loaderContext } from 'Contexts/loader.context';
import Dummy from 'UI/Dummy/Dummy';
import ReactGA from 'react-ga';
import { useQueryTiming } from 'Hooks/ga.hook';
import { useSecureMutation } from 'Utils/apollo';

const SIGNUP_TASK = 'SIGNUP_TASK';
const WAIT_BEFORE_CHECK = 1000;
const LOGIN_ICON = 'Sign_up__Sign_in/Icon_login_turdpe';
const NAME_ICON = 'Sign_up__Sign_in/Icon_name_2_ybdsbm';
const EMAIL_ICON = 'Sign_up__Sign_in/Icon_login_turdpe';
const PASSWORD_ICON = 'Sign_up__Sign_in/Icon_password_fnkt7a';
const REPEAT_ICON = 'Sign_up__Sign_in/Icon_password_fnkt7a';

const Center = styled.div`
	cursor: pointer;
	display: flex;
	align-items: center;
	margin-top: 20px;
	place-content: flex-end;
	user-select: none;

	${LED} {
		margin-top: 11px;
	}
`;

const LowerWrap = styled.div`
	position: relative;
	margin: 0 auto;
	width: 100%;
	max-width: ${({ theme }) => theme.other.containerMaxWidth};
	z-index: 1;
`;

const TITLE_WIDTH = 180;

const Wrap = styled(LowerWrap)`
	padding: ${({ theme }) => theme.other.signPagesPadding};

	&:before,
	&:after {
		content: '';
		position: absolute;
		top: 0;
		height: 100%;
		background-color: rgba(25, 31, 33, 0.54);
		border-radius: 4px;
		z-index: 0;
	}

	&:before {
		left: -100%;
		width: calc(100% + ${TITLE_WIDTH + 30}px);
		border-top-left-radius: 0;
		border-bottom-left-radius: 0;
		margin-right: 100px;
	}

	&:after {
		left: ${TITLE_WIDTH + 120}px;
		width: 100vw;
		border-top-right-radius: 0;
		border-bottom-right-radius: 0;
	}
`;

const Upper = styled.div`
	position: relative;
	z-index: 1;
`;

const Label = styled.label`
	display: flex;
	width: 100%;
	align-items: center;

	&:not(:first-child) {
		margin-top: 42px;
	}
`;

const StyledTitle = styled(Title)`
	display: flex;
	justify-content: center;
	margin: 0 66px 10px 32px;
	width: ${TITLE_WIDTH}px;
`;

const ButtonWrapper = styled.div`
	display: flex;
	width: 100%;
	place-content: flex-end;
	margin-top: 27px;
`;

const Container = styled.div`
	position: relative;
	top: 0;
	margin: 0 auto;
	padding: ${({ theme }) => theme.other.signPagesPadding};
	color: white;

	${PrevChildren} {
		position: relative;
		top: 0;
		left: 0;
		width: 100%;
		margin-bottom: 10px;

		${Upper} {
			display: flex;
			flex-direction: column;
		}

		${LowerWrap} {
			display: flex;
			flex-direction: row;
			place-content: flex-end;
		}

		${Wrap} {
			padding: 0;
			align-items: flex-end;

			&:before,
			&:after {
				display: none;
			}
		}

		${StyledTitle} {
			display: none;
		}

		${Label} {
			margin-top: 0;
		}
	}
`;

const StyledForm = styled(Form)`
	overflow: hidden;
`;

const Signup: React.FC = () => {
	const [isRepeatAccepted, setIsRepeatAccepted] = useReducer<(value: boolean) => boolean>(
		(value: boolean) => !value,
		false,
	);
	const [checkLoginDebounce, setCheckLoginDebounce] = useState<() => void>(() => () => {
		return;
	});
	const [checkEmailDebounce, setCheckEmailDebounce] = useState<() => void>(() => () => {
		return;
	});
	const [login, setLogin] = useState<string>('');
	const [name, setName] = useState<string>('');
	const [email, setEmail] = useState<string>('');
	const [password, setPassword] = useState<string>('');
	const [repeat, setRepeat] = useState<string>('');
	const { startTiming, endTiming } = useQueryTiming({
		category: GA_CATEGORY_SIGNUP,
		variable: GA_SUBMIT,
		label: 'total time spent for registration',
	});

	const { setError } = useContext(errorContext);
	const { user } = useContext(appContext);
	const { incrementProgress, clearProgress } = useContext(loaderContext);

	const [signup, { error }] = useSecureMutation<GraphQLWrap<'signup', ResponseUser>, { newValue: UserCreateInput }>(
		signupMutation,
		{
			variables: {
				newValue: {
					login,
					name,
					email,
					password,
				},
			},
		},
	);
	const [checkLogin, { data: checkLoginData, loading: checkLoginLoading }] = useSecureMutation<
		GraphQLWrap<'checkLogin', boolean>,
		{
			login: string;
		}
	>(checkLoginMutation, {
		variables: {
			login,
		},
	});
	const [checkEmail, { data: checkEmailData, loading: checkEmailLoading }] = useSecureMutation<
		GraphQLWrap<'checkEmail', boolean>,
		{
			email: string;
		}
	>(checkEmailMutation, {
		variables: {
			email,
		},
	});

	const loginAvailable = checkLoginData === undefined ? true : checkLoginData.checkLogin;
	const emailAvailable = checkEmailData === undefined ? true : checkEmailData.checkEmail;

	useEffect(() => {
		startTiming();
	}, [startTiming]);

	useEffect(() => {
		setCheckLoginDebounce(() => debounce(checkLogin, WAIT_BEFORE_CHECK));
	}, [checkLogin]);

	useEffect(() => {
		setCheckEmailDebounce(() => debounce(checkEmail, WAIT_BEFORE_CHECK));
	}, [checkEmail]);

	const changeLogin = useCallback(
		(value) => {
			setLogin(value);
			checkLoginDebounce();
		},
		[checkLoginDebounce],
	);

	const changeEmail = useCallback(
		(value) => {
			setEmail(value);
			checkEmailDebounce();
		},
		[checkEmailDebounce],
	);

	const repeatValidation = useCallback((value: string): boolean => value === password, [password]);

	const apply = useCallback(
		(event: FormEvent) => {
			grecaptcha.ready(() => {
				void grecaptcha.execute(config.google.recaptcha.siteKey, { action: 'signup' }).then(async () => {
					if (isRepeatAccepted) {
						ReactGA.event({
							category: GA_CATEGORY_SIGNUP,
							action: GA_REPEAT_PASSWORD_FEATURE,
						});
					}
					ReactGA.event({
						category: GA_CATEGORY_SIGNUP,
						action: GA_SUBMIT,
					});
					endTiming();

					event.preventDefault();
					incrementProgress(SIGNUP_TASK);

					await signup().catch((err: Error) => {
						setError(err.message);
					});

					clearProgress(SIGNUP_TASK);
					void routePush(
						{
							pathname: '/signin',
							query: {
								message: d('signin.messages.afterSignup'),
							},
						},
						{
							pathname: '/signin',
							query: {},
						},
					);
				});
			});
		},
		[incrementProgress, setError, clearProgress, signup, endTiming, isRepeatAccepted],
	);

	const changeStep = useCallback((step: number) => {
		ReactGA.event({
			category: GA_CATEGORY_SIGNUP,
			action: GA_CHANGE_STEP,
			value: step,
		});
	}, []);

	useEffect(() => {
		if (isRepeatAccepted) {
			setRepeat(password);
		}
	}, [isRepeatAccepted, password]);

	useEffect(() => {
		if (user !== null) {
			void routePush({
				pathname: '/404',
				query: {},
			});
		}
	}, [user]);

	return (
		<Container>
			<StyledForm onSubmit={apply} onChangeStep={changeStep}>
				<Step>
					<Wrap>
						<Upper>
							<Label>
								<StyledTitle>{d('signup.form.login.value')}</StyledTitle>
								<Input
									iconSrc={LOGIN_ICON}
									value={login}
									name="login"
									backgroundType={0}
									status={checkLoginLoading ? InputStatus.loading : InputStatus.idle}
									error={!loginAvailable ? d('signup.form.login.errorMessage.occupied') : undefined}
									validation={{
										required: {
											value: true,
											errorMessage: d('signup.form.login.errorMessage.required'),
										},
										minLength: {
											value: LOGIN_MIN_LENGTH,
											errorMessage: d('signup.form.login.errorMessage.minLength', LOGIN_MIN_LENGTH),
										},
										maxLength: {
											value: LOGIN_MAX_LENGTH,
											errorMessage: d('signup.form.login.errorMessage.maxLength', LOGIN_MAX_LENGTH),
										},
									}}
									textMemory={true}
									onChange={changeLogin}
								/>
							</Label>
							<Label>
								<StyledTitle>{d('signup.form.name.value')}</StyledTitle>
								<Input
									iconSrc={NAME_ICON}
									value={name}
									name="name"
									backgroundType={1}
									validation={{
										required: {
											value: true,
											errorMessage: d('signup.form.name.errorMessage.required'),
										},
										maxLength: {
											value: NAME_MAX_LENGTH,
											errorMessage: d('signup.form.name.errorMessage.maxLength', NAME_MAX_LENGTH),
										},
									}}
									textMemory={true}
									onChange={setName}
								/>
							</Label>
							<Label>
								<StyledTitle>{d('signup.form.email.value')}</StyledTitle>
								<Input
									iconSrc={EMAIL_ICON}
									value={email}
									name="email"
									backgroundType={1}
									status={checkEmailLoading ? InputStatus.loading : InputStatus.idle}
									error={!emailAvailable ? d('signup.form.email.errorMessage.occupied') : undefined}
									validation={{
										required: {
											value: true,
											errorMessage: d('signup.form.email.errorMessage.required'),
										},
										match: {
											// eslint-disable-next-line no-control-regex
											value: /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)])/,
											errorMessage: d('signup.form.email.errorMessage.match'),
										},
									}}
									textMemory={true}
									onChange={changeEmail}
								/>
							</Label>
						</Upper>
					</Wrap>
				</Step>
				<Step>
					<Wrap>
						<Upper>
							{error !== undefined && error !== null && <ErrorMessage>{error.message}</ErrorMessage>}
							<Label>
								<StyledTitle>{d('signup.form.password.value')}</StyledTitle>
								<Input
									iconSrc={PASSWORD_ICON}
									value={password}
									type="password"
									name="password"
									validation={{
										required: {
											value: true,
											errorMessage: d('signup.form.password.errorMessage.required'),
										},
										minLength: {
											value: PASSWORD_MIN_LENGTH,
											errorMessage: d('signup.form.password.errorMessage.minLength', PASSWORD_MIN_LENGTH),
										},
										maxLength: {
											value: PASSWORD_MAX_LENGTH,
											errorMessage: d('signup.form.password.errorMessage.maxLength', PASSWORD_MAX_LENGTH),
										},
									}}
									onChange={setPassword}
								/>
							</Label>
							<Label>
								<StyledTitle>{d('signup.form.repeat.value')}</StyledTitle>
								<Input
									iconSrc={REPEAT_ICON}
									value={repeat}
									type="password"
									name="repeat"
									backgroundType={1}
									validation={{
										callback: {
											value: repeatValidation,
											errorMessage: d('signup.form.repeat.errorMessage.match'),
										},
									}}
									onChange={setRepeat}
								/>
							</Label>
							<Center onClick={setIsRepeatAccepted}>
								<span>{d('signup.form.repeatAccept')}</span>
								<LED on={isRepeatAccepted} />
							</Center>
						</Upper>
					</Wrap>
				</Step>
				<LowerWrap>
					<ButtonWrapper>
						<Dummy loading={SIGNUP_TASK} transparent={true}>
							<Button>{d('signup.form.button')}</Button>
						</Dummy>
					</ButtonWrapper>
				</LowerWrap>
			</StyledForm>
		</Container>
	);
};

export default Signup;
