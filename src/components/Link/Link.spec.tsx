import { appContext } from 'Contexts/app.context';
import { render } from 'enzyme';
import * as React from 'react';
import Link from './Link';

jest.mock('next/router', () => {
	// eslint-disable-next-line @typescript-eslint/no-unsafe-return
	return {
		...jest.requireActual('../../__mocks__/next.mock'),
	};
});

describe('Component Link tests', () => {
	const lang = 'en-us';
	const page = 'index';
	const location = {
		pathname: '/',
		query: {},
	};

	it('builds valid link', () => {
		const pathname = '/pathname';

		expect(
			render(
				<appContext.Provider
					value={{
						page,
						lang,
						location,
						user: null,
						loading: false,
					}}
				>
					<Link
						location={{
							pathname,
							query: {},
						}}
					/>
				</appContext.Provider>,
			).find('a')[0].attribs.href,
		).toEqual(pathname);
	});

	it('builds valid link with specified query', () => {
		const pathname = '/pathname';

		expect(
			render(
				<appContext.Provider
					value={{
						page,
						lang,
						location,
						user: null,
						loading: false,
					}}
				>
					<Link
						location={{
							pathname,
							query: { foo: 'bar' },
						}}
					/>
				</appContext.Provider>,
			).find('a')[0].attribs.href,
		).toEqual(`${pathname}?foo=bar`);
	});
});
