import { Location } from 'Types/app.type';
import NextLink from 'next/link';
import Router from 'next/router';
import React from 'react';
import styled from 'styled-components';

interface Props {
	location: Partial<Location>;
	asLocation?: Partial<Location>;
	prefetch?: boolean;
	replace?: boolean;
	ariaLabel?: string;
	className?: string;
	children?: React.ReactNode;
	onClick?: () => void;
}

const Container = styled.div`
	height: 100%;

	a {
		color: inherit;
		text-decoration: none;
	}
`;

const Link: React.FC<Props> = ({
	location,
	asLocation,
	prefetch,
	replace,
	ariaLabel,
	className,
	children,
	onClick,
}) => {
	return (
		<Container>
			<NextLink
				href={location}
				as={{
					...(asLocation ?? location),
					pathname: `${asLocation?.pathname ?? location?.pathname ?? '/'}`,
				}}
				prefetch={prefetch}
				replace={replace}
			>
				<a className={className} aria-label={ariaLabel} onClick={onClick}>
					{children}
				</a>
			</NextLink>
		</Container>
	);
};

export default styled(Link)``;

export const routePush = async (location: Location, as?: Location, options?: object): Promise<void> => {
	if (as === undefined) {
		as = location;
	}

	await Router.push(location, as, options);
};

export const routeReplace = async (location: Location, as?: Location, options?: object): Promise<void> => {
	if (as === undefined) {
		as = location;
	}

	await Router.replace(location, as, options);
};
