import React from 'react';
import styled from 'styled-components';
import Spoiler, { Area } from 'UI/Spoiler';
import { d, wrap } from 'Utils/dictionary';
import Link from 'Components/Link/Link';

const Container = styled.div`
	position: relative;
	top: 0;
	color: white;
	padding: 40px;
	font-size: 1rem;

	&:after {
		content: '';
		position: absolute;
		top: 0;
		right: 0;
		width: 100%;
		height: 100%;
		background-color: rgba(25, 31, 33, 0.54);
		background-attachment: fixed;
		border-radius: 4px;
		z-index: 0;
	}
`;

const Wrap = styled.div`
	display: flex;
	flex-direction: column;
	position: relative;
	margin: 0 auto;
	width: 100%;
	max-width: ${({ theme }) => theme.other.containerMaxWidth};
	z-index: 1;

	${Spoiler} {
		margin-bottom: 5px;
	}

	${Area} {
		div {
			display: inline-block;
		}
	}
`;

const FAQ: React.FC = () => {
	return (
		<Container>
			<Wrap>
				<PreparedSpoiler
					title={d('faq.howToFindBandOrMusician.title')}
					description={d('faq.howToFindBandOrMusician.description')}
				/>
				<PreparedSpoiler title={d('faq.whyDoINeedRating.title')} description={d('faq.whyDoINeedRating.description')} />
				<PreparedSpoiler
					title={d('faq.whatIsTheFestival.title')}
					description={d('faq.whatIsTheFestival.description')}
				/>
				<PreparedSpoiler title={d('faq.howToEditProfile.title')} description={d('faq.howToEditProfile.description')} />
			</Wrap>
		</Container>
	);
};

export default FAQ;

interface PreparedSpoilerProps {
	title: string;
	description: string;
}

const StyledLink = styled(Link)`
	display: inline-block;
	color: dodgerblue !important;
	text-decoration: underline !important;
`;

const PreparedSpoiler: React.FC<PreparedSpoilerProps> = ({ title, description }) => {
	return (
		<Spoiler title={title}>
			{wrap(description, (text, context, index) => {
				switch (context.type) {
					case 'anchor':
						return (
							<StyledLink
								key={index}
								location={{
									pathname: context.href,
								}}
							>
								{text}
							</StyledLink>
						);

					case 'newLine':
						return <br key={index} />;

					default:
						return <span key={index}>{text}</span>;
				}
			})}
		</Spoiler>
	);
};
