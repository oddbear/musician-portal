import React from 'react';
import styled from 'styled-components';
import { d, wrap } from 'Utils/dictionary';

const Container = styled.div`
	position: relative;
	top: 0;
	color: white;
	padding: 40px;
	font-size: 1rem;

	&:after {
		content: '';
		position: absolute;
		top: 0;
		right: 0;
		width: 100%;
		height: 100%;
		background-color: rgba(25, 31, 33, 0.54);
		background-attachment: fixed;
		border-radius: 4px;
		z-index: 0;
	}
`;

const Wrap = styled.div`
	display: flex;
	flex-direction: column;
	position: relative;
	margin: 0 auto;
	width: 100%;
	max-width: ${({ theme }) => theme.other.containerMaxWidth};
	z-index: 1;
`;

const Anchor = styled.a`
	color: dodgerblue;
	text-decoration: underline;
`;

const Contacts: React.FC = () => {
	return (
		<Container>
			<Wrap>
				<div>
					{wrap(d('contacts.writeToSupport'), (text, context, index) => {
						switch (context.type) {
							case 'anchor':
								return (
									<Anchor key={index} href={context.href}>
										{text}
									</Anchor>
								);

							default:
								return <span key={index}>{text}</span>;
						}
					})}
				</div>
			</Wrap>
		</Container>
	);
};

export default Contacts;
