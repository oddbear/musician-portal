import { useLazyLoad } from 'Hooks/lazyload.hook';
import { useWindowSize } from 'Hooks/window-size.hook';
import React, { CSSProperties, MouseEvent, RefObject, useEffect, useRef, useState } from 'react';
import styled from 'styled-components';

export interface Props {
	ratio: number;
	src: string;
	fitHeight?: boolean;
	className?: string;
	style?: CSSProperties;
	children?: React.ReactNode;
	title?: string;
	getRef?(ref: RefObject<HTMLDivElement>): void;
	onClick?(): void;
	onMouseDown?(event: MouseEvent): void;
	onMouseMove?(event: MouseEvent): void;
}

const getCssSize = (size?: number, fitHeight?: boolean): string => {
	if (size === undefined) {
		return '';
	}

	return `${fitHeight === true ? 'width' : 'height'}: ${size.toString()}px`;
};

export interface ContainerProps {
	src: string;
	size: number;
	fitHeight?: boolean;
}

const Container = styled.div<ContainerProps>`
	position: relative;
	background: url(${({ src }) => src}) no-repeat center;
	background-size: cover;
	${({ size, fitHeight }) => getCssSize(size, fitHeight)}
`;

const FullsizeBackground: React.FC<Props> = (props) => {
	const containerRef = useRef<HTMLDivElement>(null);
	const { src } = useLazyLoad({
		src: props.src,
	});
	const [containerSize, setContainerSize] = useState<number>(0);
	const [width, height] = useWindowSize();

	useEffect(() => {
		if (containerRef.current !== null) {
			if (props.fitHeight !== undefined) {
				setContainerSize(containerRef.current.offsetHeight / props.ratio);
			} else {
				setContainerSize(props.ratio * containerRef.current.offsetWidth);
			}
		}
	}, [props.ratio, props.fitHeight, width, height]);

	useEffect(() => {
		props.getRef?.(containerRef);
	}, [containerRef, props.getRef]);

	return (
		<Container
			fitHeight={props.fitHeight}
			className={props.className}
			ref={containerRef}
			size={containerSize}
			src={src}
			style={props.style}
			title={props.title}
			onClick={props.onClick}
			onMouseDown={props.onMouseDown}
			onMouseMove={props.onMouseMove}
		>
			{props.children}
		</Container>
	);
};

FullsizeBackground.defaultProps = {
	getRef() {
		return;
	},
};

export default FullsizeBackground;
