import React, { useCallback, useContext, useEffect, useRef, useState } from 'react';
import {
	CommentCreateInput,
	GraphQLWrap,
	PostCreateInput,
	Response,
	ResponseComment,
	ResponseEmotion,
	ResponsePost,
	ResponseProfile,
} from 'Types/response.type';
import styled from 'styled-components';
import { d } from 'Utils/dictionary';
import Dummy from 'UI/Dummy/Dummy';
import { useQuery } from '@apollo/react-hooks';
import {
	addBandComment as addBandCommentMutation,
	addBandPost as addBandPostMutation,
	getComments as getCommentsMutation,
	subscribeForPost,
} from 'Schemas/Post';
import { getPosts } from 'Schemas/Post';
import { loaderContext } from 'Contexts/loader.context';
import { errorContext } from 'Contexts/error.context';
import { useLayoutEffect } from 'Hooks/effect.hook';
import { COMMENTS_LIMIT, POST_VISIBLE_ROWS, POSTS_REFRESH_DELAY, PROFILES_LIMIT } from 'Utils/constants';
import {
	addCommentEmotion as addCommentEmotionMutation,
	addPostEmotion as addPostEmotionMutation,
	getEmotions,
} from 'Schemas/Emotion';
import Post from 'Components/Post';
import { useGetRef } from 'Hooks/getRef.hook';
import { appContext } from 'Contexts/app.context';
import { tooltipContext } from 'Contexts/tooltip.context';
import { getProfiles as getProfilesMutation } from 'Schemas/Profile';
import debounce from 'lodash/debounce';
import { usePrevious } from 'Hooks/previous.hook';
import { MessageWrap as PostWrap } from 'Components/Post';
import { MessageWrap as CommentWrap } from 'Components/Post/Comments';
import BlogTooltip, { BlogTooltipContext } from './BlogTooltip';
import EditForm from 'Components/Blog/EditForm';
import Modal from 'UI/Modal';
import LazyImage from 'Components/LazyImage';
import { useSecureMutation } from 'Utils/apollo';

const POST_UPLOADING_TASK = 'POST_UPLOADING_TASK';
const POSTS_LOADING_TASK = 'POSTS_LOADING_TASK';
const DELAY_PROFILE_LIST = 500;

interface Props {
	editable: boolean;
	postQueryVariables: {
		_id?: string;
		alias?: string;
		limit: number;
		visibleData: string[];
	};
}

const Container = styled.div`
	width: 100%;
	background-color: white;
	margin-top: 20px;
	padding: 0 20px;
	border-radius: 4px;
	box-sizing: border-box;
	font-size: 1rem;
	font-family: ${({ theme }) => theme.fontFamily.quiet};
	color: black;
	text-transform: initial;

	input[type='file'] {
		display: none;
	}
`;

interface BlogWrapProps {
	maxHeight: number;
}

const BlogWrap = styled.div<BlogWrapProps>`
	position: relative;
	font-size: 0.8em;
	display: flex;
	align-items: center;
	flex-direction: column;
	width: 100%;
	padding: 10px 0;
	margin: 10px 0;

	${PostWrap}, ${CommentWrap} {
		max-height: ${({ maxHeight }) => maxHeight}px;
	}
`;

const Empty = styled.span`
	font-size: 1.5em;
	color: gray;
	padding: 40px 0;
	font-family: ${({ theme }) => theme.fontFamily.aggressive};
`;

const LoadMore = styled.div`
	cursor: pointer;
	display: flex;
	align-items: center;
	place-content: center;
	height: 60px;
	margin: 20px 0;
	color: #555;
	width: 100%;
	border-top: 1px solid #bbb;
	border-bottom: 1px solid #bbb;
`;

interface FullsizeImageProps {
	inWidth: boolean;
}

const FullsizeImage = styled(LazyImage)<FullsizeImageProps>`
	${({ inWidth }) => (inWidth ? 'height: 80vh' : 'width: 80vw')};
`;

const subscriptionState: {
	responsePosts: Response<ResponsePost>;
} = {
	responsePosts: {
		totalLength: 0,
		data: [],
	},
};

const Blog: React.FC<Props> = ({ editable, postQueryVariables }) => {
	const [showRefresh, setShowRefresh] = useState<boolean>(false);
	const [attachedElement, setAttachedElement] = useState<HTMLElement | null>(null);
	const [tooltipVisible, setTooltipVisible] = useState<boolean>(false);
	const [context, setContext] = useState<BlogTooltipContext>({});
	const [profileList, setProfileList] = useState<ResponseProfile[]>([]);
	const [imagePreview, setImagePreview] = useState<null | string>(null);
	const containerRef = useRef<HTMLDivElement>(null);
	const loadMoreRef = useGetRef<HTMLDivElement>();
	const imagePreviewRef = useGetRef<HTMLImageElement>();

	const { user } = useContext(appContext);
	const { clearProgress, incrementProgress, isLoading } = useContext(loaderContext);
	const { setError } = useContext(errorContext);

	const changeImagePreview = useCallback(
		(value: null | string) => () => {
			setImagePreview(value);
		},
		[],
	);

	const { data: emotionsData } = useQuery<GraphQLWrap<'emotions', ResponseEmotion[]>>(getEmotions);
	const emotions = emotionsData?.emotions ?? [];

	const { data: postsData, loading, fetchMore, subscribeToMore } = useQuery<
		GraphQLWrap<'responsePosts', Response<ResponsePost>>,
		{ alias?: string; limit: number; visibleData: string[] }
	>(getPosts, {
		ssr: false,
		variables: postQueryVariables,
	});
	const posts = postsData?.responsePosts ?? { totalLength: 0, data: [] };
	subscriptionState.responsePosts = posts;

	const [addBandPost] = useSecureMutation<GraphQLWrap<'addBandPost', ResponsePost>, PostCreateInput>(
		addBandPostMutation,
	);

	const [addBandComment] = useSecureMutation<
		GraphQLWrap<'addBandComment', Response<ResponseComment>>,
		CommentCreateInput
	>(addBandCommentMutation);

	const addComment = useCallback(
		async (input: CommentCreateInput) => {
			const { postId } = input;

			await addBandComment({
				variables: input,
				update: (cache, { data }) => {
					const oldData = cache.readQuery<GraphQLWrap<'responsePosts', Response<ResponsePost>>>({
						query: getPosts,
						variables: postQueryVariables,
					});

					if (oldData === null || data === null || data === undefined) {
						return;
					}

					cache.writeQuery<GraphQLWrap<'responsePosts', Response<ResponsePost>>>({
						query: getPosts,
						variables: postQueryVariables,
						data: {
							responsePosts: {
								...oldData.responsePosts,
								data: oldData.responsePosts.data.map((post) =>
									post._id === postId
										? {
												...post,
												comments: {
													...post.comments,
													totalLength: data.addBandComment.totalLength,
													data: [...post.comments.data, ...data.addBandComment.data],
												},
										  }
										: post,
								),
							},
						},
					});
				},
			});
		},
		[addBandComment, postQueryVariables],
	);

	const loadData = useCallback(async () => {
		if (loading || isLoading(POSTS_LOADING_TASK) !== false) {
			return;
		}

		incrementProgress(POSTS_LOADING_TASK);

		try {
			await fetchMore({
				variables: {
					...postQueryVariables,
					visibleData: posts.data.map(({ _id }) => _id),
				},
				updateQuery: (prev, { fetchMoreResult }) => {
					if (fetchMoreResult === undefined) {
						return prev;
					}

					return {
						...fetchMoreResult,
						responsePosts: {
							...fetchMoreResult.responsePosts,
							data: [...prev.responsePosts.data, ...fetchMoreResult.responsePosts.data],
						},
					};
				},
			}).catch((err: Error) => setError(err.message));

			clearProgress(POSTS_LOADING_TASK);

			setShowRefresh(false);
			setTimeout(() => {
				setShowRefresh(true);
			}, POSTS_REFRESH_DELAY);
		} catch (err) {
			console.log('refresh browser to continue receive posts');
		}
	}, [isLoading, loading, setError, incrementProgress, clearProgress, postQueryVariables, fetchMore, posts]);

	const onScroll = useCallback(() => {
		if (typeof window === 'undefined' || containerRef.current === null || loadMoreRef.current === null) {
			return;
		}

		const $container = document.getElementById('container');
		if ($container === null) {
			return;
		}

		const loadMore = loadMoreRef.current;
		if (
			isLoading(POSTS_LOADING_TASK) === false &&
			!loading &&
			loadMore.getBoundingClientRect().top - $container.offsetHeight - 500 < 0
		) {
			void loadData();
		}
	}, [isLoading, loading, loadData, containerRef, loadMoreRef]);

	useLayoutEffect(() => {
		if (typeof window === 'undefined' || containerRef.current === null || loadMoreRef.current === null) {
			return;
		}

		const $container = document.getElementById('container');
		if ($container === null) {
			return;
		}

		const loadMore = loadMoreRef.current;
		if (
			isLoading(POSTS_LOADING_TASK) === false &&
			!loading &&
			loadMore.getBoundingClientRect().top - $container.offsetHeight - 500 < 0
		) {
			onScroll();
		}

		$container.addEventListener('scroll', onScroll);
		return () => $container.removeEventListener('scroll', onScroll);
	}, [isLoading, loading, onScroll, loadMoreRef]);

	useEffect(() => {
		setTimeout(() => {
			setShowRefresh(true);
		}, POSTS_REFRESH_DELAY);
	}, []);

	const [addPostEmotion] = useSecureMutation<
		GraphQLWrap<'addPostEmotion', boolean | undefined>,
		{ alias: string; postId: string }
	>(addPostEmotionMutation);

	const [addCommentEmotion] = useSecureMutation<
		GraphQLWrap<'addCommentEmotion', boolean | undefined>,
		{ alias: string; commentId: string }
	>(addCommentEmotionMutation);

	const addEmotion = useCallback(
		async (_id: string, emotionAlias: string, isPost?: boolean) => {
			if (isPost === true) {
				await addPostEmotion({
					variables: {
						alias: emotionAlias,
						postId: _id,
					},
				}).catch((err: Error) => setError(err.message));
			} else {
				await addCommentEmotion({
					variables: {
						alias: emotionAlias,
						commentId: _id,
					},
				}).catch((err: Error) => setError(err.message));
			}
		},
		[addCommentEmotion, addPostEmotion, setError],
	);

	const [getComments] = useSecureMutation<
		GraphQLWrap<'getComments', Response<ResponseComment>>,
		{
			postId: string;
			limit: number;
			visibleData: string[];
		}
	>(getCommentsMutation, {});

	const showMoreComments = useCallback(
		async (post: ResponsePost) => {
			const postId = post._id;

			await getComments({
				variables: {
					postId,
					limit: COMMENTS_LIMIT,
					visibleData: post.comments.data.map(({ _id }) => _id),
				},
				update: (cache, { data }) => {
					const oldData = cache.readQuery<GraphQLWrap<'responsePosts', Response<ResponsePost>>>({
						query: getPosts,
						variables: postQueryVariables,
					});

					if (oldData === null || data === null || data === undefined) {
						return;
					}

					cache.writeQuery<GraphQLWrap<'responsePosts', Response<ResponsePost>>>({
						query: getPosts,
						variables: postQueryVariables,
						data: {
							responsePosts: {
								...oldData.responsePosts,
								data: oldData.responsePosts.data.map((oldPost) =>
									oldPost._id === postId
										? {
												...oldPost,
												comments: {
													...oldPost.comments,
													totalLength: data.getComments.totalLength,
													data: [...oldPost.comments.data, ...data.getComments.data],
												},
										  }
										: oldPost,
								),
							},
						},
					});
				},
			}).catch((err: Error) => setError(err.message));
		},
		[postQueryVariables, getComments, setError],
	);

	const scrollTo = useCallback((element: HTMLDivElement) => {
		const $container = document.getElementById('container');
		if ($container === null) {
			return;
		}

		const offsetHeight = element.offsetHeight;
		let offsetTop = 0;
		let currentElement: HTMLElement | null = element;
		while (currentElement !== null && currentElement !== $container) {
			if (['relative', 'absolute', 'fixed'].includes(getComputedStyle(currentElement).position)) {
				offsetTop += currentElement.offsetTop;
			}
			currentElement = currentElement.parentElement;
		}
		$container.scrollTop = offsetTop + offsetHeight - $container.offsetHeight;
	}, []);

	const submit = useCallback(
		(text: string, images: Array<File | Blob>, video?: string): Promise<void> =>
			new Promise((res) => {
				if (postQueryVariables.alias !== undefined) {
					void addBandPost({
						variables: {
							text: text === '' ? undefined : text,
							images,
							video,
							alias: postQueryVariables.alias,
						},
						update: (cache, { data }) => {
							const oldData = cache.readQuery<
								GraphQLWrap<'responsePosts', Response<ResponsePost>>,
								{ alias: string; limit: number; visibleData: string[] }
							>({
								query: getPosts,
								variables: {
									...postQueryVariables,
									alias: postQueryVariables.alias!,
								},
							});

							if (oldData === null || data === null || data === undefined) {
								return;
							}

							cache.writeQuery<
								GraphQLWrap<'responsePosts', Response<ResponsePost>>,
								{ alias: string; limit: number; visibleData: string[] }
							>({
								query: getPosts,
								variables: {
									...postQueryVariables,
									alias: postQueryVariables.alias!,
								},
								data: {
									responsePosts: {
										...oldData.responsePosts,
										totalLength: oldData.responsePosts.totalLength + 1,
										data: [data.addBandPost, ...oldData.responsePosts.data],
									},
								},
							});
						},
					})
						.then(() => res())
						.catch((err: Error) => {
							setError(err.message);
							clearProgress(POST_UPLOADING_TASK);
						});
				}
			}),
		[setError, addBandPost, clearProgress, postQueryVariables],
	);

	const changeAttachedElement = useCallback(
		(element: HTMLElement | null) => {
			setAttachedElement(element);
			setTooltipVisible(element === attachedElement ? !tooltipVisible : true);
		},
		[attachedElement, tooltipVisible],
	);

	const [getProfiles, { loading: profileListLoading }] = useSecureMutation<
		GraphQLWrap<'getProfiles', ResponseProfile[]>,
		{
			name?: string;
			limit: number;
		}
	>(getProfilesMutation, {
		variables: {
			name: context.mention?.value,
			limit: PROFILES_LIMIT,
		},
		update: (cache, { data }) => {
			if (data === null || data === undefined) {
				return;
			}
			setProfileList(data.getProfiles);
		},
	});

	const loadProfileList = useCallback(async () => {
		await getProfiles().catch((err: Error) => setError(err.message));
	}, [getProfiles, setError]);

	const debounceGetProfiles = useRef(
		debounce(() => {
			void loadProfileList();
		}, DELAY_PROFILE_LIST),
	);

	const prevMention = usePrevious(context.mention);
	useEffect(() => {
		if (prevMention !== context.mention) {
			if (prevMention === undefined || context.mention?.debounce !== true) {
				void loadProfileList();
			} else {
				void debounceGetProfiles.current();
			}
		}
	}, [prevMention, context.mention, loadProfileList]);

	const onPostInit = useCallback(
		(_id: string) => {
			try {
				subscribeToMore<GraphQLWrap<'newComment', ResponseComment>, { postId: string }>({
					document: subscribeForPost,
					variables: {
						postId: _id,
					},
					updateQuery: (prev, { subscriptionData }) => {
						if (subscriptionData.data === undefined) {
							return prev;
						}

						// const commentData = subscriptionData.data.newComment;
						return {
							responsePosts: {
								...subscriptionState.responsePosts,
								data: subscriptionState.responsePosts.data.map((post) =>
									post._id === _id
										? {
												...post,
												comments: {
													...post.comments,
													totalLength: post.comments.totalLength + 1,
												},
										  }
										: post,
								),
							},
						};
					},
				});
			} catch (err) {
				console.log('refresh browser to continue comments subscription flow');
			}
		},
		[subscribeToMore],
	);

	const fitWidthImagePreview =
		(imagePreviewRef.current?.naturalWidth ?? 0) < (imagePreviewRef.current?.naturalHeight ?? 0);

	return (
		<tooltipContext.Provider
			value={{
				attachedElement,
				visible: tooltipVisible,
				setAttachedElement: changeAttachedElement,
				setVisible: setTooltipVisible,
				setContext,
			}}
		>
			<Modal isOpen={imagePreview !== null} onClose={changeImagePreview(null)}>
				{imagePreview !== null && (
					<FullsizeImage
						src={imagePreview}
						alt="image preview"
						inWidth={fitWidthImagePreview}
						fitHeight={!fitWidthImagePreview}
						getRef={imagePreviewRef}
					/>
				)}
			</Modal>
			<BlogTooltip
				context={context}
				postQueryVariables={postQueryVariables}
				profileList={profileList}
				profileListLoading={profileListLoading}
				emotions={emotions}
				onAddEmotion={addEmotion}
			/>
			<Container>
				{editable && (
					<EditForm
						emotions={emotions}
						taskName={POST_UPLOADING_TASK}
						imagePreview={imagePreview}
						onSubmit={submit}
						setImagePreview={setImagePreview}
					/>
				)}
				<Dummy loading={loading}>
					<BlogWrap
						ref={containerRef}
						maxHeight={
							containerRef.current === null
								? 0
								: parseInt(getComputedStyle(containerRef.current).fontSize, 10) * POST_VISIBLE_ROWS
						}
					>
						{postQueryVariables._id === undefined && showRefresh && (
							<LoadMore onClick={loadData}>
								<Dummy loading={POSTS_LOADING_TASK} transparent={true}>
									{d('blog.refresh')}
								</Dummy>
							</LoadMore>
						)}
						{posts.totalLength > 0 ? (
							<>
								{[...posts.data]
									.sort((a, b) => (a.createdAt > b.createdAt ? -1 : 1))
									.map((post) => (
										<Post
											key={post._id}
											emotions={emotions}
											isMyBand={
												user?.administeredBands.some((band) => band.alias === postQueryVariables.alias) === true
											}
											imagePreview={imagePreview}
											postQueryVariables={postQueryVariables}
											post={post}
											scrollTo={scrollTo}
											onAddEmotion={addEmotion}
											onAddComment={addComment}
											onShowMoreComments={showMoreComments}
											onSetImagePreview={setImagePreview}
											init={onPostInit}
										/>
									))}
								{posts.totalLength > posts.data.length && (
									<LoadMore ref={loadMoreRef}>
										<Dummy loading={loading || POSTS_LOADING_TASK} transparent={true}>
											{d('blog.more')}
										</Dummy>
									</LoadMore>
								)}
							</>
						) : (
							<Empty>{d('blog.empty')}</Empty>
						)}
					</BlogWrap>
				</Dummy>
			</Container>
		</tooltipContext.Provider>
	);
};

export default Blog;
