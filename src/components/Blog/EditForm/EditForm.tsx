import React, { ChangeEvent, ClipboardEvent, KeyboardEvent, useCallback, useContext, useEffect, useState } from 'react';
import Form, { CurChildren, Input } from 'UI/Form';
import { d } from 'Utils/dictionary';
import Button from 'UI/Button/Button';
import { POST_MAX_IMAGES_LENGTH } from 'Utils/constants';
import YouTube from 'react-youtube';
import styled from 'styled-components';
import Dummy from 'UI/Dummy/Dummy';
import { useGetRef } from 'Hooks/getRef.hook';
import { loaderContext } from 'Contexts/loader.context';
import { errorContext } from 'Contexts/error.context';
import Modal from 'UI/Modal';
import { unstable_trace as trace } from 'scheduler/tracing';
import { getMentionPositions, validateUrl } from 'Utils/regexp';
import { extractIdFromYoutubeUrl, verifyImageUrl, verifyYoutubeUrl } from 'Utils/url';
import { tooltipContext } from 'Contexts/tooltip.context';
import { setCookiesMemory } from 'UI/Form/Input';
import { convertImageUrlToBlob } from 'Utils/data';
import MessageArea from 'UI/MessageArea';
import { ResponseEmotion } from 'Types/response.type';

interface Props {
	emotions: ResponseEmotion[];
	taskName: string;
	onSubmit: (text: string, images: Array<File | Blob>, video?: string) => Promise<void>;
	imagePreview: string | null;
	setImagePreview: (src: string | null) => void;
	setTextHandler?: (handler: (src: string) => void) => void;
}

const StyledForm = styled(Form)`
	margin: 10px 0;
	width: 100%;

	${CurChildren} {
		display: flex;
		flex-direction: row;
	}
`;

const StyledDummy = styled(Dummy)`
	position: relative;
	display: flex;
	flex-direction: column;
	padding: 10px 0;

	&:after {
		content: '';
		position: absolute;
		left: 0;
		bottom: -4px;
		width: 100%;
		height: 4px;
		background: linear-gradient(to bottom, rgb(200, 200, 200), rgba(200, 200, 200, 0));
	}
`;

const FormatButton = styled.span`
	cursor: pointer;
	font-size: 0.8em;
	border-bottom: 1px solid;
	color: #666;
	margin-right: 6px;
`;

const PreviewImagesWrap = styled.div`
	display: flex;
	flex-direction: row;
	margin: 10px 0;

	&:empty {
		display: none;
	}
`;

const RelativeWrap = styled.div`
	position: relative;
`;

const RemoveButton = styled.div`
	cursor: pointer;
	display: flex;
	align-items: center;
	place-content: center;
	position: absolute;
	top: 0;
	right: 0;
	font-family: monospace;
	width: 15px;
	height: 15px;
	font-size: 0.7em;
	border: 2px solid;
	color: red;
	background-color: rgba(0, 0, 0, 0.2);
`;

const Image = styled.img`
	cursor: pointer;
	height: 100px;
	max-width: 400px;
	box-shadow: 0 0 5px -2px black;

	&:not(:first-child) {
		margin-left: 10px;
	}
`;

const PreviewVideoWrap = styled.div`
	width: 300px;
	height: ${300 * (360 / 640)}px;
	margin: 10px 10px 10px 0;

	& > div {
		height: 100%;
	}

	iframe {
		width: 100%;
		height: 100%;
	}

	&:empty {
		display: none;
	}
`;

const Row = styled.div`
	display: flex;
	flex-direction: row;
`;

const EditForm: React.FC<Props> = ({ emotions, taskName, onSubmit, setImagePreview, setTextHandler }) => {
	const [isOpenVideoModal, setIsOpenVideoModal] = useState<boolean>(false);
	const [text, setText] = useState<string>('');
	const [images, setImages] = useState<Array<File | Blob>>([]);
	const [video, setVideo] = useState<string | undefined>(undefined);
	const [tempVideo, setTempVideo] = useState<string>('');
	const [previewImages, setPreviewImages] = useState<string[]>([]);
	const textareaRef = useGetRef<HTMLTextAreaElement>();

	const { clearProgress, incrementProgress } = useContext(loaderContext);
	const { setError } = useContext(errorContext);
	const { setVisible, setAttachedElement, setContext, visible } = useContext(tooltipContext);

	useEffect(() => {
		setTextHandler?.(setText);
	}, [setTextHandler]);

	const openVideoModal = useCallback(() => {
		setIsOpenVideoModal(true);
	}, []);

	const cancelVideo = useCallback(() => {
		setTempVideo('');
		setIsOpenVideoModal(false);
	}, []);

	const acceptVideo = useCallback(() => {
		const matchedId = extractIdFromYoutubeUrl(tempVideo);
		if (matchedId !== null) {
			setVideo(matchedId[1]);
		} else {
			setError(d('blog.video.error'));
		}
		setTempVideo('');
		setIsOpenVideoModal(false);
	}, [setError, tempVideo]);

	const removePreviewImage = useCallback(
		(index: number) => () => {
			setPreviewImages(previewImages.filter((_, i) => i !== index));
			setImages(images.filter((_, i) => i !== index));
		},
		[previewImages, images],
	);

	const removeYoutube = useCallback(() => {
		setVideo(undefined);
	}, []);

	const uploadImage = useCallback(
		(event: ChangeEvent<HTMLInputElement>) => {
			const files: File[] = Array.prototype.slice.call(
				event.target.files,
				0,
				Math.max(0, POST_MAX_IMAGES_LENGTH - previewImages.length),
			);

			const loaded: string[] = [];
			const loadedFiles: File[] = [];
			for (const file of files) {
				loadedFiles.push(file);
				setImages([...images, ...loadedFiles]);

				const reader = new FileReader();
				reader.onload = () => {
					if (reader.result !== null) {
						const newDataUrl = reader.result.toString();
						loaded.push(newDataUrl);
						setPreviewImages([...previewImages, ...loaded]);
					}
				};
				reader.readAsDataURL(file);
			}

			// @ts-ignore
			event.target.value = null;
		},
		[images, previewImages],
	);

	const submit = useCallback(async () => {
		if (text === '' && images.length === 0 && video === undefined) {
			return;
		}

		incrementProgress(taskName);

		await onSubmit(text, images, video).then(() => {
			setCookiesMemory(taskName, '');
			setText('');
			setImages([]);
			setPreviewImages([]);
			setVideo(undefined);
		});

		clearProgress(taskName);
	}, [taskName, onSubmit, video, text, images, incrementProgress, clearProgress]);

	const keyDown = useCallback(
		(event: KeyboardEvent<HTMLTextAreaElement>) => {
			if (event.key === 'Enter' && !event.shiftKey) {
				event.preventDefault();
				void submit();
			}
		},
		[submit],
	);

	const checkOnMention = useCallback(
		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		(newText?: any) => {
			const actualText = typeof newText === 'string' ? newText : text;

			setTimeout(() => {
				const textarea = textareaRef.current;
				if (textarea !== null) {
					let isMatched = false;

					const positions = getMentionPositions(actualText);
					const selectionStart = document.getSelection()?.focusOffset ?? 0;
					for (const { start, end, mention } of positions) {
						if (start <= selectionStart && end >= selectionStart) {
							isMatched = true;

							setAttachedElement?.(textarea);
							setVisible?.(true);
							setContext?.({
								mention: {
									text: actualText,
									value: mention,
									start,
									end,
									input: textarea,
									setText,
									debounce: typeof newText === 'string',
								},
							});
						}
					}

					if (!isMatched && visible) {
						setVisible?.(false);
						setContext?.({});
					}
				}
			}, 0);
		},
		[text, textareaRef, visible, setAttachedElement, setContext, setVisible],
	);

	const changeText = useCallback(
		(newText: string) => {
			trace("Change blog's textarea's text", performance.now(), () => {
				setText(newText);
			});
			checkOnMention(newText);
		},
		[checkOnMention],
	);

	const paste = useCallback(
		(event: ClipboardEvent<HTMLTextAreaElement>) => {
			const clipboard = event.clipboardData;

			if (clipboard.items.length > 0) {
				const newPreviewImages: string[] = [];
				const newImages: Array<File | Blob> = [];

				const updateImages = (): void => {
					setPreviewImages([...previewImages, ...newPreviewImages]);
					setImages([...images, ...newImages]);
				};

				const isImagesInClipboard = Array.prototype.some.call(clipboard.items, (item: DataTransferItem) =>
					item.type.includes('image'),
				);

				for (const item of clipboard.items) {
					if (item.type.includes('image')) {
						const file = item.getAsFile();
						if (file === null) {
							continue;
						}

						const reader = new FileReader();
						reader.onload = () => {
							if (reader.result !== null) {
								newPreviewImages.push(reader.result.toString());
								newImages.push(file);
								updateImages();
							}
						};
						reader.readAsDataURL(file);
					} else if (item.type.includes('text/plain') && !isImagesInClipboard) {
						item.getAsString((clipboardText) => {
							if (validateUrl(clipboardText)) {
								if (previewImages.length === 0 && verifyYoutubeUrl(clipboardText)) {
									const id = extractIdFromYoutubeUrl(clipboardText);
									if (id !== null) {
										setVideo(id);
									}
								} else if (video === undefined) {
									if (previewImages.length < POST_MAX_IMAGES_LENGTH) {
										void verifyImageUrl(clipboardText).then(
											async (image) => {
												void convertImageUrlToBlob(image).then((blob) => {
													newPreviewImages.push(clipboardText);
													newImages.push(blob);
													updateImages();
												});
											},
											// eslint-disable-next-line @typescript-eslint/no-empty-function
											() => {},
										);
									}
								}
							}
						});
					}
				}
			}
		},
		[video, images, previewImages],
	);

	const changeImagePreview = useCallback(
		(src: string | null) => () => {
			setImagePreview(src);
		},
		[setImagePreview],
	);

	return (
		<StyledDummy loading={taskName} transparent={true}>
			<Modal isOpen={isOpenVideoModal} onClose={cancelVideo}>
				<span>{d('blog.video.modal.title')}</span>
				<Input name="blogVideo" type="text" value={tempVideo} onChange={setTempVideo} />
				<Button onClick={cancelVideo}>{d('blog.video.modal.cancel')}</Button>
				<Button onClick={acceptVideo}>{d('blog.video.modal.accept')}</Button>
			</Modal>
			<StyledForm>
				<MessageArea
					emotions={emotions}
					placeholder={d('blog.placeholder')}
					value={text}
					getRef={textareaRef}
					onChange={changeText}
					onKeyDown={keyDown}
					onMouseDown={checkOnMention}
					onPaste={paste}
				/>
				<Button onClick={submit}>{d('blog.send')}</Button>
			</StyledForm>
			<div>
				{video === undefined && previewImages.length < POST_MAX_IMAGES_LENGTH && (
					<label>
						<input type="file" accept="image/*" multiple={true} onChange={uploadImage} />
						<FormatButton color="simple">+image</FormatButton>
					</label>
				)}
				{images.length === 0 && video === undefined && (
					<FormatButton color="simple" onClick={openVideoModal}>
						+youtube
					</FormatButton>
				)}
			</div>
			<Row>
				<PreviewVideoWrap>
					{video !== undefined && (
						<RelativeWrap>
							<RemoveButton onClick={removeYoutube}>x</RemoveButton>
							<YouTube videoId={video} />
						</RelativeWrap>
					)}
				</PreviewVideoWrap>
				<PreviewImagesWrap>
					{previewImages.map((imageSrc, index) => (
						<RelativeWrap key={index}>
							<RemoveButton onClick={removePreviewImage(index)}>x</RemoveButton>
							<Image src={imageSrc} alt="preload image" onClick={changeImagePreview(imageSrc)} />
						</RelativeWrap>
					))}
				</PreviewImagesWrap>
			</Row>
		</StyledDummy>
	);
};

export default EditForm;
