import React, { useCallback, useContext, useEffect } from 'react';
import { d } from 'Utils/dictionary';
import Link from 'Components/Link/Link';
import styled from 'styled-components';
import LazyImage from 'Components/LazyImage';
import Tooltip from 'UI/Tooltip';
import {
	GraphQLWrap,
	Response,
	ResponseComment,
	ResponseEmotion,
	ResponsePost,
	ResponseProfile,
	ResponseReaction,
} from 'Types/response.type';
import { appContext } from 'Contexts/app.context';
import { tooltipContext } from 'Contexts/tooltip.context';
import { COMMENT_ACTION_TASK } from 'Components/Post/Comments';
import { getPosts, hideComment as hideCommentMutation, hidePost as hidePostMutation } from 'Schemas/Post';
import { loaderContext } from 'Contexts/loader.context';
import { errorContext } from 'Contexts/error.context';
import EmotionsBarTooltip from 'UI/EmotionsBar/EmotionsBarTooltip';
import Emotions from 'UI/Emotions';
import { useSecureMutation } from 'Utils/apollo';

interface Props {
	context: BlogTooltipContext;
	profileList: ResponseProfile[];
	profileListLoading: boolean;
	emotions: ResponseEmotion[];
	onAddEmotion: (_id: string, emotionAlias: string, isPost?: boolean) => void;
	postQueryVariables: {
		_id?: string;
		alias?: string;
		limit: number;
		visibleData: string[];
	};
}

export interface BlogTooltipContext {
	comment?: ResponseComment;
	post?: ResponsePost;
	mention?: {
		value: string;
		debounce: boolean;
		start: number;
		end: number;
		text: string;
		input: HTMLInputElement | HTMLTextAreaElement;
		setText: (text: string) => void;
	};
	emotionsInfo?: {
		alias: string;
		title: string;
		authors: Response<ResponseProfile>;
	};
	emotions?: {
		isPost: boolean;
		_id: string;
		reactions: ResponseReaction[];
	};
}

const LittleAvatar = styled(LazyImage)`
	width: 24px;
	height: 24px;
	margin-right: 6px;
`;

const StyledTooltip = styled(Tooltip)`
	padding: 0;
`;

const List = styled.ul`
	padding: 0;
	font-family: ${({ theme }) => theme.fontFamily.quiet};
	font-size: 0.7rem;
	color: #666;

	li {
		display: flex;
		align-items: center;
		cursor: pointer;
		padding: 4px 20px;
		text-align: left;
		list-style-type: none;
		white-space: nowrap;

		&:hover {
			color: #333;
		}

		&:not(:last-child) {
			border-bottom: 1px solid #ccc;
		}
	}
`;

const ProfileList = styled(List)`
	width: 300px;
`;

let closePrevent = false;

const BlogTooltip: React.FC<Props> = (props) => {
	const { context, profileList, profileListLoading, emotions, onAddEmotion, postQueryVariables } = props;
	const { user } = useContext(appContext);
	const { setVisible } = useContext(tooltipContext);
	const { clearProgress, incrementProgress } = useContext(loaderContext);
	const { setError } = useContext(errorContext);

	const prevent = useCallback(() => {
		closePrevent = true;
	}, []);

	useEffect(() => {
		const mouseDown = (): void => {
			if (!closePrevent) {
				setVisible?.(false);
			}
			closePrevent = false;
		};

		window.addEventListener('mousedown', mouseDown);
		return () => window.removeEventListener('mousedown', mouseDown);
	}, [setVisible]);

	const changeVisible = useCallback(
		(value: boolean) => () => {
			setVisible?.(value);
		},
		[setVisible],
	);

	const addMention = useCallback(
		(login: string) => () => {
			const mention = context?.mention;
			if (mention === undefined) {
				return;
			}

			const endText = mention.text.substr(mention.end);
			mention.setText(`${mention.text.substr(0, mention.start)}@${login}${endText}${endText.length === 0 ? ' ' : ''}`);
			mention.input.focus();

			setVisible?.(false);
		},
		[context, setVisible],
	);

	const [hideComment] = useSecureMutation<GraphQLWrap<'hideComment', boolean | undefined>, { _id: string }>(
		hideCommentMutation,
	);

	const removeComment = useCallback(
		(commentId: string, postId: string) => async () => {
			setVisible?.(false);

			const commentTask = `${COMMENT_ACTION_TASK}_${commentId}`;
			incrementProgress(commentTask);

			await hideComment({
				variables: {
					_id: commentId,
				},
				update: (cache) => {
					const oldData = cache.readQuery<GraphQLWrap<'responsePosts', Response<ResponsePost>>>({
						query: getPosts,
						variables: postQueryVariables,
					});

					if (oldData === null) {
						return;
					}

					cache.writeQuery<GraphQLWrap<'responsePosts', Response<ResponsePost>>>({
						query: getPosts,
						variables: postQueryVariables,
						data: {
							responsePosts: {
								...oldData.responsePosts,
								data: oldData.responsePosts.data.map((post) =>
									post._id === postId
										? {
												...post,
												comments: {
													...post.comments,
													totalLength: post.comments.totalLength - 1,
													data: post.comments.data.filter((comment) => comment._id !== commentId),
												},
										  }
										: post,
								),
							},
						},
					});
				},
			}).catch((err: Error) => {
				setError(err.message);
			});

			clearProgress(commentTask);
		},
		[setVisible, clearProgress, incrementProgress, postQueryVariables, setError, hideComment],
	);

	const [hidePost] = useSecureMutation<GraphQLWrap<'hidePost', boolean | undefined>, { _id: string }>(hidePostMutation);

	const removePost = useCallback(
		(_id: string) => () => {
			setVisible?.(false);

			void hidePost({
				variables: {
					_id,
				},
				update: (cache) => {
					const oldData = cache.readQuery<GraphQLWrap<'responsePosts', Response<ResponsePost>>>({
						query: getPosts,
						variables: postQueryVariables,
					});

					if (oldData === null) {
						return;
					}

					cache.writeQuery<GraphQLWrap<'responsePosts', Response<ResponsePost>>>({
						query: getPosts,
						variables: postQueryVariables,
						data: {
							responsePosts: {
								...oldData.responsePosts,
								totalLength: oldData.responsePosts.totalLength - 1,
								data: oldData.responsePosts.data.filter((post) => post._id !== _id),
							},
						},
					});
				},
			}).catch((err: Error) => {
				setError(err.message);
			});
		},
		[setVisible, postQueryVariables, setError, hidePost],
	);

	const copyLink = useCallback(() => {
		setVisible?.(false);

		if (context.post === undefined) {
			return;
		}

		if (navigator.clipboard === undefined) {
			setError(d('band.clipboard.noFeature'));
			return;
		}
		navigator.clipboard
			.writeText(`${location.protocol}//${location.host}/news/${context.post._id}`)
			.catch((err: Error) => setError(d('band.clipboard', err.message)));
	}, [setVisible, context, setError]);

	const addEmotion = useCallback(
		async (emotionAlias: string) => {
			if (context.emotions !== undefined) {
				onAddEmotion(context.emotions._id, emotionAlias, context.emotions.isPost);
			}
		},
		[context.emotions, onAddEmotion],
	);

	if (context.mention !== undefined) {
		return (
			<StyledTooltip position="bottom" pointer={false} onMouseDown={prevent}>
				<ProfileList>
					{profileListLoading ? (
						<li>{d('blog.tooltip.loading')}</li>
					) : profileList.length === 0 ? (
						<li>{d('blog.tooltip.noProfileMatches')}</li>
					) : (
						profileList.map((profile) => (
							<li key={profile.login} onClick={addMention(profile.login)}>
								<LittleAvatar src={profile.avatar.src} alt={`${profile.login}'s avatar`} />
								<span>
									{profile.login} ({profile.name})
								</span>
							</li>
						))
					)}
				</ProfileList>
			</StyledTooltip>
		);
	} else if (context.emotions !== undefined) {
		return (
			<StyledTooltip position="top" pointer={false} onMouseDown={prevent}>
				<Emotions
					emotions={emotions.filter((emotion) =>
						context.emotions!.reactions.every((reaction) => reaction.emotion.alias !== emotion.alias),
					)}
					addEmotion={addEmotion}
				/>
			</StyledTooltip>
		);
	} else if (context.emotionsInfo !== undefined) {
		return (
			<StyledTooltip
				position="top"
				pointer={false}
				onMouseDown={prevent}
				onMouseEnter={changeVisible(true)}
				onMouseLeave={changeVisible(false)}
			>
				<EmotionsBarTooltip emotionsInfo={context.emotionsInfo} />
			</StyledTooltip>
		);
	} else {
		return (
			<StyledTooltip position="bottom" pointer={false} onMouseDown={prevent}>
				<List>
					{((context.comment !== undefined && context.comment.author.login === user?.login) ||
						(context.comment === undefined &&
							context.post !== undefined &&
							user?.administeredBands.some((band) => band.alias === context.post!.author.alias))) && (
						<li
							onClick={
								context.comment !== undefined && context.post !== undefined
									? removeComment(context.comment._id, context.post._id)
									: context.post !== undefined
									? removePost(context.post._id)
									: undefined
							}
						>
							{d('blog.tooltip.remove')}
						</li>
					)}
					{context.comment === undefined && context.post !== undefined && (
						<li onClick={changeVisible(false)}>
							<Link
								location={{
									pathname: '/news',
									query: {
										id: context.post._id,
									},
								}}
								asLocation={{
									pathname: `/news/${context.post._id}`,
								}}
							>
								{d('blog.tooltip.moveTo')}
							</Link>
						</li>
					)}
					{context.comment === undefined && context.post !== undefined && (
						<li onClick={copyLink}>{d('blog.tooltip.copyLink')}</li>
					)}
				</List>
			</StyledTooltip>
		);
	}
};

export default BlogTooltip;
