import React, { MouseEvent, ChangeEvent, useCallback, useContext } from 'react';
import {
	GraphQLWrap,
	ResponseBand,
	ResponseLink,
	ResponseProfile,
	ResponseProfileStatistics,
	ResponseUser,
	UserPrivacySettings,
} from 'Types/response.type';
import { d } from 'Utils/dictionary';
import Dummy from 'UI/Dummy';
import styled from 'styled-components';
import { loaderContext } from 'Contexts/loader.context';
import { appContext } from 'Contexts/app.context';
import {
	addProfileAudio as addAudioMutation,
	getProfile,
	removeProfileAudio as removeAudioMutation,
} from 'Schemas/Profile';
import { errorContext } from 'Contexts/error.context';
import { isExpired } from 'Utils/date';
import {
	GA_ADD_TO_PLAYLIST,
	GA_CATEGORY_PROFILE,
	PROFILE_AUDIO_LIMIT,
	PROFILE_AUDIO_LIMIT_PREMIUM,
} from 'Utils/constants';
import Tumbler from 'UI/Tumbler';
import { updatePrivacySettings as updatePrivacySettingsMutation } from 'Schemas/User';
import Tablet from 'UI/Tablet/Tablet';
import Audios from 'Components/Audios';
import ReactGA from 'react-ga';
import { routePush } from 'Components/Link';
import { useSecureMutation } from 'Utils/apollo';

const STATISTICS_CHANGE_TASK = 'STATISTICS_CHANGE_TASK';
const AUDIO_UPLOAD_TASK = 'AUDIO_UPLOAD_TASK';

interface Props {
	login: string;
	audios: ResponseLink[];
	statistics: ResponseProfileStatistics;
	loading: boolean;
	privacySettings: UserPrivacySettings;
}

const Container = styled(Tablet)`
	margin-top: 20px;
`;

const Wrap = styled.div`
	box-sizing: border-box;
	padding: 0 25px;
	width: 100%;
`;

const Title = styled.span`
	margin-right: 20px;
	font-size: 1.4rem;
	font-weight: 400;
	font-family: ${({ theme }) => theme.fontFamily.aggressive};
`;

const Label = styled.label`
	display: block;
	cursor: pointer;
	opacity: 0.7;
	font-size: 0.8rem;

	input {
		display: none;
	}
`;

const Empty = styled.div`
	color: ${({ theme }) => theme.color.gray50};
	font-size: 0.7rem;
`;

const Flex = styled.div`
	display: flex;
	margin-top: 20px;
	flex-direction: column;
	font-size: 0.8rem;
`;

const Row = styled.div`
	display: flex;
`;

const AudiosInfo = styled.div`
	font-size: 0.65em;
	color: #666;
`;

const AdditionalBlock: React.FC<Props> = ({ login, audios, statistics, loading, privacySettings }) => {
	const { incrementProgress, clearProgress } = useContext(loaderContext);
	const { setError } = useContext(errorContext);
	const { user } = useContext(appContext);

	const [addAudio] = useSecureMutation<
		GraphQLWrap<'addAudio', ResponseBand>,
		{
			name: string;
			file: File | Blob;
		}
	>(addAudioMutation);

	const [removeAudio] = useSecureMutation<
		GraphQLWrap<'removeAudio', ResponseBand>,
		{
			_id: string;
		}
	>(removeAudioMutation);

	const addAudioFile = useCallback(
		async (event: ChangeEvent<HTMLInputElement>) => {
			const target = event.target;

			const files = target.files ?? [];
			const file = files[0];
			if (file === undefined) {
				return;
			}

			incrementProgress(AUDIO_UPLOAD_TASK);

			await addAudio({
				variables: {
					name: file.name,
					file,
				},
			}).catch((err: Error) => {
				setError(err.message);
			});

			clearProgress(AUDIO_UPLOAD_TASK);

			// @ts-ignore
			target.value = null;
		},
		[clearProgress, setError, incrementProgress, addAudio],
	);

	const removeAudioFile = useCallback(
		(_id: string) => {
			void removeAudio({
				variables: {
					_id,
				},
			});
		},
		[removeAudio],
	);

	const checkAudioLimit = useCallback(
		(event: MouseEvent<HTMLLabelElement>) => {
			const expired = isExpired(user?.extendPack ?? 0);
			if (expired && audios.length >= PROFILE_AUDIO_LIMIT) {
				setError(d('profile.audios.limit.usual'), {
					title: d('card.inShop'),
					action: () => {
						void routePush({
							pathname: '/shop',
							query: {},
						});
					},
				});
				event.preventDefault();
			} else if (!expired && audios.length >= PROFILE_AUDIO_LIMIT_PREMIUM) {
				setError(d('profile.audios.limit.premium'));
				event.preventDefault();
			}
		},
		[audios.length, setError, user],
	);

	const [updatePrivacySettings] = useSecureMutation<
		GraphQLWrap<'updatePrivacySettings', ResponseUser>,
		{
			settings: {
				showStatistics: boolean;
			};
		}
	>(updatePrivacySettingsMutation, {
		variables: {
			settings: {
				showStatistics: !privacySettings.showStatistics,
			},
		},
		update: (cache, { data }) => {
			const oldData = cache.readQuery<GraphQLWrap<'profile', ResponseProfile>>({
				query: getProfile,
			});

			if (oldData === null || data === null || data === undefined) {
				return;
			}

			cache.writeQuery<GraphQLWrap<'profile', ResponseProfile>>({
				query: getProfile,
				data: {
					profile: {
						...oldData.profile,
						settings: {
							...oldData.profile.settings,
							privacy: {
								...oldData.profile.settings.privacy,
								...data.updatePrivacySettings.settings.privacy,
							},
						},
					},
				},
			});
		},
	});

	const addToPlaylist = useCallback(() => {
		ReactGA.event({
			category: GA_CATEGORY_PROFILE,
			action: GA_ADD_TO_PLAYLIST,
		});
	}, []);

	const changePrivacy = useCallback(() => {
		void updatePrivacySettings().catch((err: Error) => setError(err.message));
	}, [updatePrivacySettings, setError]);

	const isMe = user?.login === login;

	return (
		<Container>
			<Wrap>
				<Title>{d('profile.audios.title')}</Title>
				<Dummy loading={loading}>
					{!isMe && audios.length === 0 && <Empty>{d('profile.audios.empty')}</Empty>}
					<Audios tracks={audios} onRemove={removeAudioFile} onAddToPlaylist={addToPlaylist} />
					{isMe && (
						<Dummy loading={AUDIO_UPLOAD_TASK} transparent={true}>
							<Label onClick={checkAudioLimit}>
								<AudiosInfo>{d('profile.audios.info')}</AudiosInfo>
								<span>{d('profile.audios.add')}</span>
								<input type="file" onChange={addAudioFile} accept="audio/mpeg" />
							</Label>
						</Dummy>
					)}
				</Dummy>
				{(isMe || privacySettings.showStatistics) && (
					<Flex>
						<Row>
							<Title>{d('profile.statistics.title')}</Title>
							{isMe && (
								<Dummy loading={STATISTICS_CHANGE_TASK} transparent={true}>
									<Tumbler
										on={privacySettings.showStatistics}
										title={d('profile.statistics.showStatistics')}
										onClick={changePrivacy}
									/>
								</Dummy>
							)}
						</Row>
						<Dummy loading={loading}>
							<span>
								{d('profile.statistics.bandsHired')}: {statistics.bandsHired}
							</span>
						</Dummy>
						<Dummy loading={loading}>
							<span>
								{d('profile.statistics.bandsMember')}: {statistics.bandsMember}
							</span>
						</Dummy>
					</Flex>
				)}
			</Wrap>
		</Container>
	);
};

export default AdditionalBlock;
