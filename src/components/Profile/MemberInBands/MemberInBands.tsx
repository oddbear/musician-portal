import { GraphQLWrap, ResponseBand, ResponseProfile } from 'Types/response.type';
import React, { useCallback, useContext, useState } from 'react';
import styled from 'styled-components';
import { d } from 'Utils/dictionary';
import LazyImage from 'Components/LazyImage';
import { appContext } from 'Contexts/app.context';
import Link from 'Components/Link';
import Button from 'UI/Button/Button';
import Modal from 'UI/Modal';
import { getProfile, leaveBand as leaveBandMutation } from 'Schemas/Profile';
import { loaderContext } from 'Contexts/loader.context';
import { errorContext } from 'Contexts/error.context';
import Dummy from 'UI/Dummy/Dummy';
import Tablet from 'UI/Tablet/Tablet';
import Scroll from 'UI/Scroll';
import ReactGA from 'react-ga';
import { GA_CATEGORY_PROFILE, GA_LEAVE } from 'Utils/constants';
import { useSecureMutation } from 'Utils/apollo';

const LEAVE_BAND_TASK = 'LEAVE_BAND_TASK';

interface Props {
	login: string;
	memberInBands: ResponseBand[];
}

const Container = styled(Tablet)`
	margin-right: 20px;
`;

const Wrap = styled.div`
	box-sizing: border-box;
	padding: 0 25px;
	width: 100%;
`;

const Title = styled.span`
	font-size: 1.4rem;
	font-weight: 400;
	font-family: ${({ theme }) => theme.fontFamily.aggressive};
`;

const Row = styled.div`
	margin: 27px 0 23px 0;
	align-items: center;
	font-size: 0.75rem;
`;

const Line = styled.div`
	position: relative;
	margin-left: 53px;
	border-bottom: 1px solid #bab9b9;
`;

const AdminScroll = styled(Scroll)`
	box-sizing: border-box;
	max-height: 190px;
	overflow: auto;
`;

const StyledLink = styled(Link)`
	display: flex;
	align-items: center;
`;

const Empty = styled.span`
	color: ${({ theme }) => theme.color.primary};
	font-size: 0.7rem;
`;

const MemberInBands: React.FC<Props> = ({ login, memberInBands }) => {
	const [confirmOpened, setConfirmOpened] = useState<boolean>(false);
	const [bandToLeave, setBandToLeave] = useState<ResponseBand | undefined>();

	const { clearProgress, incrementProgress } = useContext(loaderContext);
	const { setError } = useContext(errorContext);

	const openConfirm = useCallback((band: ResponseBand) => {
		setConfirmOpened(true);
		setBandToLeave(band);
	}, []);

	const [leaveBand] = useSecureMutation<GraphQLWrap<'leaveBand', boolean>, { alias: string }>(leaveBandMutation);

	const leave = useCallback(async () => {
		setConfirmOpened(false);

		if (bandToLeave !== undefined) {
			incrementProgress(LEAVE_BAND_TASK);
			await leaveBand({
				variables: {
					alias: bandToLeave.alias,
				},
				update: (cache) => {
					const oldData = cache.readQuery<GraphQLWrap<'profile', ResponseProfile>>({
						query: getProfile,
					});
					if (oldData === null) {
						return;
					}

					cache.writeQuery<GraphQLWrap<'profile', ResponseProfile>>({
						query: getProfile,
						data: {
							profile: {
								...oldData.profile,
								memberInBands: oldData.profile.memberInBands.filter(({ _id }) => bandToLeave._id !== _id),
							},
						},
					});
				},
			})
				.then(() => {
					ReactGA.event({
						category: GA_CATEGORY_PROFILE,
						action: GA_LEAVE,
					});
				})
				.catch((err: Error) => {
					setError(err.message);
				});

			clearProgress(LEAVE_BAND_TASK);
		}
	}, [clearProgress, setError, incrementProgress, bandToLeave, leaveBand]);

	const closeConfirm = useCallback(() => {
		setConfirmOpened(false);
	}, []);

	return (
		<Container>
			<Modal isOpen={confirmOpened} onClose={closeConfirm}>
				<span>{d('profile.memberInBands.confirm.message', bandToLeave?.name ?? '')}</span>
				<div>
					<Button color="simple" onClick={leave}>
						{d('profile.memberInBands.confirm.accept')}
					</Button>
					<Button color="simple" onClick={closeConfirm}>
						{d('profile.memberInBands.confirm.cancel')}
					</Button>
				</div>
			</Modal>
			<Wrap>
				<Title>{d('profile.memberInBands.title')}</Title>
				<AdminScroll>
					{memberInBands.length !== 0 ? (
						memberInBands.map((band) => (
							<Row key={band.alias}>
								<Band login={login} band={band} onLeave={openConfirm} />
								<Line />
							</Row>
						))
					) : (
						<Empty>{d('profile.memberInBands.empty')}</Empty>
					)}
				</AdminScroll>
			</Wrap>
		</Container>
	);
};

export default MemberInBands;

interface BandProps {
	login: string;
	band: ResponseBand;
	onLeave: (alias: ResponseBand) => void;
}

const MemberBand = styled.div`
	display: flex;
	place-content: space-between;
	align-items: center;
`;

const Avatar = styled(LazyImage)`
	height: 42px;
	width: 42px;
	margin-right: 10px;
	background-color: white;
`;

const Band: React.FC<BandProps> = ({ login, band, onLeave }) => {
	const { user } = useContext(appContext);

	const leave = useCallback(() => {
		void onLeave(band);
	}, [onLeave, band]);

	return (
		<MemberBand>
			<StyledLink
				location={{
					query: {
						alias: band.alias,
					},
					pathname: '/band',
				}}
				asLocation={{
					pathname: `/band/${band.alias}`,
				}}
			>
				<Avatar src={band.avatar.src} alt={`${band.name}'s avatar`} />
				<span>{band.name}</span>
			</StyledLink>
			{user?.login === login && user.administeredBands.every((admBand) => admBand._id !== band._id) && (
				<Dummy loading={LEAVE_BAND_TASK} transparent={true}>
					<Button color="simple">
						<span onClick={leave}>{d('profile.memberInBands.leave')}</span>
					</Button>
				</Dummy>
			)}
		</MemberBand>
	);
};
