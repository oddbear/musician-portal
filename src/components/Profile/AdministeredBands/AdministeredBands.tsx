import LazyImage from 'Components/LazyImage';
import Link, { routePush } from 'Components/Link';
import { GraphQLWrap, ResponseLink, ResponseBand, ResponseUser, ResponseProfile } from 'Types/response.type';
import React, { MouseEvent, useCallback, useContext, useState } from 'react';
import styled from 'styled-components';
import { d } from 'Utils/dictionary';
import { removeBand as removeBandMutation } from 'Schemas/Band';
import { getUser } from 'Schemas/User';
import Modal from 'UI/Modal';
import Button from 'UI/Button/Button';
import Form from 'UI/Form/Form';
import { Input } from 'UI/Form';
import { getProfile } from 'Schemas/Profile';
import { createBand as createBandMutation } from 'Schemas/Band';
import Dummy from 'UI/Dummy/Dummy';
import { loaderContext } from 'Contexts/loader.context';
import { errorContext } from 'Contexts/error.context';
import { BAND_MAX_COUNT, BAND_MAX_COUNT_PREMIUM } from 'Utils/constants';
import { isExpired } from 'Utils/date';
import { appContext } from 'Contexts/app.context';
import Filter, { Option } from 'UI/Filter';
import Tablet from 'UI/Tablet/Tablet';
import Scroll from 'UI/Scroll';
import { useSecureMutation } from 'Utils/apollo';

const CREATE_BAND_TASK = 'CREATE_BAND_TASK';
const REMOVE_BAND_TASK = 'REMOVE_BAND_TASK';

interface Props {
	administeredBands: ResponseBand[];
}

const Container = styled(Tablet)`
	text-align: right;
`;

const Wrap = styled.div`
	box-sizing: border-box;
	padding: 0 25px;
`;

const AdminScroll = styled(Scroll)`
	box-sizing: border-box;
	max-height: 190px;
	overflow: auto;
`;

const Empty = styled.span`
	color: ${({ theme }) => theme.color.primary};
	font-size: 0.7rem;
`;

const Title = styled.span`
	font-size: 1.4rem;
	font-weight: 400;
	font-family: ${({ theme }) => theme.fontFamily.aggressive};
`;

const Avatar = styled(LazyImage)`
	height: 42px;
	width: 42px;
	margin-right: 10px;
	background-color: white;
`;

const Row = styled.div`
	margin: 27px 0 23px 0;
	align-items: center;
	font-size: 0.75rem;
`;

const AlignCenter = styled.div`
	display: flex;
	align-items: center;
`;

const StyledLink = styled(Link)`
	display: flex;
	align-items: center;
`;

const AdministeredBands: React.FC<Props> = ({ administeredBands }) => {
	const [confirmOpened, setConfirmOpened] = useState<boolean>(false);
	const [createModalOpened, setCreateModalOpened] = useState<boolean>(false);
	const [bandToDelete, setBandToDelete] = useState<string | undefined>(undefined);
	const [confirmationRepeat, setConfirmationRepeat] = useState<string>('');
	const [createBandAlias, setCreateBandAlias] = useState<string>('');
	const [type, setType] = useState<string>('band');

	const { clearProgress, incrementProgress } = useContext(loaderContext);
	const { setError } = useContext(errorContext);
	const { user } = useContext(appContext);

	const [removeBand] = useSecureMutation<GraphQLWrap<'removeBand', ResponseBand>, { alias: string }>(
		removeBandMutation,
		{
			update: (cache, { data }) => {
				if (data === undefined || data === null || data.removeBand === null) {
					return;
				}

				const oldData = cache.readQuery<GraphQLWrap<'user', ResponseUser>>({
					query: getUser,
				});
				if (oldData !== null) {
					const bands = [...oldData.user.administeredBands];
					const matchBand = bands.find((band) => band._id === data.removeBand._id);
					if (matchBand !== undefined) {
						bands.splice(oldData.user.administeredBands.indexOf(matchBand), 1);

						cache.writeQuery<GraphQLWrap<'user', ResponseUser>>({
							query: getUser,
							data: {
								user: {
									...oldData.user,
									administeredBands: bands,
								},
							},
						});
					}
				}

				const oldProfileData = cache.readQuery<GraphQLWrap<'profile', ResponseProfile>>({
					query: getProfile,
				});
				if (oldProfileData !== null) {
					cache.writeQuery<GraphQLWrap<'profile', ResponseProfile>>({
						query: getProfile,
						data: {
							...oldProfileData,
							profile: {
								...oldProfileData.profile,
								memberInBands: oldProfileData.profile.memberInBands.filter((band) => band._id !== data.removeBand._id),
							},
						},
					});
				}
			},
		},
	);

	const closeConfirm = useCallback((e?: MouseEvent<HTMLButtonElement>) => {
		if (e !== undefined) {
			e.preventDefault();
		}
		setConfirmOpened(false);
	}, []);

	const acceptConfirm = useCallback(async () => {
		if (bandToDelete !== undefined) {
			incrementProgress(REMOVE_BAND_TASK);

			await removeBand({
				variables: {
					alias: bandToDelete,
				},
			}).catch((err: Error) => {
				setError(err.message);
			});

			clearProgress(REMOVE_BAND_TASK);
			setConfirmOpened(false);
		}
	}, [clearProgress, setError, incrementProgress, bandToDelete, removeBand]);

	const remove = useCallback((alias: string) => {
		setConfirmationRepeat('');
		setBandToDelete(alias);
		setConfirmOpened(true);
	}, []);

	const [createBand] = useSecureMutation<
		GraphQLWrap<'createBand', ResponseBand>,
		{
			alias: string;
			repetitionBase: boolean;
		}
	>(createBandMutation, {
		variables: {
			alias: createBandAlias,
			repetitionBase: type === 'repetitionBase',
		},
		update: (cache, { data }) => {
			if (data !== null && data !== undefined) {
				const oldUserData = cache.readQuery<GraphQLWrap<'user', ResponseUser>>({
					query: getUser,
				});
				if (oldUserData !== null) {
					cache.writeQuery<GraphQLWrap<'user', ResponseUser>>({
						query: getUser,
						data: {
							user: {
								...oldUserData.user,
								memberInBands: oldUserData.user.memberInBands.concat([data.createBand]),
								administeredBands: oldUserData.user.administeredBands.concat([data.createBand]),
							},
						},
					});
				}

				const oldProfileData = cache.readQuery<GraphQLWrap<'profile', ResponseProfile>>({
					query: getProfile,
				});
				if (oldProfileData !== null) {
					cache.writeQuery<GraphQLWrap<'profile', ResponseProfile>>({
						query: getProfile,
						data: {
							profile: {
								...oldProfileData.profile,
								memberInBands: oldProfileData.profile.memberInBands.concat([data.createBand]),
							},
						},
					});
				}
			}
		},
		onCompleted: async (data) => {
			clearProgress(CREATE_BAND_TASK);
			await routePush(
				{
					pathname: '/band',
					query: {
						alias: data.createBand.alias,
					},
				},
				{
					pathname: `/band/${data.createBand.alias}`,
					query: {},
				},
			);
		},
		onError: (err) => {
			clearProgress(CREATE_BAND_TASK);
			setError(err.message);
		},
	});

	const createNewBand = useCallback(async () => {
		incrementProgress(CREATE_BAND_TASK);
		await createBand();
	}, [incrementProgress, createBand]);

	const openCreateModal = useCallback(() => {
		const expired = isExpired(user?.extendPack ?? 0);
		if (expired && administeredBands.length >= BAND_MAX_COUNT) {
			setError(d('profile.administeredBands.limit.usual'), {
				title: d('card.inShop'),
				action: () => {
					void routePush({
						pathname: '/shop',
						query: {},
					});
				},
			});
			return;
		} else if (!expired && administeredBands.length >= BAND_MAX_COUNT_PREMIUM) {
			setError(d('profile.administeredBands.limit.premium'));
			return;
		}

		setCreateModalOpened(true);
	}, [administeredBands, setError, user]);

	const closeCreateModal = useCallback((e: MouseEvent<HTMLButtonElement>) => {
		e.preventDefault();
		setCreateModalOpened(false);
	}, []);

	return (
		<Container>
			<Modal isOpen={confirmOpened} onClose={closeConfirm}>
				<span>{d('profile.administeredBands.delete.confirm.message', bandToDelete ?? '')}</span>
				<Form onSubmit={acceptConfirm}>
					<Input
						type="text"
						name="confirmation"
						value={confirmationRepeat}
						onChange={setConfirmationRepeat}
						validation={{
							callback: {
								errorMessage: d('profile.administeredBands.delete.confirm.errorMessage'),
								value: (value) => value.toLowerCase() === bandToDelete,
							},
						}}
					/>
					<div>
						<Button color="simple">{d('profile.administeredBands.delete.confirm.accept')}</Button>
						<Button color="simple" onClick={closeConfirm}>
							{d('profile.administeredBands.delete.confirm.cancel')}
						</Button>
					</div>
				</Form>
			</Modal>
			<Modal isOpen={createModalOpened}>
				<span>{d('profile.administeredBands.add.modal.message')}</span>
				<Form onSubmit={createNewBand}>
					<Input
						type="text"
						name="createBand"
						value={createBandAlias}
						onChange={setCreateBandAlias}
						validation={{
							callback: {
								errorMessage: d('profile.administeredBands.add.modal.errorMessage'),
								value: (value) => /^[a-zA-Z]+$/.exec(value) !== null,
							},
						}}
					/>
					<div>
						<Filter value={type} type="radio" onChange={setType}>
							<Option title={d('profile.administeredBands.add.modal.band')} value="band" />
							<Option title={d('profile.administeredBands.add.modal.repetitionBase')} value="repetitionBase" />
						</Filter>
					</div>
					<Dummy loading={CREATE_BAND_TASK} transparent={true}>
						<div>
							<Button color="simple">{d('profile.administeredBands.add.modal.accept')}</Button>
							<Button color="simple" onClick={closeCreateModal}>
								{d('profile.administeredBands.add.modal.cancel')}
							</Button>
						</div>
					</Dummy>
				</Form>
			</Modal>
			<Wrap>
				<Title>{d('profile.administeredBands.title')}</Title>
				<AdminScroll>
					{administeredBands.length !== 0 ? (
						administeredBands.map(({ alias, name, avatar }) => (
							<Row key={alias}>
								<AdministeredBand alias={alias} avatar={avatar} name={name} onRemove={remove} />
								<Line />
							</Row>
						))
					) : (
						<Empty>{d('profile.administeredBands.empty')}</Empty>
					)}
				</AdminScroll>
				<div>
					<Button color="simple" onClick={openCreateModal}>
						{d('profile.administeredBands.add.button')}
					</Button>
				</div>
			</Wrap>
		</Container>
	);
};

export default AdministeredBands;

const AdminBand = styled.div`
	display: flex;
	place-content: space-between;
	align-items: center;
`;

const AdminButton = styled.div`
	display: flex;
	align-items: center;
`;

const Line = styled.div`
	position: relative;
	margin-left: 53px;
	border-bottom: 1px solid #bab9b9;
`;

interface BandProps {
	alias: string;
	avatar: ResponseLink;
	name: string;
	onRemove: (alias: string) => void;
}

const AdministeredBand: React.FC<BandProps> = ({ alias, avatar, name, onRemove }) => {
	const remove = useCallback(() => {
		onRemove(alias);
	}, [onRemove, alias]);

	return (
		<AdminBand>
			<AlignCenter>
				<StyledLink
					location={{
						query: {
							alias,
						},
						pathname: '/band',
					}}
					asLocation={{
						pathname: `/band/${alias}`,
					}}
				>
					<Avatar src={avatar.src} alt={`${name}'s avatar`} />
					<span>{name}</span>
				</StyledLink>
			</AlignCenter>
			<Dummy loading={REMOVE_BAND_TASK} transparent={true}>
				<AdminButton>
					<Button color="simple" onClick={remove}>
						{d('profile.administeredBands.delete.button')}
					</Button>
				</AdminButton>
			</Dummy>
		</AdminBand>
	);
};
