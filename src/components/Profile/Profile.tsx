import React, { useCallback, useContext, useEffect } from 'react';
import { getProfile, updateProfile } from 'Schemas/Profile';
import { useQuery } from '@apollo/react-hooks';
import {
	BandUpdateInput,
	GraphQLWrap,
	ProfileUpdateInput,
	Response,
	ResponseConversation,
	ResponseLink,
	ResponseProfile,
	ResponseUser,
} from 'Types/response.type';
import { getConversations, getOrCreateConversation as getOrCreateConversationMutation } from 'Schemas/Conversation';
import Card from 'Components/Card';
import styled from 'styled-components';
import { appContext } from 'Contexts/app.context';
import { d } from 'Utils/dictionary';
import Button from 'UI/Button';
import { CONVERSATIONS_QUERY_VARIABLES } from 'Components/Conversations/Conversations';
import { apolloClient } from '../../pages/_app';
import { routePush } from 'Components/Link';
import AdministeredBands from './AdministeredBands';
import MemberInBands from './MemberInBands';
import AdditionalBlock from './AdditionalBlock';
import { getUser } from 'Schemas/User';
import { loaderContext } from 'Contexts/loader.context';
import Dummy from 'UI/Dummy/Dummy';
import { errorContext } from 'Contexts/error.context';
import { getDefaultAvatars } from 'Schemas/Link';
import { CityFilter, CountryFilter, GenreFilter, PositionFilter } from 'Types/filters.type';
import { getFilters } from 'Schemas/Filters';
import { dummyProfileData } from 'Utils/data';
import { useSecureMutation } from 'Utils/apollo';

interface Props {
	login?: string;
}

const Container = styled.div`
	position: relative;
	top: 0;
	color: white;
	padding: 40px;

	&:after {
		content: '';
		position: absolute;
		top: 0;
		right: 0;
		width: 100%;
		height: 100%;
		background-color: rgba(25, 31, 33, 0.54);
		background-attachment: fixed;
		border-radius: 4px;
		z-index: 0;
	}
`;

const Wrap = styled.div`
	display: flex;
	flex-direction: column;
	position: relative;
	margin: 0 auto;
	width: 100%;
	max-width: ${({ theme }) => theme.other.containerMaxWidth};
	z-index: 1;
`;

const ButtonContainer = styled.div`
	margin-bottom: 10px;
	width: 100%;
	display: flex;
	place-content: flex-end;
`;

const StyledButton = styled(Button)`
	min-width: 200px;
`;

const Flex = styled.div`
	display: flex;
	place-content: space-between;
	margin-top: 20px;
`;

const BoxDummy = styled(Dummy)`
	height: 150px;
	width: 100%;

	&:first-child {
		margin-right: 20px;
	}
`;

const Profile: React.FC<Props> = ({ login }) => {
	const { user, loading: userLoading } = useContext(appContext);
	const { clearProgress } = useContext(loaderContext);
	const { setError } = useContext(errorContext);

	const variables: { login?: string } = {};
	if (login !== user?.login) {
		variables.login = login;
	}

	const { data: avatarsData } = useQuery<
		GraphQLWrap<'defaultAvatars', ResponseLink[]>,
		{
			type: string;
		}
	>(getDefaultAvatars, {
		ssr: false,
		variables: {
			type: 'profile',
		},
	});
	const defaultAvatars =
		avatarsData?.defaultAvatars !== undefined && avatarsData.defaultAvatars !== null ? avatarsData.defaultAvatars : [];

	const { data: filtersData } = useQuery<{
		cities: CityFilter[];
		countries: CountryFilter[];
		genres: GenreFilter[];
		positions: PositionFilter[];
	}>(getFilters, {
		ssr: false,
	});

	const { data: profileData, loading } = useQuery<GraphQLWrap<'profile', ResponseProfile>, { login?: string }>(
		getProfile,
		{
			ssr: login !== undefined,
			variables,
			onError: (error) => {
				setError(error.message);
			},
		},
	);

	const profile: ResponseProfile =
		profileData?.profile !== undefined && profileData.profile !== null ? profileData.profile : dummyProfileData;

	const isMe = user?.login === profile.login;

	const [getOrCreateConversation] = useSecureMutation<
		GraphQLWrap<'getOrCreateConversation', ResponseConversation>,
		{
			participantLogin: string;
		}
	>(getOrCreateConversationMutation);

	const openConversation = useCallback(() => {
		if (profile.login !== undefined) {
			void getOrCreateConversation({
				variables: {
					participantLogin: profile.login,
				},
				update: (cache, { data }) => {
					const newConversation = data?.getOrCreateConversation;
					if (newConversation === undefined) {
						console.error(`can't get or create new conversation with login '${profile.login}'`);
						return;
					}

					/* because of conversation's page loaded not immediately and can be absent in cache it needs to write new
					conversation by async way so the only way to update cache is using directly ApolloClient instance */
					const writeInQuery = (cacheObject = cache): void => {
						const oldData = cacheObject.readQuery<GraphQLWrap<'responseConversations', Response<ResponseConversation>>>(
							{
								query: getConversations,
								variables: CONVERSATIONS_QUERY_VARIABLES,
							},
						);

						if (oldData === null || oldData === undefined || oldData.responseConversations === null) {
							return;
						}

						if (oldData.responseConversations.data.every((conversation) => conversation._id !== newConversation._id)) {
							cacheObject.writeQuery<GraphQLWrap<'responseConversations', Response<ResponseConversation>>>({
								query: getConversations,
								variables: CONVERSATIONS_QUERY_VARIABLES,
								data: {
									...oldData,
									responseConversations: {
										...oldData.responseConversations,
										totalLength: oldData.responseConversations.totalLength + 1,
										data: [...oldData.responseConversations.data, newConversation],
									},
								},
							});
						}
					};

					try {
						writeInQuery();
					} catch (err) {
						const waitForQuery = (): void => {
							try {
								if (!location.pathname.includes('conversations')) {
									return;
								}
								writeInQuery(apolloClient);
							} catch (error) {
								setTimeout(waitForQuery, 1000);
							}
						};
						setTimeout(waitForQuery, 1000);
					} finally {
						void routePush(
							{
								pathname: '/conversations',
								query: {
									id: newConversation._id,
								},
							},
							{
								pathname: `/conversations/${newConversation._id}`,
								query: {},
							},
						);
					}
				},
			});
		}
	}, [profile.login, getOrCreateConversation]);

	const [saveProfile] = useSecureMutation<
		GraphQLWrap<'updateProfile', ResponseProfile>,
		{ newValue: ProfileUpdateInput }
	>(updateProfile);

	const onChange = useCallback(
		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		async (propName: keyof ProfileUpdateInput | keyof BandUpdateInput, value: any) => {
			await saveProfile({
				variables: {
					newValue: {
						[propName]: value,
					},
				},
				update: (cache, { data }) => {
					const oldData = cache.readQuery<GraphQLWrap<'user', ResponseUser>>({
						query: getUser,
					});
					if (oldData !== null) {
						cache.writeQuery<GraphQLWrap<'user', ResponseUser>>({
							query: getUser,
							data: {
								user: {
									...oldData.user,
									name: data?.updateProfile.name ?? oldData.user.name,
									avatar: data?.updateProfile.avatar ?? oldData.user.avatar,
								},
							},
						});
					}
				},
			}).catch((error: Error) => {
				setError(error.message);
			});

			clearProgress(`card.${propName}`);
		},
		[clearProgress, setError, saveProfile],
	);

	useEffect(() => {
		if (!loading && (profileData === null || profileData === undefined)) {
			void routePush({
				pathname: '/404',
				query: {},
			});
		}
	}, [loading, profileData]);

	return (
		<Container>
			<Wrap>
				{(userLoading || user !== null) && profile.settings.privacy.conversationsEnabled && !isMe && (
					<ButtonContainer>
						<Dummy loading={loading}>
							<StyledButton onClick={openConversation}>{d('profile.conversation')}</StyledButton>
						</Dummy>
					</ButtonContainer>
				)}
				<Card
					inCard={true}
					loading={loading}
					login={profile.login}
					email={profile.email}
					name={profile.name}
					description={profile.description}
					avatar={profile.avatar}
					defaultAvatars={defaultAvatars}
					country={profile.country}
					city={profile.city}
					experience={profile.experience}
					positions={profile.positions}
					genres={profile.genres}
					registeredWithSocial={profile.registeredWithSocial}
					filtersData={filtersData}
					relations={profile.relations}
					dignities={profile.dignities}
					onChange={isMe ? onChange : undefined}
				/>
				{isMe && user !== null && (
					<Flex>
						<MemberInBands login={profile.login} memberInBands={profile.memberInBands} />
						<AdministeredBands administeredBands={user.administeredBands} />
					</Flex>
				)}
				{loading && (
					<Flex>
						<BoxDummy loading={loading} />
						<BoxDummy loading={loading} />
					</Flex>
				)}
				<AdditionalBlock
					login={profile.login}
					audios={profile.audios}
					statistics={profile.statistics}
					loading={loading}
					privacySettings={profile.settings.privacy}
				/>
			</Wrap>
		</Container>
	);
};

export default Profile;
