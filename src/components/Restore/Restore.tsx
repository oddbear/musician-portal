import React, { useCallback, useContext, useEffect, useReducer, useState } from 'react';
import Form, { Input, PrevChildren, Step } from 'UI/Form';
import Button from 'UI/Button/Button';
import styled from 'styled-components';
import { d } from 'Utils/dictionary';
import { PASSWORD_MAX_LENGTH, PASSWORD_MIN_LENGTH, RESTORE_CODE_LENGTH, RESTORE_SEND_DELAY } from 'Utils/constants';
import { restorePassword as restorePasswordMutation, sendRestoreCode as sendRestoreCodeMutation } from 'Schemas/User';
import { GraphQLWrap } from 'Types/response.type';
import { loaderContext } from 'Contexts/loader.context';
import { errorContext } from 'Contexts/error.context';
import Dummy from 'UI/Dummy/Dummy';
import { routePush } from 'Components/Link';
import { getFormattedDate } from 'Utils/date';
import { useSecureMutation } from 'Utils/apollo';

const CHECK_CODE_TASK = 'CHECK_CODE_TASK';
const RESTORE_CODE_TASK = 'RESTORE_CODE_TASK';

const Container = styled.div`
	position: relative;
	top: 0;
	color: white;
	padding: 40px;

	&:after {
		content: '';
		position: absolute;
		top: 0;
		right: 0;
		width: 100%;
		height: 100%;
		background-color: rgba(25, 31, 33, 0.54);
		background-attachment: fixed;
		border-radius: 4px;
		z-index: 0;
	}
`;

const Wrap = styled.div`
	display: flex;
	flex-direction: column;
	position: relative;
	margin: 0 auto;
	width: 100%;
	max-width: ${({ theme }) => theme.other.containerMaxWidth};
	z-index: 1;
`;

const StyledForm = styled(Form)`
	width: 500px;

	${PrevChildren} {
		display: none;
	}
`;

const Flex = styled.div`
	display: flex;
`;

const StyledButton = styled(Button)`
	margin-left: 10px;
`;

const Title = styled.h3`
	font-size: 1rem;
`;

const Retry = styled.span`
	cursor: pointer;
	color: dodgerblue;
	text-decoration: underline;
`;

const CodeInput = styled(Input)`
	text-transform: uppercase;
`;

type Reducer = (state: number, waitTime: number) => number;
const reducer: Reducer = (state, waitTime) => Math.min(RESTORE_SEND_DELAY, Math.max(0, state + waitTime));

const Restore: React.FC = () => {
	const [email, setEmail] = useState<string>('');
	const [code, setCode] = useState<string>('');
	const [password, setPassword] = useState<string>('');
	const [repeat, setRepeat] = useState<string>('');
	const [step, setStep] = useState<number>(0);
	const [delayBeforeRetry, changeDelayBeforeRetry] = useReducer<Reducer>(reducer, 0);

	const { clearProgress, incrementProgress } = useContext(loaderContext);
	const { setError } = useContext(errorContext);

	const [sendRestoreCode] = useSecureMutation<GraphQLWrap<'sendRestoreCode', boolean>, { email: string }>(
		sendRestoreCodeMutation,
		{
			variables: {
				email,
			},
		},
	);

	const [restorePassword] = useSecureMutation<
		GraphQLWrap<'restorePassword', boolean>,
		{ email: string; code: string; password?: string }
	>(restorePasswordMutation, {
		variables: {
			email,
			code,
			password: password === '' ? undefined : password,
		},
	});

	const repeatValidation = useCallback((value: string): boolean => value === password, [password]);

	const sendRestoreMessage = useCallback(() => {
		grecaptcha.ready(() => {
			void grecaptcha.execute(config.google.recaptcha.siteKey, { action: 'signup' }).then(async () => {
				void sendRestoreCode().catch((err: Error) => setError(err.message));
				changeDelayBeforeRetry(RESTORE_SEND_DELAY);
				if (step === 0) {
					setStep(step + 1);
				}
			});
		});
	}, [setError, sendRestoreCode, step]);

	const checkCode = useCallback(async () => {
		incrementProgress(CHECK_CODE_TASK);

		await restorePassword()
			.then(() => {
				setStep(step + 1);
			})
			.catch((err: Error) => setError(err.message));

		clearProgress(CHECK_CODE_TASK);
	}, [restorePassword, step, setError, incrementProgress, clearProgress]);

	const restorePasswordAction = useCallback(async () => {
		incrementProgress(RESTORE_CODE_TASK);

		await restorePassword()
			.then(() => {
				void routePush({
					pathname: '/signin',
					query: {
						message: d('signin.messages.afterSignup'),
					},
				});
			})
			.catch((err: Error) => setError(err.message));

		clearProgress(RESTORE_CODE_TASK);
	}, [restorePassword, setError, incrementProgress, clearProgress]);

	useEffect(() => {
		let timer = 0;
		let lastTime = Date.now();

		(function update() {
			const time = Date.now();
			changeDelayBeforeRetry(lastTime - time);
			lastTime = time;

			timer = setTimeout(update, 1000);
		})();

		return () => clearTimeout(timer);
	}, []);

	const delayTime = getFormattedDate(delayBeforeRetry);

	return (
		<Container>
			<Wrap>
				{step === 0 && (
					<StyledForm onSubmit={sendRestoreMessage}>
						<Step>
							<Title>{d('restore.email.title')}</Title>
							<Flex>
								<Input
									name="email"
									textMemory={true}
									placeholder={d('restore.email.placeholder')}
									value={email}
									onChange={setEmail}
									validation={{
										required: {
											value: true,
											errorMessage: d('restore.email.error.required'),
										},
										match: {
											// eslint-disable-next-line no-control-regex
											value: /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)])/,
											errorMessage: d('restore.email.error.match'),
										},
									}}
								/>
								<StyledButton color="primary">{d('restore.button')}</StyledButton>
							</Flex>
						</Step>
					</StyledForm>
				)}
				{step === 1 && (
					<StyledForm onSubmit={checkCode}>
						<Step>
							<Title>{d('restore.code.title')}</Title>
							<Flex>
								<CodeInput
									name="code"
									placeholder={d('restore.code.placeholder')}
									value={code}
									onChange={setCode}
									validation={{
										required: {
											value: true,
											errorMessage: d('restore.code.error.required'),
										},
										minLength: {
											value: RESTORE_CODE_LENGTH,
											errorMessage: d('restore.code.error.length', RESTORE_CODE_LENGTH),
										},
										maxLength: {
											value: RESTORE_CODE_LENGTH,
											errorMessage: d('restore.code.error.length', RESTORE_CODE_LENGTH),
										},
									}}
								/>
								<Dummy loading={CHECK_CODE_TASK} transparent={true}>
									<StyledButton color="primary">{d('restore.button')}</StyledButton>
								</Dummy>
							</Flex>
							<span>{d('restore.noMessage')}</span>&nbsp;
							{delayBeforeRetry === 0 ? (
								<Retry onClick={sendRestoreMessage}>{d('restore.retry')}</Retry>
							) : (
								<span>{d('restore.delayBeforeRetry', `${delayTime.getMinutes()}:${delayTime.getSeconds()}`)}</span>
							)}
						</Step>
					</StyledForm>
				)}
				{step === 2 && (
					<StyledForm onSubmit={restorePasswordAction}>
						<Step>
							<Title>{d('restore.password.title')}</Title>
							<Input
								name="password"
								type="password"
								placeholder={d('restore.password.placeholder')}
								value={password}
								onChange={setPassword}
								validation={{
									required: {
										value: true,
										errorMessage: d('restore.password.error.required'),
									},
									minLength: {
										value: PASSWORD_MIN_LENGTH,
										errorMessage: d('restore.password.error.minLength', PASSWORD_MIN_LENGTH),
									},
									maxLength: {
										value: PASSWORD_MAX_LENGTH,
										errorMessage: d('restore.password.error.maxLength', PASSWORD_MAX_LENGTH),
									},
								}}
							/>
							<Input
								name="repeat"
								type="password"
								placeholder={d('restore.repeat.placeholder')}
								value={repeat}
								onChange={setRepeat}
								validation={{
									callback: {
										value: repeatValidation,
										errorMessage: d('restore.repeat.error.equal'),
									},
								}}
							/>
							<Dummy loading={RESTORE_CODE_TASK} transparent={true}>
								<Button color="primary">{d('restore.button')}</Button>
							</Dummy>
						</Step>
					</StyledForm>
				)}
			</Wrap>
		</Container>
	);
};

export default Restore;
