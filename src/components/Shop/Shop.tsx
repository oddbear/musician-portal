import React, { useCallback } from 'react';
import styled from 'styled-components';
import { d } from 'Utils/dictionary';
import Button from 'UI/Button/Button';
import { buyExtendPack as buyExtendPackMutation, buyRankPack as buyRankPackMutation } from 'Schemas/Profile';
import { GraphQLWrap } from 'Types/response.type';
import { useSecureMutation } from 'Utils/apollo';

const Container = styled.div`
	position: relative;
	top: 0;
	margin: 0 auto;
	color: white;
	padding: 110px 0 110px 0;
`;

const Wrap = styled.div`
	display: flex;
	position: relative;
	margin: 0 auto;
	width: 100%;
	max-width: ${({ theme }) => theme.other.containerMaxWidth};
	z-index: 1;
	flex-direction: row;
	place-content: space-between;
`;

const Card = styled.div`
	padding: 40px 20px;
	border: 1px solid black;
	margin: 0 20px;

	&:first-child {
		margin-left: 0;
	}

	&:last-child {
		margin-right: 0;
	}
`;

const Title = styled.div`
	font-weight: 700;
	text-align: center;
`;

const Shop: React.FC = () => {
	const [buyRankPack] = useSecureMutation<GraphQLWrap<'buyRankPack', boolean>, { type: string }>(buyRankPackMutation);
	const [buyExtendPack] = useSecureMutation<GraphQLWrap<'buyRankPack', boolean>, { type: string }>(
		buyExtendPackMutation,
	);

	const buyMonthlyRankPack = useCallback(() => {
		void buyRankPack({
			variables: {
				type: 'monthly',
			},
		});
	}, [buyRankPack]);

	const buyMonthlyExtendPack = useCallback(() => {
		void buyExtendPack({
			variables: {
				type: 'monthly',
			},
		});
	}, [buyExtendPack]);

	const buyAnnualRankPack = useCallback(() => {
		void buyRankPack({
			variables: {
				type: 'annual',
			},
		});
	}, [buyRankPack]);

	const buyAnnualExtendPack = useCallback(() => {
		void buyExtendPack({
			variables: {
				type: 'annual',
			},
		});
	}, [buyExtendPack]);

	return (
		<Container>
			<Wrap>
				<Card>
					<Title>{d('shop.extendPack.monthly')}</Title>
					<Button color="primary" onClick={buyMonthlyExtendPack}>
						{d('shop.buy')}
					</Button>
				</Card>
				<Card>
					<Title>{d('shop.extendPack.annual')}</Title>
					<Button color="primary" onClick={buyAnnualExtendPack}>
						{d('shop.buy')}
					</Button>
				</Card>
				<Card>
					<Title>{d('shop.rankPack.monthly')}</Title>
					<Button color="primary" onClick={buyMonthlyRankPack}>
						{d('shop.buy')}
					</Button>
				</Card>
				<Card>
					<Title>{d('shop.rankPack.annual')}</Title>
					<Button color="primary" onClick={buyAnnualRankPack}>
						{d('shop.buy')}
					</Button>
				</Card>
			</Wrap>
		</Container>
	);
};

export default Shop;
