import { useLazyLoad } from 'Hooks/lazyload.hook';
import React, { MouseEvent, useEffect, useState } from 'react';
import { useWindowSize } from 'Hooks/window-size.hook';
import { useGetRef } from 'Hooks/getRef.hook';
import styled from 'styled-components';

interface Props {
	src: string;
	alt: string;
	title?: string;
	fitHeight?: boolean;
	className?: string;
	getRef?: React.MutableRefObject<HTMLImageElement | null>;
	onClick?: (event: MouseEvent<HTMLElement>) => void;
	onMouseDown?: (event: MouseEvent<HTMLElement>) => void;
	onMouseUp?: (event: MouseEvent<HTMLElement>) => void;
}

const getCssSize = (size?: number, fitHeight?: boolean): string => {
	if (size === undefined) {
		return '';
	}

	return `${fitHeight === true ? 'width' : 'height'}: ${size.toString()}px`;
};

export interface ImageProps {
	size: number;
	fitHeight?: boolean;
}

const Image = styled.img<ImageProps>`
	${({ size, fitHeight }) => getCssSize(size, fitHeight)}
`;

const LazyImage: React.FC<Props> = (props) => {
	const isCompleteUrl = /\/\//.exec(props.src) !== null;

	const imageRef = props.getRef ?? useGetRef<HTMLImageElement>();
	const { src } = useLazyLoad({
		src: isCompleteUrl ? '' : props.src,
	});
	const [imageSize, setImageSize] = useState<number>(0);
	const [width, height] = useWindowSize();

	useEffect(() => {
		const image = imageRef.current;
		if (image !== null) {
			let ratio = image.naturalHeight / image.naturalWidth;
			if (isNaN(ratio)) {
				ratio = 0;
			}

			if (props.fitHeight === false) {
				setImageSize(image.offsetHeight / ratio);
			} else {
				setImageSize(ratio * image.offsetWidth);
			}
		}
	}, [imageRef, props.fitHeight, width, height]);

	return (
		<Image
			ref={imageRef}
			size={imageSize}
			fitHeight={props.fitHeight}
			src={isCompleteUrl ? props.src : src}
			alt={props.alt}
			title={props.title}
			className={props.className}
			onClick={props.onClick}
			onMouseDown={props.onMouseDown}
			onMouseUp={props.onMouseUp}
		/>
	);
};

LazyImage.defaultProps = {
	onClick: () => {
		return;
	},
};

export default LazyImage;
