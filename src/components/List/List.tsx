import React from 'react';
import styled, { css } from 'styled-components';

interface Props {
	horizontal?: boolean;
	className?: string;
	children: React.ReactNode;
}

interface UlProps {
	horizontal?: boolean;
}

const Ul = styled.ul<UlProps>`
	${({ horizontal = false }) =>
		horizontal &&
		css`
			display: inline-flex;
		`};
	padding: 0;
	margin: 0;

	&:last-child {
		padding: 0;
		margin: 0 0 0 90px;
	}

	li {
		${({ horizontal = false }) =>
			horizontal &&
			css`
				display: inline-block;
			`};
		list-style-type: none;
		margin: 0;
		padding: 0;
	}
`;

const List: React.FC<Props> = (props) => {
	return (
		<Ul className={props.className} horizontal={props.horizontal}>
			{props.children}
		</Ul>
	);
};

export default List;
