import React, { useCallback, useContext, useState } from 'react';
import Audio from 'UI/Audio';
import { GraphQLWrap, ResponseLink, ResponsePlaylist } from 'Types/response.type';
import { addTrackToPlaylist as addTrackToPlaylistMutation } from 'Schemas/Playlist';
import { errorContext } from 'Contexts/error.context';
import Tooltip from 'UI/Tooltip';
import { tooltipContext } from 'Contexts/tooltip.context';
import { appContext } from 'Contexts/app.context';
import { d } from 'Utils/dictionary';
import styled from 'styled-components';
import { useSecureMutation } from 'Utils/apollo';

interface Props {
	tracks: ResponseLink[];
	onRemove?: (_id: string) => void;
	onAddToPlaylist?: () => void;
}

const StyledTooltip = styled(Tooltip)`
	padding: 0;
`;

const List = styled.ul`
	padding: 0;
	font-family: ${({ theme }) => theme.fontFamily.quiet};
	font-size: 0.7rem;
	color: #666;

	li {
		display: flex;
		align-items: center;
		cursor: pointer;
		padding: 4px 20px;
		text-align: left;
		list-style-type: none;
		white-space: nowrap;

		&:hover {
			color: #333;
		}

		&:not(:last-child) {
			border-bottom: 1px solid #ccc;
		}
	}
`;

const Audios: React.FC<Props> = ({ tracks, onRemove, onAddToPlaylist }) => {
	const [attachedElement, setAttachedElement] = useState<HTMLElement | null>(null);
	const [tooltipVisible, setTooltipVisible] = useState<boolean>(false);
	const [context, setContext] = useState<{
		audioId?: string;
	}>({});

	const { user } = useContext(appContext);
	const { setError } = useContext(errorContext);

	const [addTrackToPlaylist] = useSecureMutation<
		GraphQLWrap<'addTrackToPlaylist', ResponsePlaylist>,
		{
			playlistId: string;
			audioId: string;
		}
	>(addTrackToPlaylistMutation);

	const addToPlaylist = useCallback(
		(playlistId: string) => async () => {
			if (context.audioId === undefined) {
				return;
			}

			await addTrackToPlaylist({
				variables: {
					playlistId,
					audioId: context.audioId,
				},
			}).catch((err: Error) => setError(err.message));

			onAddToPlaylist?.();
		},
		[context, addTrackToPlaylist, setError, onAddToPlaylist],
	);

	const changeVisible = useCallback(
		(value: boolean) => () => {
			setTooltipVisible(value);
		},
		[],
	);

	const availablePlaylists = (user?.playlists ?? []).filter((playlist) =>
		playlist.tracks.every(({ _id }) => context.audioId !== _id),
	);

	return (
		<tooltipContext.Provider
			value={{
				visible: tooltipVisible,
				setVisible: setTooltipVisible,
				attachedElement,
				setAttachedElement,
				setContext,
			}}
		>
			{tracks.length > 0 ? (
				<div>
					<StyledTooltip
						pointer={false}
						position="bottom"
						onMouseEnter={changeVisible(true)}
						onMouseLeave={changeVisible(false)}
					>
						<List>
							{availablePlaylists.length === 0 ? (
								<li>{d('audios.noAvailablePlaylists')}</li>
							) : (
								availablePlaylists.map((playlist) => (
									<li key={playlist._id} onClick={addToPlaylist(playlist._id)}>
										{playlist.name}
									</li>
								))
							)}
						</List>
					</StyledTooltip>
					{tracks.map(({ _id, description, src }) => (
						<Audio key={_id} _id={_id} name={description} src={src} onRemove={onRemove} />
					))}
				</div>
			) : (
				<div>{d('audios.empty')}</div>
			)}
		</tooltipContext.Provider>
	);
};

export default Audios;
