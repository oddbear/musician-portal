import React, { useEffect } from 'react';
import styled from 'styled-components';
import { d } from 'Utils/dictionary';
import { confirmEmail as confirmEmailMutation } from 'Schemas/User';
import { VerificationResponse } from 'Utils/enum';
import { GraphQLWrap } from 'Types/response.type';
import { routePush } from 'Components/Link';
import { useSecureMutation } from 'Utils/apollo';

interface Props {
	id: string;
}

const Container = styled.div`
	position: relative;
	width: 100%;
	max-width: ${({ theme }) => theme.other.containerMaxWidth};
	margin: 0 auto;
	padding: 100px 0;
`;

const Verification: React.FC<Props> = ({ id }) => {
	const [confirmEmail, { loading, data }] = useSecureMutation<
		GraphQLWrap<'confirmEmail', VerificationResponse>,
		{
			id: string;
		}
	>(confirmEmailMutation);

	useEffect(() => {
		void confirmEmail({
			variables: {
				id,
			},
		});
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	if (data !== undefined && !(data.confirmEmail in VerificationResponse)) {
		throw new Error(`invalid response from server on verification: ${JSON.stringify(data)}`);
	}

	if (data !== undefined && data.confirmEmail === VerificationResponse.correct) {
		setTimeout(() => {
			void routePush({
				pathname: '/',
				query: {},
			});
		}, 5000);
	}

	return (
		<Container>
			{loading ? (
				<span>{d('verification.loading')}</span>
			) : (
				data !== undefined && <span>{d(`verification.${VerificationResponse[data.confirmEmail]}`)}</span>
			)}
		</Container>
	);
};

export default Verification;
