import React from 'react';
import styled from 'styled-components';
import Blog from 'Components/Blog';
import { POSTS_LIMIT } from 'Utils/constants';

interface Props {
	id?: string;
}

const Container = styled.div`
	position: relative;
	top: 0;
	color: white;
	padding: 40px;

	&:after {
		content: '';
		position: absolute;
		top: 0;
		right: 0;
		width: 100%;
		height: 100%;
		background-color: rgba(25, 31, 33, 0.54);
		background-attachment: fixed;
		border-radius: 4px;
		z-index: 0;
	}
`;

const Wrap = styled.div`
	display: flex;
	flex-direction: row;
	position: relative;
	margin: 0 auto;
	width: 100%;
	max-width: ${({ theme }) => theme.other.containerMaxWidth};
	z-index: 1;
`;

const FiltersSection = styled.div`
	display: flex;
	flex-direction: column;
`;

const News: React.FC<Props> = ({ id }) => {
	return (
		<Container>
			<Wrap>
				<Blog
					editable={false}
					postQueryVariables={{
						_id: id,
						limit: POSTS_LIMIT,
						visibleData: [],
					}}
				/>
				<FiltersSection>
					<span />
				</FiltersSection>
			</Wrap>
		</Container>
	);
};

export default News;
