import React, { MutableRefObject, useCallback, useContext, useState } from 'react';
import styled from 'styled-components';
import { Response, ResponseComment, ResponseEmotion, ResponsePost, ResponseProfile } from 'Types/response.type';
import LazyImage from 'Components/LazyImage';
import { d } from 'Utils/dictionary';
import YouTube, { Options as YoutubeOptions } from 'react-youtube';
import { getFormattedDate } from 'Utils/date';
import Dummy from 'UI/Dummy/Dummy';
import { errorContext } from 'Contexts/error.context';
import { loaderContext } from 'Contexts/loader.context';
import { appContext } from 'Contexts/app.context';
import Link from 'Components/Link/Link';
import EmotionsBar from 'UI/EmotionsBar';
import { useGetRef } from 'Hooks/getRef.hook';
import { MORE_SRC } from 'Components/Post/Post';
import { tooltipContext } from 'Contexts/tooltip.context';
import Scroll from 'UI/Scroll';
import EditForm from 'Components/Blog/EditForm';
import Message from 'Components/Post/Message';

export const COMMENT_ACTION_TASK = 'COMMENT_ACTION_TASK';
const LOADING_MORE_TASK = 'LOADING_MORE_TASK';
const COMMENT_INPUT_NAME = 'COMMENT_INPUT_NAME';
const YOUTUBE_OPTIONS: YoutubeOptions = {
	height: '390',
	width: '640',
	playerVars: {
		autoplay: 0,
	},
};

interface Props {
	emotions: ResponseEmotion[];
	imagePreview: string | null;
	post: ResponsePost;
	comments: Response<ResponseComment>;
	setImagePreview: (src: string | null) => void;
	scrollTo: (element: HTMLDivElement) => void;
	onAddEmotion: (emotionAlias: string, commentId: string) => void;
	onAddComment: (input: { text?: string; images: Array<File | Blob>; video?: string }) => Promise<void>;
	onShowMore: () => Promise<void>;
}

const Container = styled.div`
	position: relative;
	display: block;
	font-size: 1rem;
	margin-left: 50px;
`;

const CommentWrap = styled.div`
	display: flex;
	flex-direction: row;
	font-size: 0.8em;
`;

const ProfileInfo = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
	margin-right: 20px;
	width: 50px;
`;

const Avatar = styled(LazyImage)`
	height: 42px;
	width: 42px;
`;

const CommentImage = styled(LazyImage)`
	cursor: pointer;
	height: 150px;
	max-width: 400px;
	margin-right: 10px;
`;

const Date = styled.div`
	color: gray;
	font-size: 0.9em;
`;

export const MessageWrap = styled(Scroll)`
	color: black;
	font-family: inherit;
	overflow: hidden;
`;

const LoadMore = styled.div`
	cursor: pointer;
	color: dodgerblue;
	font-size: 0.8em;
`;

const Name = styled.span`
	width: 100%;
	overflow: hidden;
	text-overflow: ellipsis;
	text-align: center;
`;

const CommentBody = styled.div`
	position: relative;
	width: 100%;
`;

const StyledEmotionsBar = styled(EmotionsBar)`
	margin: 10px 0;
`;

const Functions = styled.div`
	display: flex;
	flex-direction: row;
	padding-bottom: 10px;
	margin-bottom: 10px;
	border-bottom: 1px solid #aaa;
`;

const FunctionText = styled.span`
	cursor: pointer;
	font-size: 0.8em;
	color: dodgerblue;
`;

const More = styled(LazyImage)`
	cursor: pointer;
	position: absolute;
	top: 5px;
	right: 10px;
	width: 20px;
`;

const Comments: React.FC<Props> = ({
	emotions,
	post,
	comments,
	imagePreview,
	scrollTo,
	onAddEmotion,
	onAddComment,
	onShowMore,
	setImagePreview,
}) => {
	const [setText, setSetText] = useState<(src: string) => void>(() => () => {
		return;
	});
	const textareaRef = useGetRef<HTMLTextAreaElement>();
	const inputContainerRef = useGetRef<HTMLDivElement>();

	const postId = post._id;
	const commentInputName = `${COMMENT_INPUT_NAME}_${postId}`;

	const { user, loading } = useContext(appContext);
	const { setError } = useContext(errorContext);
	const { incrementProgress, clearProgress } = useContext(loaderContext);

	const submitComment = useCallback(
		async (text: string, images: Array<File | Blob>, video?: string): Promise<void> =>
			new Promise((res) => {
				if (text === '' && images.length === 0 && video === undefined) {
					return;
				}

				return onAddComment({
					text,
					images,
					video,
				})
					.catch((err: Error) => {
						setError(err.message);
					})
					.then(() => res());
			}),
		[setError, onAddComment],
	);

	const showMore = useCallback(async () => {
		incrementProgress(LOADING_MORE_TASK);
		await onShowMore();
		clearProgress(LOADING_MORE_TASK);
	}, [onShowMore, incrementProgress, clearProgress]);

	const setTextHandler = useCallback((handler: (src: string) => void) => {
		setSetText(() => handler);
	}, []);

	return (
		<Container ref={inputContainerRef}>
			<div>
				{comments.data
					.sort((a, b) => (a.createdAt > b.createdAt ? 1 : -1))
					.map((comment) => (
						<Comment
							key={comment._id}
							emotions={emotions}
							comment={comment}
							onAddEmotion={onAddEmotion}
							scrollTo={scrollTo}
							post={post}
							setImagePreview={setImagePreview}
							setText={setText}
							inputContainerRef={inputContainerRef}
							textareaRef={textareaRef}
						/>
					))}
			</div>
			{comments.totalLength > comments.data.length && (
				<Dummy loading={LOADING_MORE_TASK} transparent={true}>
					<LoadMore onClick={showMore}>
						{d('blog.comments.moreComments', comments.totalLength - comments.data.length)}
					</LoadMore>
				</Dummy>
			)}
			{(loading || user !== null) && (
				<EditForm
					emotions={emotions}
					taskName={commentInputName}
					imagePreview={imagePreview}
					onSubmit={submitComment}
					setImagePreview={setImagePreview}
					setTextHandler={setTextHandler}
				/>
			)}
		</Container>
	);
};

export default Comments;

interface CommentProps {
	emotions: ResponseEmotion[];
	comment: ResponseComment;
	post: ResponsePost;
	onAddEmotion: (emotionAlias: string, commentId: string) => void;
	scrollTo: (element: HTMLDivElement) => void;
	setText: (text: string) => void;
	textareaRef: MutableRefObject<HTMLTextAreaElement | null>;
	inputContainerRef: MutableRefObject<HTMLDivElement | null>;
	setImagePreview: (src: null | string) => void;
}

const Comment: React.FC<CommentProps> = ({
	emotions,
	comment,
	post,
	setText,
	textareaRef,
	inputContainerRef,
	setImagePreview,
	onAddEmotion,
	scrollTo,
}) => {
	const moreRef = useGetRef<HTMLImageElement>();

	const { setAttachedElement, setContext } = useContext(tooltipContext);

	const more = useCallback(
		(commentArg: ResponseComment) => () => {
			setAttachedElement?.(moreRef.current);
			setContext?.({
				post,
				comment: commentArg,
			});
		},
		[post, setContext, setAttachedElement, moreRef],
	);

	const answer = useCallback(
		(author: ResponseProfile) => () => {
			setText(`@${author.login}, `);
			if (textareaRef.current !== null) {
				textareaRef.current.select();
			}
			if (inputContainerRef.current !== null) {
				scrollTo(inputContainerRef.current);
			}
		},
		[setText, scrollTo, textareaRef, inputContainerRef],
	);

	const addEmotion = useCallback(
		(_id: string) => (emotionAlias: string) => {
			onAddEmotion(emotionAlias, _id);
		},
		[onAddEmotion],
	);

	const changeImagePreview = useCallback(
		(src: string | null) => () => {
			setImagePreview(src);
		},
		[setImagePreview],
	);

	const { getMinutes, getHours, getDay, getMonthName, getFullYear } = getFormattedDate(comment.createdAt);

	return (
		<Dummy key={comment._id} loading={`${COMMENT_ACTION_TASK}_${comment._id}`} transparent={true}>
			<CommentWrap>
				<Link
					location={{
						pathname: '/profile',
						query: {
							login: comment.author.login,
						},
					}}
					asLocation={{
						pathname: `/profile/${comment.author.login}`,
					}}
				>
					<ProfileInfo>
						<Name>{comment.author.name}</Name>
						<Avatar src={comment.author.avatar.src} alt="author avatar" fitHeight={true} />
					</ProfileInfo>
				</Link>
				<CommentBody>
					<More src={MORE_SRC} alt="more" fitHeight={true} getRef={moreRef} onClick={more(comment)} />
					<Message
						text={comment.text}
						mentions={comment.mentions}
						emotions={emotions.filter(({ dignity }) => comment.author.dignities.some(({ name }) => name === dignity))}
					/>
					{comment.video !== null && <YouTube videoId={comment.video} opts={YOUTUBE_OPTIONS} />}
					{comment.images.map(({ src }) => (
						<CommentImage key={src} src={src} alt="uploaded image" onClick={changeImagePreview(src)} />
					))}
					<Date>{`${getDay()} ${getMonthName()} ${getFullYear()} ${getHours()}:${getMinutes()}`}</Date>
				</CommentBody>
			</CommentWrap>
			<StyledEmotionsBar _id={comment._id} reactions={comment.reactions} addEmotion={addEmotion(comment._id)} />
			<Functions>
				<FunctionText onClick={answer(comment.author)}>{d('blog.comments.functions.answer')}</FunctionText>
			</Functions>
		</Dummy>
	);
};
