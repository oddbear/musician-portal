import React, { useEffect } from 'react';
import { ResponseEmotion, ResponseMention } from 'Types/response.type';
import { wrap } from 'Utils/dictionary';
import { MessageWrap } from 'Components/Post';
import styled from 'styled-components';
import Link from 'Components/Link/Link';
import LazyImage from 'Components/LazyImage';
import { Replacer } from 'Utils/replacer';
import { mentionRegexp, urlRegexp } from 'Utils/regexp';

const replacer = new Replacer([
	{
		key: 'link',
		regexp: new RegExp(urlRegexp, 'g'),
	},
	{
		key: 'mention',
		regexp: new RegExp(mentionRegexp, 'g'),
		splittable: false,
	},
	{
		key: 'emotion',
		regexp: /$^/g,
	},
]);

interface Props {
	text: string | null;
	mentions: ResponseMention[];
	emotions: ResponseEmotion[];
}

const String = styled.div`
	display: flex;
	flex-flow: row wrap;
	align-items: center;
	margin: 0;
`;

const Mention = styled(Link)`
	display: flex;
	align-items: center;
	color: dodgerblue !important;
	margin: 0 2px;
`;

const LittleAvatar = styled(LazyImage)`
	width: 24px;
	height: 24px;
	margin-right: 4px;
`;

const Href = styled.a`
	color: dodgerblue;
`;

const Text = styled.span`
	margin-right: 4px;

	&:not(:first-child) {
		margin: 0 4px;
	}
`;

const Message: React.FC<Props> = ({ text, mentions, emotions }) => {
	useEffect(() => {
		if (emotions.length > 0) {
			const rule = replacer.rules.find(({ key }) => key === 'emotion')!;
			rule.regexp = (chunkText) => [
				...chunkText.matchAll(new RegExp(`[${emotions.map(({ alias }) => alias).join('')}]`, 'g')),
			];
		}

		console.log('afsdsd', new RegExp(`[${emotions.map(({ alias }) => alias).join('')}]`, 'g'));
	}, [emotions]);

	return (
		<MessageWrap>
			{wrap(text ?? '', (html, type, index) => (
				<String key={index}>
					{replacer.parse(html, (element, i) => {
						if (element.key === 'mention') {
							const matchedProfile = mentions.find(({ profile }) => profile.login === element.chunkText)?.profile;
							if (matchedProfile !== undefined) {
								return (
									<Mention
										key={`mention_${i.toString()}`}
										location={{
											pathname: '/profile',
											query: {
												login: matchedProfile.login,
											},
										}}
										asLocation={{
											pathname: `/profile/${matchedProfile.login}`,
										}}
									>
										<LittleAvatar src={matchedProfile.avatar.src} alt={`${matchedProfile.login}'s avatar`} />
										<span>{matchedProfile.name}</span>
									</Mention>
								);
							}
						} else if (element.key === 'link') {
							return (
								<Href key={`link_${i.toString()}`} href={element.chunkText}>
									{element.chunkText}
								</Href>
							);
						} else if (element.key === 'emotion') {
							return (
								<LazyImage
									key={`emotion_${i.toString()}`}
									src={emotions.find(({ alias }) => element.chunkText === alias)?.src ?? ''}
									alt={element.chunkText}
								/>
							);
						}

						return <Text key={`text_${i.toString()}`}>{element.chunkText}</Text>;
					})}
				</String>
			))}
		</MessageWrap>
	);
};

export default Message;
