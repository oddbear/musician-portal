import React, { MouseEvent, useCallback, useContext, useEffect } from 'react';
import Link from 'Components/Link/Link';
import YouTube, { Options as YoutubeOptions } from 'react-youtube';
import styled from 'styled-components';
import LazyImage from 'Components/LazyImage';
import EmotionsBar from 'UI/EmotionsBar';
import { CommentCreateInput, ResponseEmotion, ResponsePost } from 'Types/response.type';
import { getFormattedDate } from 'Utils/date';
import Comments from './Comments';
import { tooltipContext } from 'Contexts/tooltip.context';
import Scroll from 'UI/Scroll';
import Message from './Message';

export const MORE_SRC = 'elements/yatjwjjbabxi18fnkabe';
const YOUTUBE_OPTIONS: YoutubeOptions = {
	height: '390',
	width: '640',
	playerVars: {
		autoplay: 0,
	},
};

interface Props {
	emotions: ResponseEmotion[];
	isMyBand: boolean;
	imagePreview: string | null;
	post: ResponsePost;
	scrollTo: (element: HTMLDivElement) => void;
	postQueryVariables: {
		_id?: string;
		alias?: string;
		limit: number;
		visibleData: string[];
	};
	onAddEmotion: (postOrCommentId: string, emotionAlias: string, isPost?: boolean) => Promise<void>;
	onAddComment: (input: CommentCreateInput) => Promise<void>;
	onShowMoreComments: (post: ResponsePost) => Promise<void>;
	onSetImagePreview: (src: string | null) => void;
	init: (_id: string) => void;
}

const Wrap = styled.div`
	display: flex;
	flex-direction: row;
	width: 100%;
`;

const ProfileInfo = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
	margin-right: 20px;
	width: 50px;
`;

const Container = styled.div`
	display: flex;
	position: relative;
	flex-direction: column;
	width: 100%;
	padding: 10px 20px;
	box-sizing: border-box;
`;

export const MessageWrap = styled(Scroll)`
	color: black;
	font-family: inherit;
	overflow: hidden;
`;

const Body = styled.div`
	position: relative;
	width: 100%;
`;

const Avatar = styled(LazyImage)`
	height: 62px;
	width: 62px;
`;

const Date = styled.div`
	color: gray;
	font-size: 0.9em;
`;

const PostImage = styled(LazyImage)`
	cursor: pointer;
	height: 150px;
	max-width: 400px;
	margin-right: 10px;
`;

const VideoWrap = styled.div`
	width: 400px;
	height: ${400 * (360 / 640)}px;

	& > div {
		height: 100%;
	}

	iframe {
		width: 100%;
		height: 100%;
	}

	&:empty {
		display: none;
	}
`;

const Name = styled.span`
	width: 100%;
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
	text-align: center;
`;

const StyledEmotionsBar = styled(EmotionsBar)`
	padding: 10px 0;
	margin-bottom: 10px;
	border-bottom: 1px solid #aaa;
`;

const More = styled(LazyImage)`
	cursor: pointer;
	position: absolute;
	top: 5px;
	right: 10px;
	width: 20px;
`;

const Post: React.FC<Props> = ({
	emotions,
	post,
	imagePreview,
	scrollTo,
	onSetImagePreview,
	onAddEmotion,
	onAddComment,
	onShowMoreComments,
	init,
}) => {
	const { setAttachedElement, setContext } = useContext(tooltipContext);

	useEffect(() => {
		init(post._id);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const changeImagePreview = useCallback(
		(src: string | null) => () => {
			onSetImagePreview(src);
		},
		[onSetImagePreview],
	);

	const addEmotion = useCallback(
		(postId: string) => async (emotionAlias: string, commentId?: string) => {
			await onAddEmotion(commentId ?? postId, emotionAlias, commentId === undefined);
		},
		[onAddEmotion],
	);

	const showMoreComments = useCallback(
		(commentsPost: ResponsePost) => async () => {
			await onShowMoreComments(commentsPost);
		},
		[onShowMoreComments],
	);

	const addComment = useCallback(
		(postId: string) => async (input: { text?: string; images: Array<File | Blob>; video?: string }) => {
			await onAddComment({
				...input,
				postId,
				visibleData: post.comments.data.map(({ _id }) => _id),
			});
		},
		[onAddComment, post.comments.data],
	);

	const more = useCallback(
		(event: MouseEvent<HTMLElement>) => {
			setAttachedElement?.(event.currentTarget);
			setContext?.({
				post: post,
			});
		},
		[post, setContext, setAttachedElement],
	);

	const formattedDate = getFormattedDate(post.createdAt);

	return (
		<Container key={post._id}>
			<Wrap>
				<Link
					location={{
						pathname: '/band',
						query: {
							alias: post.author.alias,
						},
					}}
					asLocation={{
						pathname: `/band/${post.author.alias}`,
					}}
				>
					<ProfileInfo>
						<Name>{post.author.name}</Name>
						<Avatar src={post.author.avatar.src} alt="author avatar" fitHeight={true} />
					</ProfileInfo>
				</Link>
				<Body>
					<More src={MORE_SRC} alt="more" fitHeight={true} onClick={more} />
					<Date>{`${formattedDate.getDay()} ${formattedDate.getMonthName()} ${formattedDate.getFullYear()} ${formattedDate.getHours()}:${formattedDate.getMinutes()}`}</Date>
					<Message
						text={post.text}
						mentions={post.mentions}
						emotions={emotions.filter(({ dignity }) => post.author.dignities.some(({ name }) => name === dignity))}
					/>
					{post.video !== null && (
						<VideoWrap>
							<YouTube videoId={post.video} opts={YOUTUBE_OPTIONS} />
						</VideoWrap>
					)}
					{post.images.map(({ src }) => (
						<PostImage key={src} src={src} alt="uploaded image" onClick={changeImagePreview(src)} />
					))}
				</Body>
			</Wrap>
			<StyledEmotionsBar _id={post._id} isPost={true} reactions={post.reactions} addEmotion={addEmotion(post._id)} />
			<Comments
				emotions={emotions}
				imagePreview={imagePreview}
				post={post}
				comments={post.comments}
				setImagePreview={onSetImagePreview}
				scrollTo={scrollTo}
				onAddEmotion={addEmotion(post._id)}
				onAddComment={addComment(post._id)}
				onShowMore={showMoreComments(post)}
			/>
		</Container>
	);
};

export default Post;
