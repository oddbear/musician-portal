import React, { KeyboardEvent, useCallback, useState } from 'react';
import styled from 'styled-components';
import { ValidateOptions } from 'UI/Form/Input/validation';
import { Input } from 'UI/Form';

interface ValueProps {
	name?: string;
	input?: boolean;
	type?: 'password' | 'text';
	className?: string;
	children: string;
	placeholder?: string;
	validation?: ValidateOptions;
	onChange?: (value: string) => void;
	onAccept?: () => void;
	onCancel?: (isSpare?: boolean) => void;
}

const ValueSpan = styled.span`
	white-space: nowrap;
	overflow: hidden;
	text-overflow: ellipsis;
`;

const ValuePlaceholder = styled(ValueSpan)`
	color: gray;
`;

export const ValueInput = styled(Input)`
	color: inherit;
	border: none;
	background: none;
	padding: 0;
	overflow: hidden;
	box-shadow: inset 0 0 5px -3px transparent;
	transition: box-shadow ${({ theme }) => theme.other.defaultTransitionDuration};

	input {
		text-align: inherit;
		background: none;
		color: inherit;
		border: none;
		outline: none;
		padding: 0;
		text-overflow: ellipsis;
		text-transform: uppercase;
		font-size: 1em;
	}
`;

const Value: React.FC<ValueProps> = ({
	name,
	input,
	type,
	className,
	children,
	placeholder,
	validation,
	onChange,
	onCancel,
	onAccept,
}) => {
	const [isChanges, setIsChanges] = useState<boolean>(false);

	const change = useCallback(
		(value: string) => {
			setIsChanges(true);
			onChange?.(value);
		},
		[onChange],
	);

	const keyDown = useCallback(
		(event: KeyboardEvent<HTMLInputElement>) => {
			const target = event.currentTarget;

			if (event.key.toLowerCase() === 'enter') {
				onAccept?.();
				setIsChanges(false);
				setTimeout(() => {
					target.blur();
				}, 0);
			} else if (event.key.toLowerCase() === 'escape') {
				onCancel?.(true);
				setTimeout(() => {
					target.blur();
				}, 0);
			}
		},
		[onAccept, onCancel],
	);

	const cancel = useCallback(() => {
		if (isChanges) {
			onCancel?.();
		}
	}, [isChanges, onCancel]);

	if (input === true) {
		return (
			<ValueInput
				name={name ?? 'unnamed'}
				type={type}
				className={className}
				placeholder={placeholder}
				value={children}
				validation={validation}
				instantValidation={true}
				onChange={change}
				onBlur={cancel}
				onKeyDown={keyDown}
			/>
		);
	} else {
		return children !== '' ? (
			<ValueSpan className={className}>{children}</ValueSpan>
		) : (
			<ValuePlaceholder className={className}>{placeholder ?? ''}</ValuePlaceholder>
		);
	}
};

export default Value;
