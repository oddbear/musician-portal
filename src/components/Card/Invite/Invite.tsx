import React, { useCallback, useContext, useEffect, useState } from 'react';
import { ProfileRelations, Relations } from 'Types/response.type';
import { InviteRelations, InviteType } from 'Utils/enum';
import styled from 'styled-components';
import { appContext } from 'Contexts/app.context';
import { d } from 'Utils/dictionary';
import Button from 'UI/Button';

const WARN_COLOR = '#e7c2b1';

interface InviteProps {
	relations?: ProfileRelations | Relations[];
	position: string;
	onChoose(type: InviteType, band: string | null): void;
}

const InviteContainer = styled.div`
	width: 100%;
	display: flex;
	align-items: flex-start;
	place-content: flex-end;
	padding: 8px 3px 6px 3px;
	background-color: #ccc;
	box-sizing: border-box;
`;

interface FlexProps {
	direction?: string;
	flex?: string | number;
	width?: string | number;
	alignItems?: string;
	placeContent?: string;
}

const Flex = styled.div<FlexProps>`
	display: flex;
	width: ${({ width }) => width ?? '100%'};
	flex-direction: ${({ direction }) => direction ?? 'row'};
	flex: ${({ flex }) => flex ?? 'initial'};
	place-content: ${({ placeContent }) => placeContent ?? 'initial'};
	word-wrap: break-word;
`;

interface StyledButtonProps {
	inFilter?: boolean;
}

const StyledButton = styled(Button)<StyledButtonProps>`
	margin-left: 5px;
	padding-top: 3px;

	&:not(:disabled) {
		${({ inFilter, theme }) => (inFilter === true ? 'box-shadow: inset 0 0 5px -2px ' + theme.color.fourth + ';' : '')}
	}
`;

const Invite: React.FC<InviteProps> = ({ relations, position, onChoose }) => {
	const [currentBand, setCurrentBand] = useState<string | null>(null);
	const { user } = useContext(appContext);

	const choose = useCallback(
		(type: InviteType) => {
			onChoose(type, relations instanceof Array ? null : currentBand);
		},
		[onChoose, relations, currentBand],
	);

	useEffect(() => {
		setCurrentBand(null);
	}, [position]);

	if (relations === undefined) {
		return null;
	}

	const relation =
		relations instanceof Array
			? relations.find((rel) => rel.position.value === position)
			: currentBand !== null
			? relations[currentBand].find((rel) => rel.position.value === position)
			: undefined;

	return (
		<InviteContainer>
			{relations instanceof Array ? (
				<InviteButtons isBand={true} status={relation?.status} onChoose={choose} />
			) : (
				<Flex>
					{Object.keys(relations).map((bandAlias) => (
						<InviteBand
							key={bandAlias}
							alias={bandAlias}
							name={user?.administeredBands.find((band) => band.alias === bandAlias)?.name ?? ''}
							onChoose={setCurrentBand}
							warn={Object.entries(relations).some(
								([alias, rels]) => alias === bandAlias && rels.some((rel) => rel.position.value === position),
							)}
						>
							{currentBand === bandAlias && (
								<InviteButtons isBand={false} status={relation?.status} onChoose={choose} />
							)}
						</InviteBand>
					))}
				</Flex>
			)}
		</InviteContainer>
	);
};

interface InviteBandProps {
	alias: string;
	name: string;
	children: React.ReactNode;
	warn: boolean;
	onChoose(alias: string): void;
}

const BandName = styled.span<{ warn: boolean }>`
	cursor: pointer;
	font-size: 0.8rem;
	color: ${({ warn }) => (warn ? WARN_COLOR : 'inherit')};
	padding-bottom: 3px;
`;

const InviteBand: React.FC<InviteBandProps> = ({ alias, name, children, warn, onChoose }) => {
	const choose = useCallback(() => {
		onChoose(alias);
	}, [alias, onChoose]);

	return (
		<Flex direction="column" alignItems="center">
			<BandName onClick={choose} warn={warn}>
				{name}
			</BandName>
			<span>{children}</span>
		</Flex>
	);
};

interface InviteButtonsProps {
	isBand: boolean;
	status?: InviteRelations;
	onChoose(type: InviteType): void;
}

const InviteButtons: React.FC<InviteButtonsProps> = ({ status, isBand, onChoose }) => {
	const onCancel = useCallback(() => {
		onChoose(InviteType.cancel);
	}, [onChoose]);

	const onAccept = useCallback(() => {
		onChoose(InviteType.accept);
	}, [onChoose]);

	const onDecline = useCallback(() => {
		onChoose(InviteType.decline);
	}, [onChoose]);

	const onInvite = useCallback(() => {
		onChoose(InviteType.invite);
	}, [onChoose]);

	switch (status) {
		case InviteRelations.invited:
			return (
				<Flex placeContent="flex-end">
					<StyledButton color="simple" onClick={onCancel}>
						{d('joinBlock.cancel')}
					</StyledButton>
				</Flex>
			);

		case InviteRelations.invites:
			return (
				<Flex placeContent="flex-end">
					<StyledButton color="simple" onClick={onAccept}>
						{d(isBand ? 'joinBlock.acceptInvite' : 'joinBlock.acceptJoin')}
					</StyledButton>
					<StyledButton color="simple" onClick={onDecline}>
						{d('joinBlock.decline')}
					</StyledButton>
				</Flex>
			);

		case InviteRelations.unavailable:
			return (
				<Flex placeContent="flex-end">
					<StyledButton color="simple" onClick={onInvite} disabled={true}>
						{d('joinBlock.already')}
					</StyledButton>
				</Flex>
			);

		default:
			return (
				<Flex placeContent="flex-end">
					<StyledButton color="simple" onClick={onInvite}>
						{d(isBand ? 'joinBlock.join' : 'joinBlock.invite')}
					</StyledButton>
				</Flex>
			);
	}
};

export default Invite;
