import React, { useCallback, useContext, useEffect, useState } from 'react';
import styled from 'styled-components';
import {
	BandPosition,
	BandUpdateInput,
	GraphQLWrap,
	InviteInput,
	PositionInput,
	ProfilePosition,
	ProfileRelations,
	ProfileUpdateInput,
	Relations,
	Response,
	ResponseBand,
	ResponseCity,
	ResponseCountry,
	ResponseDignity,
	ResponseGenre,
	ResponseLink,
	ResponseNotification,
	ResponseProfile,
} from 'Types/response.type';
import LazyImage from 'Components/LazyImage';
import { d } from 'Utils/dictionary';
import Link, { routePush } from 'Components/Link';
import Button from 'UI/Button';
import { InviteRelations, InviteType, PositionVariant } from 'Utils/enum';
import { appContext } from 'Contexts/app.context';
import {
	sendOrChangeInviteToBand as sendOrChangeInviteToBandMutation,
	sendOrChangeInviteToProfile as sendOrChangeInviteToProfileMutation,
} from 'Schemas/Invite';
import { getNotifications as getNotificationsQuery, getNotificationsMutation } from 'Schemas/Notification';
import { DataProxy } from 'apollo-cache';
import { CityFilter, CountryFilter, GenreFilter, PositionFilter, Positions } from 'Types/filters.type';
import Modal from 'UI/Modal';
import Dummy from 'UI/Dummy/Dummy';
import { loaderContext } from 'Contexts/loader.context';
import Select, { Menu } from 'UI/Select';
import ControllableSelect from './ControllableSelect';
import Invite from './Invite';
import PositionButton from './PositionButton';
import ActionButton from './ActionButton';
import Value from './Value';
import {
	BAND_GENRE_LIMIT,
	BAND_GENRE_LIMIT_PREMIUM,
	BAND_POSITION_LIMIT,
	BAND_POSITION_LIMIT_PREMIUM,
	DESCRIPTION_MAX_LENGTH,
	EMAIL_MAX_LENGTH,
	EXPERIENCE_MAXIMUM_OFFSET,
	GA_CATEGORY_SEARCH,
	GA_LINK,
	NAME_MAX_LENGTH,
	NAME_MIN_LENGTH,
	NOTIFICATIONS_LIMIT,
	PASSWORD_MAX_LENGTH,
	PASSWORD_MIN_LENGTH,
	PROFILE_GENRE_LIMIT,
	PROFILE_GENRE_LIMIT_PREMIUM,
	PROFILE_POSITION_LIMIT,
	PROFILE_POSITION_LIMIT_PREMIUM,
} from 'Utils/constants';
import { getCachedDate, isExpired } from 'Utils/date';
import Form from 'UI/Form/Form';
import { errorContext } from 'Contexts/error.context';
import AvatarEditor from './AvatarEditor';
import { ValueInput } from 'Components/Card/Value/Value';
import { validateEmail } from 'Utils/regexp';
import Tablet from 'UI/Tablet/Tablet';
import ReactGA from 'react-ga';
import { useSecureMutation } from 'Utils/apollo';

interface Props {
	login?: string;
	alias?: string;
	email?: string | null;
	members?: ResponseProfile[];
	name: string;
	description: string | null;
	avatar: ResponseLink;
	country: ResponseCountry | null;
	city: ResponseCity | null;
	experience: number;
	positions: (ProfilePosition | BandPosition)[];
	genres: ResponseGenre[];
	registeredWithSocial?: boolean;
	relations: ProfileRelations | Relations[];
	defaultAvatars?: ResponseLink[];
	filterPositions?: Positions;
	filtersData?: {
		cities: CityFilter[];
		countries: CountryFilter[];
		genres: GenreFilter[];
		positions: PositionFilter[];
	};
	inCard?: boolean;
	loading?: boolean;
	className?: string;
	dignities: ResponseDignity[];
	repetitionBase?: boolean;
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	onChange?: (propName: keyof ProfileUpdateInput | keyof BandUpdateInput, value: any) => Promise<void>;
}

const Container = styled(Tablet)`
	font-size: 0.8rem;
`;

interface FlexProps {
	direction?: string;
	flex?: string | number;
	width?: string | number;
	alignItems?: string;
	placeContent?: string;
}

const Flex = styled.div<FlexProps>`
	display: flex;
	width: ${({ width }) => width ?? '100%'};
	flex-direction: ${({ direction }) => direction ?? 'row'};
	flex: ${({ flex }) => flex ?? 'initial'};
	place-content: ${({ placeContent }) => placeContent ?? 'initial'};
	align-items: ${({ alignItems }) => alignItems ?? 'initial'};
	word-wrap: break-word;
`;

const AreaWrap = styled.div`
	display: flex;
	flex-direction: column;
	align-items: flex-end;
`;

const Name = styled(Value)`
	width: 100%;
	text-align: center;
	font-size: 1.4rem;
	font-family: ${({ theme }) => theme.fontFamily.aggressive};

	input {
		font-family: ${({ theme }) => theme.fontFamily.aggressive};
	}
`;

const Password = styled(ValueInput)`
	width: 100%;
`;

const Avatar = styled(LazyImage)`
	width: 176px;
	height: 176px;
	position: relative;
	left: -19px;
	box-shadow: 0 0 4px -2px black;
	background-color: #eee;
`;

interface UnderlineProps {
	width?: number;
}

const Underline = styled.div<UnderlineProps>`
	width: ${({ width }) => (width !== undefined ? width.toString() + '%' : 'initial')};
	margin-left: ${({ width }) => `${(100 - (width ?? 100)).toString()}%`};
	border-bottom: 1px solid #aaa;
	display: flex;
	place-content: space-between;
	flex-direction: row;
	align-items: flex-end;
	padding-bottom: 3px;

	&:not(:first-child) {
		margin-top: 20px;
	}
`;

const Bold = styled.span`
	font-weight: bold;
`;

const RowName = styled.span`
	color: ${({ theme }) => theme.color.gray50};
`;

interface ProfileInfoProps {
	inCard: boolean;
}

const ProfileInfo = styled(Flex)<ProfileInfoProps>`
	padding-right: 44px;
	margin-right: 5px;
`;

const Buttons = styled.div`
	display: flex;
	place-content: flex-end;
	padding: 4px 0 3px;
`;

interface StyledButtonProps {
	inFilter?: boolean;
}

const StyledButton = styled(Button)<StyledButtonProps>`
	margin-left: 5px;
	padding-top: 3px;

	&:not(:disabled) {
		${({ inFilter, theme }) => (inFilter === true ? 'box-shadow: inset 0 0 5px -2px ' + theme.color.fourth + ';' : '')}
	}
`;

const Description = styled(Value)`
	word-break: break-all;
	width: 100%;
	text-align: left;
`;

const Email = styled(Value)`
	width: 100%;
`;

const ProfileDiv = styled.div`
	display: flex;
	flex-direction: column;

	& > div {
		text-align: left;
	}

	${Dummy} {
		&:first-child {
			margin-bottom: 10px;
			width: calc(100% - 38px);
		}
	}
`;

const StyledSelect = styled(Select)`
	width: auto;

	label {
		border-radius: 20px;
		padding: 0 6px;
	}

	${Menu} {
		top: 16px;
	}
`;

const SpanWithOffset = styled.span`
	margin-left: 6px;
`;

interface PositionStatusProps {
	status: PositionVariant;
}

const PositionStatus = styled.div<PositionStatusProps>`
	position: relative;
	font-family: ${({ theme }) => theme.fontFamily.quiet};
	font-size: 0.7rem;
	margin-top: 4px;
	color: ${({ status }) => (status === PositionVariant.available ? 'green' : 'gray')};
`;

const Rating = styled.div`
	font-size: 1rem;
	font-weight: bold;
	width: 100%;
	text-align: center;
	font-family: ${({ theme }) => theme.fontFamily.quiet};
`;

const AvatarEditorDummy = styled(Dummy)`
	width: 176px;
	height: 176px;
`;

const Card: React.FC<Props> = ({
	login,
	alias,
	email,
	members,
	name,
	description,
	avatar,
	defaultAvatars,
	country,
	city,
	experience,
	positions,
	genres,
	registeredWithSocial,
	relations,
	filterPositions,
	filtersData,
	inCard,
	loading,
	className,
	dignities,
	repetitionBase,
	onChange,
}) => {
	const inProfile = login !== undefined;

	const [confirm, setConfirm] = useState<{
		open: boolean;
		propName?: keyof ProfileUpdateInput;
		before?: string;
		after?: string;
	}>({
		open: false,
	});
	const [positionModal, setPositionModal] = useState<{
		open: boolean;
		positionIndex?: number;
		chooseProfile?: string | null;
	}>({
		open: false,
	});
	const [currentPosition, setCurrentPosition] = useState<string | null>(null);
	const { user } = useContext(appContext);
	const { incrementProgress } = useContext(loaderContext);
	const { setError } = useContext(errorContext);

	const positionsWithoutOwner = positions.filter(({ position }) => position.value !== 'owner');

	const [editPassword, setEditPassword] = useState<string | null>(null);
	const [editName, setEditName] = useState<string | null>(null);
	const [editDescription, setEditDescription] = useState<string | null>(null);
	const [editPosition, setEditPosition] = useState<string | null | undefined>(undefined);
	const [editGenre, setEditGenre] = useState<boolean>(false);
	const [editEmail, setEditEmail] = useState<string | null>(null);
	const [repeatPassword, setRepeatPassword] = useState<string>('');
	useEffect(() => {
		setEditName(name);
		setEditDescription(description);
	}, [name, description]);

	const [sendOrChangeInviteToProfile] = useSecureMutation<
		GraphQLWrap<'sendOrChangeInviteToProfile', ResponseProfile>,
		{
			invite: InviteInput;
		}
	>(sendOrChangeInviteToProfileMutation);
	const [sendOrChangeInviteToBand] = useSecureMutation<
		GraphQLWrap<'sendOrChangeInviteToBand', ResponseBand>,
		{
			invite: InviteInput;
		}
	>(sendOrChangeInviteToBandMutation);
	const [getNotifications] = useSecureMutation<
		GraphQLWrap<'getNotifications', Response<ResponseNotification>>,
		{
			limit: number;
			visibleData: string[];
		}
	>(getNotificationsMutation);

	const choose = useCallback(
		(type: InviteType, band: string | null) => {
			if ((alias === undefined && !inProfile) || currentPosition === null) {
				throw new Error('wrong card definition - position and login or alias must be specified');
			}

			const update = (cache: DataProxy): void => {
				if ([InviteType.invite, InviteType.cancel].includes(type)) {
					return;
				}

				const oldData = cache.readQuery<GraphQLWrap<'responseNotifications', Response<ResponseNotification>>>({
					query: getNotificationsQuery,
					variables: {
						limit: NOTIFICATIONS_LIMIT,
						visibleData: [],
					},
				});
				if (oldData === undefined || oldData === null || oldData.responseNotifications === null) {
					return;
				}

				void getNotifications({
					variables: {
						limit: oldData.responseNotifications.data.length,
						visibleData: [],
					},
					update: (updatedCache, { data }) => {
						if (data === null || data === undefined) {
							return;
						}

						updatedCache.writeQuery<GraphQLWrap<'responseNotifications', Response<ResponseNotification>>>({
							query: getNotificationsQuery,
							variables: {
								limit: NOTIFICATIONS_LIMIT,
								visibleData: [],
							},
							data: {
								responseNotifications: data.getNotifications,
							},
						});
					},
				});
			};

			if (inProfile && band !== null) {
				void sendOrChangeInviteToProfile({
					variables: {
						invite: {
							position: currentPosition,
							to: login!,
							from: band,
							type,
						},
					},
					update,
				});
			} else if (alias !== undefined && band === null) {
				void sendOrChangeInviteToBand({
					variables: {
						invite: {
							position: currentPosition,
							to: alias,
							type,
						},
					},
					update,
				});
			} else {
				throw new Error(
					"mutation sendOrChange invite can't be used for login and no band specified or alias and band specified",
				);
			}
		},
		[inProfile, getNotifications, alias, currentPosition, login, sendOrChangeInviteToBand, sendOrChangeInviteToProfile],
	);

	const changeConfirm = useCallback(
		(value: boolean) => () => {
			setConfirm({
				...confirm,
				open: value,
			});
		},
		[confirm],
	);

	const onClosePositionModal = useCallback(() => {
		setTimeout(() => {
			setPositionModal({
				open: false,
			});
		}, 100);
	}, []);

	const changePositionModal = useCallback(
		(value: boolean) => () => {
			setPositionModal({
				...positionModal,
				open: value,
			});
			onClosePositionModal();
		},
		[onClosePositionModal, positionModal],
	);

	const declineValue = useCallback(
		(propName?: keyof ProfileUpdateInput) => () => {
			changeConfirm(false)();

			switch (propName) {
				case 'name':
					setEditName(null);
					break;

				case 'description':
					setEditDescription(null);
					break;

				case 'email':
					setEditEmail(null);
					break;

				default:
					return;
			}
		},
		[changeConfirm],
	);

	const acceptValue = useCallback(
		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		(propName?: keyof ProfileUpdateInput) => async (value?: any) => {
			if (propName === undefined) {
				return;
			}

			let after;
			switch (propName) {
				case 'name':
					after = editName ?? '';
					break;

				case 'description':
					after = editDescription ?? '';
					break;

				case 'avatar':
					after = value;
					break;

				case 'email':
					after = editEmail;
					break;

				default:
					return;
			}

			incrementProgress(`card.${propName}`);

			changeConfirm(false)();

			await onChange?.(propName, after);
			declineValue(propName);
		},
		[incrementProgress, editDescription, editName, editEmail, changeConfirm, onChange, declineValue],
	);

	const spareValue = useCallback(
		(propName: keyof ProfileUpdateInput) => (isSpare = false) => {
			if (isSpare) {
				declineValue(propName)();
				return;
			}

			let before: string;
			let after: string;

			switch (propName) {
				case 'name':
					if (editName === null) {
						return;
					}

					before = name;
					after = editName;
					break;

				case 'description':
					if (editDescription === null) {
						return;
					}

					before = description ?? '';
					after = editDescription;
					break;

				case 'email':
					if (editEmail === null) {
						return;
					}

					before = email ?? '';
					after = editEmail;
					break;

				default:
					return;
			}

			if (before === after) {
				return;
			}

			setConfirm({
				open: true,
				propName,
				before,
				after,
			});
		},
		[declineValue, editName, name, editDescription, description, editEmail, email],
	);

	const addPosition = useCallback(() => {
		const premium = !isExpired(user?.extendPack ?? 0);
		const positionsLimitPremium = inProfile ? PROFILE_POSITION_LIMIT_PREMIUM : BAND_POSITION_LIMIT_PREMIUM;
		const positionsLimit = inProfile ? PROFILE_POSITION_LIMIT : BAND_POSITION_LIMIT;

		if (premium && positionsWithoutOwner.length > positionsLimitPremium) {
			setError(d('card.positions.limit', positionsLimitPremium));
		} else if (!premium && positionsWithoutOwner.length >= positionsLimit) {
			setError(d('card.positions.limit', positionsLimit), {
				title: d('card.inShop'),
				action: () => {
					void routePush({
						pathname: '/shop',
						query: {},
					});
				},
			});
		} else {
			setEditPosition(null);
		}
	}, [setError, positionsWithoutOwner, user, inProfile]);

	const addGenre = useCallback(() => {
		const premium = !isExpired(user?.extendPack ?? 0);
		const genresLimitPremium = inProfile ? PROFILE_GENRE_LIMIT_PREMIUM : BAND_GENRE_LIMIT_PREMIUM;
		const genresLimit = inProfile ? PROFILE_GENRE_LIMIT : BAND_GENRE_LIMIT;

		if (premium && genres.length >= genresLimitPremium) {
			setError(d('card.genres.limit', genresLimitPremium));
		} else if (!premium && genres.length > genresLimit) {
			setError(d('card.genres.limit', genresLimit), {
				title: d('card.inShop'),
				action: () => {
					void routePush({
						pathname: '/shop',
						query: {},
					});
				},
			});
		} else {
			setEditGenre(true);
		}
	}, [inProfile, setError, genres, user]);

	const editable = onChange !== undefined;

	const closeEditGenre = useCallback(() => {
		setEditGenre(false);
	}, []);

	const changeEditGenre = useCallback(
		(value: string | null) => {
			setEditGenre(false);
			if (value === null) {
				return;
			}

			incrementProgress('card.genres');

			void onChange?.('genres', [value].concat(genres.map((genre) => genre.value)));
		},
		[genres, onChange, incrementProgress],
	);

	const closeEditPosition = useCallback(() => {
		setEditPosition(undefined);
	}, []);

	const changeEditPosition = useCallback(
		async (value: string | null) => {
			setEditPosition(undefined);
			if (value === null) {
				return;
			}

			incrementProgress('card.positions');

			if (inProfile) {
				void onChange?.(
					'positions',
					positions.map((position) => (position as ProfilePosition).position.value).concat([value]),
				);
			} else {
				const positionIndex = positions.length;
				await onChange?.(
					'positions',
					positions
						.map(
							(position): PositionInput => {
								const p = position as BandPosition;
								return {
									value: p.position.value,
									status: p.status,
									profileLogin: p.profile?.login ?? null,
								};
							},
						)
						.concat([
							{
								value,
								status: PositionVariant.available,
								profileLogin: null,
							},
						]),
				);

				setPositionModal({
					open: true,
					positionIndex,
				});
			}
		},
		[inProfile, positions, onChange, incrementProgress],
	);

	const remove = useCallback(
		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		(propName: keyof ProfileUpdateInput, value: any, index: number) => () => {
			switch (propName) {
				case 'positions':
					if (!inProfile) {
						void onChange?.(
							'positions',
							positions
								.filter((_, i) => i !== index)
								.map((position) => ({
									value: position.position.value,
									status: (position as BandPosition).status,
									profileLogin: (position as BandPosition).profile?.login ?? null,
								})),
						);
					} else {
						void onChange?.(
							'positions',
							positions.filter((_, i) => i !== index).map((position) => position.position.value),
						);
					}
					break;

				case 'genres':
					const newGenres = [...genres];
					newGenres.splice(index, 1);

					void onChange?.(
						'genres',
						newGenres.map((genre) => genre.value),
					);
					break;

				default:
					return;
			}
		},
		[inProfile, positions, genres, onChange],
	);

	const editPositionModalWithProfile = useCallback(
		(profileLogin: string) => {
			let isMatched = false;
			const positionsInput: PositionInput[] = (positions as BandPosition[]).map(
				({ position, status, profile }, index) => {
					if (!isMatched && index === positionModal.positionIndex) {
						isMatched = true;
						return {
							value: position.value,
							status: PositionVariant.occupied,
							profileLogin,
						};
					} else {
						return {
							value: position.value,
							status: status,
							profileLogin: profile?.login ?? null,
						};
					}
				},
			);

			void onChange?.('positions', positionsInput);
			setPositionModal({
				...positionModal,
				open: false,
			});
			onClosePositionModal();
		},
		[onClosePositionModal, onChange, positionModal, positions],
	);

	const editPositionModal = useCallback(
		(positionStatus: PositionVariant) => () => {
			if (positionStatus === PositionVariant.available) {
				const positionsInput: PositionInput[] = (positions as BandPosition[]).map(
					({ position, status, profile }, index) => {
						if (index === positionModal.positionIndex) {
							return {
								value: position.value,
								status: positionStatus,
								profileLogin: null,
							};
						} else {
							return {
								value: position.value,
								status: status,
								profileLogin: profile?.login ?? null,
							};
						}
					},
				);

				void onChange?.('positions', positionsInput);
				setPositionModal({
					...positionModal,
					open: false,
				});
				onClosePositionModal();
			} else {
				setPositionModal({
					...positionModal,
					chooseProfile: null,
				});
			}
		},
		[onClosePositionModal, onChange, positionModal, positions],
	);

	const setEditPositionOpen = useCallback(
		(index: number) => () => {
			setPositionModal({
				...positionModal,
				chooseProfile:
					(positions[index] as BandPosition).status === PositionVariant.occupied
						? (positions[index] as BandPosition).profile?.login ?? null
						: undefined,
				positionIndex: index,
				open: true,
			});
		},
		[positions, positionModal],
	);

	const select = useCallback(
		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		(propName: keyof ProfileUpdateInput & keyof BandUpdateInput) => (value: any) => {
			if (propName === 'experience') {
				value = parseInt(value);
			}

			void onChange?.(propName, value);
		},
		[onChange],
	);

	const cancelPasswordEdit = useCallback(() => {
		setEditPassword(null);
		setRepeatPassword('');
	}, []);

	const acceptPassword = useCallback(async () => {
		if (editPassword !== null && onChange !== undefined) {
			incrementProgress('card.password');
			await onChange('password', editPassword);
			cancelPasswordEdit();
		}
	}, [editPassword, onChange, cancelPasswordEdit, incrementProgress]);

	const moveToCard = useCallback(() => {
		ReactGA.event({
			category: GA_CATEGORY_SEARCH,
			action: GA_LINK,
		});
	}, []);

	return (
		<Form>
			<Container className={className}>
				{inCard === true && (
					<>
						<Modal isOpen={confirm.open} onClose={changeConfirm(false)}>
							<span>
								{d(
									'card.remake.confirm.message',
									d(`card.${confirm.propName ?? ''}.title`),
									confirm.before ?? '',
									confirm.after ?? '',
								)}
							</span>
							<div>
								<Button color="simple" onClick={declineValue(confirm.propName)}>
									{d('card.remake.confirm.cancel')}
								</Button>
								<Button color="simple" onClick={acceptValue(confirm.propName)}>
									{d('card.remake.confirm.accept')}
								</Button>
							</div>
						</Modal>
						<Modal isOpen={positionModal.open} onClose={changePositionModal(false)}>
							<span>
								{d(
									'card.positions.modal.message',
									positionModal.positionIndex !== undefined
										? d(`positions.${positions[positionModal.positionIndex].position.value}`)
										: '',
								)}
							</span>
							<div>
								<Button color="simple" onClick={editPositionModal(PositionVariant.available)}>
									{d('card.positions.modal.available')}
								</Button>
								<Button color="simple" onClick={editPositionModal(PositionVariant.occupied)}>
									{d('card.positions.modal.occupied')}
								</Button>
							</div>
							{positionModal.chooseProfile !== undefined && (
								<Select
									value={positionModal.chooseProfile}
									placeholder={d('card.positions.modal.choose')}
									options={(members ?? []).map((profile) => ({
										value: profile.login,
										title: profile.name,
									}))}
									overlay={true}
									onChange={editPositionModalWithProfile}
								/>
							)}
						</Modal>
					</>
				)}
				<Flex direction="column" flex={0} placeContent="center">
					{inCard === true ? (
						<ProfileDiv>
							<Dummy loading={loading === true}>
								<Rating>
									{inProfile ? d(`dignities.profile.${dignities[0]?.name}`) : d(`dignities.band.${dignities[0]?.name}`)}
								</Rating>
								<Dummy loading="card.name" transparent={loading !== true}>
									<Name
										input={editable}
										name="name"
										validation={{
											maxLength: {
												value: NAME_MAX_LENGTH,
												errorMessage: d('card.name.error.max', NAME_MAX_LENGTH),
											},
											minLength: {
												value: NAME_MIN_LENGTH,
												errorMessage: d('card.name.error.min', NAME_MIN_LENGTH),
											},
										}}
										onChange={setEditName}
										onAccept={acceptValue('name')}
										onCancel={spareValue('name')}
									>
										{editName ?? name}
									</Name>
								</Dummy>
							</Dummy>
							{editable ? (
								<AvatarEditorDummy loading={loading === true || 'card.avatar'} transparent={loading !== true}>
									<AvatarEditor
										src={avatar.src}
										defaultAvatars={defaultAvatars ?? []}
										onChange={acceptValue('avatar')}
									/>
								</AvatarEditorDummy>
							) : (
								<Avatar src={avatar.src} alt="avatar" />
							)}
						</ProfileDiv>
					) : (
						<Link
							location={{
								pathname: !inProfile ? '/band' : '/profile',
								query: !inProfile ? { alias: alias! } : { login: login! },
							}}
							asLocation={{
								pathname: !inProfile ? `/band/${alias!}` : `/profile/${login!}`,
							}}
							onClick={moveToCard}
						>
							<ProfileDiv>
								<Dummy loading={loading === true} transparent={loading !== true}>
									<Rating>
										{inProfile
											? d(`dignities.profile.${dignities[0]?.name}`)
											: d(`dignities.band.${dignities[0]?.name}`)}
									</Rating>
									<Name input={editable}>{name}</Name>
								</Dummy>
								<Avatar src={avatar.src} alt="avatar" />
							</ProfileDiv>
						</Link>
					)}
				</Flex>
				<ProfileInfo direction="column" inCard={inCard ?? false}>
					{editable && inProfile && registeredWithSocial !== true && (
						<>
							<Underline width={65}>
								<RowName>{d('card.password.title')}</RowName>
								<Dummy loading={loading === true || 'card.password'} transparent={loading !== true}>
									<Password
										name="password"
										canSeePassword={false}
										value={editPassword ?? ''}
										placeholder="********"
										type="password"
										validation={{
											maxLength: {
												value: PASSWORD_MAX_LENGTH,
												errorMessage: d('card.password.error.max', PASSWORD_MAX_LENGTH),
											},
											callback: {
												value: (value) => value.length >= PASSWORD_MIN_LENGTH || value.length === 0,
												errorMessage: d('card.password.error.min', PASSWORD_MIN_LENGTH),
											},
										}}
										onChange={setEditPassword}
									/>
									{editPassword !== null && (
										<>
											<Password
												name="repeat"
												canSeePassword={false}
												value={repeatPassword}
												placeholder={d('card.repeat.placeholder')}
												type="password"
												validation={{
													callback: {
														errorMessage: d('card.repeat.error.equal'),
														value: (value) => value === editPassword,
													},
												}}
												onChange={setRepeatPassword}
											/>
											<Button color="simple" onClick={cancelPasswordEdit}>
												{d('card.password.cancel')}
											</Button>
											<Button color="simple" onClick={acceptPassword}>
												{d('card.password.accept')}
											</Button>
										</>
									)}
								</Dummy>
							</Underline>
							{email !== undefined && email !== null && (
								<Underline width={65}>
									<RowName>{d('card.email.title')}</RowName>
									<Dummy loading={loading === true || 'card.email'} transparent={loading !== true}>
										<Email
											input={editable}
											placeholder={d('card.email.placeholder')}
											validation={{
												maxLength: {
													value: EMAIL_MAX_LENGTH,
													errorMessage: d('card.email.error.max', EMAIL_MAX_LENGTH),
												},
												callback: {
													value: validateEmail,
													errorMessage: d('card.email.error.invalid'),
												},
											}}
											onChange={setEditEmail}
											onAccept={acceptValue('email')}
											onCancel={spareValue('email')}
										>
											{editEmail ?? email}
										</Email>
									</Dummy>
								</Underline>
							)}
						</>
					)}
					<Underline width={65}>
						<RowName>{d('card.place.title')}</RowName>
						<Dummy loading={loading === true}>
							<AreaWrap>
								{editable ? (
									<Flex direction="column" width="initial">
										<ControllableSelect
											loading="card.country"
											value={country?.value ?? null}
											placeholder={d('countries.empty')}
											options={
												filtersData?.countries.map(({ value }) => ({
													value,
													title: d(`countries.${value}`),
												})) ?? []
											}
											onChange={select('country')}
										>
											{country === null ? (
												<span>{d('card.notSpecified')}</span>
											) : (
												<Bold>{d(`countries.${country.value}`)}</Bold>
											)}
										</ControllableSelect>
									</Flex>
								) : country === null ? (
									<span>{d('card.notSpecified')}</span>
								) : (
									<Bold>{d(`countries.${country.value}`)}</Bold>
								)}
								{editable ? (
									country !== null && (
										<Flex direction="column" width="initial">
											<ControllableSelect
												loading="card.city"
												value={city?.value ?? null}
												placeholder={d('cities.empty')}
												options={
													filtersData?.cities
														.filter((filterCity) => filterCity.country.value === country.value)
														.map(({ value }) => ({
															value,
															title: d(`cities.${value}`),
														})) ?? []
												}
												onChange={select('city')}
											>
												{city !== null ? d(`cities.${city.value}`) : d('cities.empty')}
											</ControllableSelect>
										</Flex>
									)
								) : (
									<span>{city !== null ? d(`cities.${city.value}`) : d('cities.empty')}</span>
								)}
							</AreaWrap>
						</Dummy>
					</Underline>
					<Underline width={65}>
						<RowName>{d('card.experience.title')}</RowName>
						<Dummy loading={loading === true}>
							{editable ? (
								<ControllableSelect
									loading="card.experience"
									value={experience.toString()}
									options={new Array(EXPERIENCE_MAXIMUM_OFFSET + 1).fill(0).map((_, offset) => {
										const value = (getCachedDate().getFullYear() - offset).toString();
										return {
											value,
											title: value,
										};
									})}
									onChange={select('experience')}
								>
									<span>{d('card.since', experience)}</span>
								</ControllableSelect>
							) : (
								<span>{d('card.since', experience)}</span>
							)}
						</Dummy>
					</Underline>
					{repetitionBase !== true && (
						<Underline>
							<Flex direction="column" alignItems="flex-end">
								<RowName>{d('card.positions.title')}</RowName>
								<Dummy loading={loading === true}>
									<Buttons>
										{editable &&
											(editPosition !== undefined ? (
												<StyledSelect
													placeholder={d('card.positions.default')}
													opened={true}
													overlay={true}
													value={editPosition}
													onChange={changeEditPosition}
													onClose={closeEditPosition}
													options={
														filtersData?.positions
															.filter(({ value }) =>
																inProfile ? positions.every(({ position }) => position.value !== value) : true,
															)
															.map(({ value }) => ({
																value,
																title: d(`positions.${value}`),
															})) ?? []
													}
												/>
											) : (
												<Dummy loading="card.positions" transparent={loading !== true}>
													<StyledButton color="simple" onClick={addPosition}>
														{d('card.positions.add')}
													</StyledButton>
												</Dummy>
											))}
										{(editable ? positions : positionsWithoutOwner).length > 0 ? (
											(editable ? positions : positionsWithoutOwner).map((position, index) =>
												editable ? (
													<Flex key={index} direction="column" width="initial" alignItems="center">
														<ActionButton
															value={d(`positions.${position.position.value}`)}
															onRemove={remove('positions', position.position.value, index)}
															onClick={alias !== undefined ? setEditPositionOpen(index) : undefined}
														/>
														{alias !== undefined && (
															<PositionStatus status={(position as BandPosition).status}>
																{(position as BandPosition).profile?.name ??
																	d(`positionVariants.${PositionVariant[(position as BandPosition).status]}`)}
															</PositionStatus>
														)}
													</Flex>
												) : (
													<PositionButton
														key={index}
														value={position.position.value}
														inFilter={filterPositions !== undefined && position.position.value in filterPositions}
														status={!inProfile ? (position as BandPosition).status : undefined}
														active={currentPosition === position.position.value}
														disabled={
															user === null ||
															(!inProfile &&
																(relations as Relations[]).find(
																	(relation) => relation.position.value === position.position.value,
																)?.status === InviteRelations.unavailable) ||
															(inProfile && Object.keys(relations).length === 0) ||
															login === user.login
														}
														warn={
															relations instanceof Array
																? relations.some(
																		(relation) =>
																			relation.position.value === position.position.value &&
																			(position as BandPosition).status === PositionVariant.available &&
																			relation.status !== InviteRelations.unavailable,
																  )
																: Object.values(relations).some((rels) =>
																		rels.some(
																			(relation) =>
																				relation.position.value === position.position.value &&
																				relation.status !== InviteRelations.unavailable,
																		),
																  )
														}
														onClick={setCurrentPosition}
													/>
												),
											)
										) : (
											<span>{d('card.notSpecified')}</span>
										)}
									</Buttons>
								</Dummy>
							</Flex>
						</Underline>
					)}
					{currentPosition !== null && <Invite relations={relations} position={currentPosition} onChoose={choose} />}
					<Underline>
						<Flex direction="column" alignItems="flex-end">
							<RowName>{d('card.genres.title')}</RowName>
							<Dummy loading={loading === true}>
								<Buttons>
									{editable &&
										(editGenre ? (
											<StyledSelect
												placeholder={d('card.genres.default')}
												opened={true}
												overlay={true}
												value={null}
												onChange={changeEditGenre}
												onClose={closeEditGenre}
												options={
													filtersData?.genres
														.filter(({ value }) => genres.every((genre) => genre.value !== value))
														.map(({ value }) => ({
															value,
															title: d(`genres.${value}`),
														})) ?? []
												}
											/>
										) : (
											<Dummy loading="card.genres" transparent={loading !== true}>
												<StyledButton color="simple" onClick={addGenre}>
													{d('card.genres.add')}
												</StyledButton>
											</Dummy>
										))}
									{genres.length > 0 ? (
										genres.map(({ value }, index) =>
											editable ? (
												<ActionButton
													key={index}
													onRemove={remove('genres', value, index)}
													value={d(`genres.${value}`)}
												/>
											) : (
												<SpanWithOffset key={index}>{d(`genres.${value}`)}</SpanWithOffset>
											),
										)
									) : (
										<span>{d('card.notSpecified')}</span>
									)}
								</Buttons>
							</Dummy>
						</Flex>
					</Underline>
					<Underline>
						<Flex direction="column" alignItems="flex-end">
							<RowName>{d('card.description.title')}</RowName>
							<Dummy loading={loading === true || 'card.description'} transparent={loading !== true}>
								<Description
									name="description"
									input={editable}
									placeholder={d('card.description.placeholder')}
									validation={{
										maxLength: {
											value: DESCRIPTION_MAX_LENGTH,
											errorMessage: d('card.description.error.max', DESCRIPTION_MAX_LENGTH),
										},
									}}
									onChange={setEditDescription}
									onAccept={acceptValue('description')}
									onCancel={spareValue('description')}
								>
									{editDescription ?? description ?? ''}
								</Description>
							</Dummy>
						</Flex>
					</Underline>
				</ProfileInfo>
			</Container>
		</Form>
	);
};

export default Card;
