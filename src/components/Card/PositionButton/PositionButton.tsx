import React, { useCallback } from 'react';
import { PositionVariant } from 'Utils/enum';
import { d } from 'Utils/dictionary';
import styled from 'styled-components';
import Button from 'UI/Button';

const WARN_COLOR = '#e7c2b1';

interface PositionButtonProps {
	value: string;
	active: boolean;
	disabled: boolean;
	warn: boolean;
	inFilter: boolean;
	status?: PositionVariant;
	onClick: (positionValue: string) => void;
}

interface StyledButtonProps {
	inFilter?: boolean;
}

const StyledButton = styled(Button)<StyledButtonProps>`
	margin-left: 5px;
	padding-top: 3px;

	&:not(:disabled) {
		${({ inFilter, theme }) => (inFilter === true ? 'box-shadow: inset 0 0 5px -2px ' + theme.color.fourth + ';' : '')}
	}
`;

const WarnButton = styled(StyledButton)`
	background-color: ${WARN_COLOR} !important;
`;

const PositionButton: React.FC<PositionButtonProps> = ({
	value,
	status,
	active,
	disabled,
	warn,
	inFilter,
	onClick,
}) => {
	const click = useCallback(() => {
		onClick(value);
	}, [value, onClick]);

	return warn ? (
		<WarnButton
			color="simple"
			disabled={disabled || (status !== undefined && status !== PositionVariant.available)}
			inFilter={inFilter}
			active={active}
			onClick={click}
		>
			{d(`positions.${value}`)}
		</WarnButton>
	) : (
		<StyledButton
			color="simple"
			disabled={disabled || (status !== undefined && status !== PositionVariant.available)}
			inFilter={inFilter}
			active={active}
			onClick={click}
		>
			{d(`positions.${value}`)}
		</StyledButton>
	);
};

export default PositionButton;
