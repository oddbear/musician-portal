import React, { useCallback, useContext, useEffect, useRef, useState } from 'react';
import ReactCrop, { Crop } from 'react-image-crop';
import Modal from 'UI/Modal';
import Button from 'UI/Button';
import { d } from 'Utils/dictionary';
import styled from 'styled-components';
import { AvatarInput, ResponseLink } from 'Types/response.type';
import ImagesTab from './ImagesTab';
import LazyImage from 'Components/LazyImage';
import ReactGA from 'react-ga';
import { appContext } from 'Contexts/app.context';

interface Props {
	src?: string;
	defaultAvatars: ResponseLink[];
	onChange(avatarData: AvatarInput): void;
}

const Container = styled.div`
	width: 100%;
	height: 100%;
	color: ${({ theme }) => theme.color.primary};
	font-family: ${({ theme }) => theme.fontFamily.aggressive};
`;

const Wrap = styled.div`
	width: 100%;
	height: 100%;
	display: flex;
	flex-wrap: wrap;
	place-content: space-between;
`;

const ImageWrap = styled.div`
	background-color: white;
	width: 100%;
	height: 100%;
	overflow: hidden;
`;

const ImageAvatar = styled(LazyImage)`
	width: 100%;
`;

const ImageAvatarNatural = styled.img`
	width: 100%;
	height: 100%;
`;

const defaultCrop: Crop = { aspect: 1, width: 999999, height: 999999 };

const AvatarEditor: React.FC<Props> = ({ src, defaultAvatars, onChange }) => {
	const [file, setFile] = useState<File | null>(null);
	const [dataUrl, setDataUrl] = useState<string | null>(null);
	const [crop, setCrop] = useState<Crop>(defaultCrop);
	const [relativeCrop, setRelativeCrop] = useState<Crop>(defaultCrop);
	const [currentSrc, setCurrentSrc] = useState<string | undefined>(src);
	const [isEditOpen, setIsEditOpen] = useState<boolean>(false);
	const [isChooseOpen, setIsChooseOpen] = useState<boolean>(false);
	const imageContainer = useRef<ReactCrop>(null);

	const { page } = useContext(appContext);

	const uploadImage = useCallback((newFile: File) => {
		setCrop(defaultCrop);
		setFile(newFile);
		setIsEditOpen(true);
		setIsChooseOpen(false);
	}, []);

	const chooseDefaultImage = useCallback(
		(_id: string) => {
			setFile(null);
			setDataUrl(null);
			onChange({ defaultAvatarId: _id });
			setCurrentSrc(defaultAvatars.find((avatar) => avatar._id === _id)?.src);
			setIsChooseOpen(false);
		},
		[defaultAvatars, onChange],
	);

	const applyEdit = useCallback(() => {
		if (file === null) {
			throw Error('file not uploaded');
		}

		setIsEditOpen(false);

		if (dataUrl !== null) {
			const img = new Image();
			img.src = dataUrl;
			img.onload = async () => {
				const verifiedCrop = {
					x: relativeCrop.x ?? 0,
					y: relativeCrop.y ?? 0,
					width: relativeCrop.width ?? 0,
					height: relativeCrop.height ?? 0,
				};

				const canvas = await getCroppedDataUrl(img, verifiedCrop);
				setCurrentSrc(canvas.toDataURL());
				canvas.toBlob((blob) => {
					if (blob !== null) {
						onChange({
							file: blob,
						});
					}
				}, 'image/jpeg');
			};
		}
	}, [relativeCrop, onChange, file, dataUrl]);

	const closeEdit = useCallback(() => {
		setIsEditOpen(false);
	}, []);

	const closeChoose = useCallback(() => {
		setIsChooseOpen(false);
	}, []);

	const openChoose = useCallback(() => {
		setIsChooseOpen(true);

		ReactGA.modalview(page);
	}, [page]);

	const calculateCrop = useCallback((newCrop: Crop) => {
		setCrop(newCrop);

		const containerRef = imageContainer.current;
		if (
			containerRef !== null &&
			newCrop.x !== undefined &&
			newCrop.y !== undefined &&
			newCrop.width !== undefined &&
			newCrop.height !== undefined
		) {
			// @ts-ignore
			const imageRef = containerRef.imageRef as HTMLImageElement;
			setRelativeCrop({
				x: newCrop.x * (imageRef.naturalWidth / imageRef.offsetWidth),
				y: newCrop.y * (imageRef.naturalHeight / imageRef.offsetHeight),
				width: newCrop.width * (imageRef.naturalWidth / imageRef.offsetWidth),
				height: newCrop.height * (imageRef.naturalHeight / imageRef.offsetHeight),
			});
		}
	}, []);

	useEffect(() => {
		setCurrentSrc(src);
	}, [src]);

	useEffect(() => {
		if (file !== null) {
			const reader = new FileReader();
			reader.onload = () => {
				if (reader.result !== null) {
					const newDataUrl = reader.result.toString();
					setDataUrl(newDataUrl);
				}
			};
			reader.readAsDataURL(file);
		}
	}, [file]);

	return (
		<Container>
			<Modal isOpen={isEditOpen} onClose={closeEdit}>
				{dataUrl !== null && <ReactCrop ref={imageContainer} src={dataUrl} crop={crop} onChange={calculateCrop} />}
				<Button onClick={applyEdit} color="primary">
					{d('avatarEditor.apply')}
				</Button>
			</Modal>
			<Modal isOpen={isChooseOpen} onClose={closeChoose}>
				<ImagesTab images={defaultAvatars} onUpload={uploadImage} onChoose={chooseDefaultImage} />
			</Modal>
			<Wrap>
				<ImageWrap>
					{currentSrc !== undefined &&
						(currentSrc.includes('data:image') ? (
							<ImageAvatarNatural src={currentSrc} alt="cropped avatar" />
						) : (
							<ImageAvatar src={currentSrc} fitHeight={true} alt="avatar" />
						))}
				</ImageWrap>
				<label>
					<Button color="primary" onClick={openChoose}>
						{d('avatarEditor.change')}
					</Button>
				</label>
			</Wrap>
		</Container>
	);
};

export default AvatarEditor;

const getCroppedDataUrl = (img: HTMLImageElement, relativeCrop: Crop): Promise<HTMLCanvasElement> =>
	new Promise((res, rej) => {
		const canvas = document.createElement('canvas');
		const ctx = canvas.getContext('2d');
		canvas.width = relativeCrop.width!;
		canvas.height = relativeCrop.height!;

		if (ctx === null) {
			return rej('context 2d not found, use newer browser');
		}

		ctx.drawImage(
			img,
			relativeCrop.x!,
			relativeCrop.y!,
			relativeCrop.width!,
			relativeCrop.height!,
			0,
			0,
			relativeCrop.width!,
			relativeCrop.height!,
		);

		res(canvas);
	});
