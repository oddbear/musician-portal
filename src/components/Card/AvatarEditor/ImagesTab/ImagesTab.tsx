import React, { ChangeEvent, useCallback } from 'react';
import { ResponseLink } from 'Types/response.type';
import styled from 'styled-components';
import LazyImage from 'Components/LazyImage';

interface Props {
	images: ResponseLink[];
	onUpload(file: File): void;
	onChoose(_id: string): void;
}

const Container = styled.div`
	input[type='file'] {
		display: none;
	}
`;

const ImagesTab: React.FC<Props> = ({ images, onUpload, onChoose }) => {
	const change = useCallback(
		(event: ChangeEvent<HTMLInputElement>) => {
			const files = event.target.files ?? [];
			const file = files[0];

			if (file !== undefined) {
				onUpload(files[0]);
			}

			// @ts-ignore
			event.target.value = null;
		},
		[onUpload],
	);

	return (
		<Container>
			<label>
				<input type="file" onChange={change} />
				NEW
			</label>
			{images.map(({ _id, src }) => (
				<Img key={_id} _id={_id} src={src} onChoose={onChoose} />
			))}
		</Container>
	);
};

export default ImagesTab;

interface ImgProps {
	_id: string;
	src: string;
	onChoose(_id: string): void;
}

const ImgContainer = styled(LazyImage)`
	cursor: pointer;
	width: 40px;
	height: 40px;
	margin-right: 10px;
`;

const Img: React.FC<ImgProps> = ({ _id, src, onChoose }) => {
	const click = useCallback(() => {
		onChoose(_id);
	}, [_id, onChoose]);

	return <ImgContainer src={src} alt="default avatar" onClick={click} />;
};
