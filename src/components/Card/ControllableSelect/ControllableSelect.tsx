import React, { useCallback, useContext, useState } from 'react';
import styled from 'styled-components';
import Select, { Menu } from 'UI/Select';
import Dummy from 'UI/Dummy';
import { loaderContext } from 'Contexts/loader.context';

interface Option {
	value: string | null;
	title: string;
}

interface Props {
	loading: string;
	value: string | null;
	options: Option[];
	placeholder?: string;
	children: React.ReactNode;
	onChange: (value: string) => void;
}

const StyledSelect = styled(Select)`
	width: auto;

	label {
		border-radius: 20px;
		padding: 0 6px;
	}

	${Menu} {
		top: 16px;
	}
`;

const Span = styled.span`
	cursor: pointer;
`;

const ControllableSelect: React.FC<Props> = ({ loading, options, value, placeholder, children, onChange }) => {
	const [isEditable, setIsEditable] = useState<boolean>(false);
	const { incrementProgress } = useContext(loaderContext);

	const change = useCallback(
		(newValue: string) => {
			setIsEditable(false);
			onChange(newValue);
			incrementProgress(loading);
		},
		[loading, incrementProgress, onChange],
	);

	const makeEditable = useCallback(() => {
		setIsEditable(true);
	}, []);

	const disableEditable = useCallback(() => {
		setIsEditable(false);
	}, []);

	return (
		<Dummy loading={loading}>
			{isEditable ? (
				<StyledSelect
					opened={true}
					overlay={true}
					placeholder={placeholder}
					options={options}
					value={value}
					onChange={change}
					onClose={disableEditable}
				/>
			) : (
				<Span onClick={makeEditable}>{children}</Span>
			)}
		</Dummy>
	);
};

export default ControllableSelect;
