import React from 'react';
import styled from 'styled-components';
import Button from 'UI/Button';

interface ActionButtonProps {
	value: string;
	onClick?: () => void;
	onRemove: () => void;
}

const RemoveButton = styled.div`
	cursor: pointer;
	display: flex;
	place-content: center;
	align-items: center;
	border-radius: 200px;
	padding: 0 4px;
	background-color: red;
	border: 1px solid darkred;
	border-left: none;
	color: #fff;
	font-family: monospace;
	transition: background-color 0.15s;
	box-shadow: 0 0 -5px 2px transparent;

	&:hover {
		background-color: rgba(139, 0, 0, 0.87);
		box-shadow: 0 0 5px -2px red;
	}

	&:active {
		background-color: darkred;
	}
`;

const ActionButtonContainer = styled.div`
	display: flex;
	flex-direction: row;
	margin-left: 4px;

	${Button} {
		border-bottom-right-radius: 0;
		border-top-right-radius: 0;
	}

	&.removable {
		${Button} {
			cursor: default;

			&:hover,
			&:active {
				box-shadow: 0 0 2px 0 transparent;
				color: #888;
				border: 1px solid #888;
				background-color: #fff;
			}
		}
	}

	&.editable {
		${RemoveButton} {
			border-bottom-left-radius: 0;
			border-top-left-radius: 0;
		}
	}
`;

const SpanWithOffset = styled.span`
	margin: 0 2px 0 6px;
	font-family: ${({ theme }) => theme.fontFamily.quiet};
`;

const ActionButton: React.FC<ActionButtonProps> = ({ value, onClick, onRemove }) => {
	return (
		<ActionButtonContainer className={onClick === undefined ? 'removable' : 'editable'}>
			{onClick === undefined ? (
				<SpanWithOffset>{value}</SpanWithOffset>
			) : (
				<Button color="simple" onClick={onClick}>
					{value}
				</Button>
			)}
			<RemoveButton onClick={onRemove}>&#10006;</RemoveButton>
		</ActionButtonContainer>
	);
};

export default ActionButton;
