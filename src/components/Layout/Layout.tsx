import { useQuery } from '@apollo/react-hooks';
import { appContext } from 'Contexts/app.context';
import { themeContext } from 'Contexts/theme.context';
import { useLazyLoad } from 'Hooks/lazyload.hook';
import { usePrevious } from 'Hooks/previous.hook';
import { useWindowSize } from 'Hooks/window-size.hook';
import Cookies from 'js-cookie';
import { Location } from 'Types/app.type';
import { GraphQLWrap, ResponseUser } from 'Types/response.type';
import NextHead from 'next/head';
import React, { ReactNode, useCallback, useEffect, useState } from 'react';
import styled, { createGlobalStyle, ThemeProvider } from 'styled-components';
import Notification from 'UI/Notification';
import { d } from 'Utils/dictionary';
import { RouterListener } from 'Utils/router';
import { DefaultTheme, getFontSize, theme as initialTheme } from 'Utils/styles';
import { parseLocation } from 'Utils/url-resolver';
import CookiesAgreement from './CookiesAgreement';
import Footer from './Footer';
import Header from './Header';
import { getUser, resendEmailConfirmation } from 'Schemas/User';
import { playerContext } from 'Contexts/player.context';
import { loaderContext } from 'Contexts/loader.context';
import { errorContext } from 'Contexts/error.context';
import Player from 'UI/Player';
import Modal from 'UI/Modal';
import { TIME_TO_DELETE_UNVERIFIED_ACCOUNT } from 'Utils/constants';
import ReactGA from 'react-ga';
import Button from 'UI/Button/Button';
import { getToken, notify, subscribeToNotifications } from 'Utils/notifications';
import { addFirebaseToken } from 'Schemas/User';
import LazyImage from 'Components/LazyImage';
import ExternalLink from 'Components/ExternalLink';
import { useSecureMutation } from 'Utils/apollo';

const BACKGROUND_SRC = 'BG/bg_1116_1_eonlix';
const RECAPTCHA_LOGO_SRC = 'Common/l2mo5yxtvnbi7pgbboti';

function fontFace(name: string, src: string, fontWeight = 400, fontStyle = 'normal'): string {
	return `
      @font-face {
			font-family: "${name}";
			src: url(${src});
			
			font-display: swap;
			font-style: ${fontStyle};
			font-weight: ${fontWeight.toString()};
		}
  `;
}

interface GlobalProps {
	windowWidth: number;
}

const Global = createGlobalStyle<GlobalProps>`
	${fontFace(
		'TrixieCyr-Plain',
		'https://res.cloudinary.com/alain-corp-cdn/raw/upload/v1598337849/fonts/n0aq3x6iwgoqcyyowruc.TTF',
	)}
	${fontFace(
		'Myriad Pro',
		'https://res.cloudinary.com/alain-corp-cdn/raw/upload/v1595589872/fonts/MYRIADPRO_1_xijzos.ttf',
		300,
	)}
	${fontFace(
		'Myriad Pro',
		'https://res.cloudinary.com/alain-corp-cdn/raw/upload/v1595589868/fonts/MYRIADPRO-REGULAR_qzdbhq.otf',
		400,
	)}
	${fontFace(
		'Myriad Pro',
		'https://res.cloudinary.com/alain-corp-cdn/raw/upload/v1595589871/fonts/MYRIADPRO-LIGHTCOND_t1cfbb.otf',
		100,
	)}
	${fontFace(
		'Myriad Pro',
		'https://res.cloudinary.com/alain-corp-cdn/raw/upload/v1595589869/fonts/MYRIADPRO-BOLDCOND_umll5w.otf',
		700,
	)}
	${fontFace(
		'Myriad Pro',
		'https://res.cloudinary.com/alain-corp-cdn/raw/upload/v1595589871/fonts/MYRIADPRO-BOLDCONDIT_edityx.otf',
		700,
		'italic',
	)}
	${fontFace(
		'Myriad Pro',
		'https://res.cloudinary.com/alain-corp-cdn/raw/upload/v1595589868/fonts/MYRIADPRO-SEMIBOLD_iphrvc.otf',
		600,
	)}
	${fontFace(
		'Aardvark',
		'https://res.cloudinary.com/alain-corp-cdn/raw/upload/v1595773255/fonts/Aardvark_Bold_ruj5hy.ttf',
	)}
	
	#__next {
		height: 100%;
	}
  
	body {
		overflow: auto;
	}

	html, body {
		width: 100%;
		height: 100%;
		margin: 0;
		padding: 0;
		font-size: ${({ windowWidth }) => getFontSize(windowWidth)};
		text-transform: uppercase;
	}
	
	button,
	input,
	textarea {
		text-transform: inherit;
	}
	
	.grecaptcha-badge {
		display: none;
	}
`;

interface Props {
	lang: string;
	pathname: string;
	children: ReactNode;
}

interface ContainerProps {
	backgroundSrc: string;
}

const Container = styled.div<ContainerProps>`
	height: 100%;
	display: flex;
	flex-direction: column;
	background: url(${({ backgroundSrc }) => backgroundSrc}) #33414a repeat top local;
	background-size: 100% auto;
	overflow: auto;
`;

const StyledContentContainer = styled.div`
	margin-top: 40px;
	position: relative;
	flex: 1;
`;

const Body = styled.div`
	height: 100%;
	font-family: ${({ theme }) => theme.fontFamily.aggressive};
`;

const DeactivationMessage = styled.div`
	width: 100%;
	padding: 0 50px;
	background-color: #ffee58;
	color: black;
	text-align: center;
	font-size: 0.8rem;
	position: relative;
	box-sizing: border-box;
	z-index: 3;
`;

const ResendMessage = styled.span`
	cursor: pointer;
	text-decoration: underline;
	color: dodgerblue;
`;

const ErrorButtonWrap = styled.div`
	display: flex;
	place-content: center;
`;

const ErrorButton = styled(Button)`
	&:not(first-child) {
		margin-left: 10px;
	}
`;

const ErrorText = styled.div`
	text-align: center;
`;

const Recaptcha = styled.div`
	display: flex;
	position: fixed;
	bottom: 10px;
	right: -200px;
	background-color: #fff;
	border-radius: 4px;
	z-index: 999;
	box-shadow: 0 0 5px -2px black;
	font-family: Roboto, helvetica, arial, sans-serif;
	overflow: hidden;
	text-transform: none;
	opacity: 0.7;
	transition: right 0.3s, opacity 0.15s;

	&:hover {
		opacity: 1;
		right: 4px;
	}
`;

const RecaptchaContainerLeft = styled.div`
	width: 60px;
	display: flex;
	flex-direction: column;
	align-items: center;
	background-color: #ddd;
	padding: 0 5px 2px 5px;
`;

const RecaptchaContainerRight = styled.div`
	display: flex;
	flex-direction: column;
	flex: 1;
	width: 200px;
	background-color: #273033;
	color: #fff;
	padding: 10px 10px;
	font-size: 12px;
	font-weight: bold;
	box-sizing: border-box;
`;

const RecaptchaLogo = styled(LazyImage)`
	padding: 5px 0 2px 0;
	height: 40px;
`;

const RecaptchaLittleText = styled.div`
	cursor: pointer;
	font-size: 5px;
	text-align: center;

	a {
		color: black;
		text-decoration: none;

		&:hover {
			text-decoration: underline;
		}
	}
`;

const RecaptchaRightLittleText = styled.div`
	font-size: 10px;
	font-weight: lighter;

	a {
		color: #fff;
		text-decoration: none;

		&:hover {
			text-decoration: underline;
		}
	}
`;

interface UserQueryVariables {
	conversationsCount: number;
	notificationsCount: number;
}

interface Tasks {
	[key: string]: number;
}

const Layout: React.FC<Props> = ({ lang, pathname, children }) => {
	const getParsedLocation = useCallback((): { location: Location; page: string } => {
		return parseLocation(typeof window !== 'undefined' ? window.location.href : pathname);
	}, [pathname]);

	const size = useWindowSize();
	const [isCookiesAgreement, setIsCookiesAgreement] = useState<boolean>(false);
	const [styles, setStyles] = useState<DefaultTheme>(initialTheme);
	const [page, setPage] = useState<string>(getParsedLocation().page);
	const [location, setLocation] = useState<Location>({
		pathname: '/',
		query: {},
	});
	const [errorMessage, setErrorMessage] = useState<string | null>(null);
	const [errorButton, setErrorButton] = useState<{ title: string; action?: () => void } | null>(null);
	const [tasks, setTasks] = useState<Tasks>({
		user: 1,
	});

	const setError = useCallback((message: string, button?: { title: string; action?: () => void }) => {
		setErrorMessage(message);
		setErrorButton(button ?? null);
	}, []);

	const [resendConfirmation] = useSecureMutation(resendEmailConfirmation);

	const { data, loading: userLoading } = useQuery<GraphQLWrap<'user', ResponseUser>, UserQueryVariables>(getUser, {
		ssr: false,
		onError: (err) => {
			setError(err.message);
			clearProgress('user');
		},
		onCompleted: () => {
			clearProgress('user');
		},
	});
	const user = data?.user ?? null;

	useEffect(() => {
		if (!userLoading) {
			if (user === null) {
				Cookies.remove('logged');
			} else {
				Cookies.set('logged', 'true');
			}
		}
	}, [userLoading, user]);

	const closeError = useCallback(() => {
		setErrorMessage(null);
	}, []);

	const incrementProgress = useCallback(
		(taskName: string, progress = 1): number => {
			const currentProgress = taskName in tasks ? tasks[taskName] : 0;
			const actualProgress = currentProgress + (progress as number);
			setTasks({
				...tasks,
				[taskName]: actualProgress,
			});
			return actualProgress;
		},
		[tasks],
	);

	const decrementProgress = useCallback(
		(taskName: string, progress = 1): number => {
			const currentProgress = taskName in tasks ? tasks[taskName] : 0;

			let actualProgress = currentProgress - (progress as number);
			if (actualProgress < 0) {
				console.error("can't decrease task progress below 0");
				actualProgress = 0;
			}

			setTasks({
				...tasks,
				[taskName]: actualProgress,
			});
			return actualProgress;
		},
		[tasks],
	);

	const clearProgress = useCallback(
		(taskName?: string): void => {
			if (taskName !== undefined) {
				const newTasks = { ...tasks };
				delete newTasks[taskName];
				setTasks(newTasks);
			} else {
				setTasks({});
			}
		},
		[tasks],
	);

	const isLoading = useCallback(
		(taskName: string): boolean | number => {
			const progress = tasks[taskName];
			return progress === undefined || progress === 0 ? false : progress;
		},
		[tasks],
	);

	const [audioState, setAudioState] = useState<string>('stop');
	const [currentAudioSrc, setCurrentAudioSrc] = useState<{
		_id: string | null;
		src: string | null;
	}>({
		_id: null,
		src: null,
	});

	const playAudio = useCallback((src, _id) => {
		setCurrentAudioSrc({
			_id,
			src,
		});
		setAudioState('play');
	}, []);

	const pauseAudio = useCallback(() => {
		setAudioState('pause');
	}, []);

	const prevPage = usePrevious(page);
	const locationHandler = useCallback(() => {
		const parsed = getParsedLocation();
		setLocation(parsed.location);
		setPage(parsed.page);

		if (prevPage !== parsed.page) {
			ReactGA.pageview(parsed.page);
		}
	}, [prevPage, getParsedLocation]);

	const locationHref = typeof window !== 'undefined' ? window.location.href : undefined;
	const prevHref = usePrevious(locationHref);
	useEffect(() => {
		if (prevHref !== locationHref) {
			locationHandler();
		}
	}, [prevHref, locationHandler, locationHref]);

	const [addToken] = useSecureMutation<
		GraphQLWrap<'addFirebaseToken', void>,
		{
			device: string;
			token: string;
		}
	>(addFirebaseToken);

	useEffect(() => {
		const cookiesAgreement = Cookies.get('cookiesAgreement');
		if (cookiesAgreement === undefined) {
			setIsCookiesAgreement(true);
		}

		grecaptcha.ready(() => {
			void grecaptcha.execute(config.google.recaptcha.siteKey, { action: 'homepage' });
		});
	}, []);

	useEffect(() => {
		if (typeof window !== 'undefined' && !userLoading && user !== null) {
			void notify().then(async (type) => {
				if (type === 'granted') {
					subscribeToNotifications();

					const device = navigator.userAgent;
					const workerUrl = `/firebase-messaging-sw.js?apiKey=${config.google.firebase.apiKey}&messagingSenderId=${config.google.firebase.messagingSenderId}&projectId=${config.google.firebase.projectId}&appId=${config.google.firebase.appId}`;
					if (
						user.firebaseToken[device] === undefined ||
						(await navigator.serviceWorker.getRegistrations()).every(
							({ active }) => active?.scriptURL.replace(/^https?:\/\/[^/]+/, '') !== workerUrl,
						)
					) {
						const token = await getToken(workerUrl);
						await addToken({
							variables: {
								device,
								token,
							},
							update: (cache) => {
								const oldData = cache.readQuery<GraphQLWrap<'user', ResponseUser>>({
									query: getUser,
								});

								if (oldData === null) {
									return;
								}

								cache.writeQuery<GraphQLWrap<'user', ResponseUser>>({
									query: getUser,
									data: {
										user: {
											...oldData.user,
											firebaseToken: { ...oldData.user.firebaseToken, [device]: token },
										},
									},
								});
							},
						});
					}
				}
			});
		}
	}, [addToken, userLoading, user]);

	const acceptCookies = useCallback(() => {
		setIsCookiesAgreement(false);
		Cookies.set('cookiesAgreement', 'true');
	}, []);

	const setStyle = useCallback(
		(name: string, value: string) => {
			setStyles({
				...styles,
				[name]: value,
			});
		},
		[styles],
	);

	const resendConfirmationAction = useCallback(() => {
		void resendConfirmation().catch((err: Error) => {
			setErrorMessage(err.message);
		});
	}, [resendConfirmation]);

	const buttonAction = useCallback(() => {
		closeError();
		errorButton?.action?.();
	}, [closeError, errorButton]);

	const { src: backgroundSrc } = useLazyLoad({
		src: BACKGROUND_SRC,
	});

	return (
		<appContext.Provider
			value={{
				page,
				location,
				lang,
				user,
				loading: userLoading,
				setPage,
			}}
		>
			<themeContext.Provider
				value={{
					styles,
					setStyle,
				}}
			>
				<playerContext.Provider
					value={{
						current: currentAudioSrc,
						state: audioState,
						setState: setAudioState,
						play: playAudio,
						pause: pauseAudio,
					}}
				>
					<loaderContext.Provider
						value={{
							incrementProgress,
							decrementProgress,
							clearProgress,
							isLoading,
						}}
					>
						<errorContext.Provider
							value={{
								setError,
							}}
						>
							<ThemeProvider theme={styles}>
								<>
									<NextHead>
										<meta
											name="viewport"
											content="width=device-width, initial-scale=1, shrink-to-fit=no, viewport-fit=cover"
										/>
										<meta name="theme-color" content="#000000" />
										<meta content="IE=Edge" httpEquiv="X-UA-Compatible" />
										<meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
										<meta httpEquiv="Content-Language" content={lang} />
										<meta name="description" content="Musician portal site, find u band or musician" />
										<script src={`https://www.google.com/recaptcha/api.js?render=${config.google.recaptcha.siteKey}`} />

										<title>{d(`${page}.title`)}</title>
									</NextHead>
									<Global windowWidth={size[0]} />
									<Body>
										<Recaptcha>
											<RecaptchaContainerLeft>
												<RecaptchaLogo fitHeight={false} src={RECAPTCHA_LOGO_SRC} alt="recaptcha logo" />
												<div>
													<RecaptchaLittleText>
														<ExternalLink href="https://www.google.com/intl/ru/policies/privacy/" rel="noopener">
															Confidentiality
														</ExternalLink>
													</RecaptchaLittleText>
													<RecaptchaLittleText>
														<ExternalLink href="https://www.google.com/intl/ru/policies/terms/" rel="noopener">
															Terms Of Use
														</ExternalLink>
													</RecaptchaLittleText>
												</div>
											</RecaptchaContainerLeft>
											<RecaptchaContainerRight>
												<div>reCAPTCHA spam protection</div>
												<RecaptchaRightLittleText>
													<ExternalLink href="https://www.google.com/intl/ru/policies/privacy/" rel="noopener">
														Confidentiality
													</ExternalLink>
													<span> - </span>
													<ExternalLink href="https://www.google.com/intl/ru/policies/terms/" rel="noopener">
														Terms Of Use
													</ExternalLink>
												</RecaptchaRightLittleText>
											</RecaptchaContainerRight>
										</Recaptcha>
										<Modal isOpen={errorMessage !== null} onClose={closeError}>
											<ErrorText>{errorMessage?.replace(/^graphql error:\s/i, '') ?? ''}</ErrorText>
											<ErrorButtonWrap>
												<ErrorButton onClick={closeError}>{d('error.close')}</ErrorButton>
												{errorButton !== null && <ErrorButton onClick={buttonAction}>{errorButton.title}</ErrorButton>}
											</ErrorButtonWrap>
										</Modal>
										<Container backgroundSrc={backgroundSrc} id="container">
											{user?.deactivated === true && (
												<DeactivationMessage>
													{d(
														'deactivationMessage.text',
														Math.trunc(TIME_TO_DELETE_UNVERIFIED_ACCOUNT / (1000 * 60 * 60 * 24)),
													)}
													&nbsp;
													<ResendMessage onClick={resendConfirmationAction}>
														{d('deactivationMessage.retry')}
													</ResendMessage>
												</DeactivationMessage>
											)}
											<Header />
											<StyledContentContainer>{children}</StyledContentContainer>
											<Footer />
											<Notification isVisible={isCookiesAgreement}>
												<CookiesAgreement onAccept={acceptCookies} />
											</Notification>
										</Container>
									</Body>
									<RouterListener />
									{currentAudioSrc.src !== null && <Player state={audioState} src={currentAudioSrc.src} />}
								</>
							</ThemeProvider>
						</errorContext.Provider>
					</loaderContext.Provider>
				</playerContext.Provider>
			</themeContext.Provider>
		</appContext.Provider>
	);
};

export default Layout;
