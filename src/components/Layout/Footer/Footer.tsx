import ContentContainer from 'Components/ContentContainer';
import FullsizeBackground from 'Components/FullsizeBackground';
import Link from 'Components/Link';
import List from 'Components/List';
import { useLazyLoad } from 'Hooks/lazyload.hook';
import React, { useCallback, useState } from 'react';
import styled from 'styled-components';
import CircuitTitle from 'UI/CircuitTitle';
import LED from 'UI/LED';
import { d } from 'Utils/dictionary';
import Screw from './Screw';
import ReactGA from 'react-ga';
import { GA_CATEGORY_FOOTER, GA_LINK } from 'Utils/constants';

const BACKGROUND_SRC = 'BG/Bg_footer_opntrt';
const BARCODE_SRC = 'BG/barcode_adwwvc';
const SYMBOL_SRC = 'BG/%D0%A1%D0%95_njstno';

interface ContentProps {
	src: string;
}

const StyledContentContainer = styled(ContentContainer)<ContentProps>`
	position: relative;
	background-size: auto 100%;
	padding-top: 117px;
	color: ${({ theme }) => theme.color.secondary};
	font-size: 26.5px;

	&:before {
		content: '';
		position: absolute;
		left: 0;
		top: 0;
		width: 100%;
		height: 100%;
		background-size: auto 100%;
		z-index: 0;
		background: url(${({ src }) => src}) repeat-x center;
	}
`;

const Container = styled.div`
	width: 100%;
	position: relative;
	display: flex;
`;

const TopContainer = styled(Container)`
	padding-top: 62px;
	place-content: space-evenly;
	margin-left: 150px;
`;

const BottomContainer = styled(Container)`
	place-content: space-between;
	align-items: flex-end;
	padding-bottom: 100px;
`;

const StyledList = styled(List)`
	&:last-child {
		margin-right: 46px;
	}

	li {
		margin-top: 20px;

		&:first-child {
			margin-top: 0;
		}
	}
`;

const StyledLink = styled.div`
	display: flex;
	place-content: flex-end;
	place-items: center;
	color: white;
	transition: color ${({ theme }) => theme.other.defaultTransitionDuration};

	&:hover {
		color: ${({ theme }) => theme.color.hovernav};
	}

	span {
		margin-right: 10px;
	}
`;

const Barcode = styled(FullsizeBackground)`
	width: 167px;
	height: 83px;
	opacity: 0.49;
`;

const Symbol = styled(FullsizeBackground)`
	width: 41px;
	height: 33px;
	opacity: 0.66;
`;

const CircuitWrap = styled.li`
	position: relative;
	padding-right: 0.7rem !important;
	margin-top: 10px !important;
	right: 15px;
	top: 24px;
	font-size: 1.2rem;
	display: flex;
	place-content: flex-end;
`;

interface FooterLinkProps {
	alias: string;
	pathname: string;
	query?: { [key: string]: string };
}

export const FooterLink: React.FC<FooterLinkProps> = ({ alias, pathname, query }) => {
	const [status, setStatus] = useState<boolean>(false);

	const onMouseOver = useCallback(() => setStatus(true), []);
	const onMouseOut = useCallback(() => setStatus(false), []);

	const footerLink = useCallback(() => {
		ReactGA.event({
			category: GA_CATEGORY_FOOTER,
			action: GA_LINK,
			label: pathname,
		});
	}, [pathname]);

	return (
		<li onMouseOver={onMouseOver} onMouseOut={onMouseOut}>
			<Link
				location={{
					pathname,
					query,
				}}
				onClick={footerLink}
			>
				<StyledLink>
					<span>{d(`footer.${alias}`).toUpperCase()}</span>
					<LED on={status} />
				</StyledLink>
			</Link>
		</li>
	);
};

FooterLink.defaultProps = {
	query: {},
};

const Footer: React.FC = () => {
	const { src } = useLazyLoad({
		src: BACKGROUND_SRC,
	});

	return (
		<StyledContentContainer src={src}>
			<div>
				<Screw x={0} y={0} />
				<Screw x={1} y={0} />
				<Screw x={1} y={1} />
				<Screw x={0} y={1} />
			</div>
			<TopContainer>
				<StyledList>
					{[
						{
							alias: 'main',
							pathname: '/',
						},
						{
							alias: 'band',
							pathname: '/search',
							query: {
								type: 'band',
							},
						},
						{
							alias: 'musician',
							pathname: '/search',
							query: {
								type: 'musician',
							},
						},
					].map(({ alias, pathname, query }) => (
						<FooterLink key={alias} pathname={pathname} query={query} alias={alias} />
					))}
				</StyledList>
				<StyledList>
					{[
						{
							alias: 'signin',
							pathname: '/signin',
						},
						{
							alias: 'signup',
							pathname: '/signup',
						},
						{
							alias: 'createBand',
							pathname: '/band/new',
						},
						{
							alias: 'contacts',
							pathname: '/contacts',
						},
					].map(({ alias, pathname }) => (
						<FooterLink key={alias} pathname={pathname} alias={alias} />
					))}
					<CircuitWrap>
						<Link
							location={{
								pathname: '/faq',
							}}
						>
							<CircuitTitle title={d('footer.faq')} />
						</Link>
					</CircuitWrap>
				</StyledList>
			</TopContainer>
			<BottomContainer>
				<Barcode src={BARCODE_SRC} ratio={0.49700598802395207} fitHeight={true} />
				<Symbol src={SYMBOL_SRC} ratio={0.8048780487804879} fitHeight={true} />
			</BottomContainer>
		</StyledContentContainer>
	);
};

export default Footer;
