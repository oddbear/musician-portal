import FullsizeBackground from 'Components/FullsizeBackground';
import React from 'react';
import styled, { css } from 'styled-components';

const SCREW_SRC = 'BG/BG_bolts_dit2bp';

interface Props {
	x: number;
	y: number;
	className?: string;
}

interface ContainerProps {
	x: number;
	y: number;
}

const Container = styled(FullsizeBackground)<ContainerProps>`
	width: 1.8rem;
	position: absolute;
	border-radius: 200px;
	${({ x }) => (x === 0 ? css({ left: '25px' }) : css({ right: '25px' }))};
	${({ y }) => (y === 0 ? css({ top: '-30px' }) : css({ bottom: '30px' }))};
	z-index: 1;
`;

const Screw: React.FC<Props> = (props) => {
	return <Container className={props.className} x={props.x} y={props.y} src={SCREW_SRC} ratio={1.135135135135135} />;
};

export default Screw;
