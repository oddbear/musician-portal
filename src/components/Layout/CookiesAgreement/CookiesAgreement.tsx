import React from 'react';
import styled from 'styled-components';
import { d } from 'Utils/dictionary';

interface Props {
	onAccept(): void;
}

const Button = styled.button`
	cursor: pointer;
	margin-left: 10px;
`;

const CookiesAgreement: React.FC<Props> = ({ onAccept }) => {
	return (
		<div>
			<span>{d('cookiesAgreement.text')}</span>
			<Button onClick={onAccept}>{d('cookiesAgreement.button')}</Button>
		</div>
	);
};

export default CookiesAgreement;
