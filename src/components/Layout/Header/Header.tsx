import ContentContainer from 'Components/ContentContainer';
import List from 'Components/List';
import { appContext } from 'Contexts/app.context';
import { useLazyLoad } from 'Hooks/lazyload.hook';
import React, { useCallback, useContext, useState } from 'react';
import styled from 'styled-components';
import NavItem, { Group } from 'UI/NavItem';
import PageTitle from 'UI/Seal';
import { d } from 'Utils/dictionary';
import Profile from './Profile';
import { tooltipContext } from 'Contexts/tooltip.context';
import ReactGA from 'react-ga';
import { GA_CATEGORY_HEADER, GA_LINK } from 'Utils/constants';

const BACKGROUND_SRC = 'BG/BG_Header_m8kzjn';
const LAYER_SRC = 'BG/header_hcfpqy';

interface ContentProps {
	src: string;
	layersrc: string;
}

const StyledContentContainer = styled(ContentContainer)<ContentProps>`
	position: relative;
	color: ${({ theme }) => theme.color.secondary};
	font-size: 29.2px;
	height: 315px;
	font-stretch: ultra-condensed;

	&:before {
		background: url(${({ layersrc }) => layersrc}) repeat-x center;
	}

	&:after {
		background: url(${({ src }) => src}) repeat-x center;
	}

	&:before,
	&:after {
		content: '';
		position: absolute;
		left: 0;
		top: 0;
		width: 100%;
		height: 100%;
		background-size: auto 100%;
		z-index: 0;
	}
`;

const Container = styled.div`
	display: flex;
	place-content: space-between;
	flex-direction: row-reverse;
	align-items: flex-end;
	padding: 70px 0;
	position: relative;
	z-index: 3;

	li:not(:last-child) {
		margin-right: 40px;
	}
`;

const StyledList = styled(List)`
	align-items: center;
`;

const StyledPageTitle = styled(PageTitle)`
	position: relative;
	top: 90px;
	left: 62px;
	transform: rotate(-8deg);
	font-size: 2.4rem;
	word-spacing: -21px;
`;

const Header: React.FC = () => {
	const [tooltipAttachedElement, setTooltipAttachedElement] = useState<HTMLElement | null>(null);
	const [tooltipVisible, setTooltipVisible] = useState<boolean>(false);

	const { src } = useLazyLoad({
		src: BACKGROUND_SRC,
	});
	const { src: layersrc } = useLazyLoad({
		src: LAYER_SRC,
	});
	const { page, user } = useContext(appContext);

	const headerLink = useCallback(
		(pathname: string) => () => {
			ReactGA.event({
				category: GA_CATEGORY_HEADER,
				action: GA_LINK,
				label: pathname,
			});
		},
		[],
	);

	return (
		<tooltipContext.Provider
			value={{
				attachedElement: tooltipAttachedElement,
				visible: tooltipVisible,
				setAttachedElement: setTooltipAttachedElement,
				setVisible: setTooltipVisible,
			}}
		>
			<StyledContentContainer src={src} layersrc={layersrc}>
				<Container>
					<div>
						<StyledList horizontal={true}>
							{[
								{
									alias: 'main',
									href: '/',
								},
								{
									alias: 'news',
									href: '/news',
								},
								{
									alias: 'search',
									href: '/search',
								},
								{
									alias: 'playlists',
									href: '/playlists',
								},
								{
									alias: 'shop',
									href: '/shop',
								},
							].map(({ alias, href }) => (
								<li key={alias} onClick={headerLink(href)}>
									<NavItem href={href} title={d(`header.navbar.${alias}`)} />
								</li>
							))}
							<li>
								{user !== null ? (
									<Profile />
								) : (
									<Group title={d('header.navbar.auth')}>
										<NavItem key="signin" href="/signin" secondary={true} />
									</Group>
								)}
							</li>
						</StyledList>
					</div>
					<StyledPageTitle title={d(`${page}.pageTitle`)} />
				</Container>
			</StyledContentContainer>
		</tooltipContext.Provider>
	);
};

export default Header;
