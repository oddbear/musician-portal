import { useLayoutEffect } from 'Hooks/effect.hook';
import React, { MouseEvent, useCallback, useContext } from 'react';
import styled from 'styled-components';
import { d } from 'Utils/dictionary';
import {
	GraphQLWrap,
	InviteInput,
	Response,
	ResponseBand,
	ResponseNotification,
	ResponseProfile,
} from 'Types/response.type';
import { InviteType, MemberHistoryActionType, NotificationType } from 'Utils/enum';
import Button from 'UI/Button';
import {
	sendOrChangeInviteToBand as sendOrChangeInviteToBandMutation,
	sendOrChangeInviteToProfile as sendOrChangeInviteToProfileMutation,
} from 'Schemas/Invite';
import { getNotifications, hideNotification as hideNotificationMutation } from 'Schemas/Notification';
import { DataProxy } from 'apollo-cache';
import { block as blockMutation } from 'Schemas/Profile';
import { errorContext } from 'Contexts/error.context';
import Link from 'Components/Link';
import { getFormattedDate } from 'Utils/date';
import { useGetRef } from 'Hooks/getRef.hook';
import { NOTIFICATIONS_LIMIT } from 'Utils/constants';
import { useSecureMutation } from 'Utils/apollo';

const MAX_MENTION_LENGTH = 100;

interface Props {
	notifications: Response<ResponseNotification>;
	loading: boolean;
	onLoadMore: () => void;
}

const Container = styled.div`
	font-family: ${({ theme }) => theme.fontFamily.quiet};
	width: 350px;
	font-size: 0.7rem;
	font-weight: 400;
	color: #2c2c2c;
`;

const Title = styled.h2`
	font-size: inherit;
	font-weight: 700;
	margin: 0;
`;

const Empty = styled.div`
	color: #888;
	margin-bottom: 30px;
`;

const Notices = styled.div`
	width: 100%;
	max-height: 60vh;
	overflow: auto;
`;

const Notice = styled.div`
	position: relative;
	display: flex;
	flex-direction: column;
	padding: 10px 0;

	&:not(:first-child) {
		border-top: 1px solid rgba(100, 100, 100, 0.3);
	}

	${Link} {
		cursor: pointer;
	}
`;

const More = styled.div`
	width: 100%;
	display: flex;
	place-content: center;
	color: #ccc;
	padding: 15px 0;
`;

const Remove = styled.div`
	cursor: pointer;
	position: absolute;
	right: 5px;
	font-family: monospace;
`;

const Time = styled.div`
	color: gray;
	font-size: 0.9em;
`;

interface NotificationLinkProps {
	notification: ResponseNotification;
}

const NotificationLink: React.FC<NotificationLinkProps> = ({ notification }) => {
	const { castBy, castIn, json, type } = notification;
	const fromProfile = json.fromProfile === true;

	const { getFullYear, getMonthName, getDay, getHours, getMinutes } = getFormattedDate(notification.createdAt);
	const time = `${getDay()} ${getMonthName()} ${getFullYear()} ${getHours()}:${getMinutes()}`;

	if (type === NotificationType.invite) {
		if (json.position !== null && json.position !== undefined && castBy !== null && castIn !== null) {
			if (fromProfile) {
				return (
					<Link
						location={{
							pathname: '/profile',
							query: {
								login: castBy.login,
							},
						}}
						asLocation={{
							pathname: `/profile/${castBy.login}`,
						}}
					>
						<Time>{time}</Time>
						<span>
							{d(`header.notifications.type.join`, castBy.name, castIn.name, d(`positions.${json.position.value}`))}
						</span>
					</Link>
				);
			} else {
				return (
					<Link
						location={{
							pathname: '/band',
							query: {
								alias: castIn.alias,
							},
						}}
						asLocation={{
							pathname: `/band/${castIn.alias}`,
						}}
					>
						<Time>{time}</Time>
						<span>{d(`header.notifications.type.invite`, castIn.name, d(`positions.${json.position.value}`))}</span>
					</Link>
				);
			}
		} else {
			return null;
		}
	} else if (castBy !== null && castIn !== null) {
		if (type === NotificationType.dismissFromBand) {
			return (
				<Link
					location={{
						pathname: '/band',
						query: {
							alias: castIn.alias,
						},
					}}
					asLocation={{
						pathname: `/band/${castIn.alias}`,
					}}
				>
					<Time>{time}</Time>
					<span>{d('header.notifications.type.dismissFromBand', castIn.name)}</span>
				</Link>
			);
		} else if (type === NotificationType.leaveBand) {
			return (
				<Link
					location={{
						pathname: '/profile',
						query: {
							login: castBy.login,
						},
					}}
					asLocation={{
						pathname: `/profile/${castBy.login}`,
					}}
				>
					<Time>{time}</Time>
					<span>{d('header.notifications.type.leaveBand', castBy.name, castIn.name)}</span>
				</Link>
			);
		} else if (type === NotificationType.hireToBand) {
			return (
				<Link
					location={{
						pathname: '/band',
						query: {
							alias: castIn.alias,
						},
					}}
					asLocation={{
						pathname: `/band/${castIn.alias}`,
					}}
				>
					<Time>{time}</Time>
					<span>{d('header.notifications.type.hireToBand', castIn.name)}</span>
				</Link>
			);
		} else if (type === NotificationType.joinBand) {
			return (
				<Link
					location={{
						pathname: '/profile',
						query: {
							login: castBy.login,
						},
					}}
					asLocation={{
						pathname: `/profile/${castBy.login}`,
					}}
				>
					<Time>{time}</Time>
					<span>{d('header.notifications.type.joinBand', castBy.name, castIn.name)}</span>
				</Link>
			);
		} else if (type === NotificationType.decline) {
			if (fromProfile) {
				return (
					<Link
						location={{
							pathname: '/profile',
							query: {
								login: castBy.login,
							},
						}}
						asLocation={{
							pathname: `/profile/${castBy.login}`,
						}}
					>
						<Time>{time}</Time>
						<span>{d('header.notifications.type.decline', castBy.name, castIn.name)}</span>
					</Link>
				);
			} else {
				return (
					<Link
						location={{
							pathname: '/band',
							query: {
								alias: castIn.alias,
							},
						}}
						asLocation={{
							pathname: `/band/${castIn.alias}`,
						}}
					>
						<Time>{time}</Time>
						<span>{d('header.notifications.type.decline', castBy.name, castIn.name)}</span>
					</Link>
				);
			}
		} else if (type === NotificationType.mention) {
			return (
				<Link
					location={{
						pathname: '/news',
						query: {
							id: json.mention?.post._id ?? '',
						},
					}}
					asLocation={{
						pathname: `/news/${json.mention?.post._id ?? ''}`,
					}}
				>
					<Time>{time}</Time>
					<span>
						{d(
							'header.notifications.type.mention',
							(json.mention?.comment?.text ?? json.mention?.post.text ?? '')
								.split('\n')[0]
								.substr(0, MAX_MENTION_LENGTH),
							castBy.name,
						)}
					</span>
				</Link>
			);
		} else {
			return null;
		}
	} else {
		return null;
	}
};

const Notifications: React.FC<Props> = ({ notifications, loading, onLoadMore }) => {
	const containerRef = useGetRef<HTMLDivElement>();
	const loadMoreRef = useGetRef<HTMLDivElement>();

	const { setError } = useContext(errorContext);

	const [hideNotification] = useSecureMutation<
		GraphQLWrap<'hideNotification', ResponseNotification>,
		{
			_id: string;
			type: number;
		}
	>(hideNotificationMutation);

	const hideNotificationHandler = useCallback(
		(_id: string, type: number) => (event: MouseEvent<HTMLDivElement>) => {
			event.stopPropagation();

			void hideNotification({
				variables: {
					_id,
					type,
				},
				update: (cache) => {
					const oldData = cache.readQuery<GraphQLWrap<'responseNotifications', Response<ResponseNotification>>>({
						query: getNotifications,
						variables: {
							limit: NOTIFICATIONS_LIMIT,
							visibleData: [],
						},
					});

					if (oldData === null) {
						return;
					}

					cache.writeQuery<GraphQLWrap<'responseNotifications', Response<ResponseNotification>>>({
						query: getNotifications,
						variables: {
							limit: NOTIFICATIONS_LIMIT,
							visibleData: [],
						},
						data: {
							responseNotifications: {
								...oldData.responseNotifications,
								totalLength: oldData.responseNotifications.totalLength - 1,
								data: oldData.responseNotifications.data.filter((notification) => notification._id !== _id),
							},
						},
					});
				},
			}).catch((err: Error) => {
				setError(err.message);
			});
		},
		[hideNotification, setError],
	);

	const onScroll = useCallback(() => {
		if (containerRef.current === null || loadMoreRef.current === null) {
			return;
		}

		if (
			containerRef.current.scrollTop >=
			containerRef.current.scrollHeight - containerRef.current.offsetHeight - loadMoreRef.current.offsetHeight
		) {
			onLoadMore();
		}
	}, [onLoadMore, containerRef, loadMoreRef]);

	useLayoutEffect(() => {
		if (containerRef.current === null || loadMoreRef.current === null) {
			return;
		}

		const container = containerRef.current;

		if (
			!loading &&
			containerRef.current.scrollTop >=
				containerRef.current.scrollHeight - containerRef.current.offsetHeight - loadMoreRef.current.offsetHeight
		) {
			onLoadMore();
		}

		container.addEventListener('scroll', onScroll);
		return () => container.removeEventListener('scroll', onScroll);
	}, [loading, onLoadMore, onScroll, containerRef, loadMoreRef]);

	if (notifications === undefined) {
		return null;
	}

	return (
		<Container>
			<Title>{d('header.notifications.title')}</Title>
			<Notices ref={containerRef}>
				{notifications.data.length !== 0 ? (
					<>
						{[...notifications.data]
							.sort((n1, n2) => (n1.createdAt < n2.createdAt ? 1 : n1.createdAt > n2.createdAt ? -1 : 0))
							.map((notification) => (
								<Notice key={notification._id}>
									<NotificationLink notification={notification} />
									<Buttons notification={notification} />
									{(NotificationType[notification.type] in MemberHistoryActionType ||
										notification.type === NotificationType.mention) && (
										<Remove onClick={hideNotificationHandler(notification._id, notification.type)}>x</Remove>
									)}
								</Notice>
							))}
						{notifications.totalLength > notifications.data.length && (
							<More ref={loadMoreRef}>{d('header.notifications.more')}</More>
						)}
					</>
				) : (
					<Empty>{d('header.notifications.empty')}</Empty>
				)}
			</Notices>
		</Container>
	);
};

export default Notifications;

const Row = styled.div`
	display: flex;
	margin-top: 5px;

	${Button} {
		margin-right: 3px;
		margin-bottom: -2px;

		&:hover {
			color: #2c2c2c;
		}
	}
`;

interface ButtonsProps {
	notification: ResponseNotification;
}

const Buttons: React.FC<ButtonsProps> = ({ notification: { _id, json, castIn, castBy, castOn, type } }) => {
	const fromProfile = json.fromProfile === true;

	const updateQueryDecline = useCallback(
		(cache: DataProxy) => {
			const oldData = cache.readQuery<
				GraphQLWrap<'responseNotifications', Response<ResponseNotification> & { uncheckedLength: number }>,
				{
					limit: number;
					visibleData: string[];
				}
			>({
				query: getNotifications,
				variables: {
					limit: NOTIFICATIONS_LIMIT,
					visibleData: [],
				},
			});

			if (oldData === null || oldData === undefined || oldData.responseNotifications === null) {
				return;
			}

			cache.writeQuery<
				GraphQLWrap<'responseNotifications', Response<ResponseNotification> & { uncheckedLength: number }>,
				{
					limit: number;
					visibleData: string[];
				}
			>({
				query: getNotifications,
				variables: {
					limit: NOTIFICATIONS_LIMIT,
					visibleData: [],
				},
				data: {
					responseNotifications: {
						...oldData.responseNotifications,
						uncheckedLength: oldData.responseNotifications.uncheckedLength - 1,
						totalLength: oldData.responseNotifications.totalLength - 1,
						data: oldData.responseNotifications.data.filter((notification) => notification._id !== _id),
					},
				},
			});
		},
		[_id],
	);

	const updateQueryAccept = useCallback(
		(cache: DataProxy) => {
			const oldData = cache.readQuery<
				GraphQLWrap<'responseNotifications', Response<ResponseNotification> & { uncheckedLength: number }>,
				{
					limit: number;
					visibleData: string[];
				}
			>({
				query: getNotifications,
				variables: {
					limit: NOTIFICATIONS_LIMIT,
					visibleData: [],
				},
			});

			if (oldData === null || oldData === undefined || oldData.responseNotifications === null) {
				return;
			}

			const newData = oldData.responseNotifications.data.filter(
				(notification) => notification.type === NotificationType.invite && notification.castBy?._id !== castBy?._id,
			);

			cache.writeQuery<
				GraphQLWrap<'responseNotifications', Response<ResponseNotification> & { uncheckedLength: number }>,
				{
					limit: number;
					visibleData: string[];
				}
			>({
				query: getNotifications,
				variables: {
					limit: NOTIFICATIONS_LIMIT,
					visibleData: [],
				},
				data: {
					responseNotifications: {
						...oldData.responseNotifications,
						uncheckedLength:
							oldData.responseNotifications.uncheckedLength -
							(oldData.responseNotifications.data.length - newData.length),
						totalLength:
							oldData.responseNotifications.totalLength - (oldData.responseNotifications.data.length - newData.length),
						data: newData,
					},
				},
			});
		},
		[castBy],
	);

	const [sendOrChangeInviteToProfile] = useSecureMutation<
		GraphQLWrap<'sendOrChangeInviteToProfile', ResponseProfile>,
		{
			invite: InviteInput;
		}
	>(sendOrChangeInviteToProfileMutation);
	const [sendOrChangeInviteToBand] = useSecureMutation<
		GraphQLWrap<'sendOrChangeInviteToBand', ResponseBand>,
		{
			invite: InviteInput;
		}
	>(sendOrChangeInviteToBandMutation);

	const varName = castBy !== null && 'alias' in castBy ? 'alias' : 'login';
	const [block] = useSecureMutation<
		GraphQLWrap<'block', boolean>,
		{
			alias?: string;
			login?: string;
		}
	>(blockMutation, {
		variables: {
			[varName]: fromProfile ? castBy?.login : castIn?.alias,
		},
		update: (cache) => {
			const oldData = cache.readQuery<
				GraphQLWrap<'responseNotifications', Response<ResponseNotification> & { uncheckedLength: number }>
			>({
				query: getNotifications,
				variables: {
					limit: NOTIFICATIONS_LIMIT,
					visibleData: [],
				},
			});
			if (oldData === null) {
				return;
			}

			cache.writeQuery<
				GraphQLWrap<'responseNotifications', Response<ResponseNotification> & { uncheckedLength: number }>
			>({
				query: getNotifications,
				variables: {
					limit: NOTIFICATIONS_LIMIT,
					visibleData: [],
				},
				data: {
					responseNotifications: {
						...oldData.responseNotifications,
						data: oldData.responseNotifications.data.filter((notification) => notification._id !== _id),
					},
				},
			});
		},
	});

	const onAccept = useCallback(() => {
		if (json.position !== null && json.position !== undefined) {
			if (fromProfile) {
				void sendOrChangeInviteToProfile({
					variables: {
						invite: {
							position: json.position.value,
							type: InviteType.accept,
							to: (castBy as ResponseProfile).login,
							from: castIn!.alias,
						},
					},
					update: updateQueryAccept,
				});
			} else {
				void sendOrChangeInviteToBand({
					variables: {
						invite: {
							position: json.position.value,
							type: InviteType.accept,
							to: castIn!.alias,
							from: (castOn as ResponseProfile).login,
						},
					},
					update: updateQueryAccept,
				});
			}
		}
	}, [
		updateQueryAccept,
		castIn,
		castOn,
		castBy,
		fromProfile,
		json,
		sendOrChangeInviteToProfile,
		sendOrChangeInviteToBand,
	]);

	const onDecline = useCallback(() => {
		if (json.position !== null && json.position !== undefined) {
			if (fromProfile) {
				if (castBy === null) {
					throw new Error('castBy is null then decline value');
				}

				void sendOrChangeInviteToProfile({
					variables: {
						invite: {
							position: json.position.value,
							type: InviteType.decline,
							from: castIn?.alias,
							to: castBy.login,
						},
					},
					update: updateQueryDecline,
				});
			} else {
				if (castIn === null) {
					throw new Error('castIn is null then decline value');
				}

				void sendOrChangeInviteToBand({
					variables: {
						invite: {
							position: json.position.value,
							type: InviteType.decline,
							from: castOn?.login,
							to: castIn.alias,
						},
					},
					update: updateQueryDecline,
				});
			}
		}
	}, [
		updateQueryDecline,
		castIn,
		castOn,
		castBy,
		fromProfile,
		json,
		sendOrChangeInviteToProfile,
		sendOrChangeInviteToBand,
	]);

	const onBlock = useCallback(() => {
		void block();
	}, [block]);

	switch (type) {
		case NotificationType.invite:
			return (
				<Row>
					<Button color="simple" onClick={onAccept}>
						{d('joinBlock.accept')}
					</Button>
					<Button color="simple" onClick={onDecline}>
						{d('joinBlock.deny')}
					</Button>
					<Button color="simple" onClick={onBlock}>
						{d('header.notifications.block')}
					</Button>
				</Row>
			);

		default:
			return null;
	}
};
