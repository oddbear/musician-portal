import { appContext } from 'Contexts/app.context';
import Conversations from '../Conversations';
import { d } from 'Utils/dictionary';
import { Group } from 'UI/NavItem';
import LazyImage from 'Components/LazyImage';
import LED from 'UI/LED';
import Link from 'Components/Link';
import Notifications from '../Notifications';
import styled from 'styled-components';
import { tooltipContext } from 'Contexts/tooltip.context';
import React, { MouseEvent, useCallback, useContext, useEffect, useRef, useState } from 'react';
import Tooltip, { Pointer } from 'UI/Tooltip';
import { useQuery } from '@apollo/react-hooks';
import {
	GraphQLWrap,
	Response,
	ResponseBand,
	ResponseConversation,
	ResponseNotification,
	ResponseProfile,
	UserNotificationsSettings,
	UserSettings,
} from 'Types/response.type';
import {
	checkNotifications as checkNotificationsMutation,
	getNotifications,
	subscribeForNotification,
} from 'Schemas/Notification';
import { MemberHistoryActionType, NotificationType } from 'Utils/enum';
import { getProfileMutation } from 'Schemas/Profile';
import { getBandMutation } from 'Schemas/Band';
import { getConversations, subscribeForMessage } from 'Schemas/Conversation';
import { CONVERSATIONS_QUERY_VARIABLES } from 'Components/Conversations/Conversations';
import { errorContext } from 'Contexts/error.context';
import Dummy from 'UI/Dummy/Dummy';
import { useLazyLoad } from 'Hooks/lazyload.hook';
import { NOTIFICATIONS_LIMIT } from 'Utils/constants';
import { useSecureMutation } from 'Utils/apollo';

const ENVELOPE_ICON = 'BG/conversations_xnadpr';
const BELL_ICON = 'BG/notifications_p9sdlb';
const MESSAGE_SOUND_SRC = 'Notifications/message_sound';
const NOTIFICATION_SOUND_SRC = 'Notifications/notification_sound';

const UNCHECKABLE_NOTIFICATIONS = [NotificationType.invite];

enum settingsToNotificationMap {
	notifyOnInvite = NotificationType.invite,
	notifyOnLeave = NotificationType.leaveBand,
	notifyOnDismiss = NotificationType.dismissFromBand,
	notifyOnDecline = NotificationType.decline,
	notifyOnHire = NotificationType.hireToBand,
	notifyOnJoin = NotificationType.joinBand,
	notifyOnMention = NotificationType.mention,
}

const Container = styled.div`
	display: flex;
	width: 100%;
	place-content: space-around;
	margin: 19px 10px;
	padding: 0;

	${Pointer} {
		background-color: rgba(255, 255, 255, 0.8);
	}
`;

const IconWrap = styled.div`
	cursor: pointer;
	position: relative;
	margin-left: 6px;
`;

const Icon = styled(LazyImage)`
	height: 24px;
`;

const ARROW_SIZE = '5px';

const Arrow = styled.div`
	cursor: pointer;
	margin-top: 10px;
	border-left: ${ARROW_SIZE} solid transparent;
	border-right: ${ARROW_SIZE} solid transparent;
	border-top: 10px solid ${({ theme }) => theme.color.secondary};

	&:hover {
		border-top: 10px solid #aeaeae;
	}
`;

const Line = styled.div`
	border-left: 1px solid #f1f1f1;
	height: 32px;
`;

const Counter = styled.div`
	display: flex;
	place-content: center;
	align-items: center;
	position: absolute;
	right: -11px;
	top: -3px;
	border-radius: 500px;
	width: 14px;
	height: 14px;
	font-family: ${({ theme }) => theme.fontFamily.quiet};
	font-size: 9px;
	color: white;
	background-color: darkred;
`;

const StyledTooltip = styled(Tooltip)`
	cursor: default;
	background-color: rgba(255, 255, 255, 0.8);
`;

const subscriptionState: {
	notifications: Response<ResponseNotification> & { uncheckedLength: number };
	conversations: Response<ResponseConversation>;
	settings: UserSettings;
} = {
	notifications: {
		uncheckedLength: 0,
		totalLength: 0,
		data: [],
	},
	conversations: {
		totalLength: 0,
		data: [],
	},
	settings: {
		notifications: {
			soundOnNewMessage: true,
			soundOnNewNotification: true,
			notifyOnInvite: true,
			notifyOnJoin: true,
			notifyOnHire: true,
			notifyOnDecline: true,
			notifyOnDismiss: true,
			notifyOnLeave: true,
			notifyOnMention: true,
		},
		privacy: {
			conversationsEnabled: true,
			showStatistics: false,
		},
		conversations: [],
	},
};

const Profile: React.FC = () => {
	const [isLoadingNotifications, setIsLoadingNotifications] = useState<boolean>(false);
	const [tooltipAttachedElement, setTooltipAttachedElement] = useState<string | null>(null);
	const [tooltipVisible, setTooltipVisible] = useState<boolean>(false);
	const [playAudio, setPlayAudio] = useState<string | null>(null);
	const containerRef = useRef<HTMLDivElement>(null);
	const conversationRef = useRef<HTMLDivElement>(null);
	const notificationRef = useRef<HTMLDivElement>(null);
	const iframeRef = useRef<HTMLIFrameElement>(null);

	const { user } = useContext(appContext);
	const { visible, setAttachedElement, setVisible } = useContext(tooltipContext);

	if (user !== null) {
		subscriptionState.settings = user.settings;
	}

	const { src: messageAudioSrc } = useLazyLoad({
		src: MESSAGE_SOUND_SRC,
		tag: 'audio',
	});
	const { src: notificationAudioSrc } = useLazyLoad({
		src: NOTIFICATION_SOUND_SRC,
		tag: 'audio',
	});

	const changeTooltipVisibleState = useCallback(() => {
		setVisible?.(!visible);
	}, [visible, setVisible]);

	const { data: conversationsData, subscribeToMore: subscribeToConversations } = useQuery<
		GraphQLWrap<'responseConversations', Response<ResponseConversation>>,
		{
			limit: number;
			visibleData: string[];
		}
	>(getConversations, {
		variables: CONVERSATIONS_QUERY_VARIABLES,
		ssr: false,
	});
	const conversations =
		conversationsData === undefined
			? {
					totalLength: 0,
					data: [],
			  }
			: conversationsData.responseConversations;
	subscriptionState.conversations = conversations;

	const conversationsCounter: number = conversations.data.filter(({ messages }) =>
		messages.data.some(({ isChecked, from }) => !isChecked && from._id !== user?._id),
	).length;

	const {
		data: notificationsData,
		subscribeToMore: subscribeToNotifications,
		fetchMore,
		loading: loadingNotifications,
	} = useQuery<
		GraphQLWrap<'responseNotifications', Response<ResponseNotification> & { uncheckedLength: number }>,
		{
			limit: number;
			visibleData: string[];
		}
	>(getNotifications, {
		variables: {
			limit: NOTIFICATIONS_LIMIT,
			visibleData: [],
		},
		ssr: false,
	});
	const notifications =
		notificationsData === undefined
			? {
					uncheckedLength: 0,
					totalLength: 0,
					data: [],
			  }
			: notificationsData.responseNotifications;
	const notificationsCounter: number = notifications.uncheckedLength;
	subscriptionState.notifications = notifications;

	const uncheckedNotifications = notifications.data.filter(
		(notification) => !UNCHECKABLE_NOTIFICATIONS.includes(notification.type) && !notification.isChecked,
	);

	const [checkNotifications, { loading: checkNotificationsLoading }] = useSecureMutation<
		GraphQLWrap<'checkNotifications', ResponseNotification[]>,
		{
			notificationsIds: string[];
		}
	>(checkNotificationsMutation, {
		variables: {
			notificationsIds: uncheckedNotifications.map((notification) => notification._id),
		},
		update: (cache) => {
			const oldData = cache.readQuery<
				GraphQLWrap<'responseNotifications', Response<ResponseNotification> & { uncheckedLength: number }>
			>({
				query: getNotifications,
				variables: {
					limit: NOTIFICATIONS_LIMIT,
					visibleData: [],
				},
			});

			if (oldData === null) {
				return;
			}

			cache.writeQuery<
				GraphQLWrap<'responseNotifications', Response<ResponseNotification> & { uncheckedLength: number }>
			>({
				query: getNotifications,
				variables: {
					limit: NOTIFICATIONS_LIMIT,
					visibleData: [],
				},
				data: {
					responseNotifications: {
						...oldData.responseNotifications,
						uncheckedLength:
							oldData.responseNotifications.uncheckedLength -
							oldData.responseNotifications.data.filter(
								(notification) => !UNCHECKABLE_NOTIFICATIONS.includes(notification.type) && !notification.isChecked,
							).length,
						data: oldData.responseNotifications.data.map((notification) => ({
							...notification,
							isChecked: UNCHECKABLE_NOTIFICATIONS.includes(notification.type) ? notification.isChecked : true,
						})),
					},
				},
			});
		},
	});

	useEffect(() => {
		if (
			!checkNotificationsLoading &&
			uncheckedNotifications.length > 0 &&
			tooltipAttachedElement === 'notifications' &&
			tooltipVisible
		) {
			void checkNotifications();
		}
	}, [checkNotificationsLoading, uncheckedNotifications, checkNotifications, tooltipAttachedElement, tooltipVisible]);

	const [getProfile] = useSecureMutation<GraphQLWrap<'getProfile', ResponseProfile>, { login: string }>(
		getProfileMutation,
	);
	const [getBand] = useSecureMutation<GraphQLWrap<'getBand', ResponseBand>, { alias: string }>(getBandMutation);

	useEffect(() => {
		if (iframeRef.current !== null) {
			if (playAudio !== null) {
				setPlayAudio(null);

				// the way to trick the browser's security, it's wouldn't be possible to play audio without user interaction
				const iframeCurrent = iframeRef.current;
				iframeCurrent.innerHTML = '';
				iframeCurrent.innerHTML = `<iframe src="${
					playAudio === 'message' ? messageAudioSrc : notificationAudioSrc
				}" allow="autoplay" style="display: none"></iframe>`;
			}
		}
	}, [iframeRef, playAudio, notificationAudioSrc, messageAudioSrc]);

	useEffect(() => {
		try {
			subscribeToNotifications<GraphQLWrap<'newNotification', ResponseNotification>>({
				document: subscribeForNotification,
				updateQuery: (prev, { subscriptionData }) => {
					if (subscriptionData.data === undefined) {
						return prev;
					}

					const notificationData = subscriptionData.data.newNotification;

					if (NotificationType[notificationData.type] in MemberHistoryActionType) {
						if (notificationData.castBy !== null) {
							if ('login' in notificationData.castBy) {
								void getProfile({
									variables: {
										login: notificationData.castBy.login,
									},
								});
							}
						}
						if (notificationData.castIn !== null) {
							if ('alias' in notificationData.castIn) {
								void getBand({
									variables: {
										alias: notificationData.castIn.alias,
									},
								});
							}
						}
					}

					if (
						NotificationType[notificationData.type] in MemberHistoryActionType ||
						NotificationType.mention === notificationData.type ||
						NotificationType.invite === notificationData.type
					) {
						if (
							subscriptionState.settings.notifications.soundOnNewNotification &&
							subscriptionState.settings.notifications[
								settingsToNotificationMap[notificationData.type] as keyof UserNotificationsSettings
							]
						) {
							setPlayAudio('notification');
						}

						return {
							responseNotifications: {
								...subscriptionState.notifications,
								uncheckedLength: subscriptionState.notifications.uncheckedLength + 1,
								totalLength: subscriptionState.notifications.totalLength + 1,
								data: [notificationData].concat(subscriptionState.notifications.data),
							},
						};
					} else if (NotificationType.cancel === notificationData.type) {
						return {
							responseNotifications: {
								...subscriptionState.notifications,
								uncheckedLength: subscriptionState.notifications.uncheckedLength - 1,
								totalLength: subscriptionState.notifications.totalLength - 1,
								data: subscriptionState.notifications.data.filter(
									(notification) => notification._id !== notificationData._id,
								),
							},
						};
					} else {
						return {
							responseNotifications: subscriptionState.notifications,
						};
					}
				},
			});
		} catch (err) {
			console.log('refresh browser to continue notifications subscription flow');
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	useEffect(() => {
		try {
			subscribeToConversations<GraphQLWrap<'newMessage', ResponseConversation>>({
				document: subscribeForMessage,
				updateQuery: (prev, { subscriptionData }) => {
					if (subscriptionData.data === undefined) {
						return prev;
					}

					const conversationData = subscriptionData.data.newMessage;
					const matchedConversation = subscriptionState.conversations.data.find(
						(conversation) => conversation._id === conversationData._id,
					);

					if (matchedConversation === undefined) {
						return {
							responseConversations: {
								...subscriptionState.conversations,
								totalLength: subscriptionState.conversations.totalLength + 1,
								data: [conversationData, ...subscriptionState.conversations.data],
							},
						};
					} else {
						if (
							subscriptionState.settings.notifications.soundOnNewMessage &&
							!subscriptionState.settings.conversations.some(
								(conversationSettings) =>
									conversationSettings._id === matchedConversation._id && conversationSettings.muted,
							) &&
							!location.href.includes(matchedConversation._id)
						) {
							setPlayAudio('message');
						}

						return {
							responseConversations: {
								...subscriptionState.conversations,
								totalLength: subscriptionState.conversations.totalLength,
								data: subscriptionState.conversations.data.map((conversation) =>
									conversation._id !== conversationData._id
										? conversation
										: {
												...conversation,
												messages: {
													...conversationData.messages,
													totalLength: conversationData.messages.totalLength,
													data: [...conversationData.messages.data, ...conversation.messages.data],
												},
										  },
								),
							},
						};
					}
				},
			});
		} catch (err) {
			console.log('refresh browser to continue conversations subscription flow');
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const hideTooltip = useCallback(() => {
		setTooltipVisible(false);
	}, []);

	const showConversations = useCallback(() => {
		setTooltipAttachedElement('conversations');
		setTooltipVisible(tooltipAttachedElement === 'conversations' ? !tooltipVisible : true);

		window.addEventListener('mousedown', hideTooltip);
		return () => window.removeEventListener('mousedown', hideTooltip);
	}, [tooltipAttachedElement, tooltipVisible, hideTooltip]);

	const showNotifications = useCallback(() => {
		setTooltipAttachedElement('notifications');
		setTooltipVisible(tooltipAttachedElement === 'notifications' ? !tooltipVisible : true);

		window.addEventListener('mousedown', hideTooltip);
		return () => window.removeEventListener('mousedown', hideTooltip);
	}, [tooltipAttachedElement, tooltipVisible, hideTooltip]);

	const prevent = useCallback((e: MouseEvent) => {
		e.stopPropagation();
	}, []);

	const loadMoreNotifications = useCallback(async () => {
		if (!loadingNotifications && !isLoadingNotifications) {
			setIsLoadingNotifications(true);

			await fetchMore({
				variables: {
					limit: NOTIFICATIONS_LIMIT,
					visibleData: notifications.data.map((notification) => notification._id),
				},
				updateQuery: (prev, { fetchMoreResult }) => {
					setTimeout(() => {
						setIsLoadingNotifications(false);
					}, 0);

					if (fetchMoreResult === undefined) {
						return prev;
					}

					return {
						...fetchMoreResult,
						responseNotifications: {
							...fetchMoreResult.responseNotifications,
							data: [...fetchMoreResult.responseNotifications.data, ...prev.responseNotifications.data],
						},
					};
				},
			});
		}
	}, [fetchMore, isLoadingNotifications, loadingNotifications, notifications.data]);

	useEffect(() => {
		const cancel = (event: KeyboardEvent): void => {
			if (event.code === 'Escape') {
				event.preventDefault();
				hideTooltip();
			}
		};

		document.documentElement.addEventListener('keydown', cancel);
		return () => document.documentElement.removeEventListener('keydown', cancel);
	}, [hideTooltip]);

	useEffect(() => {
		setTooltipVisible(false);
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [location.href]);

	useEffect(() => {
		setAttachedElement?.(containerRef.current);
	}, [setAttachedElement, containerRef]);

	return (
		<div ref={containerRef}>
			<Dummy loading="user">
				<Group title={<Header />}>
					<Container>
						<tooltipContext.Provider
							value={{
								attachedElement:
									tooltipAttachedElement === 'conversations' ? conversationRef.current : notificationRef.current,
								visible: tooltipVisible,
							}}
						>
							<StyledTooltip position="bottom" onClick={prevent} onMouseDown={prevent}>
								{tooltipAttachedElement === 'conversations' ? (
									<Conversations conversations={conversations.data} />
								) : (
									<Notifications
										notifications={notifications}
										loading={loadingNotifications || isLoadingNotifications || !tooltipVisible}
										onLoadMore={loadMoreNotifications}
									/>
								)}
							</StyledTooltip>
						</tooltipContext.Provider>
						<IconWrap ref={notificationRef} onClick={showNotifications}>
							{notificationsCounter > 0 && (
								<Counter>
									{Math.min(notificationsCounter, 9)}
									{notificationsCounter > 9 && '+'}
								</Counter>
							)}
							<Icon src={BELL_ICON} alt="notifications" />
						</IconWrap>
						<IconWrap ref={conversationRef} onClick={showConversations}>
							{conversationsCounter > 0 && (
								<Counter>
									{Math.min(conversationsCounter, 9)}
									{conversationsCounter > 9 && '+'}
								</Counter>
							)}
							<Icon src={ENVELOPE_ICON} alt="messages" />
						</IconWrap>
						<Line />
						<Arrow onClick={changeTooltipVisibleState} />
					</Container>
				</Group>
			</Dummy>
			<div ref={iframeRef} />
		</div>
	);
};

export default Profile;

const HeaderContainer = styled.div`
	display: flex;
	min-width: 150px;
	align-items: center;
	cursor: pointer;
	margin: 0;
`;

const Avatar = styled(LazyImage)`
	width: 28px;
	height: 28px;
	margin-right: 30px;
	margin-left: 15px;
	background-color: #eee;
`;

const HeaderTitle = styled.div`
	margin-right: 15px;
	padding-bottom: 3px;
	color: #192022;

	&:hover {
		color: #3a4a4f;
	}
`;

const Header: React.FC = () => {
	const [activeLED, setActiveLED] = useState<string | null>(null);

	const { user } = useContext(appContext);
	const { visible, setVisible } = useContext(tooltipContext);
	const { setError } = useContext(errorContext);

	const hideDropdown = useCallback(() => {
		setVisible?.(false);
	}, [setVisible]);

	const openDropdown = useCallback(() => {
		setVisible?.(!visible);

		window.addEventListener('mousedown', hideDropdown);
		return () => window.removeEventListener('mousedown', hideDropdown);
	}, [setVisible, visible, hideDropdown]);

	const prevent = useCallback((e: MouseEvent) => {
		e.stopPropagation();
	}, []);

	const logout = useCallback(() => {
		hideDropdown();

		void fetch(`${config.api.url}/logout`, {
			method: 'GET',
			credentials: 'include',
		})
			.then(() => {
				location.href = '/';
			})
			.catch((error: Error) => {
				setError(error.message);
			});
	}, [setError, hideDropdown]);

	useEffect(() => {
		const cancel = (event: KeyboardEvent): void => {
			if (event.code === 'Escape') {
				event.preventDefault();
				hideDropdown();
			}
		};

		document.documentElement.addEventListener('keydown', cancel);
		return () => document.documentElement.removeEventListener('keydown', cancel);
	}, [hideDropdown]);

	return (
		<HeaderContainer onClick={openDropdown}>
			<StyledTooltip position="bottom" pointer={false} onMouseDown={prevent}>
				{[
					{
						pathname: '/profile',
						alias: 'privateOffice',
					},
					{
						pathname: '/settings',
						alias: 'settings',
					},
				].map(({ pathname, alias }) => (
					<Link
						location={{
							query: {},
							pathname,
						}}
						key={alias}
					>
						<LEDRow alias={alias} isActive={activeLED === alias} setActive={setActiveLED} onClick={hideDropdown} />
					</Link>
				))}
				<LEDRow alias="signout" isActive={activeLED === 'signout'} setActive={setActiveLED} onClick={logout} />
			</StyledTooltip>
			<Avatar src={user?.avatar.src ?? ''} alt="avatar" />
			<HeaderTitle>
				<span>{user?.name ?? ''}</span>
			</HeaderTitle>
		</HeaderContainer>
	);
};

interface LEDRowProps {
	alias: string;
	isActive: boolean;
	setActive(value: string | null): void;
	onClick?(): void;
}

const Row = styled.div`
	cursor: pointer;
	display: flex;
	font-family: ${({ theme }) => theme.fontFamily.quiet};
	font-size: 0.8rem;
	align-items: center;
	line-height: 36px;

	${LED} {
		margin-right: 10px;
		margin-top: 0;
	}
`;

const RowSpan = styled.span`
	white-space: nowrap;
	word-spacing: normal;
`;

const LEDRow: React.FC<LEDRowProps> = ({ alias, isActive, setActive, onClick }) => {
	const active = useCallback(() => {
		setActive(alias);
	}, [setActive, alias]);

	const disable = useCallback(() => {
		setActive(null);
	}, [setActive]);

	return (
		<Row onMouseOver={active} onMouseOut={disable} onClick={onClick}>
			<LED on={isActive} />
			<RowSpan>{d(`header.profile.${alias}`)}</RowSpan>
		</Row>
	);
};
