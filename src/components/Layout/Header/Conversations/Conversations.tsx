import LazyImage from 'Components/LazyImage';
import Link from 'Components/Link';
import { appContext } from 'Contexts/app.context';
import React, { useCallback, useContext, useState } from 'react';
import styled from 'styled-components';
import LED from 'UI/LED';
import { d } from 'Utils/dictionary';
import { ResponseConversation } from 'Types/response.type';
import Scroll from 'UI/Scroll';

interface Props {
	conversations: ResponseConversation[];
}

const Container = styled.div`
	font-family: ${({ theme }) => theme.fontFamily.quiet};
	width: 350px;
	font-size: 0.7rem;
	font-weight: 400;
	color: #2c2c2c;
`;

const Row = styled.div`
	display: flex;
	place-content: space-between;
`;

const Title = styled.h2`
	margin: 0 0 10px 0;
	font-size: inherit;
	font-weight: 700;
`;

const Messages = styled(Scroll)`
	width: 100%;
	max-height: 176px;
`;

const MessageWrap = styled.div`
	&:not(:first-child) {
		margin-top: 10px;
	}
`;

interface MessageProps {
	checked: boolean;
}

const Message = styled(Link)<MessageProps>`
	width: 100%;
	display: flex;
	align-items: center;
	background-color: ${({ checked }) => (checked ? 'transparent' : 'rgba(100, 0, 0, 0.1)')};
`;

const Avatar = styled(LazyImage)`
	height: 28px;
	width: 28px;
	margin-right: 10px;
	background-color: white;
`;

const Content = styled.div`
	display: flex;
	flex-direction: column;
	overflow: hidden;
`;

const Text = styled.span`
	font-size: 0.6rem;
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap;
	color: rgba(44, 44, 44, 0.5);
`;

const StyledLED = styled(LED)`
	margin-top: -5px;
	margin-left: 4px;
`;

const Empty = styled.div`
	color: #888;
	margin-bottom: 30px;
`;

const Me = styled.span`
	font-style: italic;
	margin-right: 4px;
	color: rgba(44, 44, 44, 1);
`;

const Conversations: React.FC<Props> = ({ conversations }) => {
	const { location } = useContext(appContext);
	const [isLight, setIsLight] = useState<boolean>(false);
	const { user } = useContext(appContext);

	const filteredConversations = conversations.filter(({ messages }) => messages.data.length > 0);

	const disableLight = useCallback(() => {
		setIsLight(false);
	}, []);

	const enableLight = useCallback(() => {
		setIsLight(true);
	}, []);

	return (
		<Container>
			<Row>
				<Title>{d('header.conversations.title')}</Title>
				<Link location={{ query: {}, pathname: '/conversations' }}>
					<Row onMouseOver={enableLight} onMouseOut={disableLight}>
						<span>{d('header.conversations.showAll')}</span>
						<StyledLED on={isLight} />
					</Row>
				</Link>
			</Row>
			<Messages>
				{filteredConversations.length !== 0 ? (
					filteredConversations.map(({ _id, messages, bandParticipants, profileParticipants }) => {
						const message = messages.data[0];
						const sender = [...bandParticipants, ...profileParticipants].filter(
							(participant) => user?._id !== participant._id,
						)[0];

						const isMe = user?._id === message.from._id;

						return (
							<MessageWrap key={_id}>
								<Message
									location={{
										...location,
										pathname: '/conversations',
									}}
									asLocation={{
										...location,
										pathname: `/conversations/${_id}`,
									}}
									replace={true}
									prefetch={false}
									checked={message.isChecked || isMe}
								>
									<Avatar src={sender.avatar.src} alt={`${sender.name}'s avatar`} />
									<Content>
										<div>{sender.name}</div>
										<Text>
											{isMe && <Me>{d('header.conversations.you')}:</Me>}
											{message.text}
										</Text>
									</Content>
								</Message>
							</MessageWrap>
						);
					})
				) : (
					<Empty>{d('header.conversations.empty')}</Empty>
				)}
			</Messages>
		</Container>
	);
};

export default Conversations;
