import Button from 'UI/Button';
import { d } from 'Utils/dictionary';
import Interval from 'UI/Interval';
import Select from 'UI/Select';
import styled from 'styled-components';
import { useFiltersQuery } from 'Hooks/filters.hook';
import {
	CityFilter,
	CountryFilter,
	Filters,
	GenreFilter,
	Genres,
	PositionFilter,
	Positions,
	TypeFilter,
} from 'Types/filters.type';
import Filter, { Header as FilterHeader, Option } from 'UI/Filter';
import React, { useCallback, useContext, useState } from 'react';
import { useQuery } from '@apollo/react-hooks';
import { getFilters } from 'Schemas/Filters';
import { getEnumKeys, PositionVariant } from 'Utils/enum';
import Dummy from 'UI/Dummy/Dummy';
import { errorContext } from 'Contexts/error.context';

const MAX_EXPERIENCE_VALUE = 10;

interface Props {
	className?: string;
	onChange(filters: Filters, changeRoute?: boolean): void;
}

export interface Experience {
	value: number;
	title: string;
}

const Container = styled.div`
	position: relative;
	padding-bottom: 60px;
`;

const ButtonGroup = styled.div`
	display: flex;
	margin: 0 0 41px 0;

	*:first-child {
		margin-right: 20px;
	}
`;

const typeFilters: TypeFilter[] = ['profile', 'band', 'repetitionBase'];
const experienceIntervalParts = MAX_EXPERIENCE_VALUE;

interface PrepareFilter<T> {
	[key: string]: T;
}

function prepareFilter<T>(filter: PrepareFilter<T>): PrepareFilter<T> {
	return Object.entries(filter).reduce<PrepareFilter<T>>((acc, [filterKey, filterValue]) => {
		if (typeof filterValue !== 'boolean' || filterValue !== false) {
			acc[filterKey] = filterValue;
		}

		return acc;
	}, {});
}

const FiltersComponent: React.FC<Props> = ({ className, onChange }) => {
	const [type, setType] = useState<TypeFilter>(typeFilters[0]);
	const [positions, setPositions] = useState<Positions>({});
	const [genres, setGenres] = useState<Genres>({});
	const [country, setCountry] = useState<string | null>(null);
	const [city, setCity] = useState<string | null>(null);
	const [experience, setExperience] = useState<Experience>({
		value: 0,
		title: '',
	});
	const { setError } = useContext(errorContext);

	// eslint-disable-next-line prefer-const
	let { data, loading } = useQuery<{
		cities: CityFilter[];
		countries: CountryFilter[];
		genres: GenreFilter[];
		positions: PositionFilter[];
	}>(getFilters, {
		ssr: true,
		onError: (error) => {
			setError(error.message);
		},
	});

	if (data === undefined) {
		data = {
			cities: [],
			countries: [],
			genres: [],
			positions: [],
		};
	}

	const { cities: cityFilters, countries: countryFilters, genres: genreFilters, positions: positionFilters } = data;

	const changeCountry = (value: string): void => {
		setCity(null);
		setCountry(value);
	};

	const defineExperience = useCallback((percentage: number, isPart?: boolean): { value: number; title: string } => {
		let value = 0;
		let title: string = d('search.filters.experience.none');

		const part = isPart === true ? percentage : percentage * experienceIntervalParts;
		if (part >= 1) {
			value = Math.floor(part);
			title = d(`search.filters.experience.${value === 1 ? 'oneMore' : 'manyMore'}`, value);
		}

		return { value, title };
	}, []);

	const changeExperience = useCallback(
		(percentage: number, isPart?: boolean): string => {
			const { value, title } = defineExperience(percentage, isPart);

			setExperience({
				value,
				title,
			});

			return title;
		},
		[defineExperience],
	);

	const apply = useCallback(
		(newFilters?: Partial<Filters>) => {
			if (newFilters?.experience !== undefined) {
				newFilters.experience = defineExperience(newFilters.experience, true).value;
			}

			onChange({
				...{
					type,
					positions: prepareFilter(positions),
					genres: prepareFilter(genres),
					country,
					city,
					experience: experience.value,
				},
				...newFilters,
			});
		},
		[type, positions, genres, country, city, experience, defineExperience, onChange],
	);

	const applyAndPushRoute = useCallback(
		(newFilters?: Partial<Filters>): void => {
			if (newFilters?.experience !== undefined) {
				newFilters.experience = defineExperience(newFilters.experience, true).value;
			}

			onChange(
				{
					...{
						type,
						positions: prepareFilter(positions),
						genres: prepareFilter(genres),
						country,
						city,
						experience: experience.value,
					},
					...newFilters,
				},
				true,
			);
		},
		[type, positions, genres, country, city, experience, defineExperience, onChange],
	);

	const clear = useCallback(() => {
		const changes: Omit<Filters, 'type'> = {
			positions: {},
			genres: {},
			country: null,
			city: null,
			experience: 0,
		};

		setPositions(changes.positions);
		setGenres(changes.genres);
		setCountry(changes.country);
		setCity(changes.city);
		changeExperience(changes.experience);

		applyAndPushRoute(changes);
	}, [changeExperience, applyAndPushRoute]);

	const applyClick = useCallback(() => {
		applyAndPushRoute();
	}, [applyAndPushRoute]);

	useFiltersQuery(
		{
			type: {
				callback: setType,
				filters: typeFilters,
			},
			positions: {
				callback: setPositions,
				filters: positionFilters,
			},
			genres: {
				callback: setGenres,
				filters: genreFilters,
			},
			country: {
				callback: setCountry,
				filters: countryFilters,
			},
			city: {
				callback: setCity,
				filters: cityFilters,
			},
			experience: {
				callback: changeExperience,
			},
		},
		apply,
		!loading,
	);

	const changePosition = useCallback((value: { [key: string]: string | boolean }): void => {
		setPositions(value);
	}, []);

	const changeType = useCallback(
		(newType: TypeFilter): void => {
			setPositions({});
			setType(newType);
			apply({
				type: newType,
			});
		},
		[apply],
	);

	return (
		<Container className={className}>
			<ButtonGroup>
				<Button onClick={clear} color="secondary">
					{d('search.filters.clear')}
				</Button>
				<Button onClick={applyClick}>{d('search.filters.apply')}</Button>
			</ButtonGroup>
			<Filter type="radio" value={type} title={d('search.filters.type.title')} onChange={changeType}>
				{typeFilters.map((value: string) => (
					<Option key={value} title={d(`search.filters.type.${value}`)} value={value} />
				))}
			</Filter>
			<Dummy loading={loading}>
				{type !== 'repetitionBase' && (
					<Filter
						type="checkbox"
						value={positions}
						variants={
							type === 'band'
								? getEnumKeys(PositionVariant).map((variant) => ({
										value: variant,
										title: d(`positionVariants.${variant}`),
								  }))
								: undefined
						}
						title={d('search.filters.positions.title')}
						onChange={changePosition}
					>
						{positionFilters.map(({ value, group }) => (
							<Option key={value} title={d(`positions.${value}`)} value={value} group={group} />
						))}
					</Filter>
				)}
				<Filter type="checkbox" value={genres} title={d('search.filters.genres.title')} onChange={setGenres}>
					{genreFilters.map(({ value, group }) => (
						<Option key={value} title={d(`genres.${value}`)} value={value} group={group} />
					))}
				</Filter>
				<div>
					<FilterHeader>
						<span>{d('search.filters.country.title')}</span>
					</FilterHeader>
					<Select
						options={countryFilters.map(({ value }) => ({
							value,
							title: d(`countries.${value}`),
						}))}
						value={country}
						onChange={changeCountry}
						placeholder={d('search.filters.country.placeholder')}
					/>
				</div>
				{country !== null && (
					<div>
						<FilterHeader>
							<span>{d('search.filters.city.title')}</span>
						</FilterHeader>
						<Select
							options={cityFilters
								.filter(({ country: { value } }) => value === country)
								.map(({ value }) => ({
									value,
									title: d(`cities.${value}`),
								}))}
							value={city}
							onChange={setCity}
							placeholder={d('search.filters.city.placeholder')}
						/>
					</div>
				)}
				<div>
					<FilterHeader>
						<span>{d('search.filters.experience.title')}</span>
					</FilterHeader>
					<Interval
						partsCount={experienceIntervalParts}
						part={experience.value}
						title={experience.title}
						onChange={changeExperience}
					/>
				</div>
			</Dummy>
		</Container>
	);
};

export default FiltersComponent;
