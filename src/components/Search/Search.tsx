import { appContext } from 'Contexts/app.context';
import Content from 'Components/Search/Content';
import { Filters, OptionsFilters } from 'Types/filters.type';
import FiltersComponent from 'Components/Search/Filters';
import { prepareQuery } from 'Utils/url';
import styled from 'styled-components';
import React, { useCallback, useContext, useState } from 'react';
import { routePush, routeReplace } from 'Components/Link';
import { useQuery } from '@apollo/react-hooks';
import { Response, ResponseBand, ResponseProfile } from 'Types/response.type';
import { getProfilesAndBands } from 'Schemas/Search';
import { PositionVariant } from 'Utils/enum';
import { errorContext } from 'Contexts/error.context';
import {
	CARDS_LIMIT,
	GA_CATEGORY_SEARCH,
	GA_LOAD_DATA,
	GA_LOADING_DATA,
	GA_SEARCH,
	GA_SET_FILTERS,
} from 'Utils/constants';
import { useQueryTiming } from 'Hooks/ga.hook';
import ReactGA from 'react-ga';

const Container = styled.div`
	position: relative;
	top: 0;
	margin: 0 auto 110px auto;
	color: white;
	padding-top: 67px;

	&:after {
		content: '';
		position: absolute;
		top: 42px;
		right: 0;
		width: 500%;
		height: 100%;
		background-color: rgba(25, 31, 33, 0.54);
		background-attachment: fixed;
		border-radius: 4px;
		z-index: 0;
	}
`;

const Wrap = styled.div`
	display: flex;
	position: relative;
	margin: 0 auto;
	width: 100%;
	max-width: ${({ theme }) => theme.other.containerMaxWidth};
	z-index: 1;
`;

const FiltersBody = styled.div`
	position: relative;
	flex: 0.3;
	margin-right: 49px;
`;

const StyledContent = styled(Content)`
	flex: 1;
	font-family: ${({ theme }) => theme.fontFamily.quiet};
	font-weight: 300;
`;

const defaultData = { totalLength: 0, data: [] };
const subscriptionState: {
	profiles: Response<ResponseProfile>;
	bands: Response<ResponseBand>;
} = {
	profiles: defaultData,
	bands: defaultData,
};

const Search: React.FC = () => {
	const { location } = useContext(appContext);
	const { setError } = useContext(errorContext);

	const [initLoaded, setInitLoaded] = useState<boolean>(false);
	const [pageLoading, setPageLoading] = useState<boolean>(false);
	const [filters, setFilters] = useState<Filters>({
		type: 'band',
		positions: {},
		genres: {},
		country: null,
		city: null,
		experience: 0,
	});
	const [search, setSearch] = useState<string>('');
	const { startTime, startTiming, endTiming } = useQueryTiming({
		category: `${GA_CATEGORY_SEARCH} ${filters.type}`,
		variable: GA_LOADING_DATA,
	});

	const variables = {
		profileLimit: filters.type === 'profile' ? CARDS_LIMIT : 0,
		bandLimit: filters.type === 'profile' ? 0 : CARDS_LIMIT,
		visibleData: [],
		options: {
			search,
			positions: Object.entries(filters.positions)
				.filter(([, value]) => (typeof value === 'string' && value in PositionVariant) || typeof value === 'boolean')
				.reduce<{ [key: string]: number | boolean }>(
					(acc, [key, value]) => ({
						...acc,
						[key]: typeof value === 'boolean' ? value : PositionVariant[value as keyof typeof PositionVariant],
					}),
					{},
				),
			genres: filters.genres,
			country: filters.country ?? undefined,
			city: filters.city ?? undefined,
			experience: filters.experience,
		},
		repetitionBase: filters.type === 'repetitionBase',
	};
	const { data, fetchMore, loading } = useQuery<
		{
			profiles: Response<ResponseProfile>;
			bands: Response<ResponseBand>;
		},
		{ profileLimit: number; bandLimit: number; visibleData: string[]; options: OptionsFilters; repetitionBase: boolean }
	>(getProfilesAndBands, {
		variables,
		skip: startTime === 0,
		onCompleted: () => {
			setInitLoaded(true);
			endTiming();
		},
		onError: (error) => {
			setError(error.message);
			endTiming(error.message);
		},
	});

	const profiles = (subscriptionState.profiles = data === undefined ? defaultData : data.profiles);
	const bands = (subscriptionState.bands = data === undefined ? defaultData : data.bands);

	const dataLoad = useCallback(() => {
		if (!pageLoading) {
			const visibleData = [
				...bands.data.map((band: ResponseBand) => band._id),
				...profiles.data.map((profile: ResponseProfile) => profile._id),
			];

			ReactGA.event({
				category: GA_CATEGORY_SEARCH,
				action: GA_LOAD_DATA,
				value: visibleData.length,
			});

			setPageLoading(true);

			void fetchMore({
				variables: {
					...variables,
					visibleData,
				},
				updateQuery: (prev, { fetchMoreResult }) => {
					setTimeout(() => {
						setPageLoading(false);
					}, 0);

					if (fetchMoreResult === undefined) {
						return prev;
					}

					return {
						...fetchMoreResult,
						bands: {
							...fetchMoreResult.bands,
							data: [...prev.bands.data, ...fetchMoreResult.bands.data],
						},
						profiles: {
							...fetchMoreResult.profiles,
							data: [...prev.profiles.data, ...fetchMoreResult.profiles.data],
						},
					};
				},
				// eslint-disable-next-line @typescript-eslint/no-empty-function
			}).catch(() => {});
		}
	}, [fetchMore, variables, profiles, bands, pageLoading]);

	const changeFilters = useCallback(
		async (newFilters: Filters, changeRoute?: boolean) => {
			setFilters(newFilters);

			Object.entries(newFilters).forEach(([key, filter]) => {
				if (
					key !== 'type' &&
					filter !== null &&
					filter !== undefined &&
					((typeof filter === 'number' && filter !== 0) ||
						Object.values(filter).some((value) => value === true || typeof value === 'string'))
				) {
					ReactGA.event({
						category: GA_CATEGORY_SEARCH,
						action: GA_SET_FILTERS,
						label: key,
					});
				}
			});

			startTiming();

			if (changeRoute === true) {
				await routePush({
					...location,
					query: prepareQuery({
						...newFilters,
						search: location.query.search ?? null,
					}),
				});
			}
		},
		[location, startTiming],
	);

	const changeSearch = useCallback(
		async (value: string, changeRoute?: boolean, replace?: boolean) => {
			setSearch(value);

			ReactGA.event({
				category: GA_CATEGORY_SEARCH,
				action: GA_SEARCH,
				label: value,
			});

			const query = location.query;
			if (value !== '') {
				query.search = value;
			} else {
				delete query.search;
			}

			if (changeRoute === true) {
				if (replace === true) {
					await routeReplace({
						...location,
						query,
					});
				} else {
					await routePush({
						...location,
						query,
					});
				}
			}
		},
		[location],
	);

	return (
		<Container>
			<Wrap>
				<FiltersBody>
					<FiltersComponent onChange={changeFilters} />
				</FiltersBody>
				<StyledContent
					data={filters.type === 'profile' ? profiles : bands}
					type={filters.type}
					filters={filters}
					loading={!initLoaded || loading}
					dataLoading={pageLoading}
					onSearchChange={changeSearch}
					onDataLoad={dataLoad}
				/>
			</Wrap>
		</Container>
	);
};

export default Search;
