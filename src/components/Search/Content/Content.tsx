import { d } from 'Utils/dictionary';
import debounce from 'lodash/debounce';
import styled from 'styled-components';
import { Filters, TypeFilter } from 'Types/filters.type';
import { useSearchQuery } from 'Hooks/filters.hook';
import Form, { Input } from 'UI/Form';
import React, { useCallback, useState } from 'react';
import { Response, ResponseBand, ResponseProfile } from 'Types/response.type';
import Card from 'Components/Card';
import { useLayoutEffect } from 'Hooks/effect.hook';
import { Icon, StyledInput } from 'UI/Form/Input';
import Dummy from 'UI/Dummy/Dummy';
import { useGetRef } from 'Hooks/getRef.hook';
import Scroll from 'UI/Scroll';

const SEARCH_ICON = 'Search/search__search_oufnoy';

interface Props {
	type: TypeFilter;
	data: Response<ResponseProfile | ResponseBand>;
	filters: Filters;
	loading: boolean;
	dataLoading: boolean;
	className?: string;
	onSearchChange(value: string, changeRoute?: boolean, replace?: boolean): void;
	onDataLoad(): void;
}

const Container = styled.div`
	position: relative;
`;

const StyledForm = styled(Form)`
	width: 100%;
	margin-bottom: 30px;

	${StyledInput} {
		padding: 18px 46px;
		color: rgba(44, 44, 44, 0.8);
		font-size: 0.8rem;

		::placeholder {
			color: rgba(44, 44, 44, 0.5);
		}
	}

	${Icon} {
		margin: 18px 0 0 18px;
		height: 19px;
	}
`;

const Cards = styled.div`
	display: flex;
	position: relative;
	padding: 12px 2px;
	background-color: #eee;
	border-radius: 3px;
	box-sizing: border-box;
	width: 100%;
`;

const CardsScroll = styled(Scroll)`
	max-height: 703px;
	padding: 0 10px;
`;

const CardsContainer = styled.div`
	content: '';
	position: relative;
	left: 8px;
	top: 0;
	padding: 20px 0 20px;
	width: calc(100% - 16px);
	border: 1px solid #555;
	border-radius: 2px;
	box-sizing: border-box;
	z-index: 1;
`;

const Empty = styled.div`
	text-align: center;
	color: black;
`;

const LoadMore = styled.div`
	cursor: pointer;
	height: 30px;
	color: #555;
	width: 100%;
	text-align: center;
`;

const StyledCard = styled(Card)`
	margin-bottom: 10px;
`;

const StyledDummy = styled(Dummy)`
	height: 300px;
	margin: 0 0 10px 2px;
`;

const Content: React.FC<Props> = ({
	className,
	data: { data, totalLength },
	filters,
	loading,
	onSearchChange,
	onDataLoad,
}) => {
	const [search, setSearch] = useState<string>('');
	const [firstlySearched, setFirstlySearched] = useState<boolean>(false);
	const containerRef = useGetRef<HTMLDivElement>();
	const loadMoreRef = useGetRef<HTMLDivElement>();

	const onSearch = useCallback(
		debounce((value: string) => {
			onSearchChange(value, true, firstlySearched);
			setFirstlySearched(true);
		}, 500),
		[onSearchChange, firstlySearched],
	);

	const changeSearchAndRoute = useCallback(
		(value: string) => {
			setSearch(value);
			onSearch(value);
		},
		[onSearch],
	);

	const changeSearch = useCallback(
		(value: string) => {
			setSearch(value);
			onSearchChange(value);
		},
		[onSearchChange],
	);

	const onBlur = useCallback(() => {
		setFirstlySearched(false);
	}, []);

	const onScroll = useCallback(() => {
		if (typeof window === 'undefined' || containerRef.current === null || loadMoreRef.current === null) {
			return;
		}

		const container = containerRef.current;
		const loadMore = loadMoreRef.current;
		if (container.scrollTop >= container.scrollHeight - container.offsetHeight - loadMore.offsetHeight - 800) {
			onDataLoad();
		}
	}, [containerRef, onDataLoad, loadMoreRef]);

	useLayoutEffect(() => {
		if (typeof window === 'undefined' || containerRef.current === null || loadMoreRef.current === null) {
			return;
		}

		const container = containerRef.current;
		const loadMore = loadMoreRef.current;
		if (
			!loading &&
			container.scrollTop >= container.scrollHeight - container.offsetHeight - loadMore.offsetHeight - 800
		) {
			onScroll();
		}

		container.addEventListener('scroll', onScroll);
		return () => container.removeEventListener('scroll', onScroll);
	}, [containerRef, loading, onScroll, loadMoreRef]);

	useSearchQuery({
		search: {
			callback: changeSearch,
		},
	});

	return (
		<Container className={className}>
			<StyledForm>
				<Input
					name="search"
					onChange={changeSearchAndRoute}
					onBlur={onBlur}
					value={search}
					iconSrc={SEARCH_ICON}
					placeholder={d('search.content.search.placeholder')}
				/>
			</StyledForm>
			<Cards>
				<CardsContainer>
					<CardsScroll getRef={containerRef}>
						{data.length > 0 || loading ? (
							loading ? (
								<>
									<StyledDummy loading={loading} />
									<StyledDummy loading={loading} />
								</>
							) : (
								data.map((collection) => (
									<StyledCard
										key={collection._id}
										login={'login' in collection ? collection.login : undefined}
										alias={'alias' in collection ? collection.alias : undefined}
										avatar={collection.avatar}
										name={collection.name}
										description={collection.description}
										country={collection.country}
										city={collection.city}
										experience={collection.experience}
										positions={collection.positions}
										genres={collection.genres}
										relations={collection.relations}
										filterPositions={filters.positions}
										dignities={collection.dignities}
										repetitionBase={'alias' in collection ? collection.repetitionBase : undefined}
									/>
								))
							)
						) : (
							<Empty>{d('search.content.empty')}</Empty>
						)}
						{data.length < totalLength && !loading && (
							<LoadMore ref={loadMoreRef}>{d('search.content.loadMore')}</LoadMore>
						)}
					</CardsScroll>
				</CardsContainer>
			</Cards>
		</Container>
	);
};

export default Content;
