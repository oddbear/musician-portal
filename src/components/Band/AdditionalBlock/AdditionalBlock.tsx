import React, { MouseEvent, ChangeEvent, useCallback, useContext } from 'react';
import {
	BandPrivacySettings,
	GraphQLWrap,
	ResponseBand,
	ResponseBandStatistics,
	ResponseLink,
} from 'Types/response.type';
import { d } from 'Utils/dictionary';
import Dummy from 'UI/Dummy';
import styled from 'styled-components';
import { loaderContext } from 'Contexts/loader.context';
import { appContext } from 'Contexts/app.context';
import {
	addBandAudio as addAudioMutation,
	removeBandAudio as removeAudioMutation,
	updateBandPrivacySettings as updateBandPrivacySettingsMutation,
} from 'Schemas/Band';
import { errorContext } from 'Contexts/error.context';
import { isExpired } from 'Utils/date';
import { BAND_AUDIO_LIMIT, BAND_AUDIO_LIMIT_PREMIUM, GA_ADD_TO_PLAYLIST, GA_CATEGORY_BAND } from 'Utils/constants';
import Tumbler from 'UI/Tumbler';
import Tablet from 'UI/Tablet/Tablet';
import Audios from 'Components/Audios';
import ReactGA from 'react-ga';
import { routePush } from 'Components/Link';
import { useSecureMutation } from 'Utils/apollo';

const STATISTICS_CHANGE_TASK = 'STATISTICS_CHANGE_TASK';
const AUDIO_UPLOAD_TASK = 'AUDIO_UPLOAD_TASK';

interface Props {
	privacySettings: BandPrivacySettings;
	alias: string;
	audios: ResponseLink[];
	statistics: ResponseBandStatistics;
	loading: boolean;
}

const Container = styled(Tablet)`
	color: ${({ theme }) => theme.color.primary};
	font-family: ${({ theme }) => theme.fontFamily.quiet};
	border-radius: 4px;
	width: 100%;
	background-color: #fff;
	margin-top: 20px;
`;

const Wrap = styled.div`
	box-sizing: border-box;
	padding: 0 25px;
	width: 100%;
`;

const Title = styled.span`
	margin-right: 20px;
	font-size: 1.4rem;
	font-weight: 400;
	font-family: ${({ theme }) => theme.fontFamily.aggressive};
`;

const Label = styled.label`
	display: block;
	cursor: pointer;
	opacity: 0.7;
	font-size: 0.8rem;

	input {
		display: none;
	}
`;

const Flex = styled.div`
	display: flex;
	margin-top: 20px;
	flex-direction: column;
	font-size: 0.8rem;
`;

const Row = styled.div`
	display: flex;
`;

const AudiosInfo = styled.div`
	font-size: 0.65em;
	color: #666;
`;

const AdditionalBlock: React.FC<Props> = ({ alias, audios, statistics, loading, privacySettings }) => {
	const { incrementProgress, clearProgress } = useContext(loaderContext);
	const { setError } = useContext(errorContext);
	const { user } = useContext(appContext);

	const [addAudio] = useSecureMutation<
		GraphQLWrap<'addAudio', ResponseBand>,
		{
			alias: string;
			name: string;
			file: File | Blob;
		}
	>(addAudioMutation);

	const [removeAudio] = useSecureMutation<
		GraphQLWrap<'removeAudio', ResponseBand>,
		{
			alias: string;
			_id: string;
		}
	>(removeAudioMutation);

	const addAudioFile = useCallback(
		async (event: ChangeEvent<HTMLInputElement>) => {
			const target = event.target;

			const files = target.files ?? [];
			const file = files[0];
			if (file === undefined) {
				return;
			}

			incrementProgress(AUDIO_UPLOAD_TASK);
			await addAudio({
				variables: {
					name: file.name,
					alias,
					file,
				},
			}).catch((err: Error) => {
				setError(err.message);
			});
			clearProgress(AUDIO_UPLOAD_TASK);

			// @ts-ignore
			target.value = null;
		},
		[clearProgress, setError, alias, incrementProgress, addAudio],
	);

	const removeAudioFile = useCallback(
		async (_id: string) => {
			await removeAudio({
				variables: {
					alias,
					_id,
				},
			}).catch((err: Error) => {
				setError(err.message);
			});
		},
		[alias, removeAudio, setError],
	);

	const checkAudioLimit = useCallback(
		(event: MouseEvent<HTMLLabelElement>) => {
			const expired = isExpired(user?.extendPack ?? 0);
			if (expired && audios.length >= BAND_AUDIO_LIMIT) {
				setError(d('band.audios.limit.usual'), {
					title: d('card.inShop'),
					action: () => {
						void routePush({
							pathname: '/shop',
							query: {},
						});
					},
				});
				event.preventDefault();
			} else if (!expired && audios.length >= BAND_AUDIO_LIMIT_PREMIUM) {
				setError(d('band.audios.limit.premium'));
				event.preventDefault();
			}
		},
		[audios.length, user, setError],
	);

	const [updateBandPrivacySettings] = useSecureMutation<
		GraphQLWrap<'updateBandPrivacySettings', ResponseBand>,
		{
			alias: string;
			settings: {
				showStatistics: boolean;
			};
		}
	>(updateBandPrivacySettingsMutation, {
		variables: {
			alias,
			settings: {
				showStatistics: !privacySettings.showStatistics,
			},
		},
	});

	const addTrackToPlaylist = useCallback(() => {
		ReactGA.event({
			category: GA_CATEGORY_BAND,
			action: GA_ADD_TO_PLAYLIST,
		});
	}, []);

	const changePrivacy = useCallback(() => {
		void updateBandPrivacySettings().catch((err: Error) => setError(err.message));
	}, [updateBandPrivacySettings, setError]);

	const isBandOwner = user?.administeredBands.some((band) => band.alias === alias) === true;
	const inBand = user?.memberInBands.some((band) => band.alias === alias) === true;

	return (
		<Container>
			<Wrap>
				<Title>{d('band.audios.title')}</Title>
				<Dummy loading={loading}>
					<Audios tracks={audios} onRemove={removeAudioFile} onAddToPlaylist={addTrackToPlaylist} />
					{isBandOwner && (
						<Dummy loading={AUDIO_UPLOAD_TASK} transparent={true}>
							<Label onClick={checkAudioLimit}>
								<AudiosInfo>{d('band.audios.info')}</AudiosInfo>
								<span>{d('band.audios.add')}</span>
								<input type="file" onChange={addAudioFile} accept="audio/mpeg" />
							</Label>
						</Dummy>
					)}
				</Dummy>
				{(inBand || privacySettings.showStatistics) && (
					<Flex>
						<Row>
							<Title>{d('band.statistics.title')}</Title>
							{isBandOwner && (
								<Dummy loading={STATISTICS_CHANGE_TASK} transparent={true}>
									<Tumbler
										on={privacySettings.showStatistics}
										title={d('band.statistics.showStatistics')}
										onClick={changePrivacy}
									/>
								</Dummy>
							)}
						</Row>
						<Dummy loading={loading}>
							<span>
								{d('band.statistics.membersJoined')}: {statistics.membersJoined}
							</span>
						</Dummy>
						<Dummy loading={loading}>
							<span>
								{d('band.statistics.membersInBand')}: {statistics.membersInBand}
							</span>
						</Dummy>
					</Flex>
				)}
			</Wrap>
		</Container>
	);
};

export default AdditionalBlock;
