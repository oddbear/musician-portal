import React, { useCallback, useContext, useEffect } from 'react';
import styled from 'styled-components';
import { useQuery } from '@apollo/react-hooks';
import { getBand, subscribeToBand as subscribeToBandMutation, updateBand } from 'Schemas/Band';
import {
	BandUpdateInput,
	GraphQLWrap,
	ProfileUpdateInput,
	Response,
	ResponseBand,
	ResponseConversation,
	ResponseLink,
} from 'Types/response.type';
import Button from 'UI/Button';
import { getConversations, getOrCreateConversation as getOrCreateConversationMutation } from 'Schemas/Conversation';
import { CONVERSATIONS_QUERY_VARIABLES } from 'Components/Conversations/Conversations';
import { apolloClient } from '../../pages/_app';
import { routePush } from 'Components/Link';
import { appContext } from 'Contexts/app.context';
import Card from 'Components/Card';
import { loaderContext } from 'Contexts/loader.context';
import { errorContext } from 'Contexts/error.context';
import Dummy from 'UI/Dummy/Dummy';
import { d } from 'Utils/dictionary';
import Members from 'Components/Band/Members';
import AdditionalBlock from 'Components/Band/AdditionalBlock';
import { getDefaultAvatars } from 'Schemas/Link';
import { CityFilter, CountryFilter, GenreFilter, PositionFilter } from 'Types/filters.type';
import { getFilters } from 'Schemas/Filters';
import Blog from 'Components/Blog';
import { POSTS_LIMIT } from 'Utils/constants';
import { dummyBandData } from 'Utils/data';
import { useSecureMutation } from 'Utils/apollo';

const SUBSCRIBE_TASK = 'SUBSCRIBE_TASK';

interface Props {
	alias: string;
}

const Container = styled.div`
	position: relative;
	top: 0;
	color: white;
	padding: 40px;

	&:after {
		content: '';
		position: absolute;
		top: 0;
		right: 0;
		width: 100%;
		height: 100%;
		background-color: rgba(25, 31, 33, 0.54);
		background-attachment: fixed;
		border-radius: 4px;
		z-index: 0;
	}
`;

const Wrap = styled.div`
	display: flex;
	flex-direction: column;
	position: relative;
	margin: 0 auto;
	width: 100%;
	max-width: ${({ theme }) => theme.other.containerMaxWidth};
	z-index: 1;
`;

const ButtonContainer = styled.div`
	margin: 0 0 10px 10px;
	width: initial;
	display: flex;
	place-content: flex-end;
`;

const StyledButton = styled(Button)`
	min-width: 200px;
`;

const Flex = styled.div`
	display: flex;
	place-content: space-between;
	margin-top: 20px;
`;

const BoxDummy = styled(Dummy)`
	height: 150px;
	width: 100%;
`;

const Row = styled.div`
	display: flex;
	flex-direction: row;
	place-content: flex-end;
`;

const Band: React.FC<Props> = ({ alias }) => {
	const { data: bandData, loading } = useQuery<GraphQLWrap<'band', ResponseBand>, { alias: string }>(getBand, {
		ssr: false,
		variables: {
			alias,
		},
	});
	const [getOrCreateConversation] = useSecureMutation<
		GraphQLWrap<'getOrCreateConversation', ResponseConversation>,
		{
			participantAlias: string;
		}
	>(getOrCreateConversationMutation);
	const { user, setPage } = useContext(appContext);
	const { clearProgress, incrementProgress } = useContext(loaderContext);
	const { setError } = useContext(errorContext);

	const { data: avatarsData } = useQuery<
		GraphQLWrap<'defaultAvatars', ResponseLink[]>,
		{
			type: string;
		}
	>(getDefaultAvatars, {
		ssr: false,
		variables: {
			type: 'band',
		},
	});
	const defaultAvatars =
		avatarsData?.defaultAvatars !== undefined && avatarsData.defaultAvatars !== null ? avatarsData.defaultAvatars : [];

	const { data: filtersData } = useQuery<{
		cities: CityFilter[];
		countries: CountryFilter[];
		genres: GenreFilter[];
		positions: PositionFilter[];
	}>(getFilters, {
		ssr: false,
	});

	const openConversation = useCallback(() => {
		if (alias !== undefined) {
			void getOrCreateConversation({
				variables: {
					participantAlias: alias,
				},
				update: (cache, { data }) => {
					const newConversation = data?.getOrCreateConversation;
					if (newConversation === undefined) {
						console.error(`can't get or create new conversation with alias '${alias}'`);
						return;
					}

					/* because of conversation's page loaded not immediately and can be absent in cache it needs to write new
					conversation by async way so the only way to update cache is using directly ApolloClient instance */
					const writeInQuery = (cacheObject = cache): void => {
						const oldData = cacheObject.readQuery<GraphQLWrap<'responseConversations', Response<ResponseConversation>>>(
							{
								query: getConversations,
								variables: CONVERSATIONS_QUERY_VARIABLES,
							},
						);

						if (oldData === null || oldData === undefined || oldData.responseConversations === null) {
							return;
						}

						if (oldData.responseConversations.data.every((conversation) => conversation._id !== newConversation._id)) {
							cacheObject.writeQuery<GraphQLWrap<'responseConversations', Response<ResponseConversation>>>({
								query: getConversations,
								variables: CONVERSATIONS_QUERY_VARIABLES,
								data: {
									...oldData,
									responseConversations: {
										...oldData.responseConversations,
										totalLength: oldData.responseConversations.totalLength + 1,
										data: [...oldData.responseConversations.data, newConversation],
									},
								},
							});
						}
					};

					try {
						writeInQuery();
					} catch (err) {
						const waitForQuery = (): void => {
							try {
								if (!location.pathname.includes('conversations')) {
									return;
								}
								writeInQuery(apolloClient);
							} catch (error) {
								setTimeout(waitForQuery, 1000);
							}
						};
						setTimeout(waitForQuery, 1000);
					} finally {
						void routePush(
							{
								pathname: '/conversations',
								query: {
									id: newConversation._id,
								},
							},
							{
								pathname: `/conversations/${newConversation._id}`,
								query: {},
							},
						);
					}
				},
			});
		}
	}, [alias, getOrCreateConversation]);

	const [saveBand] = useSecureMutation<
		GraphQLWrap<'updateBand', ResponseBand>,
		{ alias: string; newValue: BandUpdateInput }
	>(updateBand);

	const onChange = useCallback(
		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		async (propName: keyof BandUpdateInput | keyof ProfileUpdateInput, value: any) => {
			await saveBand({
				variables: {
					alias,
					newValue: {
						[propName]: value,
					},
				},
			}).catch((error: Error) => {
				setError(error.message);
			});

			clearProgress(`card.${propName}`);
		},
		[alias, clearProgress, setError, saveBand],
	);

	const [subscribeToBand] = useSecureMutation<GraphQLWrap<'subscribeToBand', ResponseBand>, { alias: string }>(
		subscribeToBandMutation,
		{
			variables: {
				alias,
			},
		},
	);

	const subscribe = useCallback(async () => {
		incrementProgress(SUBSCRIBE_TASK);
		await subscribeToBand().catch((err: Error) => setError(err.message));
		clearProgress(SUBSCRIBE_TASK);
	}, [subscribeToBand, incrementProgress, clearProgress, setError]);

	const band: ResponseBand = bandData?.band !== undefined ? bandData.band : dummyBandData;
	useEffect(() => {
		if (band.repetitionBase) {
			setPage?.('repetitionBase');
		}
	}, [setPage, band]);

	if (bandData?.band === null) {
		location.href = '/404';
		return null;
	}

	const isMyBand = user?.administeredBands.some((administeredBand) => administeredBand.alias === band.alias) === true;
	const inBand = user?.memberInBands.some((memberBand) => memberBand.alias === band.alias) === true;

	return (
		<Container>
			<Wrap>
				<Row>
					{(loading || inBand || band.repetitionBase) && (
						<ButtonContainer>
							<Dummy loading={loading}>
								<StyledButton onClick={openConversation}>{d('band.conversation')}</StyledButton>
							</Dummy>
						</ButtonContainer>
					)}
					{!loading && !isMyBand && user !== null && (
						<ButtonContainer>
							<Dummy loading={SUBSCRIBE_TASK} transparent={true}>
								<StyledButton onClick={subscribe}>
									{band.isSubscribed ? d('band.unsubscribe') : d('band.subscribe')}
								</StyledButton>
							</Dummy>
						</ButtonContainer>
					)}
				</Row>
				<Card
					inCard={true}
					alias={band.alias}
					members={band.members}
					loading={loading}
					onChange={isMyBand ? onChange : undefined}
					name={band.name}
					description={band.description}
					avatar={band.avatar}
					defaultAvatars={defaultAvatars}
					country={band.country}
					city={band.city}
					experience={band.experience}
					positions={band.positions}
					genres={band.genres}
					filtersData={filtersData}
					relations={band.relations}
					dignities={band.dignities}
					repetitionBase={band.repetitionBase}
				/>
				{!loading && isMyBand && (
					<Flex>
						<Members band={band} />
					</Flex>
				)}
				{loading && (
					<Flex>
						<BoxDummy loading={loading} />
					</Flex>
				)}
				<AdditionalBlock
					alias={band.alias}
					audios={band.audios}
					statistics={band.statistics}
					loading={loading}
					privacySettings={band.settings.privacy}
				/>
				<Blog
					editable={isMyBand}
					postQueryVariables={{
						alias,
						limit: POSTS_LIMIT,
						visibleData: [],
					}}
				/>
			</Wrap>
		</Container>
	);
};

export default Band;
