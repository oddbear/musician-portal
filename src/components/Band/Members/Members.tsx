import { GraphQLWrap, ResponseBand, ResponseProfile } from 'Types/response.type';
import React, { useCallback, useContext, useState } from 'react';
import styled from 'styled-components';
import { d } from 'Utils/dictionary';
import LazyImage from 'Components/LazyImage';
import Link from 'Components/Link';
import Button from 'UI/Button/Button';
import Modal from 'UI/Modal';
import { loaderContext } from 'Contexts/loader.context';
import { errorContext } from 'Contexts/error.context';
import Dummy from 'UI/Dummy/Dummy';
import { dismissMember as dismissMemberMutation } from 'Schemas/Band';
import { appContext } from 'Contexts/app.context';
import Tablet from 'UI/Tablet/Tablet';
import Scroll from 'UI/Scroll';
import ReactGA from 'react-ga';
import { GA_CATEGORY_BAND, GA_DISMISS } from 'Utils/constants';
import { useSecureMutation } from 'Utils/apollo';

const DISMISS_PROFILE_TASK = 'DISMISS_PROFILE_TASK';

interface Props {
	band: ResponseBand;
}

const Container = styled(Tablet)`
	text-align: left;
`;

const Wrap = styled.div`
	box-sizing: border-box;
	padding: 0 25px;
	width: 100%;
`;

const Title = styled.span`
	font-size: 1.4rem;
	font-weight: 400;
	font-family: ${({ theme }) => theme.fontFamily.aggressive};
`;

const Row = styled.div`
	margin: 27px 0 23px 0;
	align-items: center;
	font-size: 0.75rem;
`;

const Line = styled.div`
	position: relative;
	margin-left: 53px;
	border-bottom: 1px solid #bab9b9;
`;

const AdminScroll = styled(Scroll)`
	box-sizing: border-box;
	max-height: 190px;
	overflow: auto;
`;

const StyledLink = styled(Link)`
	display: flex;
	align-items: center;
`;

const Empty = styled.span`
	color: ${({ theme }) => theme.color.primary};
	font-size: 0.7rem;
`;

const Members: React.FC<Props> = ({ band }) => {
	const [confirmOpened, setConfirmOpened] = useState<boolean>(false);
	const [profileToDismiss, setProfileToDismiss] = useState<ResponseProfile | undefined>();

	const { clearProgress, incrementProgress } = useContext(loaderContext);
	const { setError } = useContext(errorContext);

	const [dismissMember] = useSecureMutation<
		GraphQLWrap<'dismissMember', ResponseBand>,
		{
			login: string;
			alias: string;
		}
	>(dismissMemberMutation);

	const openConfirm = useCallback((profile: ResponseProfile) => {
		setConfirmOpened(true);
		setProfileToDismiss(profile);
	}, []);

	const closeConfirm = useCallback(() => {
		setConfirmOpened(false);
	}, []);

	const dismiss = useCallback(async () => {
		setConfirmOpened(false);

		if (profileToDismiss === undefined) {
			return;
		}

		incrementProgress(DISMISS_PROFILE_TASK);

		await dismissMember({
			variables: {
				login: profileToDismiss.login,
				alias: band.alias,
			},
		})
			.then(() => {
				ReactGA.event({
					category: GA_CATEGORY_BAND,
					action: GA_DISMISS,
				});
			})
			.catch((err: Error) => {
				setError(err.message);
			});

		clearProgress(DISMISS_PROFILE_TASK);
	}, [incrementProgress, band.alias, profileToDismiss, dismissMember, setError, clearProgress]);

	return (
		<Container>
			<Modal isOpen={confirmOpened} onClose={closeConfirm}>
				<span>{d('band.members.confirm.message', profileToDismiss?.name ?? '')}</span>
				<div>
					<Button color="simple" onClick={dismiss}>
						{d('band.members.confirm.accept')}
					</Button>
					<Button color="simple" onClick={closeConfirm}>
						{d('band.members.confirm.cancel')}
					</Button>
				</div>
			</Modal>
			<Wrap>
				<Title>{d('band.members.title')}</Title>
				<AdminScroll>
					{band.members.length > 0 ? (
						band.members.map((profile) => (
							<Row key={profile.login}>
								<Profile profile={profile} band={band} onDismiss={openConfirm} />
								<Line />
							</Row>
						))
					) : (
						<Empty>{d('band.members.empty')}</Empty>
					)}
				</AdminScroll>
			</Wrap>
		</Container>
	);
};

export default Members;

interface BandProps {
	band: ResponseBand;
	profile: ResponseProfile;
	onDismiss: (profile: ResponseProfile) => void;
}

const MemberBand = styled.div`
	display: flex;
	place-content: space-between;
	align-items: center;
`;

const Avatar = styled(LazyImage)`
	height: 42px;
	width: 42px;
	margin-right: 10px;
	background-color: white;
`;

const Profile: React.FC<BandProps> = ({ band, profile, onDismiss }) => {
	const { user } = useContext(appContext);

	const dismiss = useCallback(() => {
		void onDismiss(profile);
	}, [onDismiss, profile]);

	return (
		<MemberBand>
			<StyledLink
				location={{
					query: {
						login: profile.login,
					},
					pathname: '/profile',
				}}
				asLocation={{
					pathname: `/profile/${profile.login}`,
				}}
			>
				<Avatar src={profile.avatar.src} alt={`${profile.name}'s avatar`} />
				<span>{profile.name}</span>
			</StyledLink>
			{user !== null &&
				user.login !== profile.login &&
				band.positions.find(({ position }) => position.value === 'owner')?.profile?.login === user.login && (
					<Dummy loading={DISMISS_PROFILE_TASK} transparent={true}>
						<Button color="simple">
							<span onClick={dismiss}>{d('band.members.dismiss')}</span>
						</Button>
					</Dummy>
				)}
		</MemberBand>
	);
};
