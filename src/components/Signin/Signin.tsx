import Button from 'UI/Button';
import { d } from 'Utils/dictionary';
import { ErrorMessage } from 'UI/Form/Input';
import styled from 'styled-components';
import Form, { Input, Title } from 'UI/Form';
import React, { FormEvent, useCallback, useContext, useEffect, useState } from 'react';
import { errorContext } from 'Contexts/error.context';
import { appContext } from 'Contexts/app.context';
import { routePush } from 'Components/Link';
import Dummy from 'UI/Dummy/Dummy';
import { loaderContext } from 'Contexts/loader.context';
import Link from 'Components/Link/Link';
import ReactGA from 'react-ga';
import { GA_CATEGORY_SIGNIN, GA_SUBMIT } from 'Utils/constants';

const SIGNIN_TASK = 'SIGNIN_TASK';
const LOGIN_ICON = 'Sign_up__Sign_in/Icon_login_turdpe';
const PASSWORD_ICON = 'Sign_up__Sign_in/Icon_password_fnkt7a';

interface Props {
	message?: string;
}

const Container = styled.div`
	position: relative;
	top: 0;
	margin: 0 auto;
	color: white;
	width: 100%;
`;

const LowerWrap = styled.div`
	position: relative;
	margin: 0 auto;
	width: 100%;
	max-width: ${({ theme }) => theme.other.containerMaxWidth};
	z-index: 1;
`;

const TITLE_WIDTH = 180;

const Wrap = styled(LowerWrap)`
	padding: ${({ theme }) => theme.other.signPagesPadding};

	&:before,
	&:after {
		content: '';
		position: absolute;
		top: 0;
		height: 100%;
		background-color: rgba(25, 31, 33, 0.54);
		border-radius: 4px;
		z-index: 0;
	}

	&:before {
		left: -100%;
		width: calc(100% + ${TITLE_WIDTH + 30}px);
		border-top-left-radius: 0;
		border-bottom-left-radius: 0;
		margin-right: 100px;
	}

	&:after {
		left: ${TITLE_WIDTH + 120}px;
		width: 100vw;
		border-top-right-radius: 0;
		border-bottom-right-radius: 0;
	}
`;

const Upper = styled.div`
	position: relative;
	z-index: 1;
`;

const Label = styled.label`
	display: flex;
	width: 100%;
	align-items: center;

	&:not(:first-child) {
		margin-top: 42px;
	}
`;

const StyledTitle = styled(Title)`
	display: flex;
	justify-content: center;
	margin: 0 66px 10px 32px;
	width: ${TITLE_WIDTH}px;
`;

const ButtonWrapper = styled.div`
	display: flex;
	width: 100%;
	place-content: flex-end;
	margin-top: 27px;
`;

const Social = styled.span`
	cursor: pointer;
	margin-right: 8px;
`;

const StyledForm = styled(Form)`
	overflow: hidden;
`;

const FlexEnd = styled.div`
	display: flex;
	flex-direction: column;
	align-items: flex-end;
	margin-top: 20px;
	font-family: ${({ theme }) => theme.fontFamily.quiet};
	font-size: 0.8rem;
	text-decoration: underline;
	position: relative;
	z-index: 1;
	color: #ccc;
	transition: color ${({ theme }) => theme.other.defaultTransitionDuration};

	& > * {
		cursor: pointer;

		&:first-child {
			margin-bottom: 4px;
		}

		&:hover {
			color: #aaa;
		}
	}
`;

const Signin: React.FC<Props> = ({ message }) => {
	const [login, setLogin] = useState<string>('');
	const [password, setPassword] = useState<string>('');
	const [error, setError] = useState<string | null>(null);
	const { incrementProgress, clearProgress } = useContext(loaderContext);

	const { setError: setErrorMessage } = useContext(errorContext);
	const { user } = useContext(appContext);

	const apply = useCallback(
		async (event: FormEvent) => {
			event.preventDefault();

			grecaptcha.ready(() => {
				void grecaptcha.execute(config.google.recaptcha.siteKey, { action: 'signin' }).then(async (token) => {
					incrementProgress(SIGNIN_TASK);

					await fetch(`${config.api.url}/login`, {
						method: 'POST',
						headers: {
							'Content-Type': 'application/json',
						},
						credentials: 'include',
						body: JSON.stringify({
							token,
							login,
							password,
						}),
					})
						.then((response) => {
							if (response.status === 200) {
								location.href = '/';
							} else {
								void response.text().then((err) => {
									setErrorMessage(err);
									setError(err);
								});
							}

							ReactGA.event({
								category: GA_CATEGORY_SIGNIN,
								action: GA_SUBMIT,
								label: 'native',
							});
						})
						.catch((err: Error) => {
							setErrorMessage(err.message);
						});

					clearProgress(SIGNIN_TASK);
				});
			});
		},
		[incrementProgress, clearProgress, setErrorMessage, login, password],
	);

	const googleAuth = useCallback(() => {
		ReactGA.event({
			category: GA_SUBMIT,
			action: 'google',
		});

		incrementProgress(SIGNIN_TASK);

		location.href = `${config.api.url}/auth/google`;
	}, [incrementProgress]);

	const vkAuth = useCallback(() => {
		ReactGA.event({
			category: GA_SUBMIT,
			action: 'vk',
		});

		incrementProgress(SIGNIN_TASK);

		location.href = `${config.api.url}/auth/vk`;
	}, [incrementProgress]);

	useEffect(() => {
		if (user !== null) {
			void routePush({
				pathname: '/404',
				query: {},
			});
		}
	}, [user]);

	return (
		<Container>
			{message !== undefined && <p>{message}</p>}
			<StyledForm onSubmit={apply}>
				<Wrap>
					<Upper>
						<Label>
							<StyledTitle>{d('signin.form.login.value')}</StyledTitle>
							<Input
								iconSrc={LOGIN_ICON}
								value={login}
								name="login"
								backgroundType={0}
								validation={{
									required: {
										value: true,
										errorMessage: d('signin.form.login.errorMessage.required'),
									},
								}}
								onChange={setLogin}
							/>
						</Label>
						<Label>
							<StyledTitle>{d('signin.form.password.value')}</StyledTitle>
							<Input
								iconSrc={PASSWORD_ICON}
								type="password"
								value={password}
								name="password"
								backgroundType={1}
								validation={{
									required: {
										value: true,
										errorMessage: d('signin.form.password.errorMessage.required'),
									},
								}}
								onChange={setPassword}
							/>
						</Label>
						{error !== null && <ErrorMessage>{error}</ErrorMessage>}
					</Upper>
					<FlexEnd>
						<Link
							location={{
								pathname: '/restore',
							}}
						>
							{d('signin.forget')}
						</Link>
						<Link
							location={{
								pathname: '/signup',
							}}
						>
							{d('signin.signup')}
						</Link>
					</FlexEnd>
				</Wrap>
				<LowerWrap>
					<ButtonWrapper>
						<Dummy loading={SIGNIN_TASK} transparent={true}>
							<Social onClick={googleAuth}>Google</Social>
							<Social onClick={vkAuth}>VK</Social>
							<Button>{d('signin.form.button')}</Button>
						</Dummy>
					</ButtonWrapper>
				</LowerWrap>
			</StyledForm>
		</Container>
	);
};

export default Signin;
