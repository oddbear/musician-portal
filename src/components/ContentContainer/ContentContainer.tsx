import React from 'react';
import styled from 'styled-components';

interface Props {
	className?: string;
	children: React.ReactNode;
}

const Container = styled.div`
	position: relative;
	width: 100%;
	max-width: ${({ theme }) => theme.other.containerMaxWidth};
	margin: 0 auto;
`;

const ContentContainer: React.FC<Props> = (props) => {
	return (
		<div className={props.className}>
			<Container>{props.children}</Container>
		</div>
	);
};

export default ContentContainer;
