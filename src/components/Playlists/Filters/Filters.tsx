import Button from 'UI/Button';
import { d } from 'Utils/dictionary';
import Select from 'UI/Select';
import styled from 'styled-components';
import { useFiltersQuery } from 'Hooks/filters.hook';
import {
	CityFilter,
	CountryFilter,
	GenreFilter,
	Genres,
	PlaylistFilters,
	PositionFilter,
	Positions,
} from 'Types/filters.type';
import Filter, { Header as FilterHeader, Option } from 'UI/Filter';
import React, { useCallback, useContext, useState } from 'react';
import { useQuery } from '@apollo/react-hooks';
import { getFilters } from 'Schemas/Filters';
import Dummy from 'UI/Dummy/Dummy';
import { errorContext } from 'Contexts/error.context';

interface Props {
	className?: string;
	onChange(filters: PlaylistFilters, changeRoute?: boolean): void;
}

export interface Experience {
	value: number;
	title: string;
}

const Container = styled.div`
	position: relative;
	margin-right: 49px;
`;

const ButtonGroup = styled.div`
	display: flex;
	margin-bottom: 41px;

	*:first-child {
		margin-right: 20px;
	}
`;

interface PrepareFilter<T> {
	[key: string]: T;
}

function prepareFilter<T>(filter: PrepareFilter<T>): PrepareFilter<T> {
	return Object.entries(filter).reduce<PrepareFilter<T>>((acc, [filterKey, filterValue]) => {
		if (typeof filterValue !== 'boolean' || filterValue !== false) {
			acc[filterKey] = filterValue;
		}

		return acc;
	}, {});
}

const FiltersComponent: React.FC<Props> = ({ className, onChange }) => {
	const [positions, setPositions] = useState<Positions>({});
	const [genres, setGenres] = useState<Genres>({});
	const [country, setCountry] = useState<string | null>(null);
	const [city, setCity] = useState<string | null>(null);
	const { setError } = useContext(errorContext);

	// eslint-disable-next-line prefer-const
	let { data, loading } = useQuery<{
		cities: CityFilter[];
		countries: CountryFilter[];
		genres: GenreFilter[];
		positions: PositionFilter[];
	}>(getFilters, {
		ssr: true,
		onError: (error) => {
			setError(error.message);
		},
	});

	if (data === undefined) {
		data = {
			cities: [],
			countries: [],
			genres: [],
			positions: [],
		};
	}

	const { cities: cityFilters, countries: countryFilters, genres: genreFilters, positions: positionFilters } = data;

	const changeCountry = (value: string): void => {
		setCity(null);
		setCountry(value);
	};

	const apply = useCallback(
		(newFilters?: Partial<PlaylistFilters>) => {
			onChange({
				...{
					positions: prepareFilter(positions),
					genres: prepareFilter(genres),
					country,
					city,
				},
				...newFilters,
			});
		},
		[positions, genres, country, city, onChange],
	);

	const applyAndPushRoute = useCallback(
		(newFilters?: Partial<PlaylistFilters>): void => {
			onChange(
				{
					...{
						positions: prepareFilter(positions),
						genres: prepareFilter(genres),
						country,
						city,
					},
					...newFilters,
				},
				true,
			);
		},
		[positions, genres, country, city, onChange],
	);

	const clear = useCallback(() => {
		const changes: Omit<PlaylistFilters, 'type'> = {
			positions: {},
			genres: {},
			country: null,
			city: null,
		};

		setPositions(changes.positions);
		setGenres(changes.genres);
		setCountry(changes.country);
		setCity(changes.city);

		applyAndPushRoute(changes);
	}, [applyAndPushRoute]);

	const applyClick = useCallback(() => {
		applyAndPushRoute();
	}, [applyAndPushRoute]);

	useFiltersQuery(
		{
			positions: {
				callback: setPositions,
				filters: positionFilters,
			},
			genres: {
				callback: setGenres,
				filters: genreFilters,
			},
			country: {
				callback: setCountry,
				filters: countryFilters,
			},
			city: {
				callback: setCity,
				filters: cityFilters,
			},
		},
		apply,
		!loading,
	);

	const changePosition = useCallback((value: { [key: string]: string | boolean }): void => {
		setPositions(value);
	}, []);

	return (
		<Container className={className}>
			<ButtonGroup>
				<Button onClick={clear} color="secondary">
					{d('playlists.filters.clear')}
				</Button>
				<Button onClick={applyClick}>{d('playlists.filters.apply')}</Button>
			</ButtonGroup>
			<Dummy loading={loading}>
				<Filter
					type="checkbox"
					value={positions}
					title={d('playlists.filters.positions.title')}
					onChange={changePosition}
				>
					{positionFilters.map(({ value, group }) => (
						<Option key={value} title={d(`positions.${value}`)} value={value} group={group} />
					))}
				</Filter>
				<Filter type="checkbox" value={genres} title={d('playlists.filters.genres.title')} onChange={setGenres}>
					{genreFilters.map(({ value, group }) => (
						<Option key={value} title={d(`genres.${value}`)} value={value} group={group} />
					))}
				</Filter>
				<div>
					<FilterHeader>
						<span>{d('playlists.filters.country.title')}</span>
					</FilterHeader>
					<Select
						options={countryFilters.map(({ value }) => ({
							value,
							title: d(`countries.${value}`),
						}))}
						value={country}
						onChange={changeCountry}
						placeholder={d('playlists.filters.country.placeholder')}
					/>
				</div>
				{country !== null && (
					<div>
						<FilterHeader>
							<span>{d('playlists.filters.city.title')}</span>
						</FilterHeader>
						<Select
							options={cityFilters
								.filter(({ country: { value } }) => value === country)
								.map(({ value }) => ({
									value,
									title: d(`cities.${value}`),
								}))}
							value={city}
							onChange={setCity}
							placeholder={d('playlists.filters.city.placeholder')}
						/>
					</div>
				)}
			</Dummy>
		</Container>
	);
};

export default FiltersComponent;
