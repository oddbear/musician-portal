import PlaylistCard from 'Components/PlaylistCard';
import React, { useContext, useState } from 'react';
import { useCallback } from 'react';
import styled from 'styled-components';
import { GraphQLWrap, Response, ResponsePlaylist } from 'Types/response.type';
import Dummy from 'UI/Dummy';
import { d } from 'Utils/dictionary';
import { addPlaylistEmotion as addPlaylistEmotionMutation } from 'Schemas/Emotion';
import { errorContext } from 'Contexts/error.context';
import Form, { Input } from 'UI/Form';
import { StyledInput, Icon } from 'UI/Form/Input';
import debounce from 'lodash/debounce';
import { useGetRef } from 'Hooks/getRef.hook';
import Scroll from 'UI/Scroll';
import { useSearchQuery } from 'Hooks/filters.hook';
import { useSecureMutation } from 'Utils/apollo';

const SEARCH_ICON = 'Search/search__search_oufnoy';

interface Props {
	playlists: Response<ResponsePlaylist>;
	loading: boolean;
	onDataLoad: () => void;
	onSearchChange(value: string, changeRoute?: boolean, replace?: boolean): void;
}

const Container = styled.div`
	width: 100%;
`;

const StyledForm = styled(Form)`
	width: 100%;
	margin-bottom: 30px;

	${StyledInput} {
		padding: 18px 46px;
		color: rgba(44, 44, 44, 0.8);
		font-size: 0.8rem;

		::placeholder {
			color: rgba(44, 44, 44, 0.5);
		}
	}

	${Icon} {
		margin: 18px 0 0 18px;
		height: 19px;
	}
`;

const PlaylistWrap = styled.div`
	display: flex;
	position: relative;
	padding: 12px 2px;
	background-color: #eee;
	border-radius: 3px;
	box-sizing: border-box;
	width: 100%;
`;

const PlaylistContainer = styled.div`
	content: '';
	position: relative;
	left: 8px;
	top: 0;
	padding: 20px 0 20px;
	width: calc(100% - 16px);
	border: 1px solid #555;
	border-radius: 2px;
	box-sizing: border-box;
	z-index: 1;
`;

const PlaylistScroll = styled(Scroll)`
	max-height: 703px;
	padding: 0 10px;
`;

const Empty = styled.div`
	display: flex;
	width: 100%;
	height: 200px;
	place-content: center;
	align-items: center;
	color: black;
`;

const StyledPlaylistCard = styled(PlaylistCard)`
	&:not(:first-child) {
		margin-top: 20px;
	}
`;

const StyledDummy = styled(Dummy)`
	height: 300px;
	margin: 0 0 10px 2px;
`;

const LoadMore = styled.div`
	cursor: pointer;
	height: 30px;
	color: #555;
	width: 100%;
	text-align: center;
`;

const Content: React.FC<Props> = ({ playlists, loading, onSearchChange }) => {
	const [search, setSearch] = useState<string>('');
	const [firstlySearched, setFirstlySearched] = useState<boolean>(false);
	const containerRef = useGetRef<HTMLDivElement>();
	const loadMoreRef = useGetRef<HTMLDivElement>();

	const { setError } = useContext(errorContext);

	const [addPlaylistEmotion] = useSecureMutation<
		GraphQLWrap<'addPlaylistEmotion', ResponsePlaylist>,
		{
			playlistId: string;
			alias: string;
		}
	>(addPlaylistEmotionMutation);

	const onAddEmotion = useCallback(
		async (alias: string, playlistId: string) => {
			await addPlaylistEmotion({
				variables: {
					alias,
					playlistId,
				},
			}).catch((err: Error) => setError(err.message));
		},
		[addPlaylistEmotion, setError],
	);

	const onSearch = useCallback(
		debounce((value: string) => {
			onSearchChange(value, true, firstlySearched);
			setFirstlySearched(true);
		}, 500),
		[onSearchChange, firstlySearched],
	);

	const changeSearchAndRoute = useCallback(
		(value: string) => {
			setSearch(value);
			onSearch(value);
		},
		[onSearch],
	);

	const changeSearch = useCallback(
		(value: string) => {
			setSearch(value);
			onSearchChange(value);
		},
		[onSearchChange],
	);

	const onBlur = useCallback(() => {
		setFirstlySearched(false);
	}, []);

	useSearchQuery({
		search: {
			callback: changeSearch,
		},
	});

	return (
		<Container>
			<StyledForm>
				<Input
					name="search"
					onChange={changeSearchAndRoute}
					onBlur={onBlur}
					value={search}
					iconSrc={SEARCH_ICON}
					placeholder={d('playlists.content.search.placeholder')}
				/>
			</StyledForm>
			<PlaylistWrap>
				<PlaylistContainer>
					<PlaylistScroll getRef={containerRef}>
						{playlists.totalLength === 0 ? (
							<Empty>{d('playlists.empty')}</Empty>
						) : loading ? (
							<div>
								<StyledDummy loading={loading} />
								<StyledDummy loading={loading} />
							</div>
						) : (
							<div>
								{playlists.data.map((playlist) => (
									<StyledPlaylistCard
										key={playlist._id}
										playlist={playlist}
										loading={loading}
										onAddEmotion={onAddEmotion}
									/>
								))}
							</div>
						)}
						{playlists.data.length < playlists.totalLength && !loading && (
							<LoadMore ref={loadMoreRef}>{d('playlists.content.loadMore')}</LoadMore>
						)}
					</PlaylistScroll>
				</PlaylistContainer>
			</PlaylistWrap>
		</Container>
	);
};

export default Content;
