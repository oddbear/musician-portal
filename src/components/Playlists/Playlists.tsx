import React, { MouseEvent, useCallback, useContext, useEffect, useState } from 'react';
import styled from 'styled-components';
import Tabs from 'UI/Tabs';
import { d } from 'Utils/dictionary';
import { useQuery } from '@apollo/react-hooks';
import { useSecureMutation } from 'Utils/apollo';
import { createPlaylist as createPlaylistMutation, getPlaylists } from 'Schemas/Playlist';
import { GraphQLWrap, Response, ResponseEmotion, ResponsePlaylist, ResponseUser } from 'Types/response.type';
import {
	GA_CATEGORY_PLAYLISTS,
	GA_CREATE,
	GA_LOADING_DATA,
	GA_LOAD_DATA,
	GA_SEARCH,
	GA_SET_FILTERS,
	PLAYLISTS_LIMIT,
} from 'Utils/constants';
import Dummy from 'UI/Dummy/Dummy';
import { PlaylistPublicity, PositionVariant } from 'Utils/enum';
import Modal from 'UI/Modal';
import { Input } from 'UI/Form';
import Form from 'UI/Form/Form';
import Button from 'UI/Button/Button';
import { loaderContext } from 'Contexts/loader.context';
import { errorContext } from 'Contexts/error.context';
import { tooltipContext } from 'Contexts/tooltip.context';
import { getUser } from 'Schemas/User';
import { addPlaylistEmotion as addPlaylistEmotionMutation, getEmotions } from 'Schemas/Emotion';
import PlaylistTooltip from 'Components/PlaylistCard/PlaylistTooltip';
import { PlaylistTooltipContext } from 'Components/PlaylistCard/PlaylistTooltip/PlaylistTooltip';
import { parsePreparedQuery, prepareQuery } from 'Utils/url';
import { parseUrl } from 'Utils/url-resolver';
import { routePush, routeReplace } from 'Components/Link';
import { usePrevious } from 'Hooks/previous.hook';
import ReactGA from 'react-ga';
import { appContext } from 'Contexts/app.context';
import Filters from './Filters';
import Content from './Content';
import { OptionsFilters, PlaylistFilters } from 'Types/filters.type';
import { useQueryTiming } from 'Hooks/ga.hook';

const PLAYLIST_CREATE_TASK = 'PLAYLIST_CREATE_TASK';

const Container = styled.div`
	position: relative;
	top: 0;
	color: white;
	padding: 40px;
	font-size: 1rem;

	&:after {
		content: '';
		position: absolute;
		top: 0;
		right: 0;
		width: 100%;
		height: 100%;
		background-color: rgba(25, 31, 33, 0.54);
		background-attachment: fixed;
		border-radius: 4px;
		z-index: 0;
	}
`;

const Wrap = styled.div`
	display: flex;
	flex-direction: column;
	position: relative;
	margin: 0 auto;
	width: 100%;
	max-width: ${({ theme }) => theme.other.containerMaxWidth};
	z-index: 1;
`;

const AddPlaylist = styled.div`
	cursor: pointer;
	font-size: 0.8em;
	color: white;
	margin-bottom: 20px;
	font-family: ${({ theme }) => theme.fontFamily.quiet};
`;

const ResponseBody = styled.div`
	display: flex;
	flex-direction: row;
`;

const Playlists: React.FC = () => {
	const [playlistModal, setPlaylistModal] = useState<boolean>(false);
	const [playlistName, setPlaylistName] = useState<string>('');
	const [attachedElement, setAttachedElement] = useState<HTMLElement | null>(null);
	const [visible, setVisible] = useState<boolean>(false);
	const [context, setContext] = useState<PlaylistTooltipContext>({});
	const [search, setSearch] = useState<string>('');
	const [pageLoading, setPageLoading] = useState<boolean>(false);
	const [initLoaded, setInitLoaded] = useState<boolean>(false);
	const [filters, setFilters] = useState<PlaylistFilters>({
		positions: {},
		genres: {},
		country: null,
		city: null,
	});
	const { startTime, startTiming, endTiming } = useQueryTiming({
		category: GA_CATEGORY_PLAYLISTS,
		variable: GA_LOADING_DATA,
	});

	const { incrementProgress, clearProgress } = useContext(loaderContext);
	const { setError } = useContext(errorContext);
	const { user, loading: userLoading, location } = useContext(appContext);

	const accessTitles = [
		{
			title: d('playlists.tabs.my.title'),
			value: 'my',
			disabled: !userLoading && user === null,
		},
		{
			title: d('playlists.tabs.all'),
			value: 'all',
		},
	];

	const publicityTitles = [
		{
			title: d('playlists.tabs.my.tabs.private'),
			value: PlaylistPublicity.private,
		},
		{
			title: d('playlists.tabs.my.tabs.public'),
			value: PlaylistPublicity.public,
		},
	];

	const [accessTab, setAccessTab] = useState<string>(accessTitles[1].value);
	const [publicityTab, setPublicityTab] = useState<PlaylistPublicity>(PlaylistPublicity.private);

	const changeAccessTab = useCallback(
		(value: string) => {
			void routePush({
				pathname: '/playlists',
				query: {
					access: value,
					publicity: publicityTab.toString(),
				},
			});
			setAccessTab(value);
		},
		[publicityTab],
	);

	const changePublicityTab = useCallback(
		(value: PlaylistPublicity) => {
			void routePush({
				pathname: '/playlists',
				query: {
					access: accessTab,
					publicity: value.toString(),
				},
			});
			setPublicityTab(value);
		},
		[accessTab],
	);

	const href = typeof window === 'undefined' ? '' : window.location.href;
	const prevLoading = usePrevious(userLoading);
	useEffect(() => {
		if (prevLoading !== userLoading && !userLoading) {
			const query = parsePreparedQuery(parseUrl(href).query);
			const matchedAccesssTab = accessTitles.find(
				({ value }) => (user !== null || value !== 'my') && value === query.access,
			)?.value;
			if (matchedAccesssTab !== undefined) {
				setAccessTab(matchedAccesssTab);
			}
			setPublicityTab(
				publicityTitles.find(({ value }) => value === query.publicity)?.value ?? publicityTitles[0].value,
			);
		}
	}, [prevLoading, accessTitles, publicityTitles, href, user, userLoading]);

	const variables = {
		visibleData: [],
		options: {
			search,
			positions: Object.entries(filters.positions)
				.filter(([, value]) => (typeof value === 'string' && value in PositionVariant) || typeof value === 'boolean')
				.reduce<{ [key: string]: number | boolean }>(
					(acc, [key, value]) => ({
						...acc,
						[key]: typeof value === 'boolean' ? value : PositionVariant[value as keyof typeof PositionVariant],
					}),
					{},
				),
			genres: filters.genres,
			country: filters.country ?? undefined,
			city: filters.city ?? undefined,
		},
		limit: PLAYLISTS_LIMIT,
		isMy: accessTab === 'my',
		publicity: accessTab === 'my' ? publicityTab : PlaylistPublicity.public,
	};
	const { data: playlistsData, loading, fetchMore } = useQuery<
		GraphQLWrap<'responsePlaylists', Response<ResponsePlaylist>>,
		{
			visibleData: string[];
			limit: number;
			isMy: boolean;
			publicity: PlaylistPublicity;
			options: Omit<OptionsFilters, 'experience'>;
		}
	>(getPlaylists, {
		skip: startTime === 0,
		ssr: false,
		variables,
		onCompleted: () => {
			setInitLoaded(true);
			endTiming();
		},
		onError: (err: Error) => {
			setError(err.message);
			endTiming(err.message);
		},
	});

	const playlists =
		playlistsData !== undefined
			? playlistsData.responsePlaylists
			: {
					totalLength: 0,
					data: [],
			  };

	const dataLoad = useCallback(() => {
		if (!pageLoading) {
			const visibleData = playlists.data.map(({ _id }) => _id);

			ReactGA.event({
				category: GA_CATEGORY_PLAYLISTS,
				action: GA_LOAD_DATA,
				value: visibleData.length,
			});

			setPageLoading(true);

			void fetchMore({
				variables: {
					...variables,
					visibleData,
				},
				updateQuery: (prev, { fetchMoreResult }) => {
					setTimeout(() => {
						setPageLoading(false);
					}, 0);

					if (fetchMoreResult === undefined) {
						return prev;
					}

					return {
						responsePlaylists: {
							...prev.responsePlaylists,
							totalLength: fetchMoreResult.responsePlaylists.totalLength,
							data: [...prev.responsePlaylists.data, ...fetchMoreResult.responsePlaylists.data],
						},
					};
				},
				// eslint-disable-next-line @typescript-eslint/no-empty-function
			}).catch(() => {});
		}
	}, [fetchMore, variables, playlists, pageLoading]);

	const changePlaylistModal = useCallback(
		(value: boolean) => (event?: MouseEvent<HTMLElement>) => {
			if (event !== undefined) {
				event.preventDefault();
			}
			setPlaylistModal(value);
		},
		[],
	);

	const [createPlaylist] = useSecureMutation<
		GraphQLWrap<'createPlaylist', ResponsePlaylist>,
		{
			name: string;
		}
	>(createPlaylistMutation, {
		variables: {
			name: playlistName,
		},
		update: (cache, { data }) => {
			if (data === null || data === undefined) {
				return;
			}

			const oldUserData = cache.readQuery<GraphQLWrap<'user', ResponseUser>>({
				query: getUser,
			});
			if (oldUserData !== null) {
				cache.writeQuery<GraphQLWrap<'user', ResponseUser>>({
					query: getUser,
					data: {
						user: {
							...oldUserData.user,
							playlists: [data.createPlaylist, ...oldUserData.user.playlists],
						},
					},
				});
			}

			void routePush(
				{
					pathname: '/playlist',
					query: {
						id: data.createPlaylist._id,
					},
				},
				{
					pathname: `/playlist/${data.createPlaylist._id}`,
					query: {},
				},
			);
		},
	});

	const submitPlaylistCreation = useCallback(async () => {
		incrementProgress(PLAYLIST_CREATE_TASK);

		await createPlaylist()
			.then(() => {
				ReactGA.event({
					category: GA_CATEGORY_PLAYLISTS,
					action: GA_CREATE,
				});
			})
			.catch((err: Error) => setError(err.message));

		clearProgress(PLAYLIST_CREATE_TASK);
		setPlaylistModal(false);
		setPlaylistName('');
	}, [incrementProgress, clearProgress, createPlaylist, setError]);

	const changeAttachedElement = useCallback(
		(element: HTMLElement | null) => {
			setAttachedElement(element);
			setVisible(element === attachedElement ? !visible : true);
		},
		[visible, attachedElement, setVisible],
	);

	const { data: emotionsData } = useQuery<GraphQLWrap<'emotions', ResponseEmotion[]>>(getEmotions);
	const emotions = emotionsData?.emotions ?? [];

	const [addPlaylistEmotion] = useSecureMutation<
		GraphQLWrap<'addPlaylistEmotion', ResponsePlaylist>,
		{
			playlistId: string;
			alias: string;
		}
	>(addPlaylistEmotionMutation);

	const onAddEmotion = useCallback(
		async (alias: string, playlistId: string) => {
			await addPlaylistEmotion({
				variables: {
					alias,
					playlistId,
				},
			}).catch((err: Error) => setError(err.message));
		},
		[addPlaylistEmotion, setError],
	);

	const changeFilters = useCallback(
		async (newFilters: PlaylistFilters, changeRoute?: boolean) => {
			setFilters(newFilters);

			Object.entries(newFilters).forEach(([key, filter]) => {
				if (
					key !== 'type' &&
					filter !== null &&
					filter !== undefined &&
					((typeof filter === 'number' && filter !== 0) ||
						Object.values(filter).some((value) => value === true || typeof value === 'string'))
				) {
					ReactGA.event({
						category: GA_CATEGORY_PLAYLISTS,
						action: GA_SET_FILTERS,
						label: key,
					});
				}
			});

			startTiming();

			if (changeRoute === true) {
				await routePush({
					...location,
					query: prepareQuery({
						...newFilters,
						search: location.query.search ?? null,
					}),
				});
			}
		},
		[location, startTiming],
	);

	const changeSearch = useCallback(
		async (value: string, changeRoute?: boolean, replace?: boolean) => {
			setSearch(value);

			ReactGA.event({
				category: GA_CATEGORY_PLAYLISTS,
				action: GA_SEARCH,
				label: value,
			});

			const query = location.query;
			if (value !== '') {
				query.search = value;
			} else {
				delete query.search;
			}

			if (changeRoute === true) {
				if (replace === true) {
					await routeReplace({
						...location,
						query,
					});
				} else {
					await routePush({
						...location,
						query,
					});
				}
			}
		},
		[location],
	);

	return (
		<tooltipContext.Provider
			value={{
				attachedElement,
				setAttachedElement: changeAttachedElement,
				visible,
				setVisible,
				setContext,
			}}
		>
			<PlaylistTooltip context={context} emotions={emotions} onAddEmotion={onAddEmotion} />
			<Container>
				<Modal isOpen={playlistModal} onClose={changePlaylistModal(false)}>
					<Form onSubmit={submitPlaylistCreation}>
						<span>{d('playlists.modal.title')}</span>
						<Input
							name="playlistName"
							placeholder={d('playlists.modal.placeholder')}
							value={playlistName}
							onChange={setPlaylistName}
						/>
						<Dummy loading={PLAYLIST_CREATE_TASK} transparent={true}>
							<Button color="simple" onClick={changePlaylistModal(false)}>
								{d('playlists.modal.cancel')}
							</Button>
							<Button color="simple">{d('playlists.modal.accept')}</Button>
						</Dummy>
					</Form>
				</Modal>
				<Wrap>
					<Tabs<string> titles={accessTitles} tab={accessTab} setTab={changeAccessTab} />
					{accessTab === 'my' && (
						<Tabs<PlaylistPublicity> titles={publicityTitles} tab={publicityTab} setTab={changePublicityTab} />
					)}
					{accessTab === 'my' && publicityTab === PlaylistPublicity.private && (
						<AddPlaylist onClick={changePlaylistModal(true)}>{d('playlists.add')}</AddPlaylist>
					)}
					<ResponseBody>
						<Filters onChange={changeFilters} />
						<Content
							playlists={playlists}
							loading={!initLoaded || loading}
							onDataLoad={dataLoad}
							onSearchChange={changeSearch}
						/>
					</ResponseBody>
				</Wrap>
			</Container>
		</tooltipContext.Provider>
	);
};

export default Playlists;
