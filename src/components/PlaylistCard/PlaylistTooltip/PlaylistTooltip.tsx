import React, { useCallback, useContext, useEffect } from 'react';
import Emotions from 'UI/Emotions';
import EmotionsBarTooltip from 'UI/EmotionsBar/EmotionsBarTooltip';
import styled from 'styled-components';
import Tooltip from 'UI/Tooltip';
import { tooltipContext } from 'Contexts/tooltip.context';
import {
	GraphQLWrap,
	Response,
	ResponseEmotion,
	ResponsePlaylist,
	ResponseProfile,
	ResponseReaction,
} from 'Types/response.type';
import { d } from 'Utils/dictionary';
import Link, { routePush } from 'Components/Link/Link';
import { appContext } from 'Contexts/app.context';
import { errorContext } from 'Contexts/error.context';
import {
	addPlaylistToCollection as addPlaylistToCollectionMutation,
	getPlaylists,
	removePlaylist as removePlaylistMutation,
} from 'Schemas/Playlist';
import { PlaylistPublicity } from 'Utils/enum';
import { PLAYLISTS_LIMIT } from 'Utils/constants';
import { useSecureMutation } from 'Utils/apollo';

interface Props {
	context: PlaylistTooltipContext;
	emotions: ResponseEmotion[];
	onAddEmotion: (emotionAlias: string, playlistId: string) => void;
}

export interface PlaylistTooltipContext {
	emotionsInfo?: {
		alias: string;
		title: string;
		authors: Response<ResponseProfile>;
	};
	emotions?: {
		_id: string;
		dignities: string[];
		reactions: ResponseReaction[];
	};
	playlist?: ResponsePlaylist;
	inCard?: boolean;
}

const StyledTooltip = styled(Tooltip)`
	padding: 0;
`;

const List = styled.ul`
	padding: 0;
	font-family: ${({ theme }) => theme.fontFamily.quiet};
	font-size: 0.7rem;
	color: #666;

	li {
		display: flex;
		align-items: center;
		cursor: pointer;
		padding: 4px 20px;
		text-align: left;
		list-style-type: none;
		white-space: nowrap;

		&:hover {
			color: #333;
		}

		&:not(:last-child) {
			border-bottom: 1px solid #ccc;
		}
	}
`;

let closePrevent = false;

const PlaylistTooltip: React.FC<Props> = ({ context, emotions, onAddEmotion }) => {
	const { setVisible } = useContext(tooltipContext);
	const { user } = useContext(appContext);
	const { setError } = useContext(errorContext);

	const prevent = useCallback(() => {
		closePrevent = true;
	}, []);

	useEffect(() => {
		const mouseDown = (): void => {
			if (!closePrevent) {
				setVisible?.(false);
			}
			closePrevent = false;
		};

		window.addEventListener('mousedown', mouseDown);
		return () => window.removeEventListener('mousedown', mouseDown);
	}, [setVisible]);

	const changeVisible = useCallback(
		(value: boolean) => () => {
			setVisible?.(value);
		},
		[setVisible],
	);

	const addEmotion = useCallback(
		async (emotionAlias: string) => {
			if (context.emotions !== undefined) {
				onAddEmotion(emotionAlias, context.emotions._id);
			}
		},
		[context.emotions, onAddEmotion],
	);

	const copyLink = useCallback(() => {
		setVisible?.(false);

		if (context.playlist === undefined) {
			return;
		}

		if (navigator.clipboard === undefined) {
			setError(d('playlistCard.clipboard.noFeature'));
			return;
		}
		navigator.clipboard
			.writeText(`${location.protocol}//${location.host}/playlist/${context.playlist._id}`)
			.catch((err: Error) => setError(d('playlistCard.clipboard', err.message)));
	}, [setVisible, context, setError]);

	const [removePlaylist] = useSecureMutation<
		GraphQLWrap<'removePlaylist', ResponsePlaylist>,
		{
			_id: string;
		}
	>(removePlaylistMutation);

	const removePlaylistAction = useCallback(async () => {
		if (context.playlist !== undefined) {
			await removePlaylist({
				variables: {
					_id: context.playlist._id,
				},
			}).catch((err: Error) => setError(err.message));
			setVisible?.(false);
			void routePush({
				pathname: '/playlists',
				query: {
					access: 'my',
					publicity: 'true',
				},
			});
		}
	}, [setVisible, context.playlist, removePlaylist, setError]);

	const [addPlaylistToCollection] = useSecureMutation<
		GraphQLWrap<'addPlaylistToCollection', ResponsePlaylist>,
		{
			_id: string;
		}
	>(addPlaylistToCollectionMutation);

	const addPlaylistToCollectionAction = useCallback(async () => {
		if (context.playlist !== undefined) {
			await addPlaylistToCollection({
				variables: {
					_id: context.playlist._id,
				},
				update: (cache, { data }) => {
					if (data === null || data === undefined) {
						return;
					}

					const isRemove = context.playlist!.isFollowed;

					const variables = {
						visibleData: [],
						limit: PLAYLISTS_LIMIT,
						isMy: true,
						publicity: PlaylistPublicity.public,
					};
					const oldPlayersData = cache.readQuery<
						GraphQLWrap<'responsePlaylists', Response<ResponsePlaylist>>,
						{
							visibleData: string[];
							limit: number;
							isMy: boolean;
							publicity: PlaylistPublicity;
						}
					>({
						query: getPlaylists,
						variables,
					});
					if (oldPlayersData !== null) {
						cache.writeQuery<
							GraphQLWrap<'responsePlaylists', Response<ResponsePlaylist>>,
							{
								visibleData: string[];
								limit: number;
								isMy: boolean;
								publicity: PlaylistPublicity;
							}
						>({
							query: getPlaylists,
							variables,
							data: {
								responsePlaylists: {
									...oldPlayersData.responsePlaylists,
									totalLength: oldPlayersData.responsePlaylists.totalLength + (isRemove ? -1 : 1),
									data: isRemove
										? oldPlayersData.responsePlaylists.data.filter((playlist) => playlist._id !== context.playlist!._id)
										: [data.addPlaylistToCollection, ...oldPlayersData.responsePlaylists.data],
								},
							},
						});
					}
				},
			}).catch((err: Error) => setError(err.message));
			setVisible?.(false);
		}
	}, [setVisible, context.playlist, addPlaylistToCollection, setError]);

	if (context.emotions !== undefined) {
		return (
			<StyledTooltip position="top" pointer={false} onMouseDown={prevent}>
				<Emotions
					emotions={emotions.filter((emotion) =>
						context.emotions!.reactions.every((reaction) => reaction.emotion.alias !== emotion.alias),
					)}
					addEmotion={addEmotion}
				/>
			</StyledTooltip>
		);
	} else if (context.emotionsInfo !== undefined) {
		return (
			<StyledTooltip
				position="top"
				pointer={false}
				onMouseDown={prevent}
				onMouseEnter={changeVisible(true)}
				onMouseLeave={changeVisible(false)}
			>
				<EmotionsBarTooltip emotionsInfo={context.emotionsInfo} />
			</StyledTooltip>
		);
	} else if (context.playlist !== undefined) {
		return (
			<StyledTooltip position="bottom" pointer={false} onMouseDown={prevent}>
				<List>
					{user !== null && context.playlist.publicity === PlaylistPublicity.public && (
						<li onClick={addPlaylistToCollectionAction}>
							{d(
								context.playlist.isFollowed
									? 'playlistCard.tooltip.removePlaylistFromCollection'
									: 'playlistCard.tooltip.addPlaylistToCollection',
							)}
						</li>
					)}
					{context.playlist.author.login === user?.login && context.inCard && (
						<li onClick={removePlaylistAction}>{d('playlistCard.tooltip.remove')}</li>
					)}
					<li onClick={changeVisible(false)}>
						<Link
							location={{
								pathname: '/playlist',
								query: {
									id: context.playlist._id,
								},
							}}
							asLocation={{
								pathname: `/playlist/${context.playlist._id}`,
							}}
						>
							{d('playlistCard.tooltip.moveTo')}
						</Link>
					</li>
					<li onClick={copyLink}>{d('playlistCard.tooltip.copyLink')}</li>
				</List>
			</StyledTooltip>
		);
	} else {
		return null;
	}
};

export default PlaylistTooltip;
