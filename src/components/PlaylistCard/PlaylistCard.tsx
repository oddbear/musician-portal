import React, { MouseEvent, useCallback, useContext, useState } from 'react';
import styled from 'styled-components';
import LazyImage from 'Components/LazyImage';
import { PlaylistUpdateInput, ProfileUpdateInput, ResponseLink, ResponsePlaylist } from 'Types/response.type';
import { d } from 'Utils/dictionary';
import Link from 'Components/Link/Link';
import Dummy from 'UI/Dummy/Dummy';
import Tablet from 'UI/Tablet/Tablet';
import Value from 'Components/PlaylistCard/Value';
import Modal from 'UI/Modal';
import Button from 'UI/Button';
import { loaderContext } from 'Contexts/loader.context';
import AvatarEditor from 'Components/Card/AvatarEditor';
import EmotionsBar from 'UI/EmotionsBar';
import { PlaylistPublicity } from 'Utils/enum';
import { tooltipContext } from 'Contexts/tooltip.context';

export const MORE_SRC = 'elements/yatjwjjbabxi18fnkabe';

interface Props {
	loading: boolean;
	playlist: ResponsePlaylist;
	defaultAvatars?: ResponseLink[];
	className?: string;
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	onChange?: (propName: keyof PlaylistUpdateInput, value: any) => Promise<void>;
	onAddEmotion: (alias: string, playlistId: string) => void;
}

const ContainerDiv = styled.div`
	position: relative;
	display: flex;
	width: 100%;
	font-family: ${({ theme }) => theme.fontFamily.quiet};
`;

const ContainerLink = styled(Link)`
	position: relative;
	display: flex;
	width: 100%;
	font-family: ${({ theme }) => theme.fontFamily.quiet};
`;

const AvatarWrap = styled.div`
	margin-right: 20px;
`;

const Avatar = styled(LazyImage)`
	width: 128px;
	height: 128px;
`;

const Info = styled.div`
	display: flex;
	width: 100%;
	flex-direction: column;
`;

const Name = styled(Value)`
	font-size: 1.2em;
`;

const Count = styled.span`
	color: #666;
`;

const AvatarDummy = styled(Dummy)`
	width: 128px;
`;

const StyledTablet = styled(Tablet)`
	flex-direction: column;
`;

const StyledEmotionsBar = styled(EmotionsBar)`
	margin-top: 10px;
`;

const More = styled(LazyImage)`
	cursor: pointer;
	position: absolute;
	top: 5px;
	right: 10px;
	width: 20px;
	z-index: 2;
`;

const PlaylistCard: React.FC<Props> = ({ playlist, loading, defaultAvatars, className, onChange, onAddEmotion }) => {
	const [editName, setEditName] = useState<string | null>(null);
	const [confirm, setConfirm] = useState<{
		open: boolean;
		propName?: keyof ProfileUpdateInput;
		before?: string;
		after?: string;
	}>({
		open: false,
	});

	const editable = onChange !== undefined;

	const { setAttachedElement, setContext } = useContext(tooltipContext);
	const { incrementProgress } = useContext(loaderContext);

	const changeConfirm = useCallback(
		(value: boolean) => () => {
			setConfirm({
				...confirm,
				open: value,
			});
		},
		[confirm],
	);

	const declineValue = useCallback(
		(propName?: keyof ProfileUpdateInput) => () => {
			changeConfirm(false)();

			switch (propName) {
				case 'name':
					setEditName(null);
					break;

				default:
					return;
			}
		},
		[changeConfirm],
	);

	const acceptValue = useCallback(
		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		(propName?: keyof ProfileUpdateInput) => async (value?: any) => {
			if (propName === undefined) {
				return;
			}

			let after;
			switch (propName) {
				case 'name':
					after = editName ?? '';
					break;

				case 'avatar':
					after = value;
					break;

				default:
					return;
			}

			incrementProgress(`card.${propName}`);

			changeConfirm(false)();

			await onChange?.(propName, after);
			declineValue(propName);
		},
		[incrementProgress, editName, changeConfirm, onChange, declineValue],
	);

	const spareValue = useCallback(
		(propName: keyof ProfileUpdateInput) => (isSpare = false) => {
			if (isSpare) {
				declineValue(propName)();
				return;
			}

			let before: string;
			let after: string;

			switch (propName) {
				case 'name':
					if (editName === null) {
						return;
					}

					before = playlist.name;
					after = editName;
					break;

				default:
					return;
			}

			if (before === after) {
				return;
			}

			setConfirm({
				open: true,
				propName,
				before,
				after,
			});
		},
		[declineValue, editName, playlist.name],
	);

	const addEmotion = useCallback(
		(alias: string) => {
			onAddEmotion(alias, playlist._id);
		},
		[onAddEmotion, playlist._id],
	);

	const more = useCallback(
		(event: MouseEvent<HTMLElement>) => {
			event.preventDefault();
			setAttachedElement?.(event.currentTarget);
			setContext?.({
				playlist: playlist,
			});
		},
		[playlist, setContext, setAttachedElement],
	);

	return (
		<StyledTablet className={className}>
			<Modal isOpen={confirm.open} onClose={changeConfirm(false)}>
				<span>
					{d(
						'card.remake.confirm.message',
						d(`card.${confirm.propName ?? ''}.title`),
						confirm.before ?? '',
						confirm.after ?? '',
					)}
				</span>
				<div>
					<Button color="simple" onClick={declineValue(confirm.propName)}>
						{d('card.remake.confirm.cancel')}
					</Button>
					<Button color="simple" onClick={acceptValue(confirm.propName)}>
						{d('card.remake.confirm.accept')}
					</Button>
				</div>
			</Modal>
			<Container _id={playlist._id} editable={editable}>
				<More src={MORE_SRC} alt="more" fitHeight={true} onClick={more} />
				<AvatarWrap>
					{editable ? (
						<AvatarDummy loading={loading || 'card.avatar'} transparent={!loading}>
							<AvatarEditor
								src={playlist.avatar.src}
								defaultAvatars={defaultAvatars ?? []}
								onChange={acceptValue('avatar')}
							/>
						</AvatarDummy>
					) : (
						<AvatarDummy loading={loading}>
							<Avatar src={playlist.avatar.src} alt={`playlist's "${playlist.name}" avatar`} fitHeight={true} />
						</AvatarDummy>
					)}
				</AvatarWrap>
				<Info>
					<Dummy loading={loading || 'card.name'} transparent={!loading}>
						<Name
							name="name"
							input={editable}
							type="text"
							onChange={setEditName}
							onAccept={acceptValue('name')}
							onCancel={spareValue('name')}
						>
							{editName ?? playlist.name}
						</Name>
					</Dummy>
					<Dummy loading={loading}>
						<Count>{d('playlistCard.trackCount', playlist.tracks.length)}</Count>
					</Dummy>
				</Info>
			</Container>
			{playlist.publicity === PlaylistPublicity.public && (
				<StyledEmotionsBar _id={playlist._id} reactions={playlist.reactions} addEmotion={addEmotion} />
			)}
		</StyledTablet>
	);
};

export default styled(PlaylistCard)``;

interface ContainerProps {
	_id: string;
	editable: boolean;
	children: React.ReactNode;
}

const Container: React.FC<ContainerProps> = ({ _id, editable, children }) =>
	editable ? (
		<ContainerDiv>{children}</ContainerDiv>
	) : (
		<ContainerLink
			location={{
				pathname: '/playlist',
				query: {
					id: _id,
				},
			}}
			asLocation={{
				pathname: `/playlist/${_id}`,
			}}
		>
			{children}
		</ContainerLink>
	);
