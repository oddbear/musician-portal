import React, { useCallback, useContext, useEffect, useReducer, useState } from 'react';
import styled from 'styled-components';
import { appContext } from 'Contexts/app.context';
import { d } from 'Utils/dictionary';
import { routePush } from 'Components/Link';
import Checkbox from 'UI/Checkbox';
import { usePrevious } from 'Hooks/previous.hook';
import Button from 'UI/Button/Button';
import {
	getUser,
	updateNotificationsSettings as updateNotificationsSettingsMutation,
	updatePrivacySettings as updatePrivacySettingsMutation,
} from 'Schemas/User';
import { GraphQLWrap, ResponseUser, UserNotificationsSettings, UserPrivacySettings } from 'Types/response.type';
import { loaderContext } from 'Contexts/loader.context';
import { errorContext } from 'Contexts/error.context';
import Dummy from 'UI/Dummy/Dummy';
import Tabs from 'UI/Tabs';
import { useSecureMutation } from 'Utils/apollo';

const SETTINGS_SAVE_TASK = 'SETTINGS_SAVE_TASK';

const Container = styled.div`
	position: relative;
	top: 0;
	color: white;
	padding: 40px;

	&:after {
		content: '';
		position: absolute;
		top: 0;
		right: 0;
		width: 100%;
		height: 100%;
		background-color: rgba(25, 31, 33, 0.54);
		background-attachment: fixed;
		border-radius: 4px;
		z-index: 0;
	}
`;

const Wrap = styled.div`
	display: flex;
	flex-direction: column;
	position: relative;
	margin: 0 auto;
	width: 100%;
	max-width: ${({ theme }) => theme.other.containerMaxWidth};
	z-index: 1;
`;

const Row = styled.div`
	display: flex;
	flex-direction: row;
	align-items: center;
	margin-bottom: 6px;
`;

const SettingName = styled.span`
	cursor: pointer;
	font-size: 1em;
`;

const StyledCheckbox = styled(Checkbox)`
	cursor: pointer;
	margin-right: 22px;
`;

const StyledButton = styled(Button)`
	margin-top: 10px;
`;

type Reducer = (state: boolean) => boolean;
const reducer: Reducer = (state) => !state;

const Settings: React.FC = () => {
	const settingsTitles = [
		{
			title: d('settings.notifications.title'),
			value: 'notifications',
		},
		{
			title: d('settings.privacy.title'),
			value: 'privacy',
		},
	];

	const [tab, setTab] = useState<string>(settingsTitles[0].value);

	const { incrementProgress, clearProgress } = useContext(loaderContext);
	const { setError } = useContext(errorContext);

	const { user, loading } = useContext(appContext);
	if (!loading && user === null) {
		void routePush({
			pathname: '/404',
			query: {},
		});
	}

	const notificationsSettings = user?.settings.notifications ?? {
		soundOnNewMessage: false,
		soundOnNewNotification: false,
		notifyOnInvite: false,
		notifyOnLeave: false,
		notifyOnDismiss: false,
		notifyOnDecline: false,
		notifyOnHire: false,
		notifyOnJoin: false,
		notifyOnMention: false,
	};
	const privacySettings = user?.settings.privacy ?? {
		conversationsEnabled: false,
	};

	const [soundOnNewMessage, changeSoundOnNewMessage] = useReducer<Reducer>(reducer, false);
	const [soundOnNewNotification, changeSoundOnNewNotification] = useReducer<Reducer>(reducer, false);
	const [notifyOnInvite, changeNotifyOnInvite] = useReducer<Reducer>(reducer, false);
	const [notifyOnLeave, changeNotifyOnLeave] = useReducer<Reducer>(reducer, false);
	const [notifyOnDismiss, changeNotifyOnDismiss] = useReducer<Reducer>(reducer, false);
	const [notifyOnDecline, changeNotifyOnDecline] = useReducer<Reducer>(reducer, false);
	const [notifyOnHire, changeNotifyOnHire] = useReducer<Reducer>(reducer, false);
	const [notifyOnJoin, changeNotifyOnJoin] = useReducer<Reducer>(reducer, false);
	const [notifyOnMention, changeNotifyOnMention] = useReducer<Reducer>(reducer, false);
	const [conversationsEnabled, changeConversationsEnabled] = useReducer<Reducer>(reducer, false);

	const [updatePrivacySettings] = useSecureMutation<
		GraphQLWrap<'updatePrivacySettings', ResponseUser>,
		{
			settings: Partial<UserPrivacySettings>;
		}
	>(updatePrivacySettingsMutation, {
		variables: {
			settings: {
				conversationsEnabled,
			},
		},
	});

	const [updateNotificationsSettings] = useSecureMutation<
		GraphQLWrap<'updateNotificationsSettings', ResponseUser>,
		{ settings: Partial<UserNotificationsSettings> }
	>(updateNotificationsSettingsMutation, {
		variables: {
			settings: {
				soundOnNewMessage,
				soundOnNewNotification,
				notifyOnInvite,
				notifyOnLeave,
				notifyOnDismiss,
				notifyOnDecline,
				notifyOnHire,
				notifyOnJoin,
				notifyOnMention,
			},
		},
		update: (cache, { data }) => {
			if (data === undefined || data === null) {
				return;
			}

			const oldData = cache.readQuery<GraphQLWrap<'user', ResponseUser>>({
				query: getUser,
			});

			if (oldData === null) {
				return;
			}

			cache.writeQuery<GraphQLWrap<'user', ResponseUser>>({
				query: getUser,
				data: {
					user: {
						...oldData.user,
						settings: {
							...oldData.user.settings,
							notifications: {
								...oldData.user.settings.notifications,
								...data.updateNotificationsSettings.settings.notifications,
							},
						},
					},
				},
			});
		},
	});

	const prevnotificationsSettings = usePrevious(notificationsSettings);
	useEffect(() => {
		if (prevnotificationsSettings !== notificationsSettings) {
			if (notificationsSettings.soundOnNewMessage !== soundOnNewMessage) {
				changeSoundOnNewMessage();
			}
			if (notificationsSettings.soundOnNewNotification !== soundOnNewNotification) {
				changeSoundOnNewNotification();
			}
			if (notificationsSettings.notifyOnInvite !== notifyOnInvite) {
				changeNotifyOnInvite();
			}
			if (notificationsSettings.notifyOnJoin !== notifyOnJoin) {
				changeNotifyOnJoin();
			}
			if (notificationsSettings.notifyOnHire !== notifyOnHire) {
				changeNotifyOnHire();
			}
			if (notificationsSettings.notifyOnLeave !== notifyOnLeave) {
				changeNotifyOnLeave();
			}
			if (notificationsSettings.notifyOnDismiss !== notifyOnDismiss) {
				changeNotifyOnDismiss();
			}
			if (notificationsSettings.notifyOnDecline !== notifyOnDecline) {
				changeNotifyOnDecline();
			}
			if (notificationsSettings.notifyOnMention !== notifyOnMention) {
				changeNotifyOnMention();
			}
			if (privacySettings.conversationsEnabled !== conversationsEnabled) {
				changeConversationsEnabled();
			}
		}
	}, [
		prevnotificationsSettings,
		notificationsSettings,
		privacySettings,
		soundOnNewMessage,
		soundOnNewNotification,
		notifyOnDecline,
		notifyOnDismiss,
		notifyOnInvite,
		notifyOnJoin,
		notifyOnHire,
		notifyOnLeave,
		notifyOnMention,
		conversationsEnabled,
	]);

	const save = useCallback(async () => {
		incrementProgress(SETTINGS_SAVE_TASK);

		if (tab === settingsTitles[0].value) {
			await updateNotificationsSettings().catch((err: Error) => setError(err.message));
		} else {
			await updatePrivacySettings().catch((err: Error) => setError(err.message));
		}

		clearProgress(SETTINGS_SAVE_TASK);
	}, [
		settingsTitles,
		updateNotificationsSettings,
		updatePrivacySettings,
		tab,
		incrementProgress,
		clearProgress,
		setError,
	]);

	return (
		<Container>
			<Wrap>
				<Tabs<string> titles={settingsTitles} tab={tab} setTab={setTab} />
				{tab === settingsTitles[0].value && (
					<>
						<Row onClick={changeSoundOnNewMessage}>
							<StyledCheckbox value={soundOnNewMessage} />
							<SettingName>{d('settings.notifications.soundOnNewMessage')}</SettingName>
						</Row>
						<Row onClick={changeSoundOnNewNotification}>
							<StyledCheckbox value={soundOnNewNotification} />
							<SettingName>{d('settings.notifications.soundOnNewNotification')}</SettingName>
						</Row>
						<Row onClick={changeNotifyOnInvite}>
							<StyledCheckbox value={notifyOnInvite} />
							<SettingName>{d('settings.notifications.notifyOnInvite')}</SettingName>
						</Row>
						<Row onClick={changeNotifyOnJoin}>
							<StyledCheckbox value={notifyOnJoin} />
							<SettingName>{d('settings.notifications.notifyOnJoin')}</SettingName>
						</Row>
						<Row onClick={changeNotifyOnHire}>
							<StyledCheckbox value={notifyOnHire} />
							<SettingName>{d('settings.notifications.notifyOnHire')}</SettingName>
						</Row>
						<Row onClick={changeNotifyOnLeave}>
							<StyledCheckbox value={notifyOnLeave} />
							<SettingName>{d('settings.notifications.notifyOnLeave')}</SettingName>
						</Row>
						<Row onClick={changeNotifyOnDismiss}>
							<StyledCheckbox value={notifyOnDismiss} />
							<SettingName>{d('settings.notifications.notifyOnDismiss')}</SettingName>
						</Row>
						<Row onClick={changeNotifyOnDecline}>
							<StyledCheckbox value={notifyOnDecline} />
							<SettingName>{d('settings.notifications.notifyOnDecline')}</SettingName>
						</Row>
						<Row onClick={changeNotifyOnMention}>
							<StyledCheckbox value={notifyOnMention} />
							<SettingName>{d('settings.notifications.notifyOnMention')}</SettingName>
						</Row>
					</>
				)}
				{tab === settingsTitles[1].value && (
					<>
						<Row onClick={changeConversationsEnabled}>
							<StyledCheckbox value={conversationsEnabled} />
							<SettingName>{d('settings.privacy.conversationsEnabled')}</SettingName>
						</Row>
					</>
				)}
				<Row>
					<Dummy loading={SETTINGS_SAVE_TASK} transparent={true}>
						<StyledButton onClick={save}>{d('settings.apply')}</StyledButton>
					</Dummy>
				</Row>
			</Wrap>
		</Container>
	);
};

export default Settings;
