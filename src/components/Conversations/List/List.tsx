import LazyImage from 'Components/LazyImage';
import { routeReplace } from 'Components/Link';
import { appContext } from 'Contexts/app.context';
import { useLayoutEffect } from 'Hooks/effect.hook';
import React, { MouseEvent, useCallback, useContext, useEffect } from 'react';
import styled from 'styled-components';
import { d } from 'Utils/dictionary';
import {
	GraphQLWrap,
	Response,
	ResponseBand,
	ResponseConversation,
	ResponseMessage,
	ResponseProfile,
	ResponseUser,
	UserConversationSettingsInput,
} from 'Types/response.type';
import { updateConversationSettings as updateConversationSettingsMutation } from 'Schemas/User';
import { loaderContext } from 'Contexts/loader.context';
import { errorContext } from 'Contexts/error.context';
import Dummy from 'UI/Dummy/Dummy';
import { useGetRef } from 'Hooks/getRef.hook';
import Scroll from 'UI/Scroll';
import { useSecureMutation } from 'Utils/apollo';

const SOUND_ON_SRC = 'Conversations/iscdw8ccq5kpptzdtun8';
const SOUND_OFF_SRC = 'Conversations/ipwyncyvlocopy8uuews';
const UPDATE_CONVERSATION_SETTINGS_TASK = 'UPDATE_CONVERSATION_SETTINGS_TASK';

interface Props {
	conversations: Response<ResponseConversation>;
	conversationId: string | null;
	loading: boolean;
	onChange(conversationId: string | null): void;
	onLoadMore(): void;
}

const Container = styled.div`
	position: relative;
	width: 100%;
	padding: 10px 0;
	height: 100%;
	box-sizing: border-box;
`;

const ListScroll = styled(Scroll)`
	position: relative;
	width: 95%;
	padding-right: 14px;
	max-height: 500px;
	margin-top: 12px;
	border-radius: 3px;
	overflow-x: visible !important;
`;

const More = styled.div`
	width: 100%;
	display: flex;
	place-content: center;
	color: #ccc;
	padding: 30px 0;
`;

const List: React.FC<Props> = ({ conversations, conversationId, loading, onChange, onLoadMore }) => {
	const { user, location } = useContext(appContext);
	const containerRef = useGetRef<HTMLDivElement>();
	const loadMoreRef = useGetRef<HTMLDivElement>();

	const onScroll = useCallback(() => {
		if (containerRef.current === null || loadMoreRef.current === null) {
			return;
		}

		const container = containerRef.current;
		const loadMore = loadMoreRef.current;
		if (container.scrollTop >= container.scrollHeight - container.offsetHeight - loadMore.offsetHeight) {
			onLoadMore();
		}
	}, [onLoadMore, containerRef, loadMoreRef]);

	useLayoutEffect(() => {
		if (containerRef.current === null || loadMoreRef.current === null) {
			return;
		}

		const container = containerRef.current;
		const loadMore = loadMoreRef.current;
		if (!loading && container.scrollTop >= container.scrollHeight - container.offsetHeight - loadMore.offsetHeight) {
			onScroll();
		}

		container.addEventListener('scroll', onScroll);
		return () => container.removeEventListener('scroll', onScroll);
	}, [loading, onScroll, loadMoreRef, containerRef]);

	const change = useCallback(
		(newConversationId: string | null) => {
			void routeReplace(
				{
					...location,
					pathname: '/conversations',
					query: {
						id: newConversationId,
					},
				},
				{
					...location,
					pathname: newConversationId !== null ? `/conversations/${newConversationId}` : '/conversations',
				},
			);
			onChange(newConversationId);
		},
		[onChange, location],
	);

	useEffect(() => {
		const cancel = (event: KeyboardEvent): void => {
			if (event.code === 'Escape') {
				event.preventDefault();
				change(null);
			}
		};

		document.documentElement.addEventListener('keydown', cancel);
		return () => document.documentElement.removeEventListener('keydown', cancel);
	}, [change]);

	return (
		<Container ref={containerRef}>
			<ListScroll>
				{user !== null ? (
					<>
						{conversations.data
							.sort((c1, c2) =>
								((c1.messages.data[0] as ResponseMessage | undefined)?.createdAt ?? c1.createdAt) >
								((c2.messages.data[0] as ResponseMessage | undefined)?.createdAt ?? c2.createdAt)
									? -1
									: 1,
							)
							.filter(({ _id, messages }) => messages.totalLength > 0 || _id === conversationId)
							.map(({ _id, messages, bandParticipants, profileParticipants }) => {
								const message: ResponseMessage = messages.data[0] ?? {
									text: d('conversations.messages.empty'),
									from: {},
								};
								const participants = [...bandParticipants, ...profileParticipants];
								const sender = participants.filter((participant) => user._id !== participant._id)[0];

								return (
									<Conversation
										key={_id}
										_id={_id}
										active={conversationId === _id}
										text={message.text}
										isMe={user._id === message.from._id}
										participants={participants}
										avatar={sender.avatar.src}
										soundOn={
											!user.settings.conversations.some(
												(conversationSettings) => conversationSettings._id === _id && conversationSettings.muted,
											)
										}
										onChange={change}
									/>
								);
							})}
						{conversations.totalLength > conversations.data.length && (
							<More ref={loadMoreRef}>{d('conversations.list.more')}</More>
						)}
					</>
				) : null}
			</ListScroll>
		</Container>
	);
};

export default List;

interface ConversationProps {
	_id: string;
	active: boolean;
	participants: Array<ResponseProfile | ResponseBand>;
	avatar: string;
	text: string;
	isMe: boolean;
	soundOn: boolean;
	onChange(conversationId: string): void;
}

interface ConversationContainerProps {
	active: boolean;
}

const ConversationContainer = styled.div<ConversationContainerProps>`
	position: relative;
	cursor: pointer;
	display: flex;
	align-items: center;
	padding: 10px 0;
	width: 100%;
	box-sizing: border-box;
	background-color: ${({ active }) => (active ? 'rgba(0, 0, 0, 0.1)' : 'transparent')};
	border-bottom: 1px solid #bab9b9;
`;

const Avatar = styled(LazyImage)`
	margin-right: 6px;
	height: 52px;
	width: 52px;
	background-color: white;
`;

const Content = styled.div`
	display: flex;
	flex-direction: column;
`;

const Name = styled.span`
	width: 100%;
	font-size: 1rem;
	color: #555;
	font-weight: 700;
	overflow: hidden;
	text-overflow: ellipsis;
`;

const Text = styled.span`
	width: 100%;
	font-size: 0.75rem;
	overflow: hidden;
	text-overflow: ellipsis;
`;

const Me = styled.span`
	font-weight: 300;
	font-style: italic;
	margin-right: 4px;
`;

const SoundIcon = styled(LazyImage)`
	width: 100%;
`;

const SoundIconDummy = styled(Dummy)`
	position: absolute;
	top: 5px;
	right: 5px;
	width: 20px;
`;

const Conversation: React.FC<ConversationProps> = ({
	_id,
	active,
	participants,
	text,
	isMe,
	avatar,
	soundOn,
	onChange,
}) => {
	const updateTask = `${UPDATE_CONVERSATION_SETTINGS_TASK} ${_id}`;
	const { incrementProgress, clearProgress } = useContext(loaderContext);
	const { setError } = useContext(errorContext);
	const { user } = useContext(appContext);

	const [updateConversationSettings] = useSecureMutation<
		GraphQLWrap<'updateConversationSettings', ResponseUser>,
		{
			settings: UserConversationSettingsInput;
		}
	>(updateConversationSettingsMutation, {
		variables: {
			settings: {
				_id,
				// works like reverse (soundOn is opposite to muted)
				muted: soundOn,
			},
		},
	});

	const change = useCallback(() => {
		onChange(_id);
	}, [onChange, _id]);

	const changeSoundNotifications = useCallback(
		async (event: MouseEvent<HTMLElement>) => {
			event.stopPropagation();

			incrementProgress(updateTask);
			await updateConversationSettings().catch((err: Error) => setError(err.message));
			clearProgress(updateTask);
		},
		[updateTask, clearProgress, incrementProgress, setError, updateConversationSettings],
	);

	return (
		<ConversationContainer onClick={change} active={active}>
			<SoundIconDummy loading={updateTask}>
				<SoundIcon
					src={soundOn ? SOUND_ON_SRC : SOUND_OFF_SRC}
					alt="sound notifications on/off"
					fitHeight={true}
					onClick={changeSoundNotifications}
				/>
			</SoundIconDummy>
			<Avatar src={avatar} alt="avatar" />
			<Content>
				<Name>
					{participants
						.filter((participant) => participant._id !== user?._id)
						.map(({ name }) => name)
						.join(', ')}
				</Name>
				<Text>
					{isMe && <Me>{d('conversations.list.you')}:</Me>}
					{text.replace(/\n\t/g, '')}
				</Text>
			</Content>
		</ConversationContainer>
	);
};
