import { appContext } from 'Contexts/app.context';
import { useLayoutEffect } from 'Hooks/effect.hook';
import React, { KeyboardEvent, useCallback, useContext, useEffect, useState } from 'react';
import styled from 'styled-components';
import Form, { Input } from 'UI/Form';
import { d } from 'Utils/dictionary';
import { useSecureMutation } from 'Utils/apollo';
import {
	getConversations,
	readMessages as readMessagesMutation,
	sendMessage as sendMessageMutation,
} from 'Schemas/Conversation';
import {
	GraphQLWrap,
	Response,
	ResponseBand,
	ResponseConversation,
	ResponseMessage,
	ResponseProfile,
} from 'Types/response.type';
import { setCookiesMemory } from 'UI/Form/Input';
import { usePrevious } from 'Hooks/previous.hook';
import { MonthName } from 'Utils/enum';
import Link from 'Components/Link';
import { CONVERSATIONS_QUERY_VARIABLES } from 'Components/Conversations/Conversations';
import { getFormattedDate } from 'Utils/date';
import { useGetRef } from 'Hooks/getRef.hook';
import Scroll from 'UI/Scroll';
import { errorContext } from 'Contexts/error.context';

interface Props {
	loading: boolean;
	conversationId: string | null;
	messages: Response<ResponseMessage>;
	participants: Array<ResponseProfile | ResponseBand>;
	onLoadMore(): void;
}

const Container = styled.div`
	position: relative;
	width: 100%;
	height: 100%;
`;

const Empty = styled.div`
	display: flex;
	width: 100%;
	height: 100%;
	place-content: center;
	align-items: center;
	font-size: 1.2rem;
	color: #bab9b9;
`;

const Messages = styled.div`
	width: 100%;
	flex: 1;

	${Empty} {
		margin-top: 138px;
	}
`;

interface MessageRowProps {
	checked: boolean;
}

const MessageRow = styled.div<MessageRowProps>`
	display: flex;
	width: 100%;
	box-sizing: border-box;
	transition: ${({ checked }) => (checked ? '0.5s' : '0')};
`;

const Message = styled.div`
	padding: 3px 11px;
	border-radius: 8.5px;
	font-size: 0.8rem;
	font-family: ${({ theme }) => theme.fontFamily.quiet};
	border: 0.5px solid #a5a5a5;
	word-break: break-all;
`;

const DateText = styled.span`
	font-size: 0.6rem;
	color: #ccc;
	margin: 6px 10px;
`;

const ChatScroll = styled(Scroll)`
	flex: 1;
	padding: 0 20px 10px 20px;
`;

interface MessageWrapProps {
	me: boolean;
}

const MessageWrap = styled.div<MessageWrapProps>`
	display: flex;
	flex-direction: ${({ me }): string => (me ? 'row-reverse' : 'none')};
	margin-top: 20px;
	margin-left: ${({ me }): string => (me ? 'auto' : '0')};
	max-width: 60%;
	text-align: ${({ me }): string => (me ? 'right' : 'left')};

	${Message} {
		background-color: ${({ me }): string => (me ? '#dadfe0' : '#e7e7e7')};
		color: black;
	}
`;

const TextareaWrap = styled.div`
	position: relative;
	width: 100%;
	height: 59px;
	display: flex;
	place-content: space-between;
	padding: 0 13px;
	margin: 17px 0 0 6px;
	box-sizing: border-box;
`;

const Wrap = styled.div`
	position: relative;
	display: flex;
	width: 100%;
	height: 100%;
	flex-direction: column;

	${Form} {
		width: 100%;
	}
`;

const ChatBorder = styled.div`
	display: flex;
	flex-direction: column;
	position: relative;
	margin: 8px 0 0 8px;
	padding: 20px 0;
	width: calc(100% - 16px);
	height: calc(100% - 16px);
	border: 1px solid #555;
	border-radius: 2px;
	box-sizing: border-box;
	z-index: 1;
`;

const LineTop = styled.div`
	position: relative;
	margin-top: 27px;
	border-top: 1px solid #bab9b9;
`;

const LineBottom = styled.div`
	position: relative;
	border-bottom: 1px solid #bab9b9;
`;

const StyledForm = styled(Form)`
	margin-right: 10px;
`;

const StyledInput = styled(Input)`
	width: 100%;
	height: 59px;

	textarea {
		resize: none;
		background: #e7e7e7 none;
		font-size: 0.8rem;
		color: ${({ theme }) => theme.color.gray50};
		padding: 9px 10px;
	}
`;

const More = styled.div`
	width: 100%;
	display: flex;
	place-content: center;
	color: #ccc;
	padding: 30px 0;
`;

const FullDate = styled.div`
	text-align: center;
	font-size: 0.9rem;
	font-weight: 400;
	margin-top: 20px;
	color: ${({ theme }) => theme.color.gray50};
`;

const Participants = styled.div`
	display: flex;
	margin: 13px 0 0 16px;
	color: #555;
`;

const Name = styled.div`
	font-size: 1rem;
	font-weight: 700;
	text-decoration: underline;
	color: #2c2c2c;
	margin: 2px 0 0 6px;
`;

const FixedBottomMessages = styled.div`
	position: absolute;
	top: 100%;
	bottom: 0;
	width: 100%;
	padding: 10px 0;
`;

const Chat: React.FC<Props> = ({ loading, participants, conversationId, messages, onLoadMore }) => {
	const { user } = useContext(appContext);
	const { setError } = useContext(errorContext);

	const containerRef = useGetRef<HTMLDivElement>();
	const loadMoreRef = useGetRef<HTMLDivElement>();
	const [text, setText] = useState<string>('');
	const [dummyMessages, setDummyMessages] = useState<{ text: string; createdAt: number }[]>([]);

	const [sendMessage] = useSecureMutation<
		GraphQLWrap<'sendMessage', ResponseConversation>,
		{
			conversationId: string;
			text: string;
		}
	>(sendMessageMutation);

	const [readMessages] = useSecureMutation<
		GraphQLWrap<'readMessages', ResponseMessage[]>,
		{
			messages: string[];
		}
	>(readMessagesMutation);

	const sendMessageAction = useCallback(() => {
		if (conversationId !== null && text !== '') {
			const createdDummy = Date.now();
			setDummyMessages([
				...dummyMessages,
				{
					text,
					createdAt: createdDummy,
				},
			]);
			setCookiesMemory(user?.login ?? '', '');
			setText('');

			void sendMessage({
				variables: {
					conversationId,
					text,
				},
				update: (cache, { data }) => {
					setDummyMessages([]);

					const oldData = cache.readQuery<GraphQLWrap<'responseConversations', Response<ResponseConversation>>>({
						query: getConversations,
						variables: CONVERSATIONS_QUERY_VARIABLES,
					});

					if (oldData === null || data === undefined || data === null) {
						return;
					}

					cache.writeQuery<GraphQLWrap<'responseConversations', Response<ResponseConversation>>>({
						query: getConversations,
						variables: CONVERSATIONS_QUERY_VARIABLES,
						data: {
							responseConversations: {
								...oldData.responseConversations,
								data: oldData.responseConversations.data.map((conversation) =>
									conversationId === conversation._id
										? {
												...conversation,
												messages: {
													...conversation.messages,
													totalLength: conversation.messages.totalLength + 1,
													data: [...data.sendMessage.messages.data, ...conversation.messages.data],
												},
										  }
										: conversation,
								),
							},
						},
					});
				},
			}).catch((err: Error) => setError(err.message));
		}
	}, [user, setError, conversationId, sendMessage, text, dummyMessages]);

	const keyDown = useCallback(
		(event: KeyboardEvent<HTMLInputElement | HTMLTextAreaElement>) => {
			if (event.key === 'Enter' && !event.shiftKey) {
				event.preventDefault();
				sendMessageAction();
			}
		},
		[sendMessageAction],
	);

	const onScroll = useCallback(() => {
		if (containerRef.current === null || loadMoreRef.current === null) {
			return;
		}

		if (containerRef.current.scrollTop <= loadMoreRef.current.offsetHeight) {
			onLoadMore();
		}
	}, [onLoadMore, containerRef, loadMoreRef]);

	const prevScrollBottom = usePrevious<number>(
		(containerRef.current?.scrollHeight ?? 0) -
			((containerRef.current?.offsetHeight ?? 0) + (containerRef.current?.scrollTop ?? 0)),
	);
	const prevScrollHeight = usePrevious<number>(containerRef.current?.scrollHeight ?? 0);
	useLayoutEffect(() => {
		if (containerRef.current === null) {
			return;
		}

		const container = containerRef.current;
		if (
			prevScrollHeight !== container.scrollHeight &&
			container.scrollTop !== container.scrollHeight - container.offsetHeight &&
			prevScrollBottom !== undefined
		) {
			container.scrollTop = container.scrollHeight - (prevScrollBottom + container.offsetHeight);
		}

		if (loadMoreRef.current === null) {
			return;
		}

		if (!loading && containerRef.current.scrollTop <= loadMoreRef.current.offsetHeight) {
			onLoadMore();
		}

		container.addEventListener('scroll', onScroll);
		return () => container.removeEventListener('scroll', onScroll);
	}, [prevScrollBottom, prevScrollHeight, onLoadMore, loading, onScroll, containerRef, loadMoreRef]);

	useEffect(() => {
		setText('');

		if (containerRef.current !== null) {
			containerRef.current.scrollTop = containerRef.current.scrollHeight;
		}
	}, [conversationId, containerRef]);

	useEffect(() => {
		const unreadMessages = messages.data.filter(({ from, isChecked }) => !isChecked && user?._id !== from._id);
		if (unreadMessages.length > 0) {
			void readMessages({
				variables: {
					messages: unreadMessages.map(({ _id }) => _id),
				},
			});
		}
	}, [user, readMessages, messages]);

	let currentDate: string;

	return (
		<Container>
			<Wrap>
				<ChatBorder>
					{conversationId !== null && (
						<>
							<Participants>
								<span>{d('conversations.chat.headerParticipants')}</span>
								{participants
									.filter((participant) => !('login' in participant) || participant.login !== user?.login)
									.map((participant) => (
										<Link
											key={participant._id}
											location={{
												pathname: 'alias' in participant ? `/band` : `/profile`,
												query: {
													alias: 'alias' in participant ? participant.alias : null,
													login: 'login' in participant ? participant.login : null,
												},
											}}
											asLocation={{
												pathname:
													'alias' in participant ? `/band/${participant.alias}` : `/profile/${participant.login}`,
											}}
										>
											<Name>{participant.name}</Name>
										</Link>
									))}
							</Participants>
							<LineTop />
						</>
					)}
					<ChatScroll getRef={containerRef}>
						{conversationId !== null ? (
							<Messages>
								{messages.totalLength > messages.data.length && (
									<More ref={loadMoreRef}>{d('conversations.chat.more')}</More>
								)}
								{messages.data.length + dummyMessages.length > 0 ? (
									[...messages.data].reverse().map((message) => {
										const isMe = 'login' in message.from && message.from.login === user?.login;

										const now = new Date();
										const createdAt = new Date(message.createdAt);
										let fullDate;
										if (
											now.getFullYear() === createdAt.getFullYear() &&
											now.getMonth() === createdAt.getMonth() &&
											now.getDate() === createdAt.getDate()
										) {
											fullDate = d('conversations.chat.today');
										} else if (
											now.getFullYear() === createdAt.getFullYear() &&
											now.getMonth() === createdAt.getMonth() &&
											now.getDate() - 1 === createdAt.getDate()
										) {
											fullDate = d('conversations.chat.yesterday');
										} else {
											fullDate = `${createdAt.getDate().toString()} ${d(
												'months.' + MonthName[createdAt.getMonth()],
											)} ${createdAt.getFullYear().toString()}`;
										}
										const sameDate = currentDate !== fullDate;
										currentDate = fullDate;

										const { getHours, getMinutes } = getFormattedDate(createdAt);

										return (
											<div key={message._id}>
												{sameDate && <FullDate>{currentDate}</FullDate>}
												<MessageRow checked={message.isChecked || isMe}>
													<MessageWrap me={isMe}>
														<Message>{message.text}</Message>
														<DateText>
															{getHours()}:{getMinutes()}
														</DateText>
													</MessageWrap>
												</MessageRow>
											</div>
										);
									})
								) : (
									<Empty>{d('conversations.chat.empty')}</Empty>
								)}
							</Messages>
						) : (
							<Empty>{d('conversations.chat.noConversation')}</Empty>
						)}
					</ChatScroll>
					{conversationId !== null && (
						<>
							<LineBottom />
							<TextareaWrap>
								<StyledForm>
									<StyledInput
										type="textarea"
										value={text}
										onChange={setText}
										onKeyDown={keyDown}
										placeholder={d('conversations.chat.placeholder')}
										name={user?.login ?? ''}
										textMemory={true}
									/>
								</StyledForm>
							</TextareaWrap>
						</>
					)}
					{dummyMessages.length > 0 && (
						<FixedBottomMessages>
							{dummyMessages.map((message) => {
								const { getHours, getMinutes } = getFormattedDate(message.createdAt);

								return (
									<div key={message.createdAt}>
										<MessageRow checked={true}>
											<MessageWrap me={true}>
												<Message>{message.text}</Message>
												<DateText>
													{getHours()}:{getMinutes()}
												</DateText>
											</MessageWrap>
										</MessageRow>
									</div>
								);
							})}
						</FixedBottomMessages>
					)}
				</ChatBorder>
			</Wrap>
		</Container>
	);
};

export default Chat;
