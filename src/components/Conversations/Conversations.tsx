import { appContext } from 'Contexts/app.context';
import Chat from 'Components/Conversations/Chat';
import List from 'Components/Conversations/List';
import styled from 'styled-components';
import { useLazyLoad } from 'Hooks/lazyload.hook';
import React, { useCallback, useContext, useEffect, useState } from 'react';
import { useQuery } from '@apollo/react-hooks';
import { GraphQLWrap, Response, ResponseConversation, ResponseMessage } from 'Types/response.type';
import { getConversations, getMessages as getMessagesMutation } from 'Schemas/Conversation';
import { d } from 'Utils/dictionary';
import { errorContext } from 'Contexts/error.context';
import { routePush } from 'Components/Link';
import Dummy from 'UI/Dummy/Dummy';
import { useSecureMutation } from 'Utils/apollo';

const CONVERSATIONS_STEP = 15;
const MESSAGES_STEP = 30;
export const CONVERSATIONS_QUERY_VARIABLES = {
	limit: CONVERSATIONS_STEP,
	visibleData: [],
	visibleMessages: [],
};

const BACKGROUND_SRC = 'Chats/base_chats_5520_s9vm4m';

interface ContentProps {
	src: string;
}

const Container = styled.div`
	position: relative;
	margin: 0 auto;
	display: flex;
	justify-content: center;
	padding: 122px 0 203px 0;
	height: 543px;
	font-family: ${({ theme }) => theme.fontFamily.quiet};
	font-weight: 300;

	&:after {
		content: '';
		position: absolute;
		top: 42px;
		right: 0;
		width: 500%;
		height: 84%;
		background-color: rgba(25, 31, 33, 0.54);
		background-attachment: fixed;
		border-radius: 4px;
		z-index: 0;
	}
`;

const Wrap = styled.div<ContentProps>`
	display: flex;
	position: relative;
	width: 100%;
	max-width: 1121px;
	height: 543px;
	border-radius: 4px;
	background: url(${({ src }) => src}) #cacbcc repeat center;
	z-index: 1;

	&:before {
		content: '';
		position: absolute;
		top: 0;
		width: 100%;
		height: 100%;
		z-index: 1;
	}
`;

const ListWrap = styled.div`
	width: 25%;
	height: 100%;
	z-index: 1;
`;

const ChatWrap = styled.div`
	flex: 1;
	height: 100%;
`;

const Empty = styled.div`
	display: flex;
	align-items: center;
	place-content: center;
	width: 100%;
`;

const StyledDummy = styled(Dummy)`
	width: 100%;
	max-width: 1121px;
`;

// @TODO: fix then @apollo/react-hooks will use query values as previous
const subscriptionState: { conversations: Response<ResponseConversation> } = {
	conversations: {
		totalLength: 0,
		data: [],
	},
};

const Conversations: React.FC = () => {
	const { src } = useLazyLoad({
		src: BACKGROUND_SRC,
	});
	const [activeConversationId, setActiveConversationId] = useState<string | null>(null);
	const [isLoadingConversations, setIsLoadingConversations] = useState<boolean>(false);
	const [isLoadingMessages, setIsLoadingMessages] = useState<boolean>(false);

	const { location } = useContext(appContext);
	const { setError } = useContext(errorContext);

	const { data: conversationsData, fetchMore, loading } = useQuery<
		GraphQLWrap<'responseConversations', Response<ResponseConversation>>,
		{
			limit: number;
			visibleData: string[];
		}
	>(getConversations, {
		variables: CONVERSATIONS_QUERY_VARIABLES,
		ssr: false,
		onError: (error) => {
			setError(error.message);
		},
	});
	const conversations =
		conversationsData === undefined
			? {
					totalLength: 0,
					data: [],
			  }
			: conversationsData.responseConversations;
	subscriptionState.conversations = conversations;

	const [getMessages, { loading: loadingMessages }] = useSecureMutation<
		GraphQLWrap<'getMessages', Response<ResponseMessage>>,
		{
			conversationId: string;
			limit: number;
			visibleData: string[];
		}
	>(getMessagesMutation, {
		update: (cache, { data }) => {
			setIsLoadingMessages(false);

			const oldData = cache.readQuery<GraphQLWrap<'responseConversations', Response<ResponseConversation>>>({
				query: getConversations,
				variables: CONVERSATIONS_QUERY_VARIABLES,
			});

			if (oldData === undefined || oldData === null || data === undefined || data === null) {
				return;
			}

			cache.writeQuery<GraphQLWrap<'responseConversations', Response<ResponseConversation>>>({
				query: getConversations,
				variables: CONVERSATIONS_QUERY_VARIABLES,
				data: {
					responseConversations: {
						...oldData.responseConversations,
						data: oldData.responseConversations.data.map((conversation) =>
							activeConversationId === conversation._id
								? {
										...conversation,
										messages: {
											...conversation.messages,
											totalLength: data.getMessages.totalLength,
											data: [...conversation.messages.data, ...data.getMessages.data],
										},
								  }
								: conversation,
						),
					},
				},
			});
		},
	});

	const loadMoreConversations = useCallback(() => {
		if (!loading && !isLoadingConversations) {
			setIsLoadingConversations(true);

			void fetchMore({
				variables: {
					...CONVERSATIONS_QUERY_VARIABLES,
					visibleData: conversations.data.map(({ _id }) => _id),
				},
				updateQuery: (prev, { fetchMoreResult }) => {
					setTimeout(() => {
						setIsLoadingConversations(false);
					}, 0);

					if (fetchMoreResult === undefined) {
						return prev;
					}

					return {
						...fetchMoreResult,
						responseConversations: {
							...fetchMoreResult.responseConversations,
							data: [...prev.responseConversations.data, ...fetchMoreResult.responseConversations.data],
						},
					};
				},
			});
		}
	}, [conversations.data, fetchMore, loading, isLoadingConversations]);

	const loadMoreMessages = useCallback(() => {
		if (!loadingMessages && !isLoadingMessages) {
			if (activeConversationId !== null) {
				setIsLoadingMessages(true);

				void getMessages({
					variables: {
						conversationId: activeConversationId,
						limit: MESSAGES_STEP,
						visibleData:
							conversations.data
								.find((conversation) => conversation._id === activeConversationId)
								?.messages.data.map((message) => message._id) ?? [],
					},
				});
			} else {
				throw new Error('trying to load new messages with no selected conversation');
			}
		}
	}, [getMessages, loadingMessages, conversations.data, activeConversationId, isLoadingMessages]);

	useEffect(() => {
		const parsedId = /^\/conversations\/([^/]+)\/?$/.exec(location.pathname);
		if (parsedId !== null) {
			setActiveConversationId(parsedId[1]);
		}
	}, [location.pathname]);

	useEffect(() => {
		if (!loading && (conversationsData === null || conversationsData === undefined)) {
			void routePush({
				pathname: '/404',
				query: {},
			});
		}
	}, [loading, conversationsData]);

	const currentConversation = conversations.data.find((conversation) => conversation._id === activeConversationId);

	return (
		<Container>
			<StyledDummy loading={loading}>
				<Wrap src={src}>
					{conversations.data.length > 0 ? (
						<>
							<ListWrap>
								<List
									conversationId={activeConversationId}
									conversations={conversations}
									loading={isLoadingConversations || loading}
									onChange={setActiveConversationId}
									onLoadMore={loadMoreConversations}
								/>
							</ListWrap>
							<ChatWrap>
								<Chat
									conversationId={activeConversationId}
									participants={[
										...(currentConversation?.bandParticipants ?? []),
										...(currentConversation?.profileParticipants ?? []),
									]}
									loading={isLoadingMessages || loadingMessages}
									messages={
										conversations.data.find((conversation) => conversation._id === activeConversationId)?.messages ?? {
											data: [],
											totalLength: 0,
										}
									}
									onLoadMore={loadMoreMessages}
								/>
							</ChatWrap>
						</>
					) : (
						<Empty>{d('conversations.noConversations')}</Empty>
					)}
				</Wrap>
			</StyledDummy>
		</Container>
	);
};

export default Conversations;
