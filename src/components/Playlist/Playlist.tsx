import React, { useCallback, useContext, useState } from 'react';
import { useQuery } from '@apollo/react-hooks';
import { GraphQLWrap, PlaylistUpdateInput, ResponseEmotion, ResponseLink, ResponsePlaylist } from 'Types/response.type';
import { getPlaylist, removeTrackFromPlaylist, updatePlaylist as updatePlaylistMutation } from 'Schemas/Playlist';
import PlaylistCard from 'Components/PlaylistCard';
import { dummyPlaylistData } from 'Utils/data';
import styled from 'styled-components';
import { loaderContext } from 'Contexts/loader.context';
import { errorContext } from 'Contexts/error.context';
import { routePush } from 'Components/Link';
import { getDefaultAvatars } from 'Schemas/Link';
import AdditionalBlock from './AdditionalBlock';
import { PlaylistPublicity } from 'Utils/enum';
import { addPlaylistEmotion as addPlaylistEmotionMutation, getEmotions } from 'Schemas/Emotion';
import { tooltipContext } from 'Contexts/tooltip.context';
import PlaylistTooltip from 'Components/PlaylistCard/PlaylistTooltip';
import { PlaylistTooltipContext } from 'Components/PlaylistCard/PlaylistTooltip/PlaylistTooltip';
import { useSecureMutation } from 'Utils/apollo';

interface Props {
	_id: string;
}

const Container = styled.div`
	position: relative;
	top: 0;
	color: white;
	padding: 40px;
	font-size: 1rem;

	&:after {
		content: '';
		position: absolute;
		top: 0;
		right: 0;
		width: 100%;
		height: 100%;
		background-color: rgba(25, 31, 33, 0.54);
		background-attachment: fixed;
		border-radius: 4px;
		z-index: 0;
	}
`;

const Wrap = styled.div`
	display: flex;
	flex-direction: column;
	position: relative;
	margin: 0 auto;
	width: 100%;
	max-width: ${({ theme }) => theme.other.containerMaxWidth};
	z-index: 1;
`;

const StyledPlaylistCard = styled(PlaylistCard)`
	margin-bottom: 20px;
`;

const Playlist: React.FC<Props> = ({ _id }) => {
	const [attachedElement, setAttachedElement] = useState<HTMLElement | null>(null);
	const [visible, setVisible] = useState<boolean>(false);
	const [context, setContext] = useState<PlaylistTooltipContext>({
		inCard: true,
	});

	const changeContext = useCallback((newContext: PlaylistTooltipContext) => {
		setContext({
			...newContext,
			inCard: true,
		});
	}, []);

	const { clearProgress } = useContext(loaderContext);
	const { setError } = useContext(errorContext);

	const { data: avatarsData } = useQuery<
		GraphQLWrap<'defaultAvatars', ResponseLink[]>,
		{
			type: string;
		}
	>(getDefaultAvatars, {
		ssr: false,
		variables: {
			type: 'playlist',
		},
	});
	const defaultAvatars =
		avatarsData?.defaultAvatars !== undefined && avatarsData.defaultAvatars !== null ? avatarsData.defaultAvatars : [];

	const { data: playlistData, loading } = useQuery<
		GraphQLWrap<'playlist', ResponsePlaylist>,
		{
			_id: string;
		}
	>(getPlaylist, {
		ssr: true,
		variables: {
			_id,
		},
	});
	const playlist: ResponsePlaylist = playlistData?.playlist ?? dummyPlaylistData;

	const [updatePlaylist] = useSecureMutation<
		GraphQLWrap<'updatePlaylist', ResponsePlaylist>,
		{
			_id: string;
			input: PlaylistUpdateInput;
		}
	>(updatePlaylistMutation);

	const change = useCallback(
		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		async (propName: keyof PlaylistUpdateInput, value: any): Promise<void> => {
			await updatePlaylist({
				variables: {
					_id,
					input: {
						[propName]: value,
					},
				},
			}).catch((err: Error) => setError(err.message));
			clearProgress(`card.${propName}`);
		},
		[_id, updatePlaylist, clearProgress, setError],
	);

	if (!loading && playlistData?.playlist === undefined) {
		void routePush({
			pathname: '/404',
			query: {},
		});
	}

	const [removeTrack] = useSecureMutation<
		GraphQLWrap<'removeTrackFromPlaylist', ResponsePlaylist>,
		{
			playlistId: string;
			audioId: string;
		}
	>(removeTrackFromPlaylist);

	const removeAudio = useCallback(
		async (audioId: string) => {
			await removeTrack({
				variables: {
					audioId,
					playlistId: _id,
				},
			}).catch((err: Error) => setError(err.message));
		},
		[setError, _id, removeTrack],
	);

	const [addPlaylistEmotion] = useSecureMutation<
		GraphQLWrap<'addPlaylistEmotion', ResponsePlaylist>,
		{
			playlistId: string;
			alias: string;
		}
	>(addPlaylistEmotionMutation);

	const onAddEmotion = useCallback(
		async (alias: string, playlistId: string) => {
			await addPlaylistEmotion({
				variables: {
					alias,
					playlistId,
				},
			}).catch((err: Error) => setError(err.message));
		},
		[addPlaylistEmotion, setError],
	);

	const { data: emotionsData } = useQuery<GraphQLWrap<'emotions', ResponseEmotion[]>>(getEmotions);
	const emotions = emotionsData?.emotions ?? [];

	const changeAttachedElement = useCallback(
		(element: HTMLElement | null) => {
			setAttachedElement(element);
			setVisible(element === attachedElement ? !visible : true);
		},
		[visible, attachedElement, setVisible],
	);

	const isPrivate = playlist.publicity === PlaylistPublicity.private;

	return (
		<tooltipContext.Provider
			value={{
				attachedElement,
				setAttachedElement: changeAttachedElement,
				visible,
				setVisible,
				setContext: changeContext,
			}}
		>
			<PlaylistTooltip context={context} emotions={emotions} onAddEmotion={onAddEmotion} />
			<Container>
				<Wrap>
					<StyledPlaylistCard
						loading={loading}
						playlist={playlist}
						defaultAvatars={defaultAvatars}
						onChange={isPrivate ? change : undefined}
						onAddEmotion={onAddEmotion}
					/>
					<AdditionalBlock
						isPrivate={isPrivate}
						playlist={playlist}
						onRemoveAudio={isPrivate ? removeAudio : undefined}
					/>
				</Wrap>
			</Container>
		</tooltipContext.Provider>
	);
};

export default Playlist;
