import React, { useCallback, useContext, useEffect, useRef, useState } from 'react';
import { d } from 'Utils/dictionary';
import styled from 'styled-components';
import { GraphQLWrap, Response, ResponseLink, ResponsePlaylist } from 'Types/response.type';
import Tablet from 'UI/Tablet/Tablet';
import Audios from 'Components/Audios';
import Button from 'UI/Button/Button';
import Modal from 'UI/Modal';
import { useQuery } from '@apollo/react-hooks';
import { getPlaylists, updatePlaylist as updatePlaylistMutation } from 'Schemas/Playlist';
import { PlaylistPublicity } from 'Utils/enum';
import { errorContext } from 'Contexts/error.context';
import { loaderContext } from 'Contexts/loader.context';
import Dummy from 'UI/Dummy/Dummy';
import { GA_ADD_TO_PLAYLIST, GA_CATEGORY_PLAYLIST, GA_PUBLISHED, GA_SEARCH, PLAYLISTS_LIMIT } from 'Utils/constants';
import { Input } from 'UI/Form';
import Scroll from 'UI/Scroll';
import debounce from 'lodash/debounce';
import { getTrackList } from 'Schemas/Link';
import { useGetRef } from 'Hooks/getRef.hook';
import { useLayoutEffect } from 'Hooks/effect.hook';
import ReactGA from 'react-ga';
import { useSecureMutation } from 'Utils/apollo';

const PUBLICITY_TASK = 'PUBLICITY_TASK';
const DELAY_TRACK_LIST = 500;
const TRACK_LIST_LIMIT = 20;

interface Props {
	playlist: ResponsePlaylist;
	isPrivate: boolean;
	onRemoveAudio?: (_id: string) => void;
}

const Wrap = styled.div`
	width: 100%;
	font-size: 1rem;
	padding: 0 20px;
`;

const Title = styled.h3`
	margin: 0 0 20px 0;
	font-size: 1.2em;
`;

const PublicityButton = styled(Button)`
	margin-bottom: 10px;
`;

const TracksContainer = styled(Scroll)`
	max-height: 400px;
	margin-bottom: 10px;
`;

const SearchInput = styled(Input)`
	margin-bottom: 10px;
`;

const LoadMore = styled.div`
	cursor: pointer;
	display: flex;
	align-items: center;
	place-content: center;
	height: 60px;
	margin: 20px 0;
	color: #555;
	width: 100%;
	border-top: 1px solid #bbb;
	border-bottom: 1px solid #bbb;
`;

const AdditionalBlock: React.FC<Props> = ({ playlist, isPrivate, onRemoveAudio }) => {
	const [modalOpened, setModalOpened] = useState<boolean>(false);
	const [trackName, setTrackName] = useState<string>('');
	const [trackListName, setTrackListName] = useState<string>('');
	const [listLoading, setListLoading] = useState<boolean>(false);
	const [gaSetForSearch, setGaSetForSearch] = useState<boolean>(false);
	const containerRef = useGetRef<HTMLDivElement>();
	const loadMoreRef = useGetRef<HTMLDivElement>();

	const { setError } = useContext(errorContext);
	const { incrementProgress, clearProgress } = useContext(loaderContext);

	const trackListVariables = {
		name: trackListName,
		limit: TRACK_LIST_LIMIT,
		visibleData: [],
	};

	const { data: trackListData, loading, fetchMore } = useQuery<
		GraphQLWrap<'responseTracks', Response<ResponseLink>>,
		{
			name: string;
			limit: number;
			visibleData: string[];
		}
	>(getTrackList, {
		variables: trackListVariables,
	});
	const trackList = trackListData?.responseTracks ?? {
		totalLength: 0,
		data: [],
	};

	const [updatePlaylist] = useSecureMutation<
		GraphQLWrap<'updatePlaylist', ResponsePlaylist>,
		{
			_id: string;
			input: {
				publicity: PlaylistPublicity;
			};
		}
	>(updatePlaylistMutation, {
		variables: {
			_id: playlist._id,
			input: {
				publicity: PlaylistPublicity.public,
			},
		},
	});

	const changeModalOpen = useCallback(
		(value: boolean) => () => {
			setModalOpened(value);
		},
		[],
	);

	const confirmPublicity = useCallback(async () => {
		incrementProgress(PUBLICITY_TASK);
		await updatePlaylist({
			update: (cache, { data }) => {
				ReactGA.event({
					category: GA_CATEGORY_PLAYLIST,
					action: GA_PUBLISHED,
				});

				if (data === null || data === undefined) {
					return;
				}

				const isMy = [true, true, false];
				const publicity = [PlaylistPublicity.private, PlaylistPublicity.public, PlaylistPublicity.public];
				for (let i = 0; i <= 2; i++) {
					try {
						const variables = {
							visibleData: [],
							limit: PLAYLISTS_LIMIT,
							isMy: isMy[i],
							publicity: publicity[i],
						};

						const oldData = cache.readQuery<
							GraphQLWrap<'responsePlaylists', Response<ResponsePlaylist>>,
							{
								visibleData: string[];
								limit: number;
								isMy: boolean;
								publicity: PlaylistPublicity;
							}
						>({
							query: getPlaylists,
							variables,
						});

						if (oldData === null) {
							continue;
						}
						const filteredPlaylists =
							i === 0
								? oldData.responsePlaylists.data.filter(({ _id }) => _id !== data.updatePlaylist._id)
								: [data.updatePlaylist, ...oldData.responsePlaylists.data];
						cache.writeQuery<
							GraphQLWrap<'responsePlaylists', Response<ResponsePlaylist>>,
							{
								visibleData: string[];
								limit: number;
								isMy: boolean;
								publicity: PlaylistPublicity;
							}
						>({
							query: getPlaylists,
							variables,
							data: {
								responsePlaylists: {
									...oldData.responsePlaylists,
									totalLength:
										oldData.responsePlaylists.totalLength -
										(oldData.responsePlaylists.data.length - filteredPlaylists.length),
									data: filteredPlaylists,
								},
							},
						});
						// eslint-disable-next-line no-empty
					} catch (err) {}
				}
			},
		}).catch((err: Error) => setError(err.message));

		setModalOpened(false);
		clearProgress(PUBLICITY_TASK);
	}, [updatePlaylist, incrementProgress, clearProgress, setError]);

	const debounceGetPlaylists = useRef(
		debounce((name: string) => {
			setTrackListName(name);
		}, DELAY_TRACK_LIST),
	);

	useEffect(() => {
		debounceGetPlaylists.current(trackName);
	}, [trackName]);

	const onDataLoad = useCallback(async () => {
		if (!loading && !listLoading) {
			setListLoading(true);
			await fetchMore({
				variables: {
					...trackListVariables,
					visibleData: trackList.data.map(({ _id }) => _id),
				},
				updateQuery: (prev, { fetchMoreResult }) => {
					setTimeout(() => {
						setListLoading(false);
					}, 0);

					if (fetchMoreResult === undefined) {
						return prev;
					}

					return {
						...fetchMoreResult,
						responseTracks: {
							...fetchMoreResult.responseTracks,
							data: [...prev.responseTracks.data, ...fetchMoreResult.responseTracks.data],
						},
					};
				},
				// eslint-disable-next-line @typescript-eslint/no-empty-function
			}).catch(() => {});
		}
	}, [fetchMore, listLoading, loading, trackList.data, trackListVariables]);

	const onScroll = useCallback(() => {
		if (typeof window === 'undefined' || containerRef.current === null || loadMoreRef.current === null) {
			return;
		}

		const container = containerRef.current;
		const loadMore = loadMoreRef.current;
		if (container.scrollTop >= container.scrollHeight - container.offsetHeight - loadMore.offsetHeight - 400) {
			void onDataLoad();
		}
	}, [containerRef, onDataLoad, loadMoreRef]);

	useLayoutEffect(() => {
		if (typeof window === 'undefined' || containerRef.current === null || loadMoreRef.current === null) {
			return;
		}

		const container = containerRef.current;
		const loadMore = loadMoreRef.current;
		if (
			!loading &&
			container.scrollTop >= container.scrollHeight - container.offsetHeight - loadMore.offsetHeight - 400
		) {
			onScroll();
		}

		container.addEventListener('scroll', onScroll);
		return () => container.removeEventListener('scroll', onScroll);
	}, [containerRef, loading, onScroll, loadMoreRef]);

	const changeTrackName = useCallback(
		(value: string) => {
			setTrackName(value);
			if (!gaSetForSearch) {
				setGaSetForSearch(true);
				ReactGA.event({
					category: GA_CATEGORY_PLAYLIST,
					action: GA_SEARCH,
					label: value,
				});
			}
		},
		[gaSetForSearch],
	);

	const addToPlaylist = useCallback(() => {
		ReactGA.event({
			category: GA_CATEGORY_PLAYLIST,
			action: GA_ADD_TO_PLAYLIST,
		});
	}, []);

	return (
		<Tablet>
			<Wrap>
				{playlist.publicity === PlaylistPublicity.private && (
					<>
						<Modal isOpen={modalOpened} onClose={changeModalOpen(false)}>
							<span>{d('playlist.additionalBlock.modal.title')}</span>
							<Dummy loading={PUBLICITY_TASK} transparent={true}>
								<Button onClick={confirmPublicity}>{d('playlist.additionalBlock.modal.accept')}</Button>
								<Button onClick={changeModalOpen(false)}>{d('playlist.additionalBlock.modal.cancel')}</Button>
							</Dummy>
						</Modal>
						{playlist.tracks.length > 0 && (
							<PublicityButton color="secondary" onClick={changeModalOpen(true)}>
								{d('playlist.additionalBlock.publicityButton')}
							</PublicityButton>
						)}
					</>
				)}
				{isPrivate && (
					<div>
						<Title>{d('playlist.additionalBlock.search.title')}</Title>
						<SearchInput
							name="track"
							placeholder={d('playlist.additionalBlock.search.placeholder')}
							value={trackName}
							onChange={changeTrackName}
						/>
						<TracksContainer getRef={containerRef}>
							<Audios tracks={trackList.data} onAddToPlaylist={addToPlaylist} />
							{trackList.totalLength > trackList.data.length && (
								<LoadMore ref={loadMoreRef}>
									<Dummy transparent={true} loading={loading || listLoading}>
										<span>{d('playlist.additionalBlock.more')}</span>
									</Dummy>
								</LoadMore>
							)}
						</TracksContainer>
					</div>
				)}
				<Title>{d('playlist.additionalBlock.tracks.title')}</Title>
				<Audios tracks={playlist.tracks} onRemove={onRemoveAudio} />
			</Wrap>
		</Tablet>
	);
};

export default AdditionalBlock;
