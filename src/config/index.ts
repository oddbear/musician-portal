import dotenv from 'dotenv';
import convict from 'convict';

dotenv.config();

export const config = convict({
	env: {
		env: 'NODE_ENV',
		format: ['production', 'development', 'test'],
		default: 'development',
	},
	cloudinary: {
		cloudName: {
			env: 'CLOUDINARY_CLOUD_NAME',
			format: String,
			default: '',
		},
	},
	api: {
		port: {
			env: 'API_NODE_PORT',
			format: Number,
			default: 4220,
		},
		url: {
			env: 'API_URL',
			format: String,
			default: 'http://localhost:4220',
		},
	},
	graphQL: {
		url: {
			env: 'GRAPHQL_URL',
			format: String,
			default: 'http://localhost:4220/graphql',
		},
		subscriptionUrl: {
			env: 'GRAPHQL_SUBSCRIPTION_URL',
			format: String,
			default: 'ws://localhost:4220/graphql',
		},
	},
	sentryDSN: {
		env: 'SENTRY_DSN',
		format: String,
		default: '',
	},
	google: {
		recaptcha: {
			siteKey: {
				env: 'RECAPTCHA_SITE_KEY',
				format: String,
				default: '',
			},
		},
		analytics: {
			identifier: {
				env: 'GA_IDENTIFIER',
				format: String,
				default: '',
			},
		},
		firebase: {
			projectId: {
				env: 'FIREBASE_PROJECT_ID',
				format: String,
				default: '',
			},
			appId: {
				env: 'FIREBASE_APP_ID',
				format: String,
				default: '',
			},
			apiKey: {
				env: 'FIREBASE_API_KEY',
				format: String,
				default: '',
			},
			messagingSenderId: {
				env: 'FIREBASE_MESSAGING_SENDER_ID',
				format: String,
				default: '',
			},
			vapidKey: {
				env: 'FIREBASE_VAPID_KEY',
				format: String,
				default: '',
			},
		},
	},
}).getProperties();
