import Signin from 'Components/Signin';
import React from 'react';

interface Context {
	query: {
		message?: string;
	};
}

class SigninPage extends React.Component<Context> {
	public static async getInitialProps({ query }: Context): Promise<Context> {
		return {
			query,
		};
	}

	public render(): React.ReactElement {
		const { message } = this.props.query;

		return <Signin message={message} />;
	}
}

export default SigninPage;
