import React from 'react';
import Restore from 'Components/Restore';

class RestorePage extends React.Component {
	public render(): React.ReactElement {
		return <Restore />;
	}
}

export default RestorePage;
