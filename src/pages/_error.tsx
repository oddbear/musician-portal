import * as Sentry from '@sentry/browser';
import { IncomingMessage } from 'http';
import { NextPageContext } from 'next/dist/next-server/lib/utils';
import React from 'react';

const notifySentry = (err?: Error, req?: IncomingMessage, statusCode?: number): void => {
	Sentry.configureScope((scope) => {
		if (req === undefined) {
			scope.setTag(`ssr`, 'false');
		} else {
			scope.setTag(`ssr`, 'true');
			scope.setExtra(`url`, req.url);
			scope.setExtra(`statusCode`, statusCode);
			scope.setExtra(`headers`, req.headers);
		}
	});

	Sentry.captureException(err);
};

interface InitialPropsResponse {
	statusCode: number | null;
}

export default class ErrorPage extends React.Component {
	static async getInitialProps({ req, res, err }: NextPageContext): Promise<InitialPropsResponse> {
		const statusCode = res?.statusCode ?? err?.statusCode ?? null;
		// @ts-ignore
		notifySentry(err, req, statusCode);

		// if (req?.url?.includes('404') !== true) {
		// 	res?.writeHead(302, {
		// 		Location: '/404',
		// 		'Content-Type': 'text/html; charset=utf-8',
		// 	});
		// 	res?.end();
		// }

		return { statusCode };
	}

	public render(): React.ReactElement {
		return <span>404</span>;
	}
}
