import FAQ from 'Components/FAQ';
import React from 'react';

const FaqPage: React.FC = () => {
	return <FAQ />;
};

export default FaqPage;
