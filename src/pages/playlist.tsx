import React from 'react';
import Playlist from 'Components/Playlist';

interface Context {
	query: {
		id: string;
	};
}

class PlaylistPage extends React.Component {
	public static async getInitialProps(ctx: Context): Promise<Context> {
		return {
			query: ctx.query,
		};
	}

	public render(): React.ReactElement {
		const { query } = this.props as Context;

		return <Playlist _id={query.id} />;
	}
}

export default PlaylistPage;
