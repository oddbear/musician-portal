import React from 'react';
import Conversations from 'Components/Conversations';

const ConversationsPage: React.FC = () => {
	return <Conversations />;
};

export default ConversationsPage;
