import React from 'react';
import Settings from 'Components/Settings';

class SettingsPage extends React.Component {
	public render(): React.ReactElement {
		return <Settings />;
	}
}

export default SettingsPage;
