import Profile from 'Components/Profile';
import React from 'react';

interface Context {
	query: {
		login?: string;
	};
}

class ProfilePage extends React.Component<Context> {
	public static async getInitialProps({ query }: Context): Promise<Context> {
		return {
			query,
		};
	}

	public render(): React.ReactElement {
		const { login } = this.props.query;

		return <Profile login={login} />;
	}
}

export default ProfilePage;
