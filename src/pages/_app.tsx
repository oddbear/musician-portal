import 'babel-polyfill';
import 'react-image-crop/dist/ReactCrop.css';

import * as Sentry from '@sentry/browser';
import { ApolloProvider } from '@apollo/react-hooks';
import Layout from 'Components/Layout';
import React from 'react';
import { setDictionary } from 'Utils/dictionary';
import App, { AppContext, AppInitialProps } from 'next/app';
import { initApolloClient } from 'Utils/apollo';
import { getDataFromTree } from '@apollo/react-ssr';
import Head from 'next/head';
import ApolloClient from 'apollo-client';
import { NormalizedCacheObject } from 'apollo-cache-inmemory';
import ReactGA from 'react-ga';

import firebase from 'firebase/app';
import 'firebase/messaging';

ReactGA.initialize(config.google.analytics.identifier);

const DEFAULT_LANGUAGE = 'ru';
if (config.env === 'production') {
	Sentry.init({ dsn: config.sentryDSN });
}
if (firebase.apps.length === 0) {
	firebase.initializeApp({
		appId: config.google.firebase.appId,
		apiKey: config.google.firebase.apiKey,
		projectId: config.google.firebase.projectId,
		messagingSenderId: config.google.firebase.messagingSenderId,
	});
}

interface InitialProps {
	locale?: string;
	lang?: string;
	apolloState: object;
	pathname: string;
}

export let apolloClient: ApolloClient<NormalizedCacheObject>;

export default class AppPage extends App<InitialProps> {
	public static async getInitialProps(context: AppContext): Promise<AppInitialProps & InitialProps> {
		const { AppTree, ctx, Component, router } = context;

		let pageProps: object = {};
		const locale: string = (ctx.query.locale as string | undefined) ?? '{}';
		const lang: string = (ctx.query.lang as string | undefined) ?? DEFAULT_LANGUAGE;

		apolloClient = initApolloClient();

		if (Component.getInitialProps !== undefined) {
			pageProps = await Component.getInitialProps({
				...ctx,
			});
		}

		let appProps;
		if (App.getInitialProps !== undefined) {
			appProps = await App.getInitialProps(context);
		} else {
			appProps = {
				pageProps: {},
			};
		}

		if (typeof window === 'undefined') {
			try {
				await getDataFromTree(
					<AppTree {...appProps} Component={Component} router={router} apolloClient={apolloClient} />,
				);
			} catch (error) {
				console.error('Error while running `getDataFromTree`', (error as Error).message);
			}

			Head.rewind();
		}

		const apolloState = apolloClient.cache.extract();

		return { pageProps, locale, lang, apolloState, pathname: router.asPath };
	}

	public render(): React.ReactElement {
		const props = this.props;
		const { Component, pageProps, locale, lang, apolloState, pathname } = props;

		apolloClient = initApolloClient(apolloState);

		if (locale !== undefined) {
			setDictionary(locale);
		}

		return (
			<ApolloProvider client={apolloClient}>
				<Layout lang={lang ?? DEFAULT_LANGUAGE} pathname={pathname}>
					<Component {...pageProps} />
				</Layout>
			</ApolloProvider>
		);
	}
}
