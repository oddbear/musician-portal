import Document, { DocumentContext, DocumentInitialProps, Head, Html, Main, NextScript } from 'next/document';
import React from 'react';
import { ServerStyleSheet } from 'styled-components';

interface Props {
	lang?: string;
}

export default class DocumentPage extends Document<Props> {
	public static async getInitialProps(ctx: DocumentContext): Promise<DocumentInitialProps & Props> {
		const sheet = new ServerStyleSheet();
		const originalRenderPage = ctx.renderPage;

		try {
			ctx.renderPage = () =>
				originalRenderPage({
					// eslint-disable-next-line @typescript-eslint/no-explicit-any
					enhanceApp: (App: any) => (props: any) => sheet.collectStyles(<App {...props} />),
				});

			const initialProps = await Document.getInitialProps(ctx);
			return {
				...initialProps,
				lang: (ctx.query.lang as string | undefined) ?? 'ru',
				styles: (
					<React.Fragment>
						{initialProps.styles}
						{sheet.getStyleElement()}
					</React.Fragment>
				),
			};
		} finally {
			sheet.seal();
		}
	}

	public render(): React.ReactElement {
		const { lang } = this.props;

		return (
			<Html lang={lang}>
				<Head />
				<body>
					<Main />
					<NextScript />
				</body>
			</Html>
		);
	}
}
