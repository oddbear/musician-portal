import News from 'Components/News';
import React from 'react';

interface Context {
	query: {
		id?: string;
	};
}

class NewsPage extends React.Component<Context> {
	public static async getInitialProps({ query }: Context): Promise<Context> {
		return {
			query,
		};
	}

	public render(): React.ReactElement {
		const { id } = this.props.query;

		return <News id={id} />;
	}
}

export default NewsPage;
