import React from 'react';
import Playlists from 'Components/Playlists';

const PlaylistsPage: React.FC = () => {
	return <Playlists />;
};

export default PlaylistsPage;
