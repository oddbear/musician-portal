import Band from 'Components/Band';
import React from 'react';

interface Context {
	query: {
		alias: string;
	};
}

class BandPage extends React.Component {
	public static async getInitialProps(ctx: Context): Promise<Context> {
		return {
			query: ctx.query,
		};
	}

	public render(): React.ReactElement {
		const { query } = this.props as Context;

		return <Band alias={query.alias} />;
	}
}

export default BandPage;
