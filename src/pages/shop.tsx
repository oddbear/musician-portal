import Shop from 'Components/Shop';
import React from 'react';

class ShopPage extends React.Component {
	public render(): React.ReactElement {
		return <Shop />;
	}
}

export default ShopPage;
