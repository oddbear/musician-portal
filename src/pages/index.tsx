import FullsizeBackground from 'Components/FullsizeBackground';
import LazyImage from 'Components/LazyImage';
import React from 'react';
import styled from 'styled-components';
import { d, wrap } from 'Utils/dictionary';

const BACKGROUND_SRC = 'musician-portal/%D0%91%D1%83%D0%BC%D0%B0%D0%B3%D0%B0';
const BAND_POSTER_SRC = 'BG/center_xi9aia';
const MUSICIAN_POSTER_SRC = 'BG/center_xi9aia';

const Container = styled.div`
	position: relative;
	width: 100%;
	max-width: ${({ theme }) => theme.other.containerMaxWidth};
	margin: 0 auto;
	padding: 100px 0;
`;

const Content = styled.div`
	top: 0;
	width: 50%;
	padding: 50px 50px;
	font-weight: 700;
	font-family: ${({ theme }) => theme.fontFamily.quiet};
`;

const Wrap = styled.div`
	position: absolute;
	right: 0;
	top: 0;
	width: 30%;
`;

const Poster = styled(LazyImage)`
	transform: rotate(10deg);
	width: 400px;

	&:first-child {
		margin-bottom: 50px;
	}
`;

const IndexPage: React.FC = () => {
	return (
		<Container>
			<FullsizeBackground src={BACKGROUND_SRC} ratio={0.5668449197860963}>
				<Content>
					<h3>{d('index.content.head')}</h3>
					{wrap(d('index.content.text'), (text, type, index) => (
						<p key={index}>{text}</p>
					))}
				</Content>
				<Wrap>
					<Poster src={BAND_POSTER_SRC} alt="choose your band" title="CHOOSE YOUR BAND" />
					<Poster src={MUSICIAN_POSTER_SRC} alt="choose your musician" />
				</Wrap>
			</FullsizeBackground>
		</Container>
	);
};

export default IndexPage;
