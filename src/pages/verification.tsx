import React from 'react';
import Verification from 'Components/Verification';

interface Context {
	query: {
		id: string;
	};
}

class VerificationPage extends React.Component<Context> {
	public static async getInitialProps({ query }: Context): Promise<Context> {
		return {
			query,
		};
	}

	public render(): React.ReactElement {
		const { id } = this.props.query;

		return <Verification id={id} />;
	}
}

export default VerificationPage;
