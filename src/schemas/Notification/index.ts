export { default as getNotifications } from './getNotifications.graphql';
export { default as getNotificationsMutation } from './getNotificationsMutation.graphql';
export { default as subscribeForNotification } from './subscribeForNotification.graphql';
export { default as hideNotification } from './hideNotification.graphql';
export { default as checkNotifications } from './checkNotifications.graphql';
