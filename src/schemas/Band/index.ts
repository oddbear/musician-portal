export { default as getBand } from './getBand.graphql';
export { default as getBandMutation } from './getBandMutation.graphql';
export { default as createBand } from './createBand.graphql';
export { default as removeBand } from './removeBand.graphql';
export { default as updateBand } from './updateBand.graphql';
export { default as checkAlias } from './checkAlias.graphql';
export { default as addBandAudio } from './addBandAudio.graphql';
export { default as removeBandAudio } from './removeBandAudio.graphql';
export { default as dismissMember } from './dismissMember.graphql';
export { default as updateBandPrivacySettings } from './updateBandPrivacySettings.graphql';
export { default as subscribeToBand } from './subscribeToBand.graphql';
