export { default as getEmotions } from './getEmotions.graphql';
export { default as addPostEmotion } from './addPostEmotion.graphql';
export { default as addCommentEmotion } from './addCommentEmotion.graphql';
export { default as addPlaylistEmotion } from './addPlaylistEmotion.graphql';
