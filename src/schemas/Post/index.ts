export { default as getPosts } from './getPosts.graphql';
export { default as addBandPost } from './addBandPost.graphql';
export { default as addBandComment } from './addBandComment.graphql';
export { default as getComments } from './getComments.graphql';
export { default as hidePost } from './hidePost.graphql';
export { default as hideComment } from './hideComment.graphql';
export { default as subscribeForPost } from './subscribeForPost.graphql';
