import * as React from 'react';
import { useDelay } from '../delay.hook';
import { useEffect } from 'react';
import { mount, ReactWrapper } from 'enzyme';

const delayTime = 1;
const testValue = 'foobar';

describe('Hook delay tests', () => {
	describe('useDelay', () => {
		console.error = jest.fn();

		let containerWrapper: ReactWrapper;

		it('"setValueAndActivateTimer" sets value after delay', (done) => {
			const Container: React.FC = () => {
				const [value, setValueAndActivateTimer] = useDelay<string | null>(null, delayTime);

				useEffect(() => {
					setValueAndActivateTimer(testValue);
					// eslint-disable-next-line react-hooks/exhaustive-deps
				}, []);

				return <div>{value}</div>;
			};

			containerWrapper = mount(<Container />);

			expect(containerWrapper.text()).not.toEqual(testValue);

			setTimeout(() => {
				expect(containerWrapper.text()).toEqual(testValue);
				done();
			}, delayTime + 1);
		});

		it('"resetValueAndTimer" clears timer and sets value', (done) => {
			let timesUpdated = 0;
			const Container: React.FC = () => {
				const [value, setValueAndActivateTimer, resetValueAndTimer] = useDelay<string | null>(null, delayTime);

				useEffect(() => {
					if (timesUpdated++ === 0) {
						setValueAndActivateTimer('any');
					} else {
						resetValueAndTimer(testValue);
					}
				}, [setValueAndActivateTimer, resetValueAndTimer]);

				return <div>{value}</div>;
			};

			containerWrapper = mount(<Container />);
			expect(containerWrapper.text()).toEqual(testValue);

			setTimeout(() => {
				expect(containerWrapper.text()).toEqual(testValue);
				done();
			}, delayTime + 1);
		});
	});
});
