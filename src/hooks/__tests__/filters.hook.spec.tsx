import { act } from 'react-dom/test-utils';
import { fireBeforePopState } from 'Mocks/next.mock';
import { mount } from 'enzyme';
import { prepareQuery } from 'Utils/url';
import queryString from 'querystring';
import { RouterListener } from 'Utils/router';
import { Genres, Positions } from 'Types/filters.type';
import React, { useState } from 'react';
import { useFiltersQuery, useSearchQuery } from '../filters.hook';
import { PositionVariant } from 'Utils/enum';

jest.mock('next/router', () => {
	// eslint-disable-next-line @typescript-eslint/no-unsafe-return
	return {
		...jest.requireActual('../../__mocks__/next.mock'),
	};
});

const stringify = (object: object): string =>
	Object.values(object)
		// eslint-disable-next-line @typescript-eslint/no-unsafe-call
		.map((filterValue) => (typeof filterValue === 'string' ? filterValue : JSON.stringify(filterValue)))
		.join(',');

describe('Hook filters tests', () => {
	let href = '';
	const location = {};
	Object.defineProperties(location, {
		href: {
			get() {
				return href;
			},
		},
	});
	Object.defineProperty(window, 'location', {
		value: location,
	});

	describe('useFiltersQuery', () => {
		let filters: {
			[key: string]: {
				// eslint-disable-next-line @typescript-eslint/no-explicit-any
				filters?: any[] | { [key: string]: any };
				// eslint-disable-next-line @typescript-eslint/no-explicit-any
				callback(value: any): void;
			};
		};

		beforeEach(() => {
			filters = {
				type: {
					filters: ['foo', 'bar'],
					callback() {
						return;
					},
				},
				positions: {
					filters: [
						{
							value: 'foo',
						},
						{
							value: 'bar',
						},
					],
					callback() {
						return;
					},
				},
				genres: {
					filters: [
						{
							value: 'foo',
						},
						{
							value: 'bar',
						},
					],
					callback() {
						return;
					},
				},
				country: {
					filters: [
						{
							value: 'us',
						},
						{
							value: 'en',
						},
					],
					callback() {
						return;
					},
				},
				city: {
					filters: [
						{
							value: 'california',
						},
						{
							value: 'london',
						},
					],
					callback() {
						return;
					},
				},
				experience: {
					callback() {
						return;
					},
				},
			};
		});

		interface Props {
			filters?: typeof filters;
		}

		// eslint-disable-next-line react/prop-types
		const Container: React.FC<Props> = ({ filters: newFilters = {} }) => {
			const [type, setType] = useState<string>('foo');
			const [positions, setPositions] = useState<Positions>({});
			const [genres, setGenres] = useState<Genres>({});
			const [country, setCountry] = useState<string | null>(null);
			const [city, setCity] = useState<string | null>(null);
			const [experience, setExperience] = useState<number>(0);

			const ownFilters = Object.entries(filters).reduce<typeof filters>((acc, [filterKey, filterValue]) => {
				acc[filterKey] = {
					filters: filterValue.filters,
					callback: filterValue.callback,
				};

				return acc;
			}, {});

			ownFilters.type.callback = ('type' in newFilters && newFilters.type.callback) || setType;
			ownFilters.positions.callback = ('positions' in newFilters && newFilters.positions.callback) || setPositions;
			ownFilters.genres.callback = ('genres' in newFilters && newFilters.genres.callback) || setGenres;
			ownFilters.country.callback = ('country' in newFilters && newFilters.country.callback) || setCountry;
			ownFilters.city.callback = ('country' in newFilters && newFilters.city.callback) || setCity;
			ownFilters.experience.callback = ('experience' in newFilters && newFilters.experience.callback) || setExperience;

			const onApply = (): void => {
				return;
			};

			// @ts-ignore
			useFiltersQuery(ownFilters, onApply);

			return (
				<span>
					<RouterListener />
					{type},{JSON.stringify(positions)},{JSON.stringify(genres)},{country},{city},{experience}
				</span>
			);
		};

		it('triggers all filter-callbacks on init', () => {
			const queryFilters = {
				type: 'bar',
				positions: {
					bar: PositionVariant.available,
					foo: PositionVariant.occupied,
				},
				genres: {
					bar: false,
					foo: true,
				},
				country: 'en',
				city: 'london',
				experience: 5,
			};

			href = 'http://site?' + queryString.stringify(prepareQuery(queryFilters));

			const container = mount(<Container />);

			expect(container.text()).toEqual(stringify(queryFilters));
		});

		it('triggers all filter-callbacks on popState', (done) => {
			const queryFilters = {
				type: 'bar',
				positions: {
					bar: PositionVariant.available,
					foo: PositionVariant.occupied,
				},
				genres: {
					bar: false,
					foo: true,
				},
				country: 'en',
				city: 'london',
				experience: 5,
			};

			href = 'http://site';

			const container = mount(<Container />);
			href = 'http://site?' + queryString.stringify(prepareQuery(queryFilters));
			act(() => {
				fireBeforePopState();
			});

			setTimeout(() => {
				expect(container.text()).toEqual(stringify(queryFilters));
				done();
			}, 1);
		});

		it('calls trigger if query value still the same on popState', (done) => {
			const queryFilters = {
				type: 'bar',
			};

			href = 'http://site';

			const spy = jest.fn();
			mount(
				<Container
					filters={{
						type: {
							callback: spy,
						},
					}}
				/>,
			);
			href = 'http://site?' + queryString.stringify(prepareQuery(queryFilters));

			act(() => {
				fireBeforePopState();
			});
			act(() => {
				fireBeforePopState();
			});

			expect(spy).toBeCalledTimes(2);
			done();
		});

		it('ignores query parameters that not specified in filters', (done) => {
			const queryFilters = {
				type: 'bar',
				positions: {
					set: PositionVariant.available,
				},
				genres: {
					za: false,
					foo: true,
				},
				country: 'su',
				city: 'pokchamp',
				experience: 0,
			};

			href = 'http://site?' + queryString.stringify(prepareQuery(queryFilters));

			const container = mount(<Container />);

			queryFilters.country = '';
			queryFilters.city = '';
			// @ts-ignore
			delete queryFilters.genres.za;
			// @ts-ignore
			delete queryFilters.positions.set;

			setTimeout(() => {
				expect(container.text()).toEqual(stringify(queryFilters));
				done();
			}, 1);
		});
	});

	describe('useSearchQuery', () => {
		const filters = {
			search: {
				callback() {
					return;
				},
			},
		};

		interface Props {
			// eslint-disable-next-line @typescript-eslint/no-explicit-any
			filters?: { [key: string]: { callback(value: any): void } };
		}

		// eslint-disable-next-line react/prop-types,@typescript-eslint/no-explicit-any
		const Container: React.FC<Props> = ({ filters: newFilters = {} }) => {
			const [search, setSearch] = useState<string>('');

			const ownFilters = {
				search: {
					callback: filters.search.callback,
				},
			};

			// @ts-ignore
			ownFilters.search.callback = ('search' in newFilters && newFilters.search.callback) || setSearch;

			useSearchQuery(ownFilters);

			return (
				<span>
					<RouterListener />
					{search}
				</span>
			);
		};

		it('triggers all filter-callbacks on init', () => {
			const queryFilters = {
				search: 'foobar',
			};

			href = 'http://site?' + queryString.stringify(prepareQuery(queryFilters));

			const container = mount(<Container />);
			expect(container.text()).toEqual(stringify(queryFilters));
		});

		it('triggers all filter-callbacks on popState', () => {
			const queryFilters = {
				search: 'foobar',
			};

			href = 'http://site';

			const container = mount(<Container />);
			href = 'http://site?' + queryString.stringify(prepareQuery(queryFilters));
			act(() => {
				fireBeforePopState();
			});

			expect(container.text()).toEqual(stringify(queryFilters));
		});

		it('calls trigger if query value still the same on popState', () => {
			const queryFilters = {
				search: 'foobar',
			};

			href = 'http://site';

			const spy = jest.fn();
			mount(
				<Container
					filters={{
						search: {
							callback: spy,
						},
					}}
				/>,
			);
			href = 'http://site?' + queryString.stringify(prepareQuery(queryFilters));

			act(() => {
				fireBeforePopState();
			});
			act(() => {
				fireBeforePopState();
			});

			expect(spy).toBeCalledTimes(4);
		});

		it('ignores query parameters that not specified in filters', () => {
			const queryFilters = {
				search: 'foobar',
				type: 'bar',
				genres: {
					za: false,
					foo: true,
				},
			};

			href = 'http://site?' + queryString.stringify(prepareQuery(queryFilters));

			const container = mount(<Container />);

			// @ts-ignore
			delete queryFilters.type;
			// @ts-ignore
			delete queryFilters.genres;

			expect(container.text()).toEqual(stringify(queryFilters));
		});
	});
});
