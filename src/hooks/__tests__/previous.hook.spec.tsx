import * as React from 'react';
import { Context } from 'Mocks/react.mock';
import { useWindowSize } from '../window-size.hook';
import { shallow, ShallowWrapper } from 'enzyme';

jest.mock('react', () => {
	// eslint-disable-next-line @typescript-eslint/no-unsafe-return
	return {
		...jest.requireActual('react'),
		...jest.requireActual('../../__mocks__/react.mock'),
	};
});

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const mockReact: Context = React as any;

const TestElement: React.FC = () => {
	const [width, height]: number[] = useWindowSize();

	return (
		<div>
			{width},{height}
		</div>
	);
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const globalAny = global as any;

function getExpectedContext(): string {
	// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access,@typescript-eslint/restrict-template-expressions
	return `${globalAny.innerWidth},${globalAny.innerHeight}`;
}

describe('Hook windowSize tests', () => {
	// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
	globalAny.innerWidth = 4;
	// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
	globalAny.innerHeight = 50;

	const element: ShallowWrapper = shallow(<TestElement />);
	mockReact.triggerEffects();

	it('correctly sets size of window on start', () => {
		expect(element.text()).toEqual(getExpectedContext());
	});

	it('correctly sets size then window resized', () => {
		// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
		globalAny.innerWidth = 121;
		// eslint-disable-next-line @typescript-eslint/no-unsafe-member-access
		globalAny.innerHeight = 333;

		// eslint-disable-next-line @typescript-eslint/no-unsafe-call,@typescript-eslint/no-unsafe-member-access
		globalAny.dispatchEvent(new Event('resize'));

		expect(element.text()).toEqual(getExpectedContext());
	});
});
