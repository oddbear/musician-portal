import * as Cloudinary from 'Utils/cloudinary';
import * as React from 'react';
import { act } from 'react-dom/test-utils';
import { CloudinaryResponseElement } from 'Types/cloudinary.type';
import { mount } from 'enzyme';
import { PRELOAD_IMAGE, useLazyLoad } from '../lazyload.hook';

type ResponseCallback = (response: { [key: string]: CloudinaryResponseElement }) => void;
type Handler = [{ defaults: { src: string } }, ResponseCallback, number];

jest.mock('Utils/cloudinary', () => {
	const handlers: Handler[] = [];

	return {
		load() {
			handlers.forEach(([options, callback, increment]: Handler, index: number, arr: Handler[]) => {
				callback({
					defaults: {
						src: options.defaults.src + (++increment).toString(),
						width: 0,
						height: 0,
						element: null,
					},
				});

				arr[index][2] = increment;
			});
		},
		cloudinaryLazyLoad(options: { defaults: { src: string } }, callback: ResponseCallback) {
			handlers.push([options, callback, 0]);
		},
	};
});

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const mockUtil: { load(): void } = Cloudinary as any;

const testSrc = '/image';

const TestElement: React.FC = () => {
	const { src }: CloudinaryResponseElement = useLazyLoad({
		src: testSrc,
	});

	return <div>{src}</div>;
};

describe('Hook lazyLoad tests', () => {
	const element = mount(<TestElement />);

	it("returns base64 transparent the smallest image before cloudinary's callback", () => {
		expect(element.text()).toEqual(PRELOAD_IMAGE);
	});

	it("loads and updates component's state correctly", () => {
		act(() => {
			mockUtil.load();
		});
		expect(element.text()).toEqual(testSrc + '1');
	});
});
