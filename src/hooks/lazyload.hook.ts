import { cloudinaryLazyLoad } from 'Utils/cloudinary';
import { usePrevious } from 'Hooks/previous.hook';
import { CloudinaryOptions, CloudinaryResponseElement } from 'Types/cloudinary.type';
import { ReactElement, useEffect, useState } from 'react';

export const PRELOAD_IMAGE = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';

export function useLazyLoad(
	options: CloudinaryOptions & { tag?: 'img' },
): rewrite<CloudinaryResponseElement, 'element', null | ((props: object) => ReactElement)>;
export function useLazyLoad(
	options: CloudinaryOptions & { tag: 'audio' },
): rewrite<CloudinaryResponseElement, 'element', null | HTMLAudioElement>;
export function useLazyLoad(
	options: CloudinaryOptions & { tag: 'video' },
): rewrite<CloudinaryResponseElement, 'element', null | HTMLVideoElement>;
export function useLazyLoad(options: CloudinaryOptions): CloudinaryResponseElement {
	const [response, setResponse] = useState<CloudinaryResponseElement>({
		src: PRELOAD_IMAGE,
		width: 0,
		height: 0,
		element: null,
	});

	const prevSrc = usePrevious(options.src);
	useEffect(() => {
		if (options.src === null || options.src === '') {
			return;
		}

		if (options.src !== prevSrc) {
			cloudinaryLazyLoad(
				{
					defaults: options,
				},
				({ defaults }) => {
					if (typeof defaults === 'object') {
						setResponse(defaults);
					}
				},
			);
		}
	}, [options, prevSrc]);

	return response;
}
