import { useCallback, useState } from 'react';

type Response<T> = [T, (valueAfterDelay: T) => void, (resetValue?: T) => void];

export const useDelay = <T>(initialValue: T, delayTime: number): Response<T> => {
	const [deathTimer, setDeathTimer] = useState<number>();
	const [value, setValue] = useState<T>(initialValue);

	const resetValueAndTimer = useCallback(
		(newValue?: T) => {
			if (typeof newValue !== 'undefined') {
				setValue(newValue);
			}
			clearTimeout(deathTimer);
			setDeathTimer(undefined);
		},
		[deathTimer],
	);

	const setValueAndActivateTimer = useCallback(
		(newValue: T) => {
			setDeathTimer(
				setTimeout(() => {
					setValue(newValue);
					setDeathTimer(undefined);
				}, delayTime),
			);
		},
		[delayTime],
	);

	return [value, setValueAndActivateTimer, resetValueAndTimer];
};
