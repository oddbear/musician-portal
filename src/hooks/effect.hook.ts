import { useLayoutEffect as ULE, useEffect } from 'react';

export const useLayoutEffect = typeof window === 'undefined' ? useEffect : ULE;
