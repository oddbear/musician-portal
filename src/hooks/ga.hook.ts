import { useCallback, useState } from 'react';
import ReactGA from 'react-ga';

interface QueryTimingInput {
	category: string;
	variable: string;
	label?: string;
}

const queryCached: QueryTimingInput[] = [];
export const useQueryTiming = ({
	category,
	variable,
	label,
}: QueryTimingInput): {
	startTime: number;
	startTiming: () => void;
	endTiming: (error?: string) => void;
} => {
	const [timesUsed, setTimesUsed] = useState<number>(0);
	const [startTime, setStartTime] = useState<number>(0);

	const startTiming = useCallback(() => {
		setStartTime(Date.now());
	}, []);

	const endTiming = useCallback(
		(error?: string) => {
			if (startTime === 0 || queryCached.some((input) => input.category === category && input.variable === variable)) {
				return;
			} else {
				if (error === undefined) {
					ReactGA.timing({
						category,
						variable: `${variable}${timesUsed === 0 ? ' (initial query)' : ''}`,
						value: Date.now() - startTime,
						label,
					});
				} else {
					ReactGA.exception({
						category,
						variable: `${variable}${timesUsed === 0 ? ' (initial query)' : ''}`,
						value: Date.now() - startTime,
						label: error,
					});
				}
				queryCached.push({
					category,
					variable,
				});
				setTimesUsed(timesUsed + 1);
			}
		},
		[timesUsed, category, variable, startTime, label],
	);

	return {
		startTime,
		startTiming,
		endTiming,
	};
};
