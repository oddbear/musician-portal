import { useEffect, useState } from 'react';

const isServer: boolean = typeof window === 'undefined';

const getSize = (): number[] => (isServer ? [0, 0] : [window.innerWidth, window.innerHeight]);

export const useWindowSize = (): number[] => {
	const [size, setSize] = useState<number[]>(getSize());

	useEffect(() => {
		if (isServer) {
			return;
		}

		function handleResize(): void {
			setSize(getSize());
		}

		window.addEventListener('resize', handleResize);
		return () => window.removeEventListener('resize', handleResize);
	}, []);

	return size;
};
