import { parsePreparedQuery } from 'Utils/url';
import { parseUrl } from 'Utils/url-resolver';
import { usePrevious } from 'Hooks/previous.hook';
import { beforePopState, removeEvent } from 'Utils/router';
import {
	CityFilter,
	CountryFilter,
	Filters,
	GenreFilter,
	Genres,
	PositionFilter,
	Positions,
	TypeFilter,
} from 'Types/filters.type';
import { useCallback, useEffect, useReducer, useState } from 'react';

export interface ComplexFilterResponse {
	// eslint-disable-next-line @typescript-eslint/no-explicit-any
	[key: string]: any;
}

type ComplexFilter = PositionFilter[] | GenreFilter[];

interface QueryFilters {
	type?: string;
	positions?: string[];
	genres?: boolean[];
	country?: string;
	city?: string;
	experience?: number;
}

export interface FilterParams {
	type?: {
		filters: TypeFilter[];
		callback(value: TypeFilter): void;
	};
	positions?: {
		filters: PositionFilter[];
		callback(value: Positions): void;
	};
	genres?: {
		filters: GenreFilter[];
		callback(value: Genres): void;
	};
	country?: {
		filters: CountryFilter[];
		callback(value: string | null): void;
	};
	city?: {
		filters: CityFilter[];
		callback(value: string | null): void;
	};
	experience?: {
		callback(value: number, isPart?: boolean): void;
	};
}

type Reducer = (times: number) => number;
const reducer: Reducer = (state) => state + 1;

export const useFiltersQuery = (
	params: FilterParams,
	apply: (newFilters?: Partial<Filters>) => void,
	isLoaded = true,
): void => {
	const [query, setQuery] = useState<QueryFilters>({});
	const [timesRouted, incrementTimesRouted] = useReducer<Reducer>(reducer, 0);

	const setFiltersFromQuery = useCallback((url: string) => {
		setQuery(parsePreparedQuery(parseUrl(url).query));
	}, []);

	useEffect(() => {
		const callback = (): boolean => {
			setFiltersFromQuery(location.href);
			incrementTimesRouted();
			return true;
		};

		if (typeof window !== 'undefined') {
			beforePopState(callback);
		}

		return () => removeEvent('beforePopState', callback);
	}, [setFiltersFromQuery, incrementTimesRouted]);

	useEffect(() => {
		if (typeof window !== 'undefined' && isLoaded) {
			setFiltersFromQuery(location.href);
			incrementTimesRouted();
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, [isLoaded]);

	const prevTimesRouted = usePrevious(timesRouted);
	useEffect(() => {
		if (prevTimesRouted !== undefined && prevTimesRouted !== timesRouted) {
			const changes: Partial<Filters> = {};

			// type
			if (params.type?.filters.includes(query.type as TypeFilter) === true) {
				const type = query.type as TypeFilter;
				changes.type = type;
				params.type.callback(type);
			}

			// positions
			const positions = getComplexFilters(params.positions?.filters ?? [], query.positions ?? {});
			changes.positions = positions;
			params.positions?.callback(positions);

			// genres
			const genres = getComplexFilters(params.genres?.filters ?? [], query.genres ?? {});
			changes.genres = genres;
			params.genres?.callback(genres);

			// country
			if (params.country?.filters.some(({ value }) => value === query.country) === true) {
				const country = query.country ?? null;
				changes.country = country;
				params.country.callback(country);
			}

			// city
			if (query.country !== undefined) {
				if (params.city?.filters.some(({ value }) => value === query.city) === true) {
					const city = query.city ?? null;
					changes.city = city;
					params.city.callback(city);
				}
			}

			// experience
			if (typeof query.experience === 'number') {
				changes.experience = query.experience;
				params.experience?.callback(query.experience, true);
			}

			apply(changes);
		}
	}, [apply, params, query, prevTimesRouted, timesRouted]);
};

const getComplexFilters = <Q extends object>(filters: ComplexFilter, query: Q): ComplexFilterResponse =>
	Object.entries(query)
		.filter(([queryKey]) => filters.some((filter: PositionFilter | GenreFilter) => queryKey === filter.value))
		.reduce<ComplexFilterResponse>(
			(positions, [queryKey, queryValue]) => ({
				...positions,
				[queryKey]: queryValue,
			}),
			{},
		);

interface QuerySearch {
	search?: string;
}

interface SearchParams {
	search: {
		callback(value: string): void;
	};
}

export const useSearchQuery = (params: SearchParams): void => {
	const [query, setQuery] = useState<QuerySearch>({});
	const [timesRouted, incrementTimesRouted] = useReducer<Reducer>(reducer, 0);

	const setSearchFromQuery = useCallback((url: string) => {
		setQuery(parsePreparedQuery(parseUrl(url).query));
	}, []);

	useEffect(() => {
		const callback = (): boolean => {
			setSearchFromQuery(location.href);
			incrementTimesRouted();
			return true;
		};

		if (typeof window !== 'undefined') {
			beforePopState(callback);
		}

		return () => removeEvent('beforePopState', callback);
	}, [timesRouted, setSearchFromQuery]);

	useEffect(() => {
		if (typeof window !== 'undefined') {
			setSearchFromQuery(location.href);
			incrementTimesRouted();
		}
		// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const prevTimesRouted = usePrevious(timesRouted);
	useEffect(() => {
		if (prevTimesRouted !== timesRouted) {
			params.search.callback(query.search ?? '');
		}
	}, [params, query, timesRouted, prevTimesRouted]);
};
