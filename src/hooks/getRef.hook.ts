import { MutableRefObject, useState } from 'react';
import { useCallbackRef } from 'use-callback-ref';

export const useGetRef = <T extends HTMLElement>(): MutableRefObject<T | null> => {
	const [, forceUpdate] = useState<undefined>(undefined);
	return useCallbackRef<T>(null, () => forceUpdate(undefined));
};
