FROM node:11

ENV NODE_ENV development

WORKDIR /app

EXPOSE $NODE_PORT
EXPOSE 9229

CMD bash -c "npm i && npm run dev"
